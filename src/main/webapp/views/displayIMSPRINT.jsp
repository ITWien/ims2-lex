<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>IMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <style>
            input[type=text], select{
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date], input[type=number]{
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>


    </head>
    <body>
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
    </div>
    </div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <form action="../resources/manual/IMSPRINT.pdf" target="_blank">
                    <table width="100%">
                        <tr id="dont">
                            <td width="94%" align="left"></td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <center>
                        <form id="printForm" method="post" target="_blank">
                            <input type="hidden" id="preIes" name="preIes">
                            <input type="hidden" id="genno" name="genno">
                            <input type="hidden" id="pries" name="pries">
                            <input type="hidden" id="preimpno" name="preimpno">
                            <input type="hidden" id="imcomes" name="imcomes">
                            <table width="100%">
                                <tr>
                                    <th width="25%"></th>
                                    <th style=" text-align: right;"><b>Pre-Imp no. / Imp no. :</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                    <th>
                                        <input type="text" id="impNo" name="impNo" list="impNoList" style="height: 30px; width: 300px;">
                                        <datalist id="impNoList"></datalist>
                                    </th>
                                    <th width="25%"></th>
                                </tr>
                            </table>
                            <script>
                                $(document).ready(function () {
                                    $('#impNo').keyup(function () {
                                        $(this).val($(this).val().toString().trim().toUpperCase());
                                        var impNo = $('#impNo').val();

                                        if ($(this).val().length > 0) {
                                            $('#impNoList').html('');

                                            $.ajax({
                                                url: "/IMS2/ShowDataTablesIMSFORM?mode=getImpList"
                                                        + "&impNo=" + impNo

                                            }).done(function (result) {
                                                console.log(result);
                                                for (var i = 0; i < result.length; i++) {
                                                    $('#impNoList').append('<option value="' + result[i].pimNo + '">' + result[i].pimNo + ' : ' + result[i].impNo + '</option>');
                                                }

                                            }).fail(function (jqXHR, textStatus, errorThrown) {
                                                // needs to implement if it fails
                                            });

                                        }
                                    });

                                    $('.rePrintBtn').click(function () {
                                        var impNo = $('#impNo').val();
                                        var id = $(this).attr('id');
                                        if (impNo === '') {
                                            alertify.error('กรุณากรอก Pre-Imp no. หรือ Imp no.');
                                        } else {

                                            $.ajax({
                                                url: "/IMS2/ShowDataTablesIMSFORM?mode=getImpDet"
                                                        + "&impNo=" + impNo

                                            }).done(function (result) {
                                                console.log(result);

                                                $('#preIes').val(result.pimNo);
                                                $('#genno').val(result.impNo);
                                                $('#pries').val(result.prv);
                                                $('#preimpno').val(result.pimNo);
                                                $('#imcomes').val(result.imc);

                                                $('#printForm').attr('action', '/IMS/IMS100/' + id);
                                                $('#printForm').submit();

                                            }).fail(function (jqXHR, textStatus, errorThrown) {
                                                // needs to implement if it fails
                                            });

                                        }

                                    });
                                });
                            </script>
                            <br>
                            <button id="print" type="button" style="width: 250px;" class="btn btn-info rePrintBtn">
                                <i style=" font-size: 20px;" class="fa fa-print" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Print Estimate Cost
                            </button><br><br>
                            <button id="printvat" type="button" style="width: 250px;" class="btn btn-primary rePrintBtn">
                                <i style=" font-size: 20px;" class="fa fa-print" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Print Actual Cost (Vat)
                            </button><br><br>
                            <button id="printcost" type="button" style="width: 250px;" class="btn btn-success rePrintBtn">
                                <i style=" font-size: 20px;" class="fa fa-print" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Print Actual Cost (Cost)
                            </button>
                        </form>
                    </center>
                    <!--**********************-->
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top-->
                </div>
                <br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>