<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>IMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script> 
        <style>
            input[type=text], select{
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date], input[type=number]{
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
                    "paging": false,
                    "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
                    "bSortClasses": false,
                    "order": [[0, "desc"]],
                    "columnDefs": [
                        {orderable: false, targets: [7]}
//                        {"width": "12%", "targets": 0},
                    ]
                });

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').not(":eq(7)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });

                // DataTable
                var table = $('#showTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });
        </script>
        <script>
            function subRow(qno, head) {
                var tr = document.getElementsByTagName('tr');

                for (var i = 0; i < tr.length; i++) {
                    if (tr[i].style.display === "") {
                        if (tr[i].id === qno) {
                            tr[i].style.display = "none";
                            document.getElementById(head).style.cssText = "cursor: pointer; background-color: #e5e5e5; color: black;";
                            document.getElementById(head.split("-")[0] + "-icon1-" + head.split("-")[1]).style.cssText = "font-size: 30px; color: #001384;";
                            document.getElementById(head.split("-")[0] + "-icon3-" + head.split("-")[1]).style.cssText = "font-size: 30px; color: #001384;";
                            document.getElementById(head.split("-")[0] + "-icon4-" + head.split("-")[1]).style.cssText = "font-size: 30px; color: #001384;";
                            document.getElementById(head.split("-")[0] + "-icon5-" + head.split("-")[1]).style.cssText = "font-size: 30px; color: #001384;";
                            document.getElementById(head.split("-")[0] + "-icon6-" + head.split("-")[1]).style.cssText = "font-size: 30px; color: #001384;";
                            document.getElementById(head.split("-")[0] + "-icon7-" + head.split("-")[1]).style.cssText = "font-size: 30px; color: #001384;";
                        }
                    } else {
                        if (tr[i].id === qno) {
                            tr[i].style.display = "";
                            document.getElementById(head).style.cssText = "cursor: pointer; background-color: #4d0084; color: white;";
                            document.getElementById(head.split("-")[0] + "-icon1-" + head.split("-")[1]).style.cssText = "font-size: 30px; color: white;";
                            document.getElementById(head.split("-")[0] + "-icon3-" + head.split("-")[1]).style.cssText = "font-size: 30px; color: white;";
                            document.getElementById(head.split("-")[0] + "-icon4-" + head.split("-")[1]).style.cssText = "font-size: 30px; color: white;";
                            document.getElementById(head.split("-")[0] + "-icon5-" + head.split("-")[1]).style.cssText = "font-size: 30px; color: white;";
                            document.getElementById(head.split("-")[0] + "-icon6-" + head.split("-")[1]).style.cssText = "font-size: 30px; color: white;";
                            document.getElementById(head.split("-")[0] + "-icon7-" + head.split("-")[1]).style.cssText = "font-size: 30px; color: white;";
                        }
                    }
                }
            }

            function expand() {
                document.getElementById("exp").style.display = "none";
                document.getElementById("com").style.display = "";
                var tr = document.getElementsByTagName('tr');

                for (var i = 0; i < tr.length; i++) {
                    if (tr[i].id.toString().split("-")[0] === "H") {
                        tr[i].style.cssText = "cursor: pointer; background-color: #4d0084; color: white;";

                        var icon = document.getElementsByTagName('i');
                        for (var j = 0; j < icon.length; j++) {
                            if (icon[j].id.toString().split("-")[0] === "H") {
                                icon[j].style.cssText = "font-size: 30px; color: white;";
                            }
                        }

                    } else if (tr[i].id.toString().split("-")[0] === "D") {
                        tr[i].style.display = "";
                    }
                }
            }

            function compress() {
                document.getElementById("exp").style.display = "";
                document.getElementById("com").style.display = "none";
                var tr = document.getElementsByTagName('tr');

                for (var i = 0; i < tr.length; i++) {
                    if (tr[i].id.toString().split("-")[0] === "H") {
                        tr[i].style.cssText = "cursor: pointer; background-color: #e5e5e5; color: black;";

                        var icon = document.getElementsByTagName('i');
                        for (var j = 0; j < icon.length; j++) {
                            if (icon[j].id.toString().split("-")[0] === "H") {
                                icon[j].style.cssText = "font-size: 30px; color: #001384;";
                            }
                        }

                    } else if (tr[i].id.toString().split("-")[0] === "D") {
                        tr[i].style.display = "none";
                    }
                }
            }
        </script>
        <style>
            .header {
                padding: 10px 16px;
                background: white;
                color: black;
                display: none;
            }

            .sticky {
                position: fixed;
                top: 0;
                width: 100%;
                display: block;

            }
        </style>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: 320px;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>

    </head>    
    <body>
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
    </div>
    </div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <form action="../resources/manual/IMS300.pdf" target="_blank">
                    <table width="100%">
                        <tr id="dont">
                            <td width="94%" align="left"></td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <a style=" width: 150px;" class="btn btn-primary" id="exp" name="exp" onclick="expand();">
                        <i class="fa fa-expand" style="font-size:20px;"></i>&nbsp;&nbsp; 
                        Expand All</a>
                    <a style=" width: 150px; display: none;" class="btn btn-warning" id="com" name="com" onclick="compress();">
                        <i class="fa fa-compress" style="font-size:20px;"></i>&nbsp;&nbsp; 
                        Collapse All</a>
                    <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                        <thead> 
                            <tr>
                                <th style="text-align: left; width: 30px;">IMP PAID NO.</th>
                                <th style="text-align: left;">VENDOR</th>
                                <th style="text-align: left;">BANK</th>
                                <th style="text-align: left;">PAID DATE</th>
                                <th style="text-align: left;">TOTAL EST PAID AMOUNT</th>
                                <th style="text-align: left;">TOTAL PAID AMOUNT</th>
                                <th style="text-align: left;">REMAINING AMOUNT</th>
                                <th style="text-align: center; width: 300px;">Option
                                    &nbsp;&nbsp;&nbsp;
                                    <a href="create"><i class="fa fa-plus-circle" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>
                                    &nbsp;&nbsp;&nbsp;
                                    <a href="createA"><i class="fa fa-plus-circle" aria-hidden="true" style=" font-size: 30px; color: #c92208;"></i></a>
                                </th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <c:forEach items="${aheadList}" var="x">
                                <tr id="H-${x.impPaidNo}" style="background-color: white;">
                                    <td onclick="subRow('D-${x.impPaidNo}', 'H-${x.impPaidNo}');" style="cursor: pointer;">${x.impPaidNo}</td>
                                    <td onclick="subRow('D-${x.impPaidNo}', 'H-${x.impPaidNo}');" style="cursor: pointer;">${x.vendor}</td>
                                    <td onclick="subRow('D-${x.impPaidNo}', 'H-${x.impPaidNo}');" style="cursor: pointer;">${x.bank}</td>
                                    <td onclick="subRow('D-${x.impPaidNo}', 'H-${x.impPaidNo}');" style="cursor: pointer;">${x.paidDate}</td>
                                    <td onclick="subRow('D-${x.impPaidNo}', 'H-${x.impPaidNo}');" style="cursor: pointer; text-align: right;">${x.totalEstPaidAmount}</td>
                                    <td onclick="subRow('D-${x.impPaidNo}', 'H-${x.impPaidNo}');" style="cursor: pointer; text-align: right;">${x.totalPaidAmount}</td>
                                    <td onclick="subRow('D-${x.impPaidNo}', 'H-${x.impPaidNo}');" style="cursor: pointer; text-align: right;">${x.remainingAmount}</td>
                                    <td style=" text-align: center;">
                                        <a href="edit?impPaidNo=${x.impPaidNo}" ><i id="H-icon4-${x.impPaidNo}" class="fa fa-edit" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="sdisplay?impPaidNo=${x.impPaidNo}" ><i id="H-icon5-${x.impPaidNo}" class="fa fa-television" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a onclick="document.getElementById('myModal-3-${x.impPaidNo}').style.display = 'block';" style="cursor: pointer;"><i id="H-icon1-${x.impPaidNo}" class="fa fa-trash" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="print?impPaidNo=${x.impPaidNo}" target="_blank"><i id="H-icon3-${x.impPaidNo}" class="fa fa-print" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="paid?impPaidNo=${x.impPaidNo}" ><i id="H-icon6-${x.impPaidNo}" class="fa fa-usd" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="print2?impPaidNo=${x.impPaidNo}" target="_blank"><i id="H-icon7-${x.impPaidNo}" class="fa fa-print" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>
                                        <div id="myModal-3-${x.impPaidNo}" class="modal">
                                            <!-- Modal content -->
                                            <div class="modal-content" style=" height: 200px; width: 500px;">
                                                <span id="span-3-${x.impPaidNo}" class="close" onclick="document.getElementById('myModal-3-${x.impPaidNo}').style.display = 'none';">&times;</span>
                                                <p><b><font size="4"></font></b></p>
                                                <table width="100%">
                                                    <tr>
                                                        <td align="center">
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <b style="color: #00399b;"><font size="5">Delete IMP PAID NO. ${x.impPaidNo} ?</font></b>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <br><br><br>
                                                            <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-3-${x.impPaidNo}').style.display = 'none';">
                                                                <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Cancel</a>  
                                                            &nbsp;
                                                            <a class="btn btn btn-outline btn-success" onclick="window.location.href = 'delete?impPaidNo=${x.impPaidNo}'">
                                                                <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a>                    
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>

                                        </div>
                                    </td>
                                </tr>
                                <c:forEach varStatus="ii" items="${x.detailList}" var="p"> 
                                    <c:if test="${ii.count==1}">
                                        <tr id="D-${x.impPaidNo}" style="display: none; background-color: #eeeeee;">
                                            <td><b style=" opacity: 0;">${x.impPaidNo}</b></td>
                                            <td>
                                                <table>
                                                    <tr style="background-color: #eeeeee;">
                                                        <td width="130"><b>Import No.</b></td>
                                                        <td><b>Invoice No.</b></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td></td>
                                            <td><b>Currency</b></td>
                                            <td><b>Product</b></td>
                                            <td style="text-align: right;"><b>Inv Amount</b></td>
                                            <td style="text-align: right;"><b>Foreign Inv Amount</b></td>
                                            <td style="text-align: center;"><b>Status</b></td>
                                        </tr>
                                        <tr id="D-${x.impPaidNo}" style="display: none; background-color: white;">
                                            <td><b style=" opacity: 0;">${x.impPaidNo}</b></td>
                                            <td>
                                                <table>
                                                    <tr style="background-color: white;">
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td></td>
                                            <td></td>
                                            <td style="text-align: right;"><b>TOTAL</b></td>
                                            <td style="text-align: right;"><b>${p.sumPaidInvAmount}</b></td>
                                            <td style="text-align: right;"><b>${p.sumPaidForeignInvAmount}</b></td>
                                            <td></td>
                                        </tr>
                                    </c:if>
                                    <tr id="D-${x.impPaidNo}" style="display: none; background-color: white;">
                                        <td><b style=" opacity: 0;">${x.impPaidNo}</b></td>
                                        <td>
                                            <table>
                                                <tr style="background-color: white;">
                                                    <td width="130"><b>${p.importNo}</b></td>
                                                    <td>${p.invoiceNo}</td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td></td>
                                        <td>${p.ccy}</td>
                                        <td>${p.product}</td>
                                        <td style="text-align: right;">${p.paidInvAmount}</td>
                                        <td style="text-align: right;">${p.paidForeignInvAmount}</td>
                                        <td style="text-align: center;">${p.sts}</td>
                                    </tr>
                                </c:forEach>
                            </c:forEach>
                        </tbody>
                    </table>
                    <div class="header" id="myHeader">
                        <table style="width:100%; border-bottom: 1px solid black;">
                            <tr>
                                <th style="text-align: left; width: 150px;">IMP PAID NO.</th>
                                <th style="text-align: left; width: 350px;">VENDER</th>
                                <th style="text-align: left; width: 350px;">BANK</th>
                                <th style="text-align: left; width: 190px;">PAID DATE</th>
                                <th style="text-align: right; width: 200px;">TOTAL EST PAID AMOUNT</th>
                                <th style="text-align: right; width: 230px;">TOTAL PAID AMOUNT</th>
                                <th style="text-align: right; width: 220px;">REMAINING AMOUNT</th>
                                <th style="text-align: center;">Option
                                    &nbsp;&nbsp;&nbsp;
                                    <a href="create"><i class="fa fa-plus-circle" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>
                                </th>
                            </tr>
                        </table>
                    </div>
                    <script>
                        window.onscroll = function () {
                            myFunction();
                        };

                        var header = document.getElementById("myHeader");
                        var sticky = header.offsetTop;

                        function myFunction() {
                            if (window.pageYOffset > sticky) {
                                header.classList.add("sticky");
                            } else {
                                header.classList.remove("sticky");
                            }
                        }
                    </script>
                    <!--**********************-->
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top--> 
                </div> 
                <br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>