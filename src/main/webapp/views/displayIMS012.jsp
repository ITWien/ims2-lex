<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>IMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script> 
        <style>
            input[type=text], select{
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date], input[type=number]{
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
//                    "paging": false,
                    "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
                    "bSortClasses": false,
                    "order": [[0, "desc"]],
                    "columnDefs": [
                        {orderable: false, targets: [3]}
//                        {"width": "12%", "targets": 0},
                    ]
                });

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').not(":eq(3)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });

                // DataTable
                var table = $('#showTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });
        </script>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: 320px;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>

    </head>    
    <body>
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
    </div>
    </div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <form action="../resources/manual/IMS012.pdf" target="_blank">
                    <table width="100%">
                        <tr id="dont">
                            <td width="94%" align="left"></td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                        <thead>
                            <tr>
                                <th>FFC Code</th>
                                <th>Name</th>
                                <th>Name Thai</th>
                                <th style="text-align: center;">Option
                                    &nbsp;&nbsp;&nbsp;
                                    <a href="create"><i class="fa fa-plus-circle" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>
                                </th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <c:forEach items="${aheadList}" var="x">
                                <tr>
                                    <td>${x.code}</td>
                                    <td>${x.name}</td>
                                    <td>${x.namet}</td>
                                    <td style=" text-align: center;">
                                        <a href="edit?code=${x.code}" ><i class="fa fa-edit" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a onclick="document.getElementById('myModal-3-${x.code}').style.display = 'block';" style="cursor: pointer;"><i class="fa fa-trash" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>
                                        <div id="myModal-3-${x.code}" class="modal">
                                            <!-- Modal content -->
                                            <div class="modal-content" style=" height: 200px; width: 500px;">
                                                <span id="span-3-${x.code}" class="close" onclick="document.getElementById('myModal-3-${x.code}').style.display = 'none';">&times;</span>
                                                <p><b><font size="4"></font></b></p>
                                                <table width="100%">
                                                    <tr>
                                                        <td align="center">
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <b style="color: #00399b;"><font size="5">Delete FFC Code : ${x.code} ?</font></b>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <br><br><br>
                                                            <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-3-${x.code}').style.display = 'none';">
                                                                <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Cancel</a>  
                                                            &nbsp;
                                                            <a class="btn btn btn-outline btn-success" onclick="window.location.href = 'delete?code=${x.code}'">
                                                                <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a>                    
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>

                                        </div>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <!--**********************-->
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top--> 
                </div> 
                <br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>