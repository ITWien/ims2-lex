<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>IMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <style>
            input[type=text], select{
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date], input[type=number]{
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>


    </head>
    <body>
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
    </div>
    </div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <form action="../resources/manual/IMSFORM.pdf" target="_blank">
                    <table width="100%">
                        <tr id="dont">
                            <td width="94%" align="left"></td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <center>
                        <form action="print" method="post" target="_blank">
                            <table width="100%">
                                <tr>
                                    <th width="25%"></th>
                                    <th style=" text-align: right;"><b>FORM NAME :</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                    <th>
                                        <input type="text" id="formName" name="formName" style="height: 30px; width: 300px;">
                                    </th>
                                    <th width="25%">
                                        NO : <input type="text" id="NO" name="NO" style="height: 30px; width: 300px;"><br>
                                        DATE : <input type="text" id="DATE" name="DATE" style="height: 30px; width: 300px;">
                                    </th>
                                </tr>
                            </table>
                            <hr>
                            <table width="100%">
                                <tr style=" background-color: #daf0f5;">
                                    <th colspan="3"></th>
                                    <th colspan="2" style="text-align: center; border-bottom: 1px solid #ccc;">AMOUNT</th>
                                </tr>
                                <tr style=" background-color: #daf0f5;">
                                    <th>ACCOUNT CODE</th>
                                    <th>ACCOUNT NAME</th>
                                    <th>DESCRIPTION</th>
                                    <th style="text-align: center;">DEBIT</th>
                                    <th style="text-align: center;">CREDIT</th>
                                </tr>
                                <tr>
                                    <th>
                                        <input type="text" id="ACC_CODE_1" name="ACC_CODE_1" style="height: 30px; width: 300px;">
                                        <input type="text" id="ACC_CODE_2" name="ACC_CODE_2" style="height: 30px; width: 300px;">
                                        <input type="text" id="ACC_CODE_3" name="ACC_CODE_3" style="height: 30px; width: 300px;">
                                        <input type="text" id="ACC_CODE_4" name="ACC_CODE_4" style="height: 30px; width: 300px;">
                                        <input type="text" id="ACC_CODE_5" name="ACC_CODE_5" style="height: 30px; width: 300px;">
                                        <input type="text" id="ACC_CODE_6" name="ACC_CODE_6" style="height: 30px; width: 300px;">
                                        <input type="text" id="ACC_CODE_7" name="ACC_CODE_7" style="height: 30px; width: 300px;">
                                        <input type="text" id="ACC_CODE_8" name="ACC_CODE_8" style="height: 30px; width: 300px;">
                                    </th>
                                    <th>
                                        <input type="text" id="ACC_NAME_1" name="ACC_NAME_1" style="height: 30px; width: 300px;">
                                        <input type="text" id="ACC_NAME_2" name="ACC_NAME_2" style="height: 30px; width: 300px;">
                                        <input type="text" id="ACC_NAME_3" name="ACC_NAME_3" style="height: 30px; width: 300px;">
                                        <input type="text" id="ACC_NAME_4" name="ACC_NAME_4" style="height: 30px; width: 300px;">
                                        <input type="text" id="ACC_NAME_5" name="ACC_NAME_5" style="height: 30px; width: 300px;">
                                        <input type="text" id="ACC_NAME_6" name="ACC_NAME_6" style="height: 30px; width: 300px;">
                                        <input type="text" id="ACC_NAME_7" name="ACC_NAME_7" style="height: 30px; width: 300px;">
                                        <input type="text" id="ACC_NAME_8" name="ACC_NAME_8" style="height: 30px; width: 300px;">
                                    </th>
                                    <th>
                                        <input type="text" id="DESC_1" name="DESC_1" style="height: 30px; width: 300px;">
                                        <input type="text" id="DESC_2" name="DESC_2" style="height: 30px; width: 300px;">
                                        <input type="text" id="DESC_3" name="DESC_3" style="height: 30px; width: 300px;">
                                        <input type="text" id="DESC_4" name="DESC_4" style="height: 30px; width: 300px;">
                                        <input type="text" id="DESC_5" name="DESC_5" style="height: 30px; width: 300px;">
                                        <input type="text" id="DESC_6" name="DESC_6" style="height: 30px; width: 300px;">
                                        <input type="text" id="DESC_7" name="DESC_7" style="height: 30px; width: 300px;">
                                        <input type="text" id="DESC_8" name="DESC_8" style="height: 30px; width: 300px;">
                                    </th>
                                    <th style="text-align: center;">
                                        <input type="text" id="DEBIT_1" name="DEBIT_1" class="currency" style="height: 30px; width: 300px; text-align: right;">
                                        <input type="text" id="DEBIT_2" name="DEBIT_2" class="currency" style="height: 30px; width: 300px; text-align: right;">
                                        <input type="text" id="DEBIT_3" name="DEBIT_3" class="currency" style="height: 30px; width: 300px; text-align: right;">
                                        <input type="text" id="DEBIT_4" name="DEBIT_4" class="currency" style="height: 30px; width: 300px; text-align: right;">
                                        <input type="text" id="DEBIT_5" name="DEBIT_5" class="currency" style="height: 30px; width: 300px; text-align: right;">
                                        <input type="text" id="DEBIT_6" name="DEBIT_6" class="currency" style="height: 30px; width: 300px; text-align: right;">
                                        <input type="text" id="DEBIT_7" name="DEBIT_7" class="currency" style="height: 30px; width: 300px; text-align: right;">
                                        <input type="text" id="DEBIT_8" name="DEBIT_8" class="currency" style="height: 30px; width: 300px; text-align: right;">
                                    </th>
                                    <th style="text-align: center;">
                                        <input type="text" id="CREDIT_1" name="CREDIT_1" class="currency" style="height: 30px; width: 300px; text-align: right;">
                                        <input type="text" id="CREDIT_2" name="CREDIT_2" class="currency" style="height: 30px; width: 300px; text-align: right;">
                                        <input type="text" id="CREDIT_3" name="CREDIT_3" class="currency" style="height: 30px; width: 300px; text-align: right;">
                                        <input type="text" id="CREDIT_4" name="CREDIT_4" class="currency" style="height: 30px; width: 300px; text-align: right;">
                                        <input type="text" id="CREDIT_5" name="CREDIT_5" class="currency" style="height: 30px; width: 300px; text-align: right;">
                                        <input type="text" id="CREDIT_6" name="CREDIT_6" class="currency" style="height: 30px; width: 300px; text-align: right;">
                                        <input type="text" id="CREDIT_7" name="CREDIT_7" class="currency" style="height: 30px; width: 300px; text-align: right;">
                                        <input type="text" id="CREDIT_8" name="CREDIT_8" class="currency" style="height: 30px; width: 300px; text-align: right;">
                                    </th>
                                </tr>
                                <tr style=" border-top: 1px solid #ccc; background-color: #daf0f5;">
                                    <th colspan="5" style="text-align: center;">
                                        TOTAL
                                    </th>
                                </tr>
                                <tr style=" border-top: 1px solid #ccc;">
                                    <th colspan="3">
                                        <input type="text" id="TOT_THAI" name="TOT_THAI" style="height: 30px;">
                                    </th>
                                    <th style="text-align: center;">
                                        <input type="text" id="SUM_DEBIT" name="SUM_DEBIT" class="currency" style="height: 30px; width: 300px; text-align: right;">
                                    </th>
                                    <th style="text-align: center;">
                                        <input type="text" id="SUM_CREDIT" name="SUM_CREDIT" class="currency" style="height: 30px; width: 300px; text-align: right;">
                                    </th>
                                </tr>
                            </table>
                            <script>
                                $(document).ready(function () {
                                    $('.currency').on('change', function () {
                                        var val = $(this).val().toString().replace(/,/g, '');
                                        $(this).val(currencyFormat(parseFloat(val), 2));
                                        if ($(this).val() === 'NaN') {
                                            $(this).val('');
                                        }
                                    });

                                    $('#SUM_DEBIT').on('change', function () {
                                        var val = $(this).val();
                                        ThaiBaht(val);
                                    });
                                });

                                function currencyFormat(num, fp) {
                                    var pp = Math.pow(10, fp);
                                    num = Math.round(num * pp) / pp;
                                    return num.toFixed(fp).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                                }

                                function ThaiBaht(number) {
                                    $.ajax({
                                        url: "/IMS2/ShowDataTablesIMSFORM?mode=getThaiBaht"
                                                + "&number=" + number

                                    }).done(function (result) {
                                        console.log(result);
                                        $("#TOT_THAI").val('(' + result + ')');

                                    }).fail(function (jqXHR, textStatus, errorThrown) {
                                        // needs to implement if it fails
                                    });
                                }
                            </script>
                            <br>
                            <button onclick="this.form.submit();" type="button" class="btn btn-primary">
                                <i style=" font-size: 20px;" class="fa fa-print" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Print
                            </button>
                        </form>
                    </center>
                    <!--**********************-->
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top-->
                </div>
                <br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>