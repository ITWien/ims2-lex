<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>IMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script> 
        <style>
            input[type=text], select{
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date], input[type=number]{
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            button[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[type=submit]:hover {
                background-color: #3973d6;
            }

            button[type=button] {
                width: 100%;
                background-color: #c10000;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
                    "paging": false,
                    "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
                    "bSortClasses": false,
//                    "order": [[0, "desc"]],
                    "columnDefs": [
//                        {orderable: false, targets: [5]},
                        {orderable: false, targets: [6]},
                        {orderable: false, targets: [7]},
                        {orderable: false, targets: [8]},
                        {orderable: false, targets: [9]},
                        {orderable: false, targets: [10]},
                        {orderable: false, targets: [11]},
                        {orderable: false, targets: [12]},
//                        {"width": "12%", "targets": 0},
                    ]
                });

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').not(":eq(6)").not(":eq(6)").not(":eq(6)")
                        .not(":eq(6)").not(":eq(6)").not(":eq(6)").not(":eq(6)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });

                // DataTable
                var table = $('#showTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });
        </script>
        <script>
            function currencyFormat(num) {
                return num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
            }

            function Update(x) {
                x.value = currencyFormat(parseFloat(x.value.replace(/,/g, '')));
            }

            function Validate() {
                var f = document.getElementById("dateF").value;
                var sc = document.getElementById("shipcom").value;

                if (f === "" || sc === "") {
                    document.getElementById('myModal-6').style.display = 'block';
                    return false;
                }
            }

            function UpdateAll() {
                var advImpAmt = document.getElementsByName("advImpAmt");
                var sum = 0;
                for (var i = 0; i < advImpAmt.length; i++) {
                    if (advImpAmt[i].value.toString().trim() === "") {
                        sum += 0;
                    } else {
                        sum += parseFloat(advImpAmt[i].value.replace(/,/g, ''));
                    }
                }
                document.getElementById("finalTotalAmt").innerHTML = currencyFormat(parseFloat(sum));
                document.getElementById("finalTotalAmtData").value = currencyFormat(parseFloat(sum));
            }

            function TaxDuty(line) {
                var invAmt = document.getElementById("invAmt-" + line);
                var taxDutyP = document.getElementById("taxDutyP-" + line);
                var taxDuty = document.getElementById("taxDuty-" + line);
                var result = parseFloat(invAmt.innerHTML.replace(/,/g, '')) * (parseFloat(taxDutyP.value) / 100);
                taxDuty.value = currencyFormat(parseFloat(result));

                TaxBase(line);
                Vat7(line);
                TotAmt(line);
            }

            function TaxBase(line) {
                var invAmt = document.getElementById("invAmt-" + line);
                var taxBase = document.getElementById("taxBase-" + line);
                var taxDuty = document.getElementById("taxDuty-" + line);
                var result = parseFloat(invAmt.innerHTML.replace(/,/g, '')) + parseFloat(taxDuty.value.replace(/,/g, ''));
                taxBase.value = currencyFormat(parseFloat(result));
            }

            function Vat7(line) {
                var taxBase = document.getElementById("taxBase-" + line);
                var vat7 = document.getElementById("vat7-" + line);
                var result = parseFloat(taxBase.value.replace(/,/g, '')) * (7 / 100);
                vat7.value = currencyFormat(parseFloat(result));
            }

            function TotAmt(line) {
                var taxDuty = document.getElementById("taxDuty-" + line);
                var vat7 = document.getElementById("vat7-" + line);
                var totAmt = document.getElementById("totAmt-" + line);
                var result = parseFloat(taxDuty.value.replace(/,/g, '')) + parseFloat(vat7.value.replace(/,/g, ''));
                totAmt.value = currencyFormat(parseFloat(result));
            }

            checkDelete = function () {
                var checkboxes = document.getElementsByTagName('input');
                var nodataSelect = false;

                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type === 'checkbox' && checkboxes[i].name !== 'selectAll') {
                        if (checkboxes[i].checked === true) {
                            nodataSelect = true;
                        }
                    }
                }

                if (nodataSelect) {
                    document.getElementById('formData').action = 'deleteAdd';
                    document.getElementById('formData').method = 'post';
                    document.getElementById('formData').submit();
                } else {
                    document.getElementById('myModal-4').style.display = 'block';
                }
            };
        </script>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: 320px;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
        <style>
            .header {
                padding: 10px 16px;
                background: white;
                color: black;
                display: none;
            }

            .sticky {
                position: fixed;
                top: 0;
                width: 100%;
                display: block;

            }
        </style>
        <script>
            function checkAll(ele) {
                var checkboxes = document.getElementsByTagName('input');
                if (ele.checked) {
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type == 'checkbox') {
                            checkboxes[i].checked = true;
                        }
                    }
                } else {
                    for (var i = 0; i < checkboxes.length; i++) {
                        console.log(i)
                        if (checkboxes[i].type == 'checkbox') {
                            checkboxes[i].checked = false;
                        }
                    }
                }
            }
        </script>

    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
    </div>
    </div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <br>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;"> 
                    <center>
                        <form action="create" method="post">
                            <table width="70%">
                                <tr>
                                    <th><b>TRANSACTION DATE :</b></th>
                                    <th style=" text-align: left;">
                                        <b>FROM</b>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="date" id="dateF" name="dateF" value="${dateF}" style=" height: 30px; width: 70%;">
                                    </th>
                                    <th style=" text-align: center; width: 50px;"><b>TO</b></th>
                                    <th style=" text-align: center;">
                                        <input type="date" id="dateT" name="dateT" value="${dateT}" style=" height: 30px;  width: 100%;">
                                    </th>
                                    <th></th>
                                    <th><b>ADV IMP NO.</b>
                                        <input type="text" id="no" name="no" value="${cur}" style=" height: 30px; width: 150px;" onfocus="this.blur();">
                                    </th>
                                </tr>
                                <tr>
                                    <th colspan="1"><b>SHIPPING COMPANY :</b></th>
                                    <th colspan="3">
                                        <select id="shipcom" name="shipcom" style=" height: 30px;">
                                            <option value="${shipcode}" selected hidden>${shipcode} : ${shipname} (${shipnamet})</option>
                                            <c:forEach items="${shipList}" var="x">
                                                <option value="${x.code}">${x.code} : ${x.name} (${x.namet})</option>
                                            </c:forEach>
                                        </select>
                                    </th>
                                    <th style=" width: 100px;"></th>
                                    <th>
                                        <button type="submit" style=" width: 115px;" onclick="return Validate();">Enter</button>
                                        <button type="button" style=" width: 115px;" onclick="window.location.href = 'display'">Back</button>
                                    </th>
                                </tr>
                            </table>
                        </form>
                    </center>
                    <form id="formData" name="formData" action="add" method="post">
                        <input type="hidden" id="userid" name="userid">
                        <input type="hidden" id="advImpNo" name="advImpNo" value="${cur}">
                        <input type="hidden" id="dateF" name="dateF" value="${dateF}">
                        <input type="hidden" id="dateT" name="dateT" value="${dateT}">
                        <input type="hidden" id="shipCom" name="shipCom" value="${shipcode}">
                        <input type="hidden" id="checkDate" name="checkDate" value="${checkDate}">
                        <input type="hidden" id="checkNo" name="checkNo" value="${checkNo}">
                        <input type="hidden" id="bnkCode" name="bnkCode" value="${bnkCode}">
                        <input type="hidden" id="finalTotalAmtData" name="finalTotalAmtData">
                        <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                            <thead>
                                <tr>
                                    <th style="text-align: center;">NO.</th>
                                    <th style="text-align: center;">PRE-IMPORT NO.</th>
                                    <th style="text-align: center;">IMP COMP</th>
                                    <th style="text-align: center;">INVOICE NO. LIST</th>
                                    <th style="text-align: center;">TRANS DATE</th>
                                    <th style="text-align: center;">INVOICE AMOUNT</th>
                                    <th style="text-align: center;">TAX DUTY %</th>
                                    <th style="text-align: center;">ADVANCE IMPORT AMOUNT</th>
                                    <th style="text-align: center; color: #2179ff;">TAX DUTY</th>
                                    <th style="text-align: center; color: #2179ff;">TAX BASE</th>
                                    <th style="text-align: center; color: #2179ff;">VAT 7%</th>
                                    <th style="text-align: center; color: #2179ff;">TOTAL AMOUNT</th>
                                    <th style="text-align: center;">
                                        <input style="width: 20px; height: 20px;" type="checkbox" name="selectAll" onchange="checkAll(this)">
                                        <a onclick="checkDelete();">
                                            <i style=" font-size: 30px;" class="fa fa-trash" aria-hidden="true"></i>
                                        </a>
                                    </th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot> 
                            <tbody>
                                <c:forEach items="${detList}" var="x">
                                    <tr>
                                        <td>${x.no}<input type="hidden" id="no-${x.no}" name="no" value="${x.no}"></td>
                                        <td>${x.pImpNo}<input type="hidden" id="pImpNo-${x.no}" name="pImpNo" value="${x.pImpNo}"></td>
                                        <td>${x.imcD}<input type="hidden" id="imcD-${x.no}" name="imcD" value="${x.imcD}"></td>
                                        <td>${x.invNoList}<input type="hidden" id="invNoList-${x.no}" name="invNoList" value="${x.invNoList}"></td>
                                        <td>${x.transDate}<input type="hidden" id="transDate-${x.no}" name="transDate" value="${x.transDate}"></td>
                                        <td id="invAmt-${x.no}">${x.invAmt}<input type="hidden" id="invAmtData-${x.no}" name="invAmtData" value="${x.invAmt}"></td>
                                        <td>
                                            <input id="taxDutyP-${x.no}" name="taxDutyP" type="number" min="0" max="100" onchange="TaxDuty(${x.no});" style="text-align: center;">
                                        </td>
                                        <td>
                                            <input id="advImpAmt-${x.no}" name="advImpAmt" type="text" onchange="Update(this);
                                                    UpdateAll();" style="text-align: right;"> 
                                        </td>
                                        <td><input title="= INVOICE AMOUNT × TAX DUTY %" id="taxDuty-${x.no}" name="taxDuty" type="text" style=" background-color: #ffeca8; color: #00388e; text-align: right;" onfocus="this.blur();"></td>
                                        <td><input title="= INVOICE AMOUNT + TAX DUTY" id="taxBase-${x.no}" name="taxBase" type="text" style=" background-color: #ffeca8; color: #00388e; text-align: right;" onfocus="this.blur();"></td>
                                        <td><input title="= TAX BASE × 7%" id="vat7-${x.no}" name="vat7" type="text" style=" background-color: #ffeca8; color: #00388e; text-align: right;" onfocus="this.blur();"></td>
                                        <td><input title="= TAX DUTY + VAT 7%" id="totAmt-${x.no}" name="totAmt" type="text" style=" background-color: #ffeca8; color: #00388e; text-align: right;" onfocus="this.blur();"></td>
                                        <td style="text-align: center;">
                                            ${x.checkBox}
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                        <table style=" width: 100%;">
                            <tr>
                                <th style=" width: 68%; text-align: right;">${totalAmt}</th>
                                <th style=" width: 32%;"></th>
                            </tr>
                        </table> 
                        <center>
                            ${detListSizeNotZero}
                            ${cb}
                        </center>
                    </form>
                    <div id="myModal-3" class="modal">
                        <!-- Modal content -->
                        <div class="modal-content" style=" height: 200px; width: 500px;">
                            <span id="span-3" class="close" onclick="document.getElementById('myModal-3').style.display = 'none';">&times;</span>
                            <p><b><font size="4"></font></b></p>
                            <table width="100%">
                                <tr>
                                    <td align="center">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <b style="color: #00399b;"><font size="5">Are you sure ?</font></b>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <br><br><br>
                                        <a class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-3').style.display = 'none';">
                                            <i class="fa fa-times-circle-o" style="font-size:20px;"></i> Cancel</a>  
                                        &nbsp;
                                        <a class="btn btn btn-outline btn-success" onclick="document.getElementById('formData').submit();">
                                            <i class="fa fa-check-circle-o" style="font-size:20px;"></i> Confirm</a>                    
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </div>
                    <div id="myModal-4" class="modal">
                        <!-- Modal content -->
                        <div class="modal-content" style=" height: 200px; width: 500px;">
                            <span id="span-4" class="close" onclick="document.getElementById('myModal-4').style.display = 'none';">&times;</span>
                            <p><b><font size="4"></font></b></p>
                            <table width="100%">
                                <tr>
                                    <td align="center">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <b style="color: #00399b;"><font size="5">Please select the data !</font></b>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <br>
                                        <br>
                                        <br>
                                        <a style=" width: 100px;" class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-4').style.display = 'none';">
                                            OK
                                        </a>                 
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </div>
                    <div id="myModal-6" class="modal">
                        <!-- Modal content -->
                        <div class="modal-content" style=" height: 220px; width: 800px;">
                            <span id="span-6" class="close" onclick="document.getElementById('myModal-6').style.display = 'none';">&times;</span>
                            <p><b><font size="4"></font></b></p>
                            <table width="100%">
                                <tr>
                                    <td align="center">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <b style="color: #00399b;"><font size="5">Please fill out</font></b>
                                        <br>
                                        <b style="color: #00399b;"><font size="5">Transaction Date(From) and Shipping Company !</font></b>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <br>
                                        <br>
                                        <br>
                                        <a style=" width: 100px;" class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-6').style.display = 'none';">
                                            OK
                                        </a>                 
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </div>
                    <div class="header" id="myHeader">
                        <table style="width:100%; border-bottom: 1px solid black;">
                            <tr>
                                <th style="text-align: left; width: 5%;">NO.</th>
                                <th style="text-align: left; width: 7%;">PRE-IMPORT NO.</th>
                                <th style="text-align: center; width: 14%;">INVOICE NO. LIST</th>
                                <th style="text-align: center; width: 7%;">TRANS DATE</th>
                                <th style="text-align: center; width: 7%;">INVOICE AMOUNT</th>
                                <th style="text-align: center; width: 7%;">TAX DUTY %</th>
                                <th style="text-align: center; width: 10%;">ADVANCE IMPORT AMOUNT</th>
                                <th style="text-align: left; color: #2179ff; width: 10%;">TAX DUTY</th>
                                <th style="text-align: left; color: #2179ff; width: 10%;">TAX BASE</th>
                                <th style="text-align: left; color: #2179ff; width: 10%;">VAT 7%</th>
                                <th style="text-align: left; color: #2179ff; width: 10%;">TOTAL AMOUNT</th>
                            </tr>
                        </table>
                    </div>
                    <script>
                        window.onscroll = function () {
                            myFunction();
                        };

                        var header = document.getElementById("myHeader");
                        var sticky = header.offsetTop;

                        function myFunction() {
                            if (window.pageYOffset > sticky) {
                                header.classList.add("sticky");
                            } else {
                                header.classList.remove("sticky");
                            }
                        }
                    </script>
                    <!--**********************-->
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top--> 
                </div> 
                <br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>