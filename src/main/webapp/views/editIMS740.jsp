<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>IMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script> 
        <style>
            input[type=text], select{
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date], input[type=number]{
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }

            i[id=ic]:hover {
                background-color: #042987;
                border-radius: 15px;
            }

            i[id=ic2]:hover {
                background-color: #165910;
                border-radius: 15px;
            }

            button[type=button] {
                width: 100%;
                background-color: #c10000;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
                    "paging": false,
                    "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
                    "bSortClasses": false,
//                    fixedColumns: true,
//                    "order": [[0, "desc"]],
//                    "columnDefs": [
//                        {orderable: false, targets: [3]},
//                        {orderable: false, targets: [4]},
//                        {orderable: false, targets: [5]},
//                        {orderable: false, targets: [6]},
//                        {orderable: false, targets: [7]},
//                        {orderable: false, targets: [8]},
//                        {orderable: false, targets: [9]},
//                        {orderable: false, targets: [10]},
//                        {orderable: false, targets: [11]},
//                        {"width": "12%", "targets": 0},
//                        {"width": "12%", "targets": 1},
//                        {"width": "30%", "targets": 2},
//                        {"width": "9%", "targets": 3},
//                        {"width": "5%", "targets": 4},
//                        {"width": "9%", "targets": 5},
//                        {"width": "8%", "targets": 6},
//                        {"width": "9%", "targets": 7},
//                        {"width": "9%", "targets": 8},
//                        {"width": "9%", "targets": 9},
//                        {"width": "9%", "targets": 10},
//                        {"width": "9%", "targets": 11}
//                    ]
                });

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });

                // DataTable
                var table = $('#showTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });
        </script>
        <style>
            .header {
                padding: 10px 16px;
                background: white;
                color: black;
                display: none;
            }

            .sticky {
                position: fixed;
                top: 0;
                width: 100%;
                display: block;

            }
        </style>

    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid'); SUM();">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
    </div>
    </div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <br>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <form id="frm" name="frm" action="display" method="post">
                        <input type="hidden" id="userid" name="userid">
                        <table width="100%">
                            <tr id="dont">                              
                                <td align="left"><b style="color: #00399b;">Import Company : </b>
                                    <input style="width:400px;" type="text" name="imc" id="imc" value="${imc} : ${imcName}">
                                </td>
                                <td align="right"></td>
                                <td align="right" width="360px">
                                    <button type="button" style=" width: 115px;" onclick="window.location.href = 'display'">Back</button>
                                </td>
                            </tr>
                            <tr id="dont">                              
                                <td align="left"><b style="color: #00399b;">Year / Month : </b>
                                    <input style="width:100px;" type="text" placeholder="เช่น 201906" name="ym" id="ym" value="${ym}" maxlength="6">
                                </td>
                                <td align="right"></td>
                                <td align="right" width="360px">
                                    <span class="fa-stack fa-lg fa-2x" onclick="document.getElementById('formData').action = 'print'; document.getElementById('formData').submit();" style=" cursor: pointer;">
                                        <i id="ic2" class="fa fa-circle fa-stack-2x" style="color: #0b8c00"></i>
                                        <i id="ic2" class="fa fa-print fa-stack-1x fa-inverse"></i>
                                    </span>
                                </td>
                            </tr>
                        </table>
                    </form>
                    <form id="formData" name="formData" action="edit" method="post">
                        <input type="hidden" id="ym" name="ym" value="${ym}">
                        <input type="hidden" id="imc" name="imc" value="${imc}">
                        <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                            <thead>
                                <tr id="dont">
                                    <th style="text-align: center;">IMPORT NO.</th>
                                    <th style="text-align: center;">VENDOR DEPT.</th>
                                    <th style="text-align: center;">INVOICE NO.</th>
                                    <th style="text-align: center;">PRODUCT</th>
                                    <th style="text-align: center;">TRANSPORTATION</th>
                                    <th style="text-align: center;">INVOICE DATE</th>
                                    <th style="text-align: center;">ARRIVAL DATE</th>
                                    <th style="text-align: center;">DEBIT NOTE DATE</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr id="dont">
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <c:forEach items="${rp740List}" var="x">
                                    <tr>
                                        <td>${x.impNo}</td>
                                        <td>${x.venDept}</td>
                                        <td>${x.invNo}</td>
                                        <td>${x.prod}</td>
                                        <td>${x.transport}</td>
                                        <td>${x.invDate}</td>
                                        <td>${x.arrDate}</td>
                                        <td>${x.debitDate}</td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </form>   
                    <center>
                        <span class="fa-stack fa-lg fa-2x" onclick="document.getElementById('formData').action = 'edit'; document.getElementById('formData').submit();" style=" cursor: pointer;">
                            <i id="ic" class="fa fa-circle fa-stack-2x" style="color: #154baf"></i>
                            <i id="ic" class="fa fa-save fa-stack-1x fa-inverse"></i>
                        </span>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <span class="fa-stack fa-lg fa-2x" onclick="document.getElementById('formData').action = 'print'; document.getElementById('formData').submit();" style=" cursor: pointer;">
                            <i id="ic2" class="fa fa-circle fa-stack-2x" style="color: #0b8c00"></i>
                            <i id="ic2" class="fa fa-print fa-stack-1x fa-inverse"></i>
                        </span>
                    </center>
                </div> 
                <br>
            </div> <!-- end .container-fluid -->
            <div class="header" id="myHeader">
                <table style="width:100%; border-bottom: 1px solid black;">
                    <tr id="dont">
                        <th style="text-align: center; width: 100px;">IMPORT NO.</th>
                        <th style="text-align: center; width: 220px;">VENDOR DEPT.</th>
                        <th style="text-align: center; width: 80px;">INVOICE NO.</th>
                        <th style="text-align: center; width: 80px;">PRODUCT</th>
                        <th style="text-align: center; width: 100px;">TRANSPORTATION</th>
                        <th style="text-align: center; width: 100px;">INVOICE DATE</th>
                        <th style="text-align: center; width: 100px;">ARRIVAL DATE</th>
                        <th style="text-align: center; width: 100px;">DEBIT NOTE DATE</th>
                    </tr>
                </table>
            </div>
        </div> <!-- end #wrapper -->
        <script>
            window.onscroll = function () {
                myFunction();
            };

            var header = document.getElementById("myHeader");
            var sticky = header.offsetTop;

            function myFunction() {
                if (window.pageYOffset > sticky) {
                    header.classList.add("sticky");
                } else {
                    header.classList.remove("sticky");
                }
            }
        </script>
    </script>
</body>
</html>