<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>IMS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script> 
        <style>
            input[type=text], select{
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=date], input[type=number]{
                /*width: 100%;*/
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
                    "paging": false,
                    "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
                    "bSortClasses": false,
                    "order": [[0, "desc"]],
                    "columnDefs": [
                        {orderable: false, targets: [7]}
//                        {"width": "12%", "targets": 0},
                    ]
                });

                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').not(":eq(7)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });

                // DataTable
                var table = $('#showTable').DataTable();

                // Apply the search
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });
        </script>
        <script>
            function ThaiBaht(Number)
            {
                //ตัดสิ่งที่ไม่ต้องการทิ้งลงโถส้วม
                for (var i = 0; i < Number.length; i++)
                {
                    Number = Number.replace(",", ""); //ไม่ต้องการเครื่องหมายคอมมาร์
                    Number = Number.replace(" ", ""); //ไม่ต้องการช่องว่าง
                    Number = Number.replace("บาท", ""); //ไม่ต้องการตัวหนังสือ บาท
                    Number = Number.replace("฿", ""); //ไม่ต้องการสัญลักษณ์สกุลเงินบาท
                }
                //สร้างอะเรย์เก็บค่าที่ต้องการใช้เอาไว้
                var TxtNumArr = new Array("ศูนย์", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า", "สิบ");
                var TxtDigitArr = new Array("", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน");
                var BahtText = "";
                //ตรวจสอบดูซะหน่อยว่าใช่ตัวเลขที่ถูกต้องหรือเปล่า ด้วย isNaN == true ถ้าเป็นข้อความ == false ถ้าเป็นตัวเลข
                if (isNaN(Number))
                {
                    return "ข้อมูลนำเข้าไม่ถูกต้อง";
                } else
                {
                    //ตรวสอบอีกสักครั้งว่าตัวเลขมากเกินความต้องการหรือเปล่า
                    if ((Number - 0) > 9999999.9999)
                    {
                        return "ข้อมูลนำเข้าเกินขอบเขตที่ตั้งไว้";
                    } else
                    {
                        //พรากทศนิยม กับจำนวนเต็มออกจากกัน (บาปหรือเปล่าหนอเรา พรากคู่เขา)
                        Number = Number.split(".");
                        //ขั้นตอนต่อไปนี้เป็นการประมวลผลดูกันเอาเองครับ แบบว่าขี้เกียจจะจิ้มดีดแล้ว อิอิอิ
                        if (Number[1].length > 0)
                        {
                            Number[1] = Number[1].substring(0, 2);
                        }
                        var NumberLen = Number[0].length - 0;
                        for (var i = 0; i < NumberLen; i++)
                        {
                            var tmp = Number[0].substring(i, i + 1) - 0;
                            if (tmp != 0)
                            {
                                if ((i == (NumberLen - 1)) && (tmp == 1))
                                {
                                    BahtText += "เอ็ด";
                                } else
                                if ((i == (NumberLen - 2)) && (tmp == 2))
                                {
                                    BahtText += "ยี่";
                                } else
                                if ((i == (NumberLen - 2)) && (tmp == 1))
                                {
                                    BahtText += "";
                                } else
                                {
                                    BahtText += TxtNumArr[tmp];
                                }
                                BahtText += TxtDigitArr[NumberLen - i - 1];
                            }
                        }
                        BahtText += "บาท";
                        if ((Number[1] == "0") || (Number[1] == "00"))
                        {
                            BahtText += "ถ้วน";
                        } else
                        {
                            DecimalLen = Number[1].length - 0;
                            for (var i = 0; i < DecimalLen; i++)
                            {
                                var tmp = Number[1].substring(i, i + 1) - 0;
                                if (tmp != 0)
                                {
                                    if ((i == (DecimalLen - 1)) && (tmp == 1))
                                    {
                                        BahtText += "เอ็ด";
                                    } else
                                    if ((i == (DecimalLen - 2)) && (tmp == 2))
                                    {
                                        BahtText += "ยี่";
                                    } else
                                    if ((i == (DecimalLen - 2)) && (tmp == 1))
                                    {
                                        BahtText += "";
                                    } else
                                    {
                                        BahtText += TxtNumArr[tmp];
                                    }
                                    BahtText += TxtDigitArr[DecimalLen - i - 1];
                                }
                            }
                            BahtText += "สตางค์";
                        }
                        return BahtText;
                    }
                }
            }
        </script>
        <script>
            function printSubmit(amount) {
                document.getElementById("thaiBaht").value = ThaiBaht(amount);
//                document.getElementById("printForm").submit();
            }
        </script>
        <script>
            function subRow(qno, head) {
                var tr = document.getElementsByTagName('tr');

                for (var i = 0; i < tr.length; i++) {
                    if (tr[i].style.display === "") {
                        if (tr[i].id === qno) {
                            tr[i].style.display = "none";
                            document.getElementById(head).style.cssText = "cursor: pointer; background-color: #e5e5e5; color: black;";
                            document.getElementById(head.split("-")[0] + "-icon1-" + head.split("-")[1]).style.cssText = "font-size: 30px; color: #001384;";
                            document.getElementById(head.split("-")[0] + "-icon2-" + head.split("-")[1]).style.cssText = "font-size: 30px; color: #001384;";
                            document.getElementById(head.split("-")[0] + "-icon3-" + head.split("-")[1]).style.cssText = "font-size: 30px; color: #001384;";
                        }
                    } else {
                        if (tr[i].id === qno) {
                            tr[i].style.display = "";
                            document.getElementById(head).style.cssText = "cursor: pointer; background-color: #138ac6; color: white;";
                            document.getElementById(head.split("-")[0] + "-icon1-" + head.split("-")[1]).style.cssText = "font-size: 30px; color: white;";
                            document.getElementById(head.split("-")[0] + "-icon2-" + head.split("-")[1]).style.cssText = "font-size: 30px; color: white;";
                            document.getElementById(head.split("-")[0] + "-icon3-" + head.split("-")[1]).style.cssText = "font-size: 30px; color: white;";
                        }
                    }
                }
            }

            function expand() {
                document.getElementById("exp").style.display = "none";
                document.getElementById("com").style.display = "";
                var tr = document.getElementsByTagName('tr');

                for (var i = 0; i < tr.length; i++) {
                    if (tr[i].id.toString().split("-")[0] === "H") {
                        tr[i].style.cssText = "cursor: pointer; background-color: #138ac6; color: white;";

                        var icon = document.getElementsByTagName('i');
                        for (var j = 0; j < icon.length; j++) {
                            if (icon[j].id.toString().split("-")[0] === "H") {
                                icon[j].style.cssText = "font-size: 30px; color: white;";
                            }
                        }

                    } else if (tr[i].id.toString().split("-")[0] === "D") {
                        tr[i].style.display = "";
                    }
                }
            }

            function compress() {
                document.getElementById("exp").style.display = "";
                document.getElementById("com").style.display = "none";
                var tr = document.getElementsByTagName('tr');

                for (var i = 0; i < tr.length; i++) {
                    if (tr[i].id.toString().split("-")[0] === "H") {
                        tr[i].style.cssText = "cursor: pointer; background-color: #e5e5e5; color: black;";

                        var icon = document.getElementsByTagName('i');
                        for (var j = 0; j < icon.length; j++) {
                            if (icon[j].id.toString().split("-")[0] === "H") {
                                icon[j].style.cssText = "font-size: 30px; color: #001384;";
                            }
                        }

                    } else if (tr[i].id.toString().split("-")[0] === "D") {
                        tr[i].style.display = "none";
                    }
                }
            }
        </script>
        <style>
            .header {
                padding: 10px 16px;
                background: white;
                color: black;
                display: none;
            }

            .sticky {
                position: fixed;
                top: 0;
                width: 100%;
                display: block;

            }
        </style>

    </head>    
    <body>
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
    </div>
    </div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <form action="../resources/manual/IMS110.pdf" target="_blank">
                    <table width="100%">
                        <tr id="dont">
                            <td width="94%" align="left"></td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <a style=" width: 150px;" class="btn btn-primary" id="exp" name="exp" onclick="expand();">
                        <i class="fa fa-expand" style="font-size:20px;"></i>&nbsp;&nbsp; 
                        Expand All</a>
                    <a style=" width: 150px; display: none;" class="btn btn-warning" id="com" name="com" onclick="compress();">
                        <i class="fa fa-compress" style="font-size:20px;"></i>&nbsp;&nbsp; 
                        Collapse All</a>
                    <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                        <thead>
                            <tr>
                                <th style="text-align: left; width: 30px;">ADV IMP NO.</th>
                                <th style="text-align: left;">SHIPPING COMPANY</th>
                                <th style="text-align: left;">CHEQUE DATE</th>
                                <th style="text-align: left;">CHEQUE NO.</th>
                                <th style="text-align: left;">USER</th>
                                <th style="text-align: left;">BANK CODE</th>
                                <th style="text-align: left;">TOTAL AMOUNT</th>
                                <th style="text-align: center;">Option
                                    &nbsp;&nbsp;&nbsp;
                                    <a href="create"><i class="fa fa-plus-circle" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>
                                </th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <c:forEach items="${aheadList}" var="x">
                                <tr id="H-${x.advNo}" style="background-color: white;">
                                    <td onclick="subRow('D-${x.advNo}', 'H-${x.advNo}');" style="cursor: pointer;">${x.advNo}</td>
                                    <td onclick="subRow('D-${x.advNo}', 'H-${x.advNo}');" style="cursor: pointer;">${x.shipCode}</td>
                                    <td onclick="subRow('D-${x.advNo}', 'H-${x.advNo}');" style="cursor: pointer;">${x.checkDate}</td>
                                    <td onclick="subRow('D-${x.advNo}', 'H-${x.advNo}');" style="cursor: pointer;">${x.checkNo}</td>
                                    <td onclick="subRow('D-${x.advNo}', 'H-${x.advNo}');" style="cursor: pointer;">${x.user}</td>
                                    <td onclick="subRow('D-${x.advNo}', 'H-${x.advNo}');" style="cursor: pointer;">${x.bnkCode}</td>
                                    <td onclick="subRow('D-${x.advNo}', 'H-${x.advNo}');" style="cursor: pointer; text-align: right;">${x.amount}&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td style=" text-align: center; width: 200px;">
                                        <a href="edit?advImpNo=${x.advNo}"><i id="H-icon1-${x.advNo}" class="fa fa-pencil-square-o" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="Sdisplay?advImpNo=${x.advNo}"><i id="H-icon2-${x.advNo}" class="fa fa-television" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a style=" cursor: pointer;" onclick="var x = ThaiBaht('${x.amount}'); document.getElementById('thaiBaht${x.advNo}').value = x; document.getElementById('printForm${x.advNo}').submit();"><i id="H-icon3-${x.advNo}" class="fa fa-print" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>
                                        <form id="printForm${x.advNo}" action="print" method="post" target="_blank">
                                            <input type="hidden" id="advImpNo" name="advImpNo" value="${x.advNo}">
                                            <input type="hidden" id="thaiBaht${x.advNo}" name="thaiBaht">
                                        </form>
                                    </td>
                                </tr>
                                <c:forEach varStatus="ii" items="${x.detailList}" var="p">
                                    <c:if test="${ii.count==1}">
                                        <tr id="D-${x.advNo}" style="display: none; background-color: #eeeeee;">
                                            <td><b style=" opacity: 0;">${x.advNo}</b></td>
                                            <td>
                                                <table>
                                                    <tr style="background-color: #eeeeee;">
                                                        <td>
                                                            <b>No.</b>
                                                        </td>
                                                        <td>
                                                            <b>Pre-Imp No.</b>
                                                        </td>
                                                        <td>
                                                            <b>Invoice No. List</b>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td><b>Trans Date</b></td>
                                            <td><b>Tax Duty %</b></td>
                                            <td style=" text-align: right;"><b>Invoice Amount&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
                                            <td style=" text-align: right;"><b>Est Imp Amount&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
                                            <td style=" text-align: right;"><b>Adv Imp Amount&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
                                            <td></td>
                                        </tr>
                                    </c:if>
                                    <tr id="D-${x.advNo}" style="display: none; background-color: white;">
                                        <td><b style=" opacity: 0;">${x.advNo}</b></td>
                                        <td>
                                            <table>
                                                <tr style="background-color: white;">
                                                    <td width="25">
                                                        ${p.no}
                                                    </td>
                                                    <td width="80">
                                                        ${p.pImpNo}
                                                    </td>
                                                    <td>
                                                        ${p.invNoList}
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>${p.transDate}</td>
                                        <td>&nbsp;&nbsp;&nbsp;${p.taxDutyP} %</td>
                                        <td style=" text-align: right;">${p.invAmt}&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td style=" text-align: right;">${p.totAmt}&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td style=" text-align: right;">${p.advImpAmt}&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td></td>
                                    </tr>
                                </c:forEach>
                            </c:forEach>
                        </tbody>
                    </table>
                    <div class="header" id="myHeader">
                        <table style="width:100%; border-bottom: 1px solid black;">
                            <tr>
                                <th style="text-align: left; width: 120px;">ADV IMP NO.</th>
                                <th style="text-align: left; width: 350px;">SHIPPING COMPANY</th>
                                <th style="text-align: left; width: 180px;">CHEQUE DATE</th>
                                <th style="text-align: left; width: 180px;">CHEQUE NO.</th>
                                <th style="text-align: left; width: 200px;">USER</th>
                                <th style="text-align: left; width: 300px;">BANK CODE</th>
                                <th style="text-align: left; width: 120px;">TOTAL AMOUNT</th>
                                <th style="text-align: center;">Option
                                    &nbsp;&nbsp;&nbsp;
                                    <a href="create"><i class="fa fa-plus-circle" aria-hidden="true" style=" font-size: 30px; color: #001384;"></i></a>
                                </th>
                            </tr>
                        </table>
                    </div>
                    <script>
                        window.onscroll = function () {
                            myFunction();
                        };

                        var header = document.getElementById("myHeader");
                        var sticky = header.offsetTop;

                        function myFunction() {
                            if (window.pageYOffset > sticky) {
                                header.classList.add("sticky");
                            } else {
                                header.classList.remove("sticky");
                            }
                        }
                    </script>
                    <!--**********************-->
                    <!--End Part 3-->
                    <!--</div>  end #wrapper-top--> 
                </div> 
                <br>
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>