/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSIMCDao;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import com.twc.ims.dao.IMSRP770Dao;
import com.twc.ims.dao.IMSRP770Dao;
import com.twc.ims.entity.IMSRP770;
import com.twc.ims.entity.IMSSHC;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;

/**
 *
 * @author wien
 */
public class PrintControllerIMS770 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String FILE_PATH = "/report/";
    private static final int BUFSIZE = 4096;
    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public PrintControllerIMS770() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String ym = request.getParameter("ym");
        String ym2 = request.getParameter("ym2");
        String imc = request.getParameter("imc");
        String date = request.getParameter("date");
        String transport = request.getParameter("transport");

        IMSRP770 tsp = new IMSRP770Dao().getTransportByCode(transport);
        String transportName = tsp.getTransportName();

        IMSSHC imcOP = new IMSIMCDao().findByCode(imc);

//        ***************EXCEL*************************************
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Estimate Warehouse Date");

        Map<String, Object[]> data = new TreeMap<String, Object[]>();

        int ni = 1001;
        data.put(Integer.toString(ni++), new Object[]{"IMPORT COMPANY : " + imc + " (" + imcOP.getName() + ")", null, null, "DATE : " + date});
        data.put(Integer.toString(ni++), new Object[]{"TRANSACTION DATE : From " + ym + " To " + ym2, null, null, "TRANSPORTATION : " + transport + " : " + transportName});
        data.put(Integer.toString(ni++), new Object[]{""});
        data.put(Integer.toString(ni++), new Object[]{"", "", "", "", "", "", "", "", "", "COST CALCULATION"});
        data.put(Integer.toString(ni++), new Object[]{"PRE NO.", "IMPORT NO.", "VENDOR", "INVOICE", "PRODUCT", "EST %", "INCOTERM",
            "CCY", "FOREIGN AMTOUNT", "EXC. RATE", "AMOUNT", "BOX", "CARRIER", "TRANSPORTATION", "DATE IMPORT DOCUMENT",
            "DEPARTURE ESTIMATE DATE", "ARRIVAL ESTIMATE DATE", "B/L:HAWB", "NO.ENTRY", "ENTRY DATE", " L/G AMOUNT ",
            "IMP.DUTY", "DATE ARRIVED WAREHOUSE", "REMARK"});

        List<IMSRP770> rp770List = new IMSRP770Dao().findForPrint(ym, ym2, imc, date, transport);

        for (int i = 0; i < rp770List.size(); i++) {
            String oline = "";
            if (rp770List.get(i).getGRPIMPNO().trim().equals("1")) {
                oline = "+OLINE";
            }

            String uline = "";
            if (i == rp770List.size() - 1) {
                uline = "+ULINE";
            }
            data.put(Integer.toString(ni++), new Object[]{
                rp770List.get(i).getIMSRPPIMPNO() + oline + uline,
                rp770List.get(i).getIMSRPIMPNO() + oline + uline,
                rp770List.get(i).getIMSRPVENDEPT() + oline + uline,
                rp770List.get(i).getIMSRPINVNO() + oline + uline,
                rp770List.get(i).getIMSRPPROD() + oline + uline,
                rp770List.get(i).getIMSRPEST() + oline + uline,
                rp770List.get(i).getIMSRPINCOTERM() + oline + uline,
                rp770List.get(i).getIMSRPCCY() + oline + uline,
                rp770List.get(i).getIMSRPFAMT() + oline + uline,
                rp770List.get(i).getIMSRPRATE() + oline + uline,
                rp770List.get(i).getIMSRPAMT() + oline + uline,
                rp770List.get(i).getIMSRPBOX() + oline + uline,
                rp770List.get(i).getIMSRPCARRIER() + oline + uline,
                rp770List.get(i).getIMSRPTRANSPORT() + oline + uline,
                rp770List.get(i).getIMSRPINVDATE() + oline + uline,
                rp770List.get(i).getIMSRPDEPARTDATE() + oline + uline,
                rp770List.get(i).getIMSRPARRIVALDATE() + oline + uline,
                rp770List.get(i).getIMSRPBL() + oline + uline,
                rp770List.get(i).getIMSRPNOENTRY() + oline + uline,
                rp770List.get(i).getIMSRPENTRYDATE() + oline + uline,
                rp770List.get(i).getIMSRPLGAMT() + oline + uline,
                rp770List.get(i).getIMSRPIMPDUTY() + oline + uline,
                rp770List.get(i).getIMSRPDEBITNOTEDATE() + oline + uline,
                rp770List.get(i).getIMSRPREMARK() + oline + uline,});

        }

        data.put(Integer.toString(ni++), new Object[]{""});

        List<IMSRP770> rp770Total = new IMSRP770Dao().findForPrint2(ym, ym2, imc, date, transport);

        for (int i = 0; i < rp770Total.size(); i++) {
            String tot = "";
            if (i == 0) {
                tot = "TOTAL AMOUNT";
            }
            data.put(Integer.toString(ni++), new Object[]{
                "",
                "",
                "",
                tot,
                "",
                "",
                "",
                rp770Total.get(i).getIMSRPCCY(),
                rp770Total.get(i).getIMSRPFAMT(),
                "",
                rp770Total.get(i).getIMSRPAMT()
            });
        }

        //Set Column Width
        sheet.setColumnWidth(0, 2500);
        sheet.setColumnWidth(1, 4000);
        sheet.setColumnWidth(2, 12000);
        sheet.setColumnWidth(3, 5500);
        sheet.setColumnWidth(4, 2500);
        sheet.setColumnWidth(5, 2500);
        sheet.setColumnWidth(6, 2500);
        sheet.setColumnWidth(7, 2500);
        sheet.setColumnWidth(8, 5000);
        sheet.setColumnWidth(9, 4000);
        sheet.setColumnWidth(10, 4000);
        sheet.setColumnWidth(11, 2000);
        sheet.setColumnWidth(12, 4000);
        sheet.setColumnWidth(13, 7000);
        sheet.setColumnWidth(14, 7000);
        sheet.setColumnWidth(15, 7000);
        sheet.setColumnWidth(16, 7000);
        sheet.setColumnWidth(17, 7000);
        sheet.setColumnWidth(18, 7000);
        sheet.setColumnWidth(19, 4000);
        sheet.setColumnWidth(20, 4000);
        sheet.setColumnWidth(21, 4000);
        sheet.setColumnWidth(22, 7000);
        sheet.setColumnWidth(23, 7000);
//
        XSSFFont my_font = workbook.createFont();
//        my_font.setBold(true);
        my_font.setFontHeight(12);
//
        // Iterate over data and write to sheet
        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset) {
            // this creates a new row in the sheet
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                // this line creates a cell in the next column of that row
                Cell cell = row.createCell(cellnum++);
                cell.setCellValue((String) obj);

                if (rownum == 5) {
                    CellStyle style_tmp = workbook.createCellStyle();
                    style_tmp.setAlignment(HorizontalAlignment.CENTER);
                    style_tmp.setBorderBottom(BorderStyle.THICK);
                    style_tmp.setBorderTop(BorderStyle.THICK);
                    style_tmp.setBorderRight(BorderStyle.THIN);
                    style_tmp.setBorderLeft(BorderStyle.THIN);
                    cell.setCellStyle(style_tmp);
                } else if (rownum > 5) {
                    CellStyle style_tmp = workbook.createCellStyle();

                    if (rownum <= (rp770List.size() + 5)) {

                        if (cell.getStringCellValue().contains("+OLINE")) {
                            cell.setCellValue(cell.getStringCellValue().replace("+OLINE", ""));
//                        style_tmp.setAlignment(HorizontalAlignment.RIGHT);
//                        style_tmp.setBorderBottom(BorderStyle.THIN);
                            style_tmp.setBorderTop(BorderStyle.THIN);
//                            style_tmp.setBorderRight(BorderStyle.THIN);
//                            style_tmp.setBorderLeft(BorderStyle.THIN);
                        }

                        if (cell.getStringCellValue().contains("+ULINE")) {
                            cell.setCellValue(cell.getStringCellValue().replace("+ULINE", ""));
//                        style_tmp.setAlignment(HorizontalAlignment.RIGHT);
                            style_tmp.setBorderBottom(BorderStyle.THIN);
//                        style_tmp.setBorderTop(BorderStyle.THIN);
//                            style_tmp.setBorderRight(BorderStyle.THIN);
//                            style_tmp.setBorderLeft(BorderStyle.THIN);
                        }

                        style_tmp.setBorderRight(BorderStyle.THIN);
                        style_tmp.setBorderLeft(BorderStyle.THIN);

                    }

                    if (cellnum == 7 || cellnum == 8 || cellnum == 12 || cellnum == 14 || cellnum == 15 || cellnum == 16 || cellnum == 17 || cellnum == 20 || cellnum == 23 || cellnum == 24) {
                        style_tmp.setAlignment(HorizontalAlignment.CENTER);
                    }

                    if (cellnum == 6 || cellnum == 9 || cellnum == 10 || cellnum == 11 || cellnum == 21 || cellnum == 22) {
                        style_tmp.setAlignment(HorizontalAlignment.RIGHT);
                    }

                    style_tmp.setFont(my_font);

                    cell.setCellStyle(style_tmp);
                    cell.setCellValue(cell.getStringCellValue().replace("null", ""));

                }
            }
        }

        try {
            ServletContext servletContextEX = getServletContext();    // new by ji
            String realPathEX = servletContextEX.getRealPath(FILE_PATH);        // new by ji
            String saperateEX = realPathEX.contains(":") ? "\\" : "/";
            String pathEX = realPathEX + saperateEX + "ESTIMATE_WAREHOUSE_DATE_" + ym + "_" + ym2 + "_" + imc + ".xlsx";

            FileOutputStream outputStream = new FileOutputStream(pathEX);
            workbook.write(outputStream);
            workbook.close();

//            ******************************************************
            File fileEX = new File(pathEX);
            int length = 0;
            ServletOutputStream outStream = response.getOutputStream();
            ServletContext context = getServletConfig().getServletContext();
            String mimetype = context.getMimeType(pathEX);

            // sets response content type
            if (mimetype == null) {
                mimetype = "application/octet-stream";
            }
            response.setContentType(mimetype);
            response.setContentLength((int) fileEX.length());
            String fileNameEX = (new File(pathEX)).getName();

            // sets HTTP header
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileNameEX + "\"");

            byte[] byteBuffer = new byte[BUFSIZE];
            DataInputStream in = new DataInputStream(new FileInputStream(fileEX));

            // reads the file's bytes and writes them to the response stream
            while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                outStream.write(byteBuffer, 0, length);
            }

            in.close();
            outStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        response.setHeader("Refresh", "0;/IMS2/IMS770/edit?ym=" + ym + "&ym2=" + ym2 + "&imc=" + imc + "&date=" + date + "&transport=" + transport);
    }

}
