/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSIMCDao;
import com.twc.ims.dao.IMSRP750Dao;
import com.twc.ims.entity.IMSSHC;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class DisplayControllerIMS760 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/displayIMS760.jsp";

    public DisplayControllerIMS760() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "IMS760");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Summary Balance Advance Import Amount. Report");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);

        String y = Integer.toString(year);

        request.setAttribute("ym", y);

        IMSIMCDao dao = new IMSIMCDao();
        List<IMSSHC> imcList = dao.findAll();
        request.setAttribute("imcList", imcList);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
