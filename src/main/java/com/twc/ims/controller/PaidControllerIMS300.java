/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSADETAILDao;
import com.twc.ims.dao.IMSBAMDao;
import com.twc.ims.dao.IMSCURDao;
import com.twc.ims.dao.IMSIMCDao;
import com.twc.ims.dao.IMSPDETAILDao;
import com.twc.ims.dao.IMSPHEADDao;
import com.twc.ims.dao.IMSPVDISDao;
import com.twc.ims.dao.IMSSDETAILDao;
import com.twc.ims.dao.IMSSHCDao;
import com.twc.ims.dao.IMSSHEADDao;
import com.twc.ims.dao.IMSVENDao;
import com.twc.ims.dao.QNBSERDao;
import com.twc.ims.entity.IMSAPDETAIL;
import com.twc.ims.entity.IMSPDETAIL;
import com.twc.ims.entity.IMSPHEAD;
import com.twc.ims.entity.IMSPVDIS;
import com.twc.ims.entity.IMSSDETAIL;
import com.twc.ims.entity.IMSSHC;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class PaidControllerIMS300 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/paidIMS300.jsp";
    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public PaidControllerIMS300() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "IMS300/P");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Import Payment. Entry");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        request.setCharacterEncoding("utf-8");
        String impPaidNo = request.getParameter("impPaidNo");
        request.setAttribute("importPaidNo", impPaidNo);

        String ven = request.getParameter("ven");

        IMSBAMDao dao4 = new IMSBAMDao();
        List<IMSSHC> bankList = dao4.findAllPay();
        request.setAttribute("bankList", bankList);

        IMSCURDao dao5 = new IMSCURDao();
        List<IMSSHC> ccyList = dao5.findAll();
        request.setAttribute("ccyList", ccyList);

        IMSVENDao dao6 = new IMSVENDao();
        List<IMSSHC> vendorList = dao6.findAll();
        request.setAttribute("vendorList", vendorList);

        IMSIMCDao dao7 = new IMSIMCDao();
        List<IMSSHC> pbList = dao7.findAll();
        request.setAttribute("pbList", pbList);

        IMSPHEADDao daoH = new IMSPHEADDao();
        IMSPHEAD head = daoH.findByCode(impPaidNo);

        request.setAttribute("totalEstPaidAmount", head.getTotalEstPaidAmount());
        request.setAttribute("remainingAmount", head.getRemainingAmount());
        request.setAttribute("ccy", head.getCcy());
        request.setAttribute("fee", head.getFee());
        request.setAttribute("paidDate", head.getPaidDate());

        IMSVENDao dao1 = new IMSVENDao();
        IMSSHC Vendor = dao1.findByCode(head.getVendor());

        IMSBAMDao dao2 = new IMSBAMDao();
        IMSSHC Bank = dao2.findByCode(head.getBank());

        IMSIMCDao dao8 = new IMSIMCDao();
        IMSSHC pb = dao8.findByCode(head.getPaidBy());

        request.setAttribute("bankcode", Bank.getCode());
        request.setAttribute("bankname", Bank.getName());
        request.setAttribute("vendorcode", Vendor.getCode());
        request.setAttribute("vendorname", Vendor.getName());
        request.setAttribute("pbcode", pb.getCode());
        request.setAttribute("pbname", pb.getName());

        IMSPDETAILDao daoVEN = new IMSPDETAILDao();
        List<IMSSHC> venList = daoVEN.findAllVEN(impPaidNo);
        request.setAttribute("venList", venList);

        String vvcode = "";

        if (ven == null) {

            if (!venList.isEmpty()) {
                request.setAttribute("vcode", venList.get(0).getCode());
                request.setAttribute("vname", venList.get(0).getName());
                request.setAttribute("fee", venList.get(0).getNamet());

                vvcode = venList.get(0).getCode();
            }

        } else {
            vvcode = ven;
            IMSPDETAILDao daov = new IMSPDETAILDao();
            IMSSHC ve = daov.findVENby(impPaidNo, ven);

            request.setAttribute("vcode", ve.getCode());
            request.setAttribute("vname", ve.getName());
            request.setAttribute("fee", ve.getNamet());
        }

        IMSPDETAILDao dao3 = new IMSPDETAILDao();
        List<IMSPDETAIL> detList = dao3.findByCodePaid(impPaidNo, vvcode);
        request.setAttribute("detList", detList);

        List<IMSPVDIS> DISlist = new IMSPVDISDao().findByCode(impPaidNo);
        request.setAttribute("DISlist", DISlist);
        request.setAttribute("lineNo", DISlist.size());

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "IMS300/P");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Import Payment. Entry");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        request.setCharacterEncoding("utf-8");

        String remainingAmount = request.getParameter("remainingAmount");
        String importPaidNo = request.getParameter("importPaidNo");
        String PRT = request.getParameter("PRT");
        String fee = request.getParameter("fee");
        String sumFIA = request.getParameter("sumFIA");
        String[] selectCk = request.getParameterValues("selectCk");
        String[] ia = request.getParameterValues("ia");
        String[] fia = request.getParameterValues("fia");

//        for (int i = 0; i < ia.length; i++) {
//            System.out.println(ia[i]);
//        }
        if (fee.trim().equals("") || fee.trim().equals("NaN")) {
            fee = "0.00";
        }

        String[] bankYES = request.getParameterValues("bankYES");
        String[] vendorYES = request.getParameterValues("vendorYES");
        String[] paidDateYES = request.getParameterValues("paidDateYES");
        String[] INDB = request.getParameterValues("INDB");
        String[] importNoL = request.getParameterValues("importNoL");
        String[] invoiceNoL = request.getParameterValues("invoiceNoL");

//        System.out.println(selectCk.length);
//        System.out.println(ia.length);
//        System.out.println(fia.length);
//        System.out.println(importNoL.length);
        String paidDate = request.getParameter("paidDate");
        String bank = request.getParameter("bank");

        String userid = request.getParameter("userid");

        if (selectCk != null) {

            IMSPHEADDao daoup = new IMSPHEADDao();
            daoup.updateAmt(importPaidNo, sumFIA.replace(",", ""), remainingAmount.replace(",", ""), bank, paidDate, fee.replace(",", ""));
            for (String ii : selectCk) {
                int i = Integer.parseInt(ii.split("-")[0]);

                String bap = "";
                String pab = "";
                if (INDB[i].equals("YES")) {
                    bap = (bankYES[i].trim().equals("") ? bank : bankYES[i]);
                    pab = (paidDateYES[i].trim().equals("") ? paidDate : paidDateYES[i]);
                } else {
                    bap = (bank.trim().equals("") ? "null" : bank);
                    pab = (paidDate.trim().equals("") ? "null" : paidDate);
                }

                IMSPDETAILDao daoaddd = new IMSPDETAILDao();
                if (daoaddd.updateAmt(importPaidNo, ii.split("-")[1], ii.split("x")[1], ii.split("x")[2], PRT, bap, pab, fee.replace(",", ""), ii.split("-")[2].split("x")[0])) {
                    IMSSDETAILDao daosts = new IMSSDETAILDao();
                    daosts.setStatus("IMSSDSTS301", importNoL[i], invoiceNoL[i].trim(), "'Y'");
                }

            }

        }

        response.setHeader("Refresh", "0;/IMS2/IMS300/paid?impPaidNo=" + importPaidNo);
    }

}
