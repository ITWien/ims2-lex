/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import com.twc.ims.dao.IMSADETAILDao;
import com.twc.ims.dao.IMSAHEADDao;
import com.twc.ims.dao.IMSINSDao;
import com.twc.ims.dao.IMSPDETAILDao;
import com.twc.ims.entity.IMSAHEAD;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.util.HashMap;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import net.sf.jasperreports.engine.*;

/**
 *
 * @author wien
 */
public class Print2ControllerIMS300 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/pdisplayIMS300.jsp";
    private static final String FILE_DEST = "/report/";

    public Print2ControllerIMS300() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String impPaidNo = request.getParameter("impPaidNo");
        
        try {
            IMSPDETAILDao dao5 = new IMSPDETAILDao();
            ResultSet result = dao5.reportIMS300PV(impPaidNo);

            IMSPDETAILDao daoIMC = new IMSPDETAILDao();
            String imc = daoIMC.findIMC(impPaidNo);

            String jspath = "/resources/jasper/IMS300_PV.jasper";
            if (imc.trim().equals("R")) {
                jspath = "/resources/jasper/IMS300_PV_TORA.jasper";
            }

            File reportFile = new File(getServletConfig().getServletContext()
                    .getRealPath(jspath));
            byte[] bytes;

            HashMap<String, Object> map = new HashMap<String, Object>();
            try {
                bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), map, new JRResultSetDataSource(result));

                ServletContext servletContext2 = getServletContext();
                String realPath2 = servletContext2.getRealPath(FILE_DEST);
                String saperate2 = realPath2.contains(":") ? "\\" : "/";
                String path2 = realPath2 + saperate2 + "IMS300_PV.pdf";

                FileOutputStream out = new FileOutputStream(path2);
                out.write(bytes, 0, bytes.length);
            } catch (JRException e) {
                StringWriter stringWriter = new StringWriter();
                PrintWriter printWriter = new PrintWriter(stringWriter);
                e.printStackTrace(printWriter);

            }

        } catch (Exception e) {
            System.err.println(e.toString());
        }
//         * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
        try {
            IMSPDETAILDao dao5 = new IMSPDETAILDao();
            ResultSet result = dao5.reportIMS300JV(impPaidNo);

            IMSPDETAILDao daoIMC = new IMSPDETAILDao();
            String imc = daoIMC.findIMC(impPaidNo);

            String jspath = "/resources/jasper/IMS300_JV.jasper";
            if (imc.trim().equals("R")) {
                jspath = "/resources/jasper/IMS300_JV_TORA.jasper";
            }

            File reportFile = new File(getServletConfig().getServletContext()
                    .getRealPath(jspath));
            byte[] bytes;

            HashMap<String, Object> map = new HashMap<String, Object>();
            try {
                bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), map, new JRResultSetDataSource(result));

                ServletContext servletContext2 = getServletContext();
                String realPath2 = servletContext2.getRealPath(FILE_DEST);
                String saperate2 = realPath2.contains(":") ? "\\" : "/";
                String path2 = realPath2 + saperate2 + "IMS300_JV.pdf";

                FileOutputStream out = new FileOutputStream(path2);
                out.write(bytes, 0, bytes.length);
            } catch (JRException e) {
                StringWriter stringWriter = new StringWriter();
                PrintWriter printWriter = new PrintWriter(stringWriter);
                e.printStackTrace(printWriter);

            }

        } catch (Exception e) {
            System.err.println(e.toString());
        }
//         * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
        try {
            IMSPDETAILDao dao5 = new IMSPDETAILDao();
            ResultSet result = dao5.reportIMS300SP(impPaidNo);

            File reportFile = new File(getServletConfig().getServletContext()
                    .getRealPath("/resources/jasper/IMS300_SP.jasper"));
            byte[] bytes;
            try {
                bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), null, new JRResultSetDataSource(result));

                ServletContext servletContext2 = getServletContext();
                String realPath2 = servletContext2.getRealPath(FILE_DEST);
                String saperate2 = realPath2.contains(":") ? "\\" : "/";
                String path2 = realPath2 + saperate2 + "IMS300_SP.pdf";

                FileOutputStream out = new FileOutputStream(path2);
                out.write(bytes, 0, bytes.length);
            } catch (JRException e) {
                StringWriter stringWriter = new StringWriter();
                PrintWriter printWriter = new PrintWriter(stringWriter);
                e.printStackTrace(printWriter);

            }

        } catch (Exception e) {
            System.err.println(e.toString());
        }

        request.setAttribute("PROGRAMNAME", "IMS300/P");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Import Payment. Entry");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
