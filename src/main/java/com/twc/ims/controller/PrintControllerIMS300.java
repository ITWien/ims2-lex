/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSADETAILDao;
import com.twc.ims.dao.IMSAPDETAILDao;
import com.twc.ims.dao.IMSPDETAILDao;
import com.twc.ims.dao.IMSPHEADDao;
import com.twc.ims.dao.IMSSHCDao;
import com.twc.ims.dao.QNBSERDao;
import com.twc.ims.entity.IMSAPDETAIL;
import com.twc.ims.entity.IMSSHC;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperRunManager;

/**
 *
 * @author wien
 */
public class PrintControllerIMS300 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String FILE_DEST = "/report/";
    private static final String PAGE_VIEW = "../views/pdisplayIMS300_2.jsp";

    public PrintControllerIMS300() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("utf-8");
        
        System.out.println("print");

        String impPaidNo = request.getParameter("impPaidNo");

        try {
            IMSPDETAILDao dao5 = new IMSPDETAILDao();
            ResultSet result = dao5.reportIMS300(impPaidNo);

//            ServletOutputStream servletOutputStream = response.getOutputStream();
            File reportFile = new File(getServletConfig().getServletContext()
                    .getRealPath("/resources/jasper/IMS300.jasper"));
            byte[] bytes;

            HashMap<String, Object> map = new HashMap<String, Object>();
//            map.put("TRAN_DATE", tdbDate);

            try {
                bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), map, new JRResultSetDataSource(result));
                ServletContext servletContext2 = getServletContext();
                String realPath2 = servletContext2.getRealPath(FILE_DEST);
                String saperate2 = realPath2.contains(":") ? "\\" : "/";
                String path2 = realPath2 + saperate2 + "IMS300.pdf";

                FileOutputStream out = new FileOutputStream(path2);
                out.write(bytes, 0, bytes.length);
//                out.flush();
//                out.close();
            } catch (JRException e) {
                StringWriter stringWriter = new StringWriter();
                PrintWriter printWriter = new PrintWriter(stringWriter);
                e.printStackTrace(printWriter);
//                response.setContentType("text/plain");
//                response.getOutputStream().print(stringWriter.toString());

            }

        } catch (Exception e) {
            System.err.println(e.toString());
        }

        request.setAttribute("PROGRAMNAME", "IMS300/P");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Import Payment. Entry");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
