/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSIMCDao;
import com.twc.ims.dao.IMSRP740Dao;
import com.twc.ims.dao.IMSRP770Dao;
import com.twc.ims.entity.IMSRP740;
import com.twc.ims.entity.IMSRP770;
import com.twc.ims.entity.IMSSHC;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class EditControllerIMS770 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/editIMS770.jsp";

    public EditControllerIMS770() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "IMS770/E");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Estimate Warehouse Date. Process");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String ym = request.getParameter("ym");
        String ym2 = request.getParameter("ym2");
        String imc = request.getParameter("imc");
        String date = request.getParameter("date");
        String transport = request.getParameter("transport");

        String y = ym.substring(0, 4);
        String m = ym.substring(4);

        if (m.length() < 2) {
            m = "0" + m;
        }

        ym = y + m;

        String y2 = ym2.substring(0, 4);
        String m2 = ym2.substring(4);

        if (m2.length() < 2) {
            m2 = "0" + m2;
        }

        ym2 = y2 + m2;

        request.setAttribute("ym", ym);
        request.setAttribute("ym2", ym2);
        request.setAttribute("imc", imc);
        request.setAttribute("date", date);
        request.setAttribute("transport", transport);

        IMSIMCDao dao1 = new IMSIMCDao();
        IMSSHC imcOP = dao1.findByCode(imc);
        request.setAttribute("imcName", imcOP.getName());

        List<IMSRP770> rp770List = new IMSRP770Dao().findByCode(ym, ym2, imc, date, transport);
        request.setAttribute("rp770List", rp770List);

        IMSRP770 tsp = new IMSRP770Dao().getTransportByCode(transport);
        request.setAttribute("transportName", tsp.getTransportName());

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String ym = request.getParameter("ym");
        String ym2 = request.getParameter("ym2");
        String imc = request.getParameter("imc");
        String date = request.getParameter("date");
        String transport = request.getParameter("transport");

//        String[] ccy = request.getParameterValues("ccy");
//        String[] rate = request.getParameterValues("rate");
//        String[] totalImpAmtB = request.getParameterValues("totalImpAmtB");
//        String[] accInvAmt = request.getParameterValues("accInvAmt");
//        String[] noPaymentAmt = request.getParameterValues("noPaymentAmt");
//
//        for (int i = 0; i < ccy.length; i++) {
//            String rt = rate[i].trim().equals("") ? "null" : "'" + rate[i].trim() + "'";
//            String tiab = totalImpAmtB[i].trim().equals("") ? "null" : "'" + totalImpAmtB[i].trim() + "'";
//            String acc = accInvAmt[i].trim().equals("") ? "null" : "'" + accInvAmt[i].trim() + "'";
//            String nop = noPaymentAmt[i].trim().equals("") ? "null" : "'" + noPaymentAmt[i].trim() + "'";
//
//            IMSRP710Dao daoedt = new IMSRP710Dao();
//            daoedt.edit(ym, ccy[i], rt, tiab, acc, nop);
//
//        }
        response.setHeader("Refresh", "0;/IMS2/IMS770/edit?ym=" + ym + "&ym2=" + ym2 + "&imc=" + imc + "&date=" + date + "&transport=" + transport);
    }

}
