/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSADETAILDao;
import com.twc.ims.dao.IMSAHEADDao;
import com.twc.ims.dao.IMSBAMDao;
import com.twc.ims.dao.IMSSHCDao;
import com.twc.ims.dao.IMSSHEADDao;
import com.twc.ims.dao.QNBSERDao;
import com.twc.ims.entity.IMSAHEAD;
import com.twc.ims.entity.IMSSDETAIL;
import com.twc.ims.entity.IMSSHC;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class SDisplayControllerIMS110 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/sdisplayIMS110.jsp";
    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public SDisplayControllerIMS110() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "IMS110/D");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Advance Import Setting. Entry");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********     
        IMSSHCDao dao1 = new IMSSHCDao();
        List<IMSSHC> shipList = dao1.findAll();
        request.setAttribute("shipList", shipList);

        String advImpNo = request.getParameter("advImpNo");

        request.setAttribute("cur", advImpNo);

        IMSAHEADDao daoh = new IMSAHEADDao();
        IMSAHEAD head = daoh.findByCode(advImpNo);

        IMSSHCDao dao2 = new IMSSHCDao();
        IMSSHC ship = dao2.findByCode(head.getShipCode());

        request.setAttribute("shipcode", ship.getCode());
        request.setAttribute("shipname", ship.getName());
        request.setAttribute("shipnamet", ship.getNamet());
        request.setAttribute("dateF", head.getDateF());
        request.setAttribute("dateT", head.getDateT());
        request.setAttribute("checkDate", head.getCheckDate());

        IMSADETAILDao dao3 = new IMSADETAILDao();
        List<IMSSDETAIL> detList = dao3.findByCode(advImpNo);
        request.setAttribute("detList", detList);

        if (!detList.isEmpty()) {

            request.setAttribute("checkNo", head.getCheckNo());
            request.setAttribute("bnkCode", head.getBnkCode());

            IMSBAMDao dao6 = new IMSBAMDao();
            List<IMSSHC> bankList = dao6.findAll();

            IMSBAMDao dao5 = new IMSBAMDao();
            IMSSHC bank = dao5.findByCode(head.getBnkCode());

//            String opt = "<b>CHEQUE DATE</b>\n"
//                    + "<input type=\"date\" id=\"checkDateTmp\" name=\"checkDateTmp\" value=\"" + head.getCheckDate() + "\" onchange=\"document.getElementById('checkDate').value = this.value;\" style=\" height: 30px; width: 150px;\" disabled>";
            String totAmt = "<b>TOTAL AMOUNT</b>&nbsp;&nbsp;:&nbsp;&nbsp;<b id=\"finalTotalAmt\"></b>";

            StringBuilder cb = new StringBuilder();
            cb.append("<br>\n"
                    + "<b>CHEQUE DATE</b>\n"
                    + "<input type=\"date\" id=\"checkDateTmp\" name=\"checkDateTmp\" value=\"" + head.getCheckDate() + "\" onchange=\"document.getElementById('checkDate').value = this.value;\" style=\" height: 30px; width: 150px;\" disabled>"
                    + "                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n"
                    + "                            <b>CHEQUE NO.</b>\n"
                    + "                            <input type=\"text\" id=\"checkNotmp\" name=\"checkNotmp\" value=\"" + head.getCheckNo() + "\" onchange=\"document.getElementById('checkNo').value = this.value;\" style=\" height: 30px; width: 150px;\" disabled>\n"
                    + "                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n"
                    + "                            <b>BANK CODE</b>\n"
                    + "                            <select id=\"bnkCodetmp\" name=\"bnkCodetmp\" style=\" height: 30px; width: 450px;\" onchange=\"document.getElementById('bnkCode').value = this.value;\" disabled>\n"
                    + "                                <option value=\"" + bank.getCode() + "\" selected hidden>" + bank.getCode() + " : " + bank.getName() + " (" + bank.getNamet() + ")</option>");

            for (int i = 0; i < bankList.size(); i++) {
                cb.append("<option value=\"").append(bankList.get(i).getCode()).append("\">").append(bankList.get(i).getCode()).append(" : ").append(bankList.get(i).getName() == null ? "" : bankList.get(i).getName()).append(" (").append(bankList.get(i).getNamet() == null ? "" : bankList.get(i).getNamet()).append(")</option>");
            }
            cb.append("</select>");

//            request.setAttribute("detListSizeNotZero", opt); 
            request.setAttribute("totalAmt", totAmt);
            request.setAttribute("cb", cb);
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
