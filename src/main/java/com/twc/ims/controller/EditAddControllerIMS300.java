/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSADETAILDao;
import com.twc.ims.dao.IMSAHEADDao;
import com.twc.ims.dao.IMSAPDETAILDao;
import com.twc.ims.dao.IMSAPHEADDao;
import com.twc.ims.dao.IMSBAMDao;
import com.twc.ims.dao.IMSCURDao;
import com.twc.ims.dao.IMSPDETAILDao;
import com.twc.ims.dao.IMSPHEADDao;
import com.twc.ims.dao.IMSSDETAILDao;
import com.twc.ims.dao.IMSSHCDao;
import com.twc.ims.dao.IMSSHEADDao;
import com.twc.ims.dao.IMSVENDao;
import com.twc.ims.dao.QNBSERDao;
import com.twc.ims.entity.IMSAHEAD;
import com.twc.ims.entity.IMSPDETAIL;
import com.twc.ims.entity.IMSSDETAIL;
import com.twc.ims.entity.IMSSHC;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class EditAddControllerIMS300 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public EditAddControllerIMS300() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String totalEstPaidAmount = request.getParameter("totalEstPaidAmount");
        String ccy = request.getParameter("ccy");
        String fee = request.getParameter("fee");
        String paidRate = request.getParameter("paidRate");
        String paidDate = request.getParameter("paidDate");
        String bank = request.getParameter("bank");
        String vendor = request.getParameter("vendor");
        String vendel = request.getParameter("vendel");
        String remainingAmount = request.getParameter("remainingAmount");
        String importPaidNo = request.getParameter("importPaidNo");
        String sumIA = request.getParameter("sumIA");
        String sumFIA = request.getParameter("sumFIA");
        String[] selectCk = request.getParameterValues("selectCk");
        String[] selectCkDB = request.getParameterValues("selectCkDB");
        String[] importNoL = request.getParameterValues("importNoL");
        String[] invoiceNoL = request.getParameterValues("invoiceNoL");
        String[] ccyL = request.getParameterValues("ccyL");
        String[] productL = request.getParameterValues("productL");
        String[] oia = request.getParameterValues("oia");
        String[] ofia = request.getParameterValues("ofia");
        String[] bankYES = request.getParameterValues("bankYES");
        String[] vendorYES = request.getParameterValues("vendorYES");
        String[] paidDateYES = request.getParameterValues("paidDateYES");
        String[] INDB = request.getParameterValues("INDB");

        if (fee.trim().equals("") || fee.trim().equals("NaN")) {
            fee = "0.00";
        }

        String userid = request.getParameter("userid");

        IMSPHEADDao daoup = new IMSPHEADDao();
        daoup.update(paidDate.trim().equals("") ? "" : paidDate, bank.trim().equals("") ? "" : bank, vendor, totalEstPaidAmount.replace(",", ""), sumFIA.replace(",", ""), remainingAmount.replace(",", ""), importPaidNo, fee.replace(",", ""));

        IMSPDETAILDao daoADD = new IMSPDETAILDao();
        List<IMSPDETAIL> forAddList = daoADD.findForAdd(importPaidNo, vendel);

        IMSPDETAILDao daodel = new IMSPDETAILDao();
        if (daodel.delete(importPaidNo, vendel)) {
            IMSSDETAILDao daoclr = new IMSSDETAILDao();
            daoclr.clearPaidNo(importPaidNo);
        }

        int line = 1;

//        System.out.println(selectCk.length);
        if (selectCkDB == null && selectCk != null) {
            for (int i = 0; i < forAddList.size(); i++) {
                String detail = "'TWC', '" + forAddList.get(i).getImportNo() + "', '" + forAddList.get(i).getImp() + "', " + line + ", '" + forAddList.get(i).getInvoiceNo() + "', '" + forAddList.get(i).getVendor() + "', '" + forAddList.get(i).getCcy()
                        + "', '" + forAddList.get(i).getProduct() + "', " + forAddList.get(i).getInvAmount() + ", " + forAddList.get(i).getForeignInvAmount()
                        + ", null, null, null, '" + forAddList.get(i).getBank() + "', '" + forAddList.get(i).getPaidDate() + "', '" + forAddList.get(i).getSts() + "', '" + forAddList.get(i).getEdt() + "', '" + forAddList.get(i).getCdt() + "', '" + forAddList.get(i).getUser() + "', '" + forAddList.get(i).getFee() + "'";
//                System.out.println(detail);
                line++;

                IMSPDETAILDao daoaddd = new IMSPDETAILDao();
                if (daoaddd.add(detail)) {
                    IMSSDETAILDao daosts = new IMSSDETAILDao();
                    daosts.setStatus("IMSSDSTS300", forAddList.get(i).getImportNo(), forAddList.get(i).getInvoiceNo(), "'Y'");

                    IMSSDETAILDao daoedit = new IMSSDETAILDao();
                    daoedit.editPaidNo2(importPaidNo, forAddList.get(i).getImportNo(), forAddList.get(i).getInvoiceNo());
                }
            }
        } else {
            if (selectCkDB != null) {
                for (int i = 0; i < forAddList.size(); i++) {
                    for (int j = 0; j < selectCkDB.length; j++) {
                        if (forAddList.get(i).getImportNo().trim().equals(selectCkDB[j].split("-")[1].trim())) {
                            if (forAddList.get(i).getImp().trim().equals(selectCkDB[j].split("-")[2].trim())) {
                                if (forAddList.get(i).getLine().trim().equals(selectCkDB[j].split("-")[3].trim())) {
                                    String detail = "'TWC', '" + forAddList.get(i).getImportNo() + "', '" + forAddList.get(i).getImp() + "', " + line + ", '" + forAddList.get(i).getInvoiceNo() + "', '" + forAddList.get(i).getVendor() + "', '" + forAddList.get(i).getCcy()
                                            + "', '" + forAddList.get(i).getProduct() + "', " + forAddList.get(i).getInvAmount() + ", " + forAddList.get(i).getForeignInvAmount()
                                            + ", null, null, null, '" + forAddList.get(i).getBank() + "', '" + forAddList.get(i).getPaidDate() + "', '" + forAddList.get(i).getSts() + "', '" + forAddList.get(i).getEdt() + "', '" + forAddList.get(i).getCdt() + "', '" + forAddList.get(i).getUser() + "', '" + forAddList.get(i).getFee() + "'";
//                System.out.println(detail);
                                    line++;

                                    IMSPDETAILDao daoaddd = new IMSPDETAILDao();
                                    if (daoaddd.add(detail)) {
                                        IMSSDETAILDao daosts = new IMSSDETAILDao();
                                        daosts.setStatus("IMSSDSTS300", forAddList.get(i).getImportNo(), forAddList.get(i).getInvoiceNo(), "'Y'");

                                        IMSSDETAILDao daoedit = new IMSSDETAILDao();
                                        daoedit.editPaidNo2(importPaidNo, forAddList.get(i).getImportNo(), forAddList.get(i).getInvoiceNo());
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if (selectCk != null) {

            for (String ii : selectCk) {
//                if (ii.split("-").length == 3) {
//                    IMSSDETAILDao daoedit = new IMSSDETAILDao();
//                    daoedit.editPaidNo(importPaidNo, ii.split("-")[1], ii.split("-")[2]);
//                }

                ii = ii.split("-")[0];
                int i = Integer.parseInt(ii);
                String bap = "";
                String pab = "";
                String vy = "";
                if (INDB[i].equals("YES")) {
                    bap = (bankYES[i].trim().equals("") ? "'" + bank + "'" : "'" + bankYES[i] + "'");
                    vy = (vendorYES[i].trim().equals("") ? "'" + vendor + "'" : "'" + vendorYES[i] + "'");
                    pab = (paidDateYES[i].trim().equals("") ? "'" + paidDate + "'" : "'" + paidDateYES[i] + "'");
                } else {
                    bap = (bank.trim().equals("") ? "null" : "'" + bank + "'");
                    vy = (vendor.trim().equals("") ? "null" : "'" + vendor + "'");
                    pab = (paidDate.trim().equals("") ? "null" : "'" + paidDate + "'");
                }
                String detail = "'TWC', '" + importNoL[i] + "', '" + importPaidNo + "', " + line + ", '" + invoiceNoL[i] + "', " + vy + ", '" + ccyL[i]
                        + "', '" + productL[i] + "', " + oia[i].replace(",", "") + ", " + ofia[i].replace(",", "")
                        + ", null, null, null, " + bap + ", " + pab + ", 'S', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '" + userid + "', '" + fee.replace(",", "") + "'";
//                System.out.println(detail);
                line++;

//                IMSPDETAILDao daock = new IMSPDETAILDao(); 
//                if (daock.check(importNoL[i], importPaidNo, invoiceNoL[i].trim()).equals("t")) {
                IMSPDETAILDao daoaddd = new IMSPDETAILDao();
                if (daoaddd.add(detail)) {
                    IMSSDETAILDao daosts = new IMSSDETAILDao();
                    daosts.setStatus("IMSSDSTS300", importNoL[i], invoiceNoL[i].trim(), "'Y'");

                    IMSSDETAILDao daoedit = new IMSSDETAILDao();
                    daoedit.editPaidNo2(importPaidNo, importNoL[i], invoiceNoL[i].trim());
                }
//                }
            }

        }

        response.setHeader("Refresh", "0;/IMS2/IMS300/edit?impPaidNo=" + importPaidNo);
    }

}
