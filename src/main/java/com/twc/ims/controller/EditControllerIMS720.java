/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSIMCDao;
import com.twc.ims.dao.IMSRP720Dao;
import com.twc.ims.entity.IMSRP720;
import com.twc.ims.entity.IMSSHC;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class EditControllerIMS720 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/editIMS720.jsp";

    public EditControllerIMS720() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "IMS720/E");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Summary Expense Report. Process");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String ym = request.getParameter("ym");
        String imc = request.getParameter("imc");
        request.setAttribute("imc", imc);

        String pym = "";
        String y = ym.substring(0, 4);
        String m = ym.substring(4);

        request.setAttribute("ym", y);

        if (m.length() < 2) {
            m = "0" + m;
        }

        ym = y + m;

        int yy = Integer.parseInt(y);
        int mm = Integer.parseInt(m);

        if (mm == 1) {
            yy--;
            mm = 12;
        } else {
            mm--;
        }

        y = Integer.toString(yy);
        m = Integer.toString(mm);

        if (m.length() < 2) {
            m = "0" + m;
        }

        pym = y + "/" + m;
        request.setAttribute("pym", pym);

        IMSIMCDao dao1 = new IMSIMCDao();
        IMSSHC imcOP = dao1.findByCode(imc);
        request.setAttribute("imcName", imcOP.getName());

        IMSRP720Dao daofind = new IMSRP720Dao();
        List<IMSRP720> rp720List = daofind.findByCode(y, imc);

        request.setAttribute("rp720List", rp720List);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String ym = request.getParameter("ym");
        String imc = request.getParameter("imc");

//        String[] ccy = request.getParameterValues("ccy");
//        String[] rate = request.getParameterValues("rate");
//        String[] totalImpAmtB = request.getParameterValues("totalImpAmtB");
//        String[] accInvAmt = request.getParameterValues("accInvAmt");
//        String[] noPaymentAmt = request.getParameterValues("noPaymentAmt");
//
//        for (int i = 0; i < ccy.length; i++) {
//            String rt = rate[i].trim().equals("") ? "null" : "'" + rate[i].trim() + "'";
//            String tiab = totalImpAmtB[i].trim().equals("") ? "null" : "'" + totalImpAmtB[i].trim() + "'";
//            String acc = accInvAmt[i].trim().equals("") ? "null" : "'" + accInvAmt[i].trim() + "'";
//            String nop = noPaymentAmt[i].trim().equals("") ? "null" : "'" + noPaymentAmt[i].trim() + "'";
//
//            IMSRP710Dao daoedt = new IMSRP710Dao();
//            daoedt.edit(ym, ccy[i], rt, tiab, acc, nop);
//
//        }
        response.setHeader("Refresh", "0;/IMS2/IMS720/edit?ym=" + ym + "&imc=" + imc);
    }

}
