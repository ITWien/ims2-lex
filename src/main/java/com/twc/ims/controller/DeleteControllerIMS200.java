/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSADETAILDao;
import com.twc.ims.dao.IMSAPDETAILDao;
import com.twc.ims.dao.IMSSDETAILDao;
import com.twc.ims.dao.IMSSHCDao;
import com.twc.ims.dao.IMSSHEADDao;
import com.twc.ims.dao.QNBSERDao;
import com.twc.ims.entity.IMSAPDETAIL;
import com.twc.ims.entity.IMSSHC;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class DeleteControllerIMS200 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/editIMS200.jsp";
    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public DeleteControllerIMS200() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String[] id = request.getParameterValues("selectCk");

        String advPaidNo = request.getParameter("advPaidNo");
        String checkNo = request.getParameter("checkNo");
        String allCheckNo = request.getParameter("allCheckNo");
        String shipcom = request.getParameter("shipCom");
        List<String> toDeleteList = new ArrayList<String>();
        List<String> no = new ArrayList<String>();

        if (id != null) {
            for (int i = 0; i < id.length; i++) {
                no.add(id[i].split("\\+")[0]);
//                id[i] = id[i].replace(id[i].split("\\+")[0] + "+", "");

            }
        }
        if (id != null) {
            for (int i = 0; i < id.length; i++) {
//                allCheckNo = allCheckNo.replace(",'" + id[i] + "'", "");

                if (id[i].contains("INDB")) {
                    String tdl = "";
                    for (int j = 0; j < id[i].split("-").length; j++) {
                        if (j != 0) {
                            tdl += id[i].split("-")[j];
                            if (j != (id[i].split("-").length - 1)) {
                                tdl += "-";
                            }
                        }
                    }
                    toDeleteList.add(tdl + "|" + no.get(i));

                    IMSADETAILDao daopimno = new IMSADETAILDao();
                    List<IMSAPDETAIL> use = daopimno.findByCQNO(tdl, no.get(i));

                    for (int j = 0; j < use.size(); j++) {
                        if (use.get(j).getInvNoList() != null) {
                            String[] invL = use.get(j).getInvNoList().split(",");
                            for (String inv : invL) {
                                IMSSDETAILDao daosts = new IMSSDETAILDao();
                                daosts.setStatus("IMSSDSTS200", use.get(j).getPimno(), inv.trim(), "null");
                            }
                        }
                    }
                }
            }
        }

        for (int i = 0; i < toDeleteList.size(); i++) {

            IMSAPDETAILDao daodel = new IMSAPDETAILDao();
            if (daodel.delete(advPaidNo, toDeleteList.get(i).split("\\|")[0], toDeleteList.get(i).split("\\|")[1])) {
                IMSSHEADDao daohd = new IMSSHEADDao();
                daohd.updateAdvIMS200RETURN(advPaidNo);
            }
        }
        request.setAttribute("PROGRAMNAME", "IMS200/E");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Advance Import Payment. Entry");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        request.setAttribute("cur", advPaidNo);

        IMSSHCDao dao1 = new IMSSHCDao();
        List<IMSSHC> shipList = dao1.findAll();
        request.setAttribute("shipList", shipList);

        IMSSHCDao dao2 = new IMSSHCDao();
        IMSSHC ship = dao2.findByCode(shipcom);

        request.setAttribute("shipcode", ship.getCode());
        request.setAttribute("shipname", ship.getName());
        request.setAttribute("shipnamet", ship.getNamet());
        request.setAttribute("checkNo", checkNo);
        request.setAttribute("allCheckNo", allCheckNo);

        IMSAPDETAILDao dao3 = new IMSAPDETAILDao();
        List<IMSAPDETAIL> detList = dao3.findByCode(advPaidNo);
        request.setAttribute("detList", detList);

        if (!allCheckNo.trim().equals("")) {
            IMSAPDETAILDao daoMax = new IMSAPDETAILDao();
            int line = daoMax.findNextLine(advPaidNo);

            IMSADETAILDao dao4 = new IMSADETAILDao();
            List<IMSAPDETAIL> detList2 = dao4.findAll(line, allCheckNo.substring(1), shipcom, id, null);
            request.setAttribute("detList2", detList2);

            if (!detList.isEmpty() || !detList2.isEmpty()) {
                double sumadv = 0;
                double sumestAdv = 0;
                double sumbillAmt = 0;
                double sumtax3 = 0;
                double sumtax1 = 0;
                double sumamount = 0;
                double sumtotAdv = 0;
                double sumbalance = 0;

                for (int i = 0; i < detList.size(); i++) {
                    String sa = "0";
                    if (detList.get(i).getAdvance() != null) {
                        sa = detList.get(i).getAdvance().replace(",", "");
                    }

                    sumadv += Double.parseDouble(sa);
                    sumestAdv += Double.parseDouble(detList.get(i).getEstAdv().replace(",", ""));
                    sumbillAmt += Double.parseDouble(detList.get(i).getBillAmt().replace(",", ""));
                    sumtax3 += Double.parseDouble(detList.get(i).getTax3().replace(",", ""));
                    sumtax1 += Double.parseDouble(detList.get(i).getTax1().replace(",", ""));
                    sumamount += Double.parseDouble(detList.get(i).getAmount().replace(",", ""));
                    sumtotAdv += Double.parseDouble(detList.get(i).getTotAdv().replace(",", ""));
                    sumbalance += Double.parseDouble(detList.get(i).getBalance().replace(",", "").replace("(", "").replace(")", ""));
                }

                for (int i = 0; i < detList2.size(); i++) {
                    String sa = "0";
                    if (detList2.get(i).getAdvance() != null) {
                        sa = detList2.get(i).getAdvance().replace(",", "");
                    }

                    sumadv += Double.parseDouble(sa);
                    sumestAdv += Double.parseDouble(detList2.get(i).getEstAdv().replace(",", ""));
                    sumbillAmt += Double.parseDouble(detList2.get(i).getBillAmt().replace(",", ""));
                    sumtax3 += Double.parseDouble(detList2.get(i).getTax3().replace(",", ""));
                    sumtax1 += Double.parseDouble(detList2.get(i).getTax1().replace(",", ""));
                    sumamount += Double.parseDouble(detList2.get(i).getAmount().replace(",", ""));
                    sumtotAdv += Double.parseDouble(detList2.get(i).getTotAdv().replace(",", ""));
                    sumbalance += Double.parseDouble(detList2.get(i).getBalance().replace(",", "").replace("(", "").replace(")", ""));
                }

                request.setAttribute("grt", "Grand Total");
                request.setAttribute("sumadv", formatDou.format(sumadv));
                request.setAttribute("sumestAdv", formatDou.format(sumestAdv));
                request.setAttribute("sumbillAmt", formatDou.format(sumbillAmt));
                request.setAttribute("sumtax3", formatDou.format(sumtax3));
                request.setAttribute("sumtax1", formatDou.format(sumtax1));
                request.setAttribute("sumamount", formatDou.format(sumamount));
                request.setAttribute("sumtotAdv", formatDou.format(sumtotAdv));
                request.setAttribute("sumbalance", "(" + formatDou.format(sumbalance) + ")");

                String save = "<br><br><a class=\"btn btn-success\" onclick=\"document.getElementById('myModal-3').style.display = 'block';\"><i class=\"fa fa-save\" style=\"font-size:20px;\"></i>&nbsp;&nbsp;Save</a>";
                request.setAttribute("save", save);
            }
        } else {
            if (!detList.isEmpty()) {
                double sumadv = 0;
                double sumestAdv = 0;
                double sumbillAmt = 0;
                double sumtax3 = 0;
                double sumtax1 = 0;
                double sumamount = 0;
                double sumtotAdv = 0;
                double sumbalance = 0;

                for (int i = 0; i < detList.size(); i++) {
                    String sa = "0";
                    if (detList.get(i).getAdvance() != null) {
                        sa = detList.get(i).getAdvance().replace(",", "");
                    }

                    sumadv += Double.parseDouble(sa);
                    sumestAdv += Double.parseDouble(detList.get(i).getEstAdv().replace(",", ""));
                    sumbillAmt += Double.parseDouble(detList.get(i).getBillAmt().replace(",", ""));
                    sumtax3 += Double.parseDouble(detList.get(i).getTax3().replace(",", ""));
                    sumtax1 += Double.parseDouble(detList.get(i).getTax1().replace(",", ""));
                    sumamount += Double.parseDouble(detList.get(i).getAmount().replace(",", ""));
                    sumtotAdv += Double.parseDouble(detList.get(i).getTotAdv().replace(",", ""));
                    sumbalance += Double.parseDouble(detList.get(i).getBalance().replace(",", "").replace("(", "").replace(")", ""));
                }

                request.setAttribute("grt", "Grand Total");
                request.setAttribute("sumadv", formatDou.format(sumadv));
                request.setAttribute("sumestAdv", formatDou.format(sumestAdv));
                request.setAttribute("sumbillAmt", formatDou.format(sumbillAmt));
                request.setAttribute("sumtax3", formatDou.format(sumtax3));
                request.setAttribute("sumtax1", formatDou.format(sumtax1));
                request.setAttribute("sumamount", formatDou.format(sumamount));
                request.setAttribute("sumtotAdv", formatDou.format(sumtotAdv));
                request.setAttribute("sumbalance", "(" + formatDou.format(sumbalance) + ")");

                String save = "<br><br><a class=\"btn btn-success\" onclick=\"document.getElementById('myModal-3').style.display = 'block';\"><i class=\"fa fa-save\" style=\"font-size:20px;\"></i>&nbsp;&nbsp;Save</a>";
                request.setAttribute("save", save);
            }
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

}
