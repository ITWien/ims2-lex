/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import com.twc.ims.dao.IMSRP740Dao;
import com.twc.ims.entity.IMSRP740;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;

/**
 *
 * @author wien
 */
public class PrintControllerIMS740 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String FILE_PATH = "/report/";
    private static final int BUFSIZE = 4096;
    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public PrintControllerIMS740() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String ym = request.getParameter("ym");
        String imc = request.getParameter("imc");

//        ***************EXCEL*************************************
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Import Performance Monthly");

        Map<String, Object[]> data = new TreeMap<String, Object[]>();

        String y = ym.substring(0, 4);
        String m = ym.substring(4);

        int yy = Integer.parseInt(y);
        int mm = Integer.parseInt(m);

        String month = "";
        if (mm == 1) {
            month = "���Ҥ�";
        } else if (mm == 2) {
            month = "����Ҿѹ��";
        } else if (mm == 3) {
            month = "�չҤ�";
        } else if (mm == 4) {
            month = "����¹";
        } else if (mm == 5) {
            month = "����Ҥ�";
        } else if (mm == 6) {
            month = "�Զع�¹";
        } else if (mm == 7) {
            month = "�á�Ҥ�";
        } else if (mm == 8) {
            month = "�ԧ�Ҥ�";
        } else if (mm == 9) {
            month = "�ѹ��¹";
        } else if (mm == 10) {
            month = "���Ҥ�";
        } else if (mm == 11) {
            month = "��Ȩԡ�¹";
        } else if (mm == 12) {
            month = "�ѹ�Ҥ�";
        }

        String head = "����Է���Ҿ��ù�����ѵ�شԺ ��Ш���͹ " + month + " �� " + (yy + 543);

        int ni = 101;
        data.put(Integer.toString(ni++), new Object[]{head});
        data.put(Integer.toString(ni++), new Object[]{""});
        data.put(Integer.toString(ni++), new Object[]{"IMPORT NO.", "���ͺ���ѷ�������", "", "INVOICE", "PRODUCT", "BY",
            "�ѹ������Ѻ\n�͡���", "�ѹ����ѵ�شԺ�֧\n�������/AIR PORT", "�ѹ����ѵ�شԺ�֧\nʵ�͡", "�������ҷ��\n����� (�ӹǹ�ѹ)",
            "��ҹ", "����ҹ", "%", "�ѭ��/���˵�", "�Ƿҧ������"});

        String year = ym.substring(0, 4);

        IMSRP740Dao daofind = new IMSRP740Dao();
        List<IMSRP740> rp740List = daofind.findForPrint(ym, imc);

//        data.put(Integer.toString(ni++), new Object[]{""});
        for (int i = 0; i < rp740List.size(); i++) {
            if (i == rp740List.size() - 1) {
                data.put(Integer.toString(ni++), new Object[]{rp740List.get(i).getImpNo() + "+ULINE", rp740List.get(i).getVenDept() + "+ULINE",
                    rp740List.get(i).getVenDeptName() + "+ULINE", rp740List.get(i).getInvNo() + "+ULINE", rp740List.get(i).getProd() + "+ULINE",
                    rp740List.get(i).getTransport() + "+ULINE", rp740List.get(i).getInvDate() + "+ULINE", rp740List.get(i).getArrDate() + "+ULINE",
                    rp740List.get(i).getDebitDate() + "+ULINE", "+ULINE", "+ULINE", "+ULINE", "+ULINE", "+ULINE", "+ULINE"});
            } else {
                if (!rp740List.get(i).getImpNo().equals(rp740List.get(i + 1).getImpNo())) {
                    data.put(Integer.toString(ni++), new Object[]{rp740List.get(i).getImpNo() + "+ULINE", rp740List.get(i).getVenDept() + "+ULINE",
                        rp740List.get(i).getVenDeptName() + "+ULINE", rp740List.get(i).getInvNo() + "+ULINE", rp740List.get(i).getProd() + "+ULINE",
                        rp740List.get(i).getTransport() + "+ULINE", rp740List.get(i).getInvDate() + "+ULINE", rp740List.get(i).getArrDate() + "+ULINE",
                        rp740List.get(i).getDebitDate() + "+ULINE", "+ULINE", "+ULINE", "+ULINE", "+ULINE", "+ULINE", "+ULINE"});
                } else {
                    data.put(Integer.toString(ni++), new Object[]{rp740List.get(i).getImpNo(), rp740List.get(i).getVenDept(),
                        rp740List.get(i).getVenDeptName(), rp740List.get(i).getInvNo(), rp740List.get(i).getProd(),
                        rp740List.get(i).getTransport(), rp740List.get(i).getInvDate(), rp740List.get(i).getArrDate(),
                        rp740List.get(i).getDebitDate(), "", "", "", "", "", ""});
                }
            }
        }

        data.put(Integer.toString(ni++), new Object[]{""});
        data.put(Integer.toString(ni++), new Object[]{"����¡�ù����/��͹", "", "", "", "", "", "", "", "", "",
            "", "", "",
            "", ""});

        data.put(Integer.toString(ni++), new Object[]{""});
        data.put(Integer.toString(ni++), new Object[]{"", "", "", "", "", "", "", "", "", "",
            "", "���ѹ�֡", "", "", "����Ǩ�ͺ"});
        data.put(Integer.toString(ni++), new Object[]{""});
        data.put(Integer.toString(ni++), new Object[]{"", "", "", "", "", "", "", "", "", "",
            "", "", "", "", ""});

        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 14));
//        sheet.addMergedRegion(new CellRangeAddress(2, 3, 0, 0));
        sheet.addMergedRegion(new CellRangeAddress(2, 2, 1, 2));
//        sheet.addMergedRegion(new CellRangeAddress(2, 3, 3, 3));
//        sheet.addMergedRegion(new CellRangeAddress(2, 3, 4, 4));
//        sheet.addMergedRegion(new CellRangeAddress(2, 3, 5, 5));
//        sheet.addMergedRegion(new CellRangeAddress(2, 3, 6, 6));
//        sheet.addMergedRegion(new CellRangeAddress(2, 3, 7, 7));
//        sheet.addMergedRegion(new CellRangeAddress(2, 3, 8, 8));
//        sheet.addMergedRegion(new CellRangeAddress(2, 3, 9, 9));
//        sheet.addMergedRegion(new CellRangeAddress(2, 3, 10, 10));
//        sheet.addMergedRegion(new CellRangeAddress(2, 3, 11, 11));
//        sheet.addMergedRegion(new CellRangeAddress(2, 3, 12, 12));
//        sheet.addMergedRegion(new CellRangeAddress(2, 3, 13, 13));
//        sheet.addMergedRegion(new CellRangeAddress(2, 3, 14, 14));
        sheet.addMergedRegion(new CellRangeAddress(rp740List.size() + 8, rp740List.size() + 8, 10, 12));

        //Set Column Width
        sheet.setColumnWidth(0, 5000);
        sheet.setColumnWidth(1, 3000);
        sheet.setColumnWidth(2, 9000);
        sheet.setColumnWidth(3, 5500);
        sheet.setColumnWidth(4, 2500);
        sheet.setColumnWidth(5, 3500);
        sheet.setColumnWidth(6, 5000);
        sheet.setColumnWidth(7, 7000);
        sheet.setColumnWidth(8, 5000);
        sheet.setColumnWidth(9, 7000);
        sheet.setColumnWidth(10, 2000);
        sheet.setColumnWidth(11, 2000);
        sheet.setColumnWidth(12, 2000);
        sheet.setColumnWidth(13, 8000);
        sheet.setColumnWidth(14, 8000);

        XSSFFont my_font = workbook.createFont();

        my_font.setBold(true);

        CellStyle styleF = workbook.createCellStyle();

        styleF.setAlignment(HorizontalAlignment.CENTER);

        styleF.setBorderRight(BorderStyle.THIN);

        styleF.setBorderLeft(BorderStyle.THIN);
//        styleF.setFillBackgroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
//        styleF.setFillPattern(FillPatternType.LESS_DOTS);

        CellStyle style = workbook.createCellStyle();

        style.setAlignment(HorizontalAlignment.CENTER);

        style.setBorderBottom(BorderStyle.THICK);

        style.setBorderRight(BorderStyle.THIN);

        style.setBorderLeft(BorderStyle.THIN);
//        style.setFillBackgroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
//        style.setFillPattern(FillPatternType.BIG_SPOTS);

        // Styling border and alignment of cell.  
        CellStyle style_ct2 = workbook.createCellStyle();

        style_ct2.setAlignment(HorizontalAlignment.CENTER);
        style_ct2.setVerticalAlignment(VerticalAlignment.CENTER);
        style_ct2.setBorderBottom(BorderStyle.THIN);
        style_ct2.setBorderTop(BorderStyle.THIN);
        style_ct2.setBorderRight(BorderStyle.THIN);
        style_ct2.setBorderLeft(BorderStyle.THIN);
        style_ct2.setFont(my_font);

        // Styling border and alignment of cell.  
        CellStyle style_ct = workbook.createCellStyle();

        style_ct.setAlignment(HorizontalAlignment.CENTER);
        style_ct.setVerticalAlignment(VerticalAlignment.CENTER);
        style_ct.setFont(my_font);

        CellStyle style_ct_FF = workbook.createCellStyle();
        style_ct_FF.setAlignment(HorizontalAlignment.CENTER);
        style_ct_FF.setFont(my_font);

        // Styling border and alignment of cell.
        CellStyle style_rt = workbook.createCellStyle();

        style_rt.setAlignment(HorizontalAlignment.RIGHT);

        // Styling border and alignment of cell.  
        CellStyle style_bd = workbook.createCellStyle();

        style_bd.setBorderBottom(BorderStyle.THIN);

        style_bd.setBorderTop(BorderStyle.THIN);

        style_bd.setBorderRight(BorderStyle.THIN);

        style_bd.setBorderLeft(BorderStyle.THIN);

        // Styling border and alignment of cell.  
        CellStyle style_bd_u = workbook.createCellStyle();

        style_bd_u.setBorderBottom(BorderStyle.THICK);

        style_bd_u.setBorderTop(BorderStyle.THIN);

        style_bd_u.setBorderRight(BorderStyle.THIN);

        style_bd_u.setBorderLeft(BorderStyle.THIN);

        // Styling border and alignment of cell.  
        CellStyle style_bd_ct = workbook.createCellStyle();

        style_bd_ct.setAlignment(HorizontalAlignment.CENTER);

        style_bd_ct.setBorderBottom(BorderStyle.THIN);

        style_bd_ct.setBorderTop(BorderStyle.THIN);

        style_bd_ct.setBorderRight(BorderStyle.THIN);

        style_bd_ct.setBorderLeft(BorderStyle.THIN);

        // Styling border and alignment of cell.  
        CellStyle style_bd_rt = workbook.createCellStyle();

        style_bd_rt.setAlignment(HorizontalAlignment.RIGHT);

        style_bd_rt.setBorderBottom(BorderStyle.THIN);

        style_bd_rt.setBorderTop(BorderStyle.THIN);

        style_bd_rt.setBorderRight(BorderStyle.THIN);

        style_bd_rt.setBorderLeft(BorderStyle.THIN);

        // Styling border and alignment of cell.  
        CellStyle style_bd_rt_u = workbook.createCellStyle();

        style_bd_rt_u.setAlignment(HorizontalAlignment.RIGHT);

        style_bd_rt_u.setBorderBottom(BorderStyle.THICK);

        style_bd_rt_u.setBorderTop(BorderStyle.THIN);

        style_bd_rt_u.setBorderRight(BorderStyle.THIN);

        style_bd_rt_u.setBorderLeft(BorderStyle.THIN);
//        XSSFFont my_font = workbook.createFont();
//        my_font.setBold(true);
//        style_bd_rt_u.setFont(my_font);

// Styling border and alignment of cell.  
        CellStyle style_u_ct = workbook.createCellStyle();

        style_u_ct.setBorderBottom(BorderStyle.THICK);

        style_u_ct.setAlignment(HorizontalAlignment.CENTER);

        style_u_ct.setFont(my_font);

        // Styling border and alignment of cell.  
        CellStyle style_u_ct2 = workbook.createCellStyle();

        style_u_ct2.setBorderTop(BorderStyle.THICK);

        style_u_ct2.setAlignment(HorizontalAlignment.CENTER);

        style_u_ct2.setFont(my_font);

        // Styling border and alignment of cell.  
        CellStyle style_u_b = workbook.createCellStyle();

        style_u_b.setBorderBottom(BorderStyle.THIN);

        style_u_b.setAlignment(HorizontalAlignment.LEFT);

        style_u_b.setFont(my_font);

        // Styling border and alignment of cell.  
        CellStyle style_u_rt = workbook.createCellStyle();

        style_u_rt.setBorderBottom(BorderStyle.THIN);

        style_u_rt.setAlignment(HorizontalAlignment.RIGHT);
//        style_u_rt.setFont(my_font);
//
        // Iterate over data and write to sheet 
        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset) {
            // this creates a new row in the sheet 
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                // this line creates a cell in the next column of that row 
                Cell cell = row.createCell(cellnum++);
                cell.setCellValue((String) obj);

                if (rownum == 1) {
                    cell.setCellStyle(style_ct_FF);
                } else if (rownum == 3) {
                    cell.setCellStyle(style_ct2);
                } else if (rownum == (rp740List.size() + 5)) {
                    cell.setCellStyle(style_ct2);
                } else if (rownum == (rp740List.size() + 7)) {
                    cell.setCellStyle(style_ct);
                } else if (rownum == (rp740List.size() + 9)) {
                    if ((cellnum >= 11 && cellnum <= 13) || cellnum == 15) {
                        cell.setCellStyle(style_u_ct);
                    }
                } else if (rownum >= 4 && rownum <= (rp740List.size() + 3)) {
                    if (cellnum >= 1 && cellnum <= 4) {
                        if (cell.getStringCellValue().contains("+ULINE")) {
                            cell.setCellValue(cell.getStringCellValue().replace("+ULINE", "").replace("null", ""));

                            CellStyle style_tmp = workbook.createCellStyle();
//                        style_tmp.setAlignment(HorizontalAlignment.CENTER);
                            style_tmp.setBorderBottom(BorderStyle.THIN);
//                        style_tmp.setBorderTop(BorderStyle.THIN);
                            style_tmp.setBorderRight(BorderStyle.THIN);
                            style_tmp.setBorderLeft(BorderStyle.THIN);

                            cell.setCellStyle(style_tmp);
                        } else {
                            CellStyle style_tmp = workbook.createCellStyle();
//                        style_tmp.setAlignment(HorizontalAlignment.CENTER);
//                        style_tmp.setBorderBottom(BorderStyle.THIN);
//                        style_tmp.setBorderTop(BorderStyle.THIN);
                            style_tmp.setBorderRight(BorderStyle.THIN);
                            style_tmp.setBorderLeft(BorderStyle.THIN);

                            cell.setCellStyle(style_tmp);
                        }
                    } else if (cellnum >= 5 && cellnum <= 12) {
                        if (cell.getStringCellValue().contains("+ULINE")) {
                            cell.setCellValue(cell.getStringCellValue().replace("+ULINE", "").replace("null", ""));

                            CellStyle style_tmp = workbook.createCellStyle();
                            style_tmp.setAlignment(HorizontalAlignment.CENTER);
                            style_tmp.setBorderBottom(BorderStyle.THIN);
//                        style_tmp.setBorderTop(BorderStyle.THIN);
                            style_tmp.setBorderRight(BorderStyle.THIN);
                            style_tmp.setBorderLeft(BorderStyle.THIN);

                            cell.setCellStyle(style_tmp);
                        } else {
                            CellStyle style_tmp = workbook.createCellStyle();
                            style_tmp.setAlignment(HorizontalAlignment.CENTER);
//                        style_tmp.setBorderBottom(BorderStyle.THIN);
//                        style_tmp.setBorderTop(BorderStyle.THIN);
                            style_tmp.setBorderRight(BorderStyle.THIN);
                            style_tmp.setBorderLeft(BorderStyle.THIN);

                            cell.setCellStyle(style_tmp);
                        }
                    } else if (cellnum == 13) {
                        if (cell.getStringCellValue().contains("+ULINE")) {
                            cell.setCellValue(cell.getStringCellValue().replace("+ULINE", "").replace("null", ""));

                            CellStyle style_tmp = workbook.createCellStyle();
                            style_tmp.setAlignment(HorizontalAlignment.RIGHT);
                            style_tmp.setBorderBottom(BorderStyle.THIN);
//                        style_tmp.setBorderTop(BorderStyle.THIN);
                            style_tmp.setBorderRight(BorderStyle.THIN);
                            style_tmp.setBorderLeft(BorderStyle.THIN);

                            cell.setCellStyle(style_tmp);
                        } else {
                            CellStyle style_tmp = workbook.createCellStyle();
                            style_tmp.setAlignment(HorizontalAlignment.RIGHT);
//                        style_tmp.setBorderBottom(BorderStyle.THIN);
//                        style_tmp.setBorderTop(BorderStyle.THIN);
                            style_tmp.setBorderRight(BorderStyle.THIN);
                            style_tmp.setBorderLeft(BorderStyle.THIN);

                            cell.setCellStyle(style_tmp);
                        }
                    } else if (cellnum >= 14 && cellnum <= 15) {
                        if (cell.getStringCellValue().contains("+ULINE")) {
                            cell.setCellValue(cell.getStringCellValue().replace("+ULINE", "").replace("null", ""));

                            CellStyle style_tmp = workbook.createCellStyle();
//                        style_tmp.setAlignment(HorizontalAlignment.CENTER);
                            style_tmp.setBorderBottom(BorderStyle.THIN);
//                        style_tmp.setBorderTop(BorderStyle.THIN);
                            style_tmp.setBorderRight(BorderStyle.THIN);
                            style_tmp.setBorderLeft(BorderStyle.THIN);

                            cell.setCellStyle(style_tmp);
                        } else {
                            CellStyle style_tmp = workbook.createCellStyle();
//                        style_tmp.setAlignment(HorizontalAlignment.CENTER);
//                        style_tmp.setBorderBottom(BorderStyle.THIN);
//                        style_tmp.setBorderTop(BorderStyle.THIN);
                            style_tmp.setBorderRight(BorderStyle.THIN);
                            style_tmp.setBorderLeft(BorderStyle.THIN);

                            cell.setCellStyle(style_tmp);
                        }
                    }
                }
            }
        }

        try {
            ServletContext servletContextEX = getServletContext();    // new by ji
            String realPathEX = servletContextEX.getRealPath(FILE_PATH);        // new by ji
            String saperateEX = realPathEX.contains(":") ? "\\" : "/";
            String pathEX = realPathEX + saperateEX + "IMPORT_PERFORMANCE_MONTHLY_" + ym + "_" + imc + ".xlsx";

            FileOutputStream outputStream = new FileOutputStream(pathEX);
            workbook.write(outputStream);
            workbook.close();

//            ******************************************************
            File fileEX = new File(pathEX);
            int length = 0;
            ServletOutputStream outStream = response.getOutputStream();
            ServletContext context = getServletConfig().getServletContext();
            String mimetype = context.getMimeType(pathEX);

            // sets response content type
            if (mimetype == null) {
                mimetype = "application/octet-stream";
            }
            response.setContentType(mimetype);
            response.setContentLength((int) fileEX.length());
            String fileNameEX = (new File(pathEX)).getName();

            // sets HTTP header
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileNameEX + "\"");

            byte[] byteBuffer = new byte[BUFSIZE];
            DataInputStream in = new DataInputStream(new FileInputStream(fileEX));

            // reads the file's bytes and writes them to the response stream
            while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                outStream.write(byteBuffer, 0, length);
            }

            in.close();
            outStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        response.setHeader(
                "Refresh", "0;/IMS2/IMS740/edit?ym=" + ym + "&imc=" + imc);
    }

}
