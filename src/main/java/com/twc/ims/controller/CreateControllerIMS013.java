/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSBMASDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class CreateControllerIMS013 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/createIMS013.jsp";

    public CreateControllerIMS013() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "IMS013/C");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Bank Master. Display");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String code = request.getParameter("code");
        String desc = request.getParameter("desc");
        String desc2 = request.getParameter("desc2");
        String type = request.getParameter("type");

        String userid = request.getParameter("userid");

        request.setAttribute("PROGRAMNAME", "IMS013/C");
        request.setAttribute("PROGRAMDESC", "Bank Master. Display");

        IMSBMASDao dao = new IMSBMASDao();

        String sendMessage = "";
        String forward = "";

        if (dao.check(code).equals("f")) {
            sendMessage = "<script type=\"text/javascript\">\n"
                    + "            var show = function () {\n"
                    + "                $('#myModal').modal('show');\n"
                    + "            };\n"
                    + "\n"
                    + "            window.setTimeout(show, 0);\n"
                    + "\n"
                    + "        </script>";
            forward = PAGE_VIEW;
            request.setAttribute("sendMessage", sendMessage);

            RequestDispatcher view = request.getRequestDispatcher(forward);

            view.forward(request, response);
        } else {
            dao.add(code, desc, desc2, type, userid);

            response.setHeader("Refresh", "0;/IMS2/IMS013/display");
        }

    }
}
