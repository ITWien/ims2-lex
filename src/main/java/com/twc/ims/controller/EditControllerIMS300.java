/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSADETAILDao;
import com.twc.ims.dao.IMSBAMDao;
import com.twc.ims.dao.IMSCURDao;
import com.twc.ims.dao.IMSIMCDao;
import com.twc.ims.dao.IMSPDETAILDao;
import com.twc.ims.dao.IMSPHEADDao;
import com.twc.ims.dao.IMSSDETAILDao;
import com.twc.ims.dao.IMSSHCDao;
import com.twc.ims.dao.IMSSHEADDao;
import com.twc.ims.dao.IMSVENDao;
import com.twc.ims.dao.QNBSERDao;
import com.twc.ims.entity.IMSAPDETAIL;
import com.twc.ims.entity.IMSPDETAIL;
import com.twc.ims.entity.IMSPHEAD;
import com.twc.ims.entity.IMSSDETAIL;
import com.twc.ims.entity.IMSSHC;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class EditControllerIMS300 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/editIMS300.jsp";
    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public EditControllerIMS300() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "IMS300/E");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Import Payment. Entry");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        request.setCharacterEncoding("utf-8");
        String impPaidNo = request.getParameter("impPaidNo");
        String ven = request.getParameter("ven");

        request.setAttribute("importPaidNo", impPaidNo);

        IMSBAMDao dao4 = new IMSBAMDao();
        List<IMSSHC> bankList = dao4.findAllPay();
        request.setAttribute("bankList", bankList);

        IMSCURDao dao5 = new IMSCURDao();
        List<IMSSHC> ccyList = dao5.findAll();
        request.setAttribute("ccyList", ccyList);

        IMSVENDao dao6 = new IMSVENDao();
        List<IMSSHC> vendorList = dao6.findAll();
        request.setAttribute("vendorList", vendorList);

        IMSIMCDao dao7 = new IMSIMCDao();
        List<IMSSHC> pbList = dao7.findAll();
        request.setAttribute("pbList", pbList);

        IMSPHEADDao daoH = new IMSPHEADDao();
        IMSPHEAD head = daoH.findByCode(impPaidNo);

        request.setAttribute("totalEstPaidAmount", head.getTotalEstPaidAmount());
        request.setAttribute("ccy", head.getCcy());
//        request.setAttribute("fee", head.getFee());
        request.setAttribute("paidDate", head.getPaidDate());

        IMSVENDao dao1 = new IMSVENDao();
        IMSSHC Vendor = dao1.findByCode(head.getVendor());

        IMSBAMDao dao2 = new IMSBAMDao();
        IMSSHC Bank = dao2.findByCode(head.getBank());

        IMSIMCDao dao8 = new IMSIMCDao();
        IMSSHC pb = dao8.findByCode(head.getPaidBy());

        request.setAttribute("bankcode", Bank.getCode());
        request.setAttribute("bankname", Bank.getName());
        request.setAttribute("vendorcode", Vendor.getCode());
        request.setAttribute("vendorname", Vendor.getName());
        request.setAttribute("pbcode", pb.getCode());
        request.setAttribute("pbname", pb.getName());

        IMSPDETAILDao daoVEN = new IMSPDETAILDao();
        List<IMSSHC> venList = daoVEN.findAllVEN(impPaidNo);
        request.setAttribute("venList", venList);

        String vvcode = "";

        if (ven == null) {

            if (!venList.isEmpty()) {
                request.setAttribute("vcode", venList.get(0).getCode());
                request.setAttribute("vname", venList.get(0).getName());
                request.setAttribute("fee", venList.get(0).getNamet());

                vvcode = venList.get(0).getCode();
            }

        } else {
            vvcode = ven;
            IMSPDETAILDao daov = new IMSPDETAILDao();
            IMSSHC ve = daov.findVENby(impPaidNo, ven);

            request.setAttribute("vcode", ve.getCode());
            request.setAttribute("vname", ve.getName());
            request.setAttribute("fee", ve.getNamet());
        }

        IMSPDETAILDao dao3 = new IMSPDETAILDao();
        List<IMSPDETAIL> detList = dao3.findByCode2(impPaidNo, vvcode);
        request.setAttribute("detList", detList);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "IMS300/E");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Import Payment. Entry");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        request.setCharacterEncoding("utf-8");

        IMSBAMDao dao4 = new IMSBAMDao();
        List<IMSSHC> bankList = dao4.findAllPay();
        request.setAttribute("bankList", bankList);

        IMSCURDao dao5 = new IMSCURDao();
        List<IMSSHC> ccyList = dao5.findAll();
        request.setAttribute("ccyList", ccyList);

        IMSVENDao dao6 = new IMSVENDao();
        List<IMSSHC> vendorList = dao6.findAll();
        request.setAttribute("vendorList", vendorList);

        IMSIMCDao dao7 = new IMSIMCDao();
        List<IMSSHC> pbList = dao7.findAll();
        request.setAttribute("pbList", pbList);

        String totalEstPaidAmount = request.getParameter("totalEstPaidAmountTMP");
        String ccy = request.getParameter("ccyTMP");
        String fee = request.getParameter("feeTMP");
        String paidBy = request.getParameter("paidByTMP");
        String paidDate = request.getParameter("paidDateTMP");
        String bank = request.getParameter("bankTMP");
        String vendor = request.getParameter("vendorTMP");
        String importPaidNo = request.getParameter("importPaidNoTMP");

        if (fee.trim().equals("") || fee.trim().equals("NaN")) {
            fee = "0.00";
        }

        if (vendor.contains(" : ")) {
            vendor = vendor.split(" : ")[0];
        }

        request.setAttribute("totalEstPaidAmount", totalEstPaidAmount);
        request.setAttribute("ccy", ccy);
//        request.setAttribute("fee", fee);
        request.setAttribute("paidDate", paidDate);
        request.setAttribute("importPaidNo", importPaidNo);

//        request.setAttribute("sb", "display: none;");
        IMSBAMDao dao2 = new IMSBAMDao();
        IMSSHC Bank = dao2.findByCode(bank);

        IMSVENDao dao1 = new IMSVENDao();
        IMSSHC Vendor = dao1.findByCode(vendor);

        IMSIMCDao dao8 = new IMSIMCDao();
        IMSSHC pb = dao8.findByCode(paidBy);

        request.setAttribute("bankcode", Bank.getCode());
        request.setAttribute("bankname", Bank.getName());
        request.setAttribute("vendorcode", Vendor.getCode());
        request.setAttribute("vendorname", Vendor.getName());
        request.setAttribute("pbcode", pb.getCode());
        request.setAttribute("pbname", pb.getName());

        IMSPDETAILDao daoVEN = new IMSPDETAILDao();
        List<IMSSHC> venList = daoVEN.findAllVEN(importPaidNo);
        request.setAttribute("venList", venList);

        request.setAttribute("vcode", Vendor.getCode());
        request.setAttribute("vname", Vendor.getName());

//        IMSPDETAILDao dao3 = new IMSPDETAILDao();
//        List<IMSPDETAIL> detList = dao3.findByCode(importPaidNo);
//        request.setAttribute("detList", detList);
        IMSSDETAILDao dao9 = new IMSSDETAILDao();
        List<IMSPDETAIL> detList2 = dao9.findByCode(vendor, ccy, paidBy);
        request.setAttribute("detList2", detList2);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

}
