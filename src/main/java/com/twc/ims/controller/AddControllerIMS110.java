/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSADETAILDao;
import com.twc.ims.dao.IMSAHEADDao;
import com.twc.ims.dao.IMSSDETAILDao;
import com.twc.ims.dao.IMSSHCDao;
import com.twc.ims.dao.IMSSHEADDao;
import com.twc.ims.dao.QNBSERDao;
import com.twc.ims.entity.IMSAHEAD;
import com.twc.ims.entity.IMSSDETAIL;
import com.twc.ims.entity.IMSSHC;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class AddControllerIMS110 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public AddControllerIMS110() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String advImpNo = request.getParameter("advImpNo");
        String dateF = request.getParameter("dateF");
        String dateT = request.getParameter("dateT");
        String shipCom = request.getParameter("shipCom");
        String checkDate = request.getParameter("checkDate");
        String checkNo = request.getParameter("checkNo");
        String bnkCode = request.getParameter("bnkCode");
        String finalTotalAmtData = request.getParameter("finalTotalAmtData");
        String[] no = request.getParameterValues("no");
        String[] pImpNo = request.getParameterValues("pImpNo");
        String[] invNoList = request.getParameterValues("invNoList");
        String[] transDate = request.getParameterValues("transDate");
        String[] invAmtData = request.getParameterValues("invAmtData");
        String[] taxDutyP = request.getParameterValues("taxDutyP");
        String[] advImpAmt = request.getParameterValues("advImpAmt");
        String[] taxDuty = request.getParameterValues("taxDuty");
        String[] taxBase = request.getParameterValues("taxBase");
        String[] vat7 = request.getParameterValues("vat7");
        String[] totAmt = request.getParameterValues("totAmt");

//        System.out.println(advImpNo);
//        System.out.println(dateF);
//        System.out.println(dateT);
//        System.out.println(shipCom);
//        System.out.println(checkDate);
//        System.out.println(finalTotalAmtData.replace(",", ""));
        String userid = request.getParameter("userid");

        IMSAHEADDao daock = new IMSAHEADDao();
        if (daock.check(advImpNo).equals("t")) {
            IMSAHEADDao dao = new IMSAHEADDao();
            if (dao.add(advImpNo, dateF, dateT, shipCom, checkDate, finalTotalAmtData.replace(",", ""), userid, checkNo, bnkCode)) {

                QNBSERDao daonb = new QNBSERDao();
                daonb.editCurrent(advImpNo.trim().substring(4), advImpNo.trim().substring(0, 4), "AI", "01");

                for (int i = 0; i < no.length; i++) {
//                    System.out.println(advImpNo + " - " + no[i] + " - " + pImpNo[i] + " - " + invNoList[i] + " - "
//                            + transDate[i] + " - " + invAmtData[i].replace(",", "") + " - " + taxDutyP[i] + " - "
//                            + advImpAmt[i].replace(",", "") + " - " + taxDuty[i].replace(",", "") + " - " + taxBase[i].replace(",", "") + " - "
//                            + vat7[i].replace(",", "") + " - " + totAmt[i].replace(",", ""));
//
                    IMSADETAILDao daode = new IMSADETAILDao();
                    if (daode.add(advImpNo, no[i], pImpNo[i], invNoList[i], transDate[i], invAmtData[i].replace(",", ""),
                            taxDutyP[i], advImpAmt[i].replace(",", ""), taxDuty[i].replace(",", ""),
                            taxBase[i].replace(",", ""), vat7[i].replace(",", ""), totAmt[i].replace(",", ""), userid)) {
                        IMSSHEADDao daohd = new IMSSHEADDao();
                        if (daohd.updateAdv(pImpNo[i], advImpNo)) {
                            if (invNoList[i] != null) {
                                String[] invL = invNoList[i].split(",");
                                for (String inv : invL) {
                                    IMSSDETAILDao daosts = new IMSSDETAILDao();
                                    daosts.setStatus("IMSSDSTS110", pImpNo[i], inv.trim(), "'Y'");
                                }
                            }
                        }
                    }
                }
            }
        } else {
//            IMSAHEADDao daoDelH = new IMSAHEADDao();
//            if (daoDelH.delete(advImpNo)) {
//                IMSADETAILDao daoDelD = new IMSADETAILDao();
//                if (daoDelD.delete(advImpNo)) {
//                    IMSAHEADDao dao = new IMSAHEADDao();
//                    if (dao.add(advImpNo, dateF, dateT, shipCom, checkDate, finalTotalAmtData.replace(",", ""), userid)) {
//                        for (int i = 0; i < no.length; i++) {
//                            IMSADETAILDao daode = new IMSADETAILDao();
//                            daode.add(advImpNo, no[i], pImpNo[i], invNoList[i], transDate[i], invAmtData[i].replace(",", ""),
//                                    taxDutyP[i], advImpAmt[i].replace(",", ""), taxDuty[i].replace(",", ""),
//                                    taxBase[i].replace(",", ""), vat7[i].replace(",", ""), totAmt[i].replace(",", ""), userid);
//                        }
//                    }
//                }
//            }
            IMSAHEADDao dao = new IMSAHEADDao();
            if (dao.edit(advImpNo, dateF, dateT, shipCom, checkDate, finalTotalAmtData.replace(",", ""), userid, checkNo, bnkCode)) {
                for (int i = 0; i < no.length; i++) {
                    IMSADETAILDao daockd = new IMSADETAILDao();
                    if (daockd.check(advImpNo, no[i]).equals("t")) {
                        IMSADETAILDao daode = new IMSADETAILDao();
                        if (daode.add(advImpNo, no[i], pImpNo[i], invNoList[i], transDate[i], invAmtData[i].replace(",", ""),
                                taxDutyP[i], advImpAmt[i].replace(",", ""), taxDuty[i].replace(",", ""),
                                taxBase[i].replace(",", ""), vat7[i].replace(",", ""), totAmt[i].replace(",", ""), userid)) {
                            IMSSHEADDao daohd = new IMSSHEADDao();
                            if (daohd.updateAdv(pImpNo[i], advImpNo)) {
                                if (invNoList[i] != null) {
                                    String[] invL = invNoList[i].split(",");
                                    for (String inv : invL) {
                                        IMSSDETAILDao daosts = new IMSSDETAILDao();
                                        daosts.setStatus("IMSSDSTS110", pImpNo[i], inv.trim(), "'Y'");
                                    }
                                }
                            }
                        }
                    } else {
                        IMSADETAILDao daode = new IMSADETAILDao();
                        if (daode.edit(advImpNo, no[i], pImpNo[i], invNoList[i], transDate[i], invAmtData[i].replace(",", ""),
                                taxDutyP[i], advImpAmt[i].replace(",", ""), taxDuty[i].replace(",", ""),
                                taxBase[i].replace(",", ""), vat7[i].replace(",", ""), totAmt[i].replace(",", ""), userid)) {
                            IMSSHEADDao daohd = new IMSSHEADDao();
                            daohd.updateAdv(pImpNo[i], advImpNo);
                        }
                    }

                }
            }
        }

        response.setHeader("Refresh", "0;/IMS2/IMS110/edit?advImpNo=" + advImpNo);

    }

}
