/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSIMCDao;
import com.twc.ims.dao.IMSRP790Dao;
import com.twc.ims.entity.IMSRP790;
import com.twc.ims.entity.IMSSHC;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class EditControllerIMS790 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/editIMS790.jsp";

    public EditControllerIMS790() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "IMS790/E");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Expense Sum Order by Product. Process");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String ym = request.getParameter("ym");
        String ym2 = request.getParameter("ym2");
        String imc = request.getParameter("imc");

        String y = ym.substring(0, 4);
        String m = ym.substring(4);

        if (m.length() < 2) {
            m = "0" + m;
        }

        ym = y + m;

        String y2 = ym2.substring(0, 4);
        String m2 = ym2.substring(4);

        if (m2.length() < 2) {
            m2 = "0" + m2;
        }

        ym2 = y2 + m2;

        request.setAttribute("ym", ym);
        request.setAttribute("ym2", ym2);

        request.setAttribute("imc", imc);

        IMSIMCDao dao1 = new IMSIMCDao();
        IMSSHC imcOP = dao1.findByCode(imc);
        request.setAttribute("imcName", imcOP.getName());

        List<IMSRP790> rp790List = new IMSRP790Dao().findByCode(ym, ym2, imc);
        request.setAttribute("rp790List", rp790List);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String ym = request.getParameter("ym");
        String ym2 = request.getParameter("ym2");
        String imc = request.getParameter("imc");

//        String[] ccy = request.getParameterValues("ccy");
//        String[] rate = request.getParameterValues("rate");
//        String[] totalImpAmtB = request.getParameterValues("totalImpAmtB");
//        String[] accInvAmt = request.getParameterValues("accInvAmt");
//        String[] noPaymentAmt = request.getParameterValues("noPaymentAmt");
//
//        for (int i = 0; i < ccy.length; i++) {
//            String rt = rate[i].trim().equals("") ? "null" : "'" + rate[i].trim() + "'";
//            String tiab = totalImpAmtB[i].trim().equals("") ? "null" : "'" + totalImpAmtB[i].trim() + "'";
//            String acc = accInvAmt[i].trim().equals("") ? "null" : "'" + accInvAmt[i].trim() + "'";
//            String nop = noPaymentAmt[i].trim().equals("") ? "null" : "'" + noPaymentAmt[i].trim() + "'";
//
//            IMSRP710Dao daoedt = new IMSRP710Dao();
//            daoedt.edit(ym, ccy[i], rt, tiab, acc, nop);
//
//        }
        response.setHeader("Refresh", "0;/IMS2/IMS790/edit?ym=" + ym + "&ym2=" + ym2 + "&imc=" + imc);
    }

}
