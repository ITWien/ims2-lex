/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import com.twc.ims.dao.IMSADETAILDao;
import com.twc.ims.dao.IMSAHEADDao;
import com.twc.ims.entity.IMSAHEAD;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.util.HashMap;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import net.sf.jasperreports.engine.*;

/**
 *
 * @author wien
 */
public class PrintControllerIMS110 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/pdisplayIMS110.jsp";
    private static final String FILE_DEST = "/report/";

    public PrintControllerIMS110() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String advImpNo = request.getParameter("advImpNo");
//        String thaiBaht = request.getParameter("thaiBaht");

//        System.out.println(advImpNo);
//        System.out.println(thaiBaht);
        IMSAHEADDao daoh = new IMSAHEADDao();
        IMSAHEAD head = daoh.findByCode(advImpNo);

        String dateF = head.getDateF();
        String dateT = head.getDateT();

//        IMSAHEADDao daotb = new IMSAHEADDao();
//        daotb.editTB(advImpNo, thaiBaht);
        String tdbDate = "";

        if (!dateF.trim().equals("") && !dateT.trim().equals("")) {
            String Sdd = dateF.substring(8, 10);
            int Smm = Integer.parseInt(dateF.substring(5, 7));
            String Syyyy = dateF.substring(0, 4);
            String Sdate = Sdd + " " + ConvertMonth(Smm) + " " + Syyyy;

            String Fdd = dateT.substring(8, 10);
            int Fmm = Integer.parseInt(dateT.substring(5, 7));
            String Fyyyy = dateT.substring(0, 4);
            String Fdate = Fdd + " " + ConvertMonth(Fmm) + " " + Fyyyy;

            if (Sdate.equals(Fdate)) {
                tdbDate = Sdate;
            } else if ((Smm == Fmm) && (Syyyy.equals(Fyyyy))) {
                tdbDate = Sdd + " - " + Fdd + " " + ConvertMonth(Smm) + " " + Syyyy;
            } else {
                tdbDate = Sdate + " - " + Fdate;
            }
        }

        try {
            IMSADETAILDao dao5 = new IMSADETAILDao();
            ResultSet result = dao5.reportIMS110(advImpNo);

//            ServletOutputStream servletOutputStream = response.getOutputStream();
            File reportFile = new File(getServletConfig().getServletContext()
                    .getRealPath("/resources/jasper/IMS110.jasper"));
            byte[] bytes;

            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("TRAN_DATE", tdbDate);
//            map.put("THAIBAHT", thaiBaht);

//            try {
//                bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), map, new JRResultSetDataSource(result));
//                response.setContentType("application/pdf");
//                response.setContentLength(bytes.length);
//
//                servletOutputStream.write(bytes, 0, bytes.length);
//                servletOutputStream.flush();
//                servletOutputStream.close();
//
//            } catch (JRException e) {
//                StringWriter stringWriter = new StringWriter();
//                PrintWriter printWriter = new PrintWriter(stringWriter);
//                e.printStackTrace(printWriter);
//                response.setContentType("text/plain");
//                response.getOutputStream().print(stringWriter.toString());
//
//            }
            try {
                bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), map, new JRResultSetDataSource(result));

                ServletContext servletContext2 = getServletContext();
                String realPath2 = servletContext2.getRealPath(FILE_DEST);
                String saperate2 = realPath2.contains(":") ? "\\" : "/";
                String path2 = realPath2 + saperate2 + "IMS110_Advance.pdf";

                FileOutputStream out = new FileOutputStream(path2);
                out.write(bytes, 0, bytes.length);
//                out.flush();
//                out.close();
            } catch (JRException e) {
                StringWriter stringWriter = new StringWriter();
                PrintWriter printWriter = new PrintWriter(stringWriter);
                e.printStackTrace(printWriter);
//                response.setContentType("text/plain");
//                response.getOutputStream().print(stringWriter.toString());

            }

        } catch (Exception e) {
            System.err.println(e.toString());
        }

//        *********************************************************************************
        try {
            IMSAHEADDao dao5 = new IMSAHEADDao();
            ResultSet result = dao5.reportIMS110(advImpNo);

            IMSADETAILDao daoIMC = new IMSADETAILDao();
            String imc = daoIMC.findIMC(advImpNo);

            String jspath = "/resources/jasper/IMS110_pre.jasper";
            if (imc.trim().equals("R")) {
                jspath = "/resources/jasper/IMS110_pre_TORA.jasper";
            }

//            ServletOutputStream servletOutputStream = response.getOutputStream();
            File reportFile = new File(getServletConfig().getServletContext()
                    .getRealPath(jspath));
            byte[] bytes;

//            HashMap<String, Object> map = new HashMap<String, Object>();
//            map.put("TRAN_DATE", tdbDate);
//            map.put("THAIBAHT", thaiBaht);
//            try {
//                bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), map, new JRResultSetDataSource(result));
//                response.setContentType("application/pdf");
//                response.setContentLength(bytes.length);
//
//                servletOutputStream.write(bytes, 0, bytes.length);
//                servletOutputStream.flush();
//                servletOutputStream.close();
//
//            } catch (JRException e) {
//                StringWriter stringWriter = new StringWriter();
//                PrintWriter printWriter = new PrintWriter(stringWriter);
//                e.printStackTrace(printWriter);
//                response.setContentType("text/plain");
//                response.getOutputStream().print(stringWriter.toString()); 
//
//            }
            try {
                bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), null, new JRResultSetDataSource(result));

                ServletContext servletContext2 = getServletContext();
                String realPath2 = servletContext2.getRealPath(FILE_DEST);
                String saperate2 = realPath2.contains(":") ? "\\" : "/";
                String path2 = realPath2 + saperate2 + "IMS110_Pre-Print.pdf";

                FileOutputStream out = new FileOutputStream(path2);
                out.write(bytes, 0, bytes.length);
//                out.flush();
//                out.close();
            } catch (JRException e) {
                StringWriter stringWriter = new StringWriter();
                PrintWriter printWriter = new PrintWriter(stringWriter);
                e.printStackTrace(printWriter);
//                response.setContentType("text/plain");
//                response.getOutputStream().print(stringWriter.toString());

            }

        } catch (Exception e) {
            System.err.println(e.toString());
        }

        request.setAttribute("PROGRAMNAME", "IMS110/P");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Advance Import Setting. Entry");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    public String ConvertMonth(int mm) {
        String Smonth = "";
        switch (mm) {
            case 1:
                Smonth = "January";
                break;
            case 2:
                Smonth = "February";
                break;
            case 3:
                Smonth = "March";
                break;
            case 4:
                Smonth = "April";
                break;
            case 5:
                Smonth = "May";
                break;
            case 6:
                Smonth = "June";
                break;
            case 7:
                Smonth = "July";
                break;
            case 8:
                Smonth = "August";
                break;
            case 9:
                Smonth = "September";
                break;
            case 10:
                Smonth = "October";
                break;
            case 11:
                Smonth = "November";
                break;
            case 12:
                Smonth = "December";
                break;
            default:
                Smonth = "Invalid month";
                break;
        }
        return Smonth;
    }

}
