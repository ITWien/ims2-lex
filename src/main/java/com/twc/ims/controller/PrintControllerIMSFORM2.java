/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import com.twc.ims.dao.IMSADETAILDao;
import com.twc.ims.dao.IMSAHEADDao;
import com.twc.ims.entity.IMSAHEAD;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.util.HashMap;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import net.sf.jasperreports.engine.*;

/**
 *
 * @author wien
 */
public class PrintControllerIMSFORM2 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String FILE_DEST = "/report/";
    private static final String PAGE_VIEW = "../views/pdisplayIMSFORM2.jsp";

    public PrintControllerIMSFORM2() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String formName = request.getParameter("formName");
        String NO = request.getParameter("NO");
        String DATE = request.getParameter("DATE");
        String ACC_CODE_1 = request.getParameter("ACC_CODE_1");
        String ACC_CODE_2 = request.getParameter("ACC_CODE_2");
        String ACC_CODE_3 = request.getParameter("ACC_CODE_3");
        String ACC_CODE_4 = request.getParameter("ACC_CODE_4");
        String ACC_CODE_5 = request.getParameter("ACC_CODE_5");
        String ACC_CODE_6 = request.getParameter("ACC_CODE_6");
        String ACC_CODE_7 = request.getParameter("ACC_CODE_7");
        String ACC_CODE_8 = request.getParameter("ACC_CODE_8");
        String ACC_NAME_1 = request.getParameter("ACC_NAME_1");
        String ACC_NAME_2 = request.getParameter("ACC_NAME_2");
        String ACC_NAME_3 = request.getParameter("ACC_NAME_3");
        String ACC_NAME_4 = request.getParameter("ACC_NAME_4");
        String ACC_NAME_5 = request.getParameter("ACC_NAME_5");
        String ACC_NAME_6 = request.getParameter("ACC_NAME_6");
        String ACC_NAME_7 = request.getParameter("ACC_NAME_7");
        String ACC_NAME_8 = request.getParameter("ACC_NAME_8");
        String DESC_1 = request.getParameter("DESC_1");
        String DESC_2 = request.getParameter("DESC_2");
        String DESC_3 = request.getParameter("DESC_3");
        String DESC_4 = request.getParameter("DESC_4");
        String DESC_5 = request.getParameter("DESC_5");
        String DESC_6 = request.getParameter("DESC_6");
        String DESC_7 = request.getParameter("DESC_7");
        String DESC_8 = request.getParameter("DESC_8");
        String DEBIT_1 = request.getParameter("DEBIT_1");
        String DEBIT_2 = request.getParameter("DEBIT_2");
        String DEBIT_3 = request.getParameter("DEBIT_3");
        String DEBIT_4 = request.getParameter("DEBIT_4");
        String DEBIT_5 = request.getParameter("DEBIT_5");
        String DEBIT_6 = request.getParameter("DEBIT_6");
        String DEBIT_7 = request.getParameter("DEBIT_7");
        String DEBIT_8 = request.getParameter("DEBIT_8");
        String CREDIT_1 = request.getParameter("CREDIT_1");
        String CREDIT_2 = request.getParameter("CREDIT_2");
        String CREDIT_3 = request.getParameter("CREDIT_3");
        String CREDIT_4 = request.getParameter("CREDIT_4");
        String CREDIT_5 = request.getParameter("CREDIT_5");
        String CREDIT_6 = request.getParameter("CREDIT_6");
        String CREDIT_7 = request.getParameter("CREDIT_7");
        String CREDIT_8 = request.getParameter("CREDIT_8");
        String SUM_DEBIT = request.getParameter("SUM_DEBIT");
        String SUM_CREDIT = request.getParameter("SUM_CREDIT");
        String TOT_THAI = request.getParameter("TOT_THAI");

        ACC_CODE_1 = addTab(ACC_CODE_1);
        ACC_CODE_2 = addTab(ACC_CODE_2);
        ACC_CODE_3 = addTab(ACC_CODE_3);
        ACC_CODE_4 = addTab(ACC_CODE_4);
        ACC_CODE_5 = addTab(ACC_CODE_5);
        ACC_CODE_6 = addTab(ACC_CODE_6);
        ACC_CODE_7 = addTab(ACC_CODE_7);
        ACC_CODE_8 = addTab(ACC_CODE_8);

        try {
//            ServletOutputStream servletOutputStream = response.getOutputStream();
            File reportFile = new File(getServletConfig().getServletContext()
                    .getRealPath("/resources/jasper/IMS_FORM2.jasper"));
            byte[] bytes;

            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("DOC_NAME", formName);
            map.put("NO", NO);
            map.put("DATE", DATE);
            map.put("ACC_CODE_1", ACC_CODE_1);
            map.put("ACC_CODE_2", ACC_CODE_2);
            map.put("ACC_CODE_3", ACC_CODE_3);
            map.put("ACC_CODE_4", ACC_CODE_4);
            map.put("ACC_CODE_5", ACC_CODE_5);
            map.put("ACC_CODE_6", ACC_CODE_6);
            map.put("ACC_CODE_7", ACC_CODE_7);
            map.put("ACC_CODE_8", ACC_CODE_8);
            map.put("ACC_NAME_1", ACC_NAME_1);
            map.put("ACC_NAME_2", ACC_NAME_2);
            map.put("ACC_NAME_3", ACC_NAME_3);
            map.put("ACC_NAME_4", ACC_NAME_4);
            map.put("ACC_NAME_5", ACC_NAME_5);
            map.put("ACC_NAME_6", ACC_NAME_6);
            map.put("ACC_NAME_7", ACC_NAME_7);
            map.put("ACC_NAME_8", ACC_NAME_8);
            map.put("DESC_1", DESC_1);
            map.put("DESC_2", DESC_2);
            map.put("DESC_3", DESC_3);
            map.put("DESC_4", DESC_4);
            map.put("DESC_5", DESC_5);
            map.put("DESC_6", DESC_6);
            map.put("DESC_7", DESC_7);
            map.put("DESC_8", DESC_8);
            map.put("DEBIT_1", DEBIT_1);
            map.put("DEBIT_2", DEBIT_2);
            map.put("DEBIT_3", DEBIT_3);
            map.put("DEBIT_4", DEBIT_4);
            map.put("DEBIT_5", DEBIT_5);
            map.put("DEBIT_6", DEBIT_6);
            map.put("DEBIT_7", DEBIT_7);
            map.put("DEBIT_8", DEBIT_8);
            map.put("CREDIT_1", CREDIT_1);
            map.put("CREDIT_2", CREDIT_2);
            map.put("CREDIT_3", CREDIT_3);
            map.put("CREDIT_4", CREDIT_4);
            map.put("CREDIT_5", CREDIT_5);
            map.put("CREDIT_6", CREDIT_6);
            map.put("CREDIT_7", CREDIT_7);
            map.put("CREDIT_8", CREDIT_8);
            map.put("SUM_DEBIT", SUM_DEBIT);
            map.put("SUM_CREDIT", SUM_CREDIT);
            map.put("TOT_THAI", TOT_THAI);

            try {
                bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), map, new JREmptyDataSource());
                ServletContext servletContext2 = getServletContext();
                String realPath2 = servletContext2.getRealPath(FILE_DEST);
                String saperate2 = realPath2.contains(":") ? "\\" : "/";
                String path2 = realPath2 + saperate2 + "IMSFORM2.pdf";

                FileOutputStream out = new FileOutputStream(path2);
                out.write(bytes, 0, bytes.length);
//                out.flush();
//                out.close();
            } catch (JRException e) {
                StringWriter stringWriter = new StringWriter();
                PrintWriter printWriter = new PrintWriter(stringWriter);
                e.printStackTrace(printWriter);
//                response.setContentType("text/plain");
//                response.getOutputStream().print(stringWriter.toString());

            }

        } catch (Exception e) {
            System.err.println(e.toString());
        }

        request.setAttribute("PROGRAMNAME", "IMSFORM2/P");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Reprint Form");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    public String addTab(String val) {

        String newVal = "";

        String[] array = val.split("");
        for (int i = 1; i < array.length; i++) {
            newVal += array[i] + "<t>";
        }

        return newVal;
    }

}
