/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSIMCDao;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import com.twc.ims.dao.IMSRP780Dao;
import com.twc.ims.dao.IMSRP780Dao;
import com.twc.ims.entity.IMSRP780;
import com.twc.ims.entity.IMSSHC;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;

/**
 *
 * @author wien
 */
public class PrintControllerIMS780 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String FILE_PATH = "/report/";
    private static final int BUFSIZE = 4096;
    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public PrintControllerIMS780() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String ym = request.getParameter("ym");
        String ym2 = request.getParameter("ym2");
        String imc = request.getParameter("imc");

        IMSSHC imcOP = new IMSIMCDao().findByCode(imc);

//        ***************EXCEL*************************************
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Expense Sum Order");

        Map<String, Object[]> data = new TreeMap<String, Object[]>();

        int ni = 1001;
        data.put(Integer.toString(ni++), new Object[]{"IMPORT COMPANY : " + imc + " (" + imcOP.getName() + ")"});
        data.put(Integer.toString(ni++), new Object[]{"TRANSACTION DATE : From " + ym + " To " + ym2});
        data.put(Integer.toString(ni++), new Object[]{""});
        data.put(Integer.toString(ni++), new Object[]{"", "", "", "", "", "", "", "", "", "COST CALCULATION"});
        data.put(Integer.toString(ni++), new Object[]{"",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "FOREIGN",
            "RATE",
            "",
            "",
            "",
            "CUSTOMS IMPORT",
            "",
            "",
            "CUSTOMS IMPORT",
            "CUSTOMS IMPORT",
            ""});
        data.put(Integer.toString(ni++), new Object[]{"Order Number",
            "PRE NO.",
            "IMPORT NO.",
            "VENDOR DEPT",
            "INVOICE NO.",
            "PRODUCT",
            "INCOTERM",
            "CCY",
            "AMTOUNT",
            "CUSTOMS",
            "Amount THB",
            "HANDERING",
            "INSURANCE",
            "DUTY (FEE L/G.)",
            "RENT",
            "CLEARING",
            "DUTY (CASH)",
            "DUTY (VAT)",
            "SUM EXPENSE"});

        List<IMSRP780> rp780List = new IMSRP780Dao().findForPrint(ym, ym2, imc);

        for (int i = 0; i < rp780List.size(); i++) {

            String orderno = "";

            if (i != 0) {
                if (rp780List.get(i).getIMSRPORDERNO().contains("<head>")) {
                    data.put(Integer.toString(ni++), new Object[]{""});
                    data.put(Integer.toString(ni++), new Object[]{""});
                    orderno = rp780List.get(i).getIMSRPORDERNO();
                }
            } else {
                orderno = rp780List.get(i).getIMSRPORDERNO();
            }

            data.put(Integer.toString(ni++), new Object[]{
                orderno,
                rp780List.get(i).getIMSRPPIMPNO(),
                rp780List.get(i).getIMSRPIMPNO(),
                rp780List.get(i).getIMSRPVENDEPT(),
                rp780List.get(i).getIMSRPINVNO(),
                rp780List.get(i).getIMSRPPROD(),
                rp780List.get(i).getIMSRPINCOTERM(),
                rp780List.get(i).getIMSRPCCY(),
                rp780List.get(i).getIMSRPFAMT(),
                rp780List.get(i).getIMSRPRATE(),
                rp780List.get(i).getIMSRPAMT(),
                rp780List.get(i).getIMSRPHANDERING(),
                rp780List.get(i).getIMSRPINSUR(),
                rp780List.get(i).getIMSRPFEE(),
                rp780List.get(i).getIMSRPRENT(),
                rp780List.get(i).getIMSRPCLEARING(),
                rp780List.get(i).getIMSRPCASH(),
                rp780List.get(i).getIMSRPVAT(),
                rp780List.get(i).getIMSRPSUMEXPENSE(),});

        }

        data.put(Integer.toString(ni++), new Object[]{""});

        //Set Column Width
        sheet.setColumnWidth(0, 4000);
        sheet.setColumnWidth(1, 3000);
        sheet.setColumnWidth(2, 4000);
        sheet.setColumnWidth(3, 12000);
        sheet.setColumnWidth(4, 5000);
        sheet.setColumnWidth(5, 2500);
        sheet.setColumnWidth(6, 2500);
        sheet.setColumnWidth(7, 2500);
        sheet.setColumnWidth(8, 5000);
        sheet.setColumnWidth(9, 5000);
        sheet.setColumnWidth(10, 5000);
        sheet.setColumnWidth(11, 5000);
        sheet.setColumnWidth(12, 5000);
        sheet.setColumnWidth(13, 5000);
        sheet.setColumnWidth(14, 5000);
        sheet.setColumnWidth(15, 5000);
        sheet.setColumnWidth(16, 5000);
        sheet.setColumnWidth(17, 5000);
        sheet.setColumnWidth(18, 5000);
//
        XSSFFont my_font = workbook.createFont();
//        my_font.setBold(true);
        my_font.setFontHeight(12);
//
        // Iterate over data and write to sheet 
        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset) {
            // this creates a new row in the sheet 
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                // this line creates a cell in the next column of that row 
                Cell cell = row.createCell(cellnum++);
                cell.setCellValue((String) obj);

                if (rownum == 5) {
                    CellStyle style_tmp = workbook.createCellStyle();
                    style_tmp.setBorderTop(BorderStyle.THICK);
                    cell.setCellStyle(style_tmp);
                } else if (rownum == 6) {
                    CellStyle style_tmp = workbook.createCellStyle();
                    style_tmp.setBorderBottom(BorderStyle.THICK);
                    cell.setCellStyle(style_tmp);
                } else if (rownum > 6) {

                    CellStyle style_tmp = workbook.createCellStyle();

                    if (cellnum >= 9) {
                        style_tmp.setAlignment(HorizontalAlignment.RIGHT);
                    }

                    style_tmp.setFont(my_font);

                    cell.setCellStyle(style_tmp);
                    cell.setCellValue(cell.getStringCellValue().replace("<head>", "").replace("null", ""));

                }
            }
        }

        try {
            ServletContext servletContextEX = getServletContext();    // new by ji
            String realPathEX = servletContextEX.getRealPath(FILE_PATH);        // new by ji
            String saperateEX = realPathEX.contains(":") ? "\\" : "/";
            String pathEX = realPathEX + saperateEX + "EXPENSE_SUM_ORDER_" + ym + "_" + ym2 + "_" + imc + ".xlsx";

            FileOutputStream outputStream = new FileOutputStream(pathEX);
            workbook.write(outputStream);
            workbook.close();

//            ******************************************************
            File fileEX = new File(pathEX);
            int length = 0;
            ServletOutputStream outStream = response.getOutputStream();
            ServletContext context = getServletConfig().getServletContext();
            String mimetype = context.getMimeType(pathEX);

            // sets response content type
            if (mimetype == null) {
                mimetype = "application/octet-stream";
            }
            response.setContentType(mimetype);
            response.setContentLength((int) fileEX.length());
            String fileNameEX = (new File(pathEX)).getName();

            // sets HTTP header
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileNameEX + "\"");

            byte[] byteBuffer = new byte[BUFSIZE];
            DataInputStream in = new DataInputStream(new FileInputStream(fileEX));

            // reads the file's bytes and writes them to the response stream
            while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                outStream.write(byteBuffer, 0, length);
            }

            in.close();
            outStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        response.setHeader("Refresh", "0;/IMS2/IMS780/edit?ym=" + ym + "&ym2=" + ym2 + "&imc=" + imc);
    }

}
