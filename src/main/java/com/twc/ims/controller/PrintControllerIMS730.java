/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSRP730Dao;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import com.twc.ims.dao.IMSRP740Dao;
import com.twc.ims.entity.IMSRP730;
import com.twc.ims.entity.IMSRP740;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;

/**
 *
 * @author wien
 */
public class PrintControllerIMS730 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String FILE_PATH = "/report/";
    private static final int BUFSIZE = 4096;
    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public PrintControllerIMS730() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String ym = request.getParameter("ym");
        String imc = request.getParameter("imc");

//        ***************EXCEL*************************************
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Summary Bill Amount Report");

        Map<String, Object[]> data = new TreeMap<String, Object[]>();

        String y = ym.substring(0, 4);
        String m = ym.substring(4);

        int ni = 101;

        data.put(Integer.toString(ni++), new Object[]{"Month", "PRODUCT", "TRANSPORTATION", "CO.,", "INVOICE NO.",
            "CCY", "Customs Rate", "Sum of No.", "AMOUNT(CCY)", "Amount(Baht)"});

        IMSRP730Dao daofind = new IMSRP730Dao();
        List<IMSRP730> rp730List = daofind.findByCodePrint(ym, imc);

        IMSRP730Dao daofind2 = new IMSRP730Dao();
        List<IMSRP730> rp730List2 = daofind2.findByCode22Print(ym, imc);

        rp730List.addAll(rp730List2);

        for (int i = 0; i < rp730List.size(); i++) {
            data.put(Integer.toString(ni++), new Object[]{rp730List.get(i).getPeriod(),
                rp730List.get(i).getProd(), rp730List.get(i).getTransport(), rp730List.get(i).getVen(), rp730List.get(i).getInvNo(),
                rp730List.get(i).getCcy(), rp730List.get(i).getCustRate(), rp730List.get(i).getSum(),
                rp730List.get(i).getForAmt(), rp730List.get(i).getBillAmt()});
        }

        //Set Column Width
        sheet.setColumnWidth(0, 3000);
        sheet.setColumnWidth(1, 3000);
        sheet.setColumnWidth(2, 6000);
        sheet.setColumnWidth(3, 11000);
        sheet.setColumnWidth(4, 5500);
        sheet.setColumnWidth(5, 2500);
        sheet.setColumnWidth(6, 3500);
        sheet.setColumnWidth(7, 3000);
        sheet.setColumnWidth(8, 5000);
        sheet.setColumnWidth(9, 5000);

        XSSFFont my_font = workbook.createFont();
        my_font.setBold(true);

        // Iterate over data and write to sheet
        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset) {
            // this creates a new row in the sheet
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                // this line creates a cell in the next column of that row
                Cell cell = row.createCell(cellnum++);
                cell.setCellValue((String) obj);

                if (rownum == 1) {
                    CellStyle style_tmp = workbook.createCellStyle();
                    style_tmp.setAlignment(HorizontalAlignment.CENTER);
                    style_tmp.setBorderBottom(BorderStyle.THIN);
                    style_tmp.setBorderTop(BorderStyle.THIN);
                    style_tmp.setBorderRight(BorderStyle.THIN);
                    style_tmp.setBorderLeft(BorderStyle.THIN);
                    style_tmp.setFont(my_font);
                    cell.setCellStyle(style_tmp);

                } else if (rownum >= 2 && rownum <= (rp730List.size() - (rp730List2.size() - 1))) {
                    CellStyle style_tmp = workbook.createCellStyle();

                    if (cellnum >= 6) {
                        style_tmp.setAlignment(HorizontalAlignment.RIGHT);
                        style_tmp.setBorderRight(BorderStyle.THIN);
                        style_tmp.setBorderLeft(BorderStyle.THIN);

                    } else {
                        style_tmp.setBorderRight(BorderStyle.THIN);
                        style_tmp.setBorderLeft(BorderStyle.THIN);
                    }

                    if (cell.getStringCellValue().contains("+ULINE")) {
                        style_tmp.setBorderBottom(BorderStyle.THIN);
                        cell.setCellValue(cell.getStringCellValue().replace("+ULINE", ""));
                    }

                    if (cell.getStringCellValue().contains("+CENTER")) {
                        style_tmp.setAlignment(HorizontalAlignment.CENTER);
                        cell.setCellValue(cell.getStringCellValue().replace("+CENTER", ""));
                    }

                    cell.setCellStyle(style_tmp);

                } else {
                    if (cell.getStringCellValue().contains("+TLB")) {
                        CellStyle style_tmp = workbook.createCellStyle();
                        style_tmp.setBorderBottom(BorderStyle.THICK);
                        style_tmp.setBorderTop(BorderStyle.THICK);
                        style_tmp.setBorderLeft(BorderStyle.THICK);
                        style_tmp.setFont(my_font);
                        cell.setCellStyle(style_tmp);
                        cell.setCellValue(cell.getStringCellValue().replace("+TLB", ""));

                    } else if (cell.getStringCellValue().contains("+TB")) {
                        CellStyle style_tmp = workbook.createCellStyle();
                        style_tmp.setBorderBottom(BorderStyle.THICK);
                        style_tmp.setBorderTop(BorderStyle.THICK);
                        style_tmp.setFont(my_font);
                        cell.setCellStyle(style_tmp);
                        cell.setCellValue(cell.getStringCellValue().replace("+TB", ""));

                    } else if (cell.getStringCellValue().contains("+TLRB")) {
                        CellStyle style_tmp = workbook.createCellStyle();
                        style_tmp.setAlignment(HorizontalAlignment.RIGHT);
                        style_tmp.setBorderBottom(BorderStyle.THICK);
                        style_tmp.setBorderTop(BorderStyle.THICK);
                        style_tmp.setBorderRight(BorderStyle.THICK);
                        style_tmp.setBorderLeft(BorderStyle.THICK);
                        style_tmp.setFont(my_font);
                        cell.setCellStyle(style_tmp);
                        cell.setCellValue(cell.getStringCellValue().replace("+TLRB", ""));

                    }
                }
            }
        }

        try {
            ServletContext servletContextEX = getServletContext();    // new by ji
            String realPathEX = servletContextEX.getRealPath(FILE_PATH);        // new by ji
            String saperateEX = realPathEX.contains(":") ? "\\" : "/";
            String pathEX = realPathEX + saperateEX + "SUMMARY_BILL_AMOUNT_REPORT_" + ym + "_" + imc + ".xlsx";

            FileOutputStream outputStream = new FileOutputStream(pathEX);
            workbook.write(outputStream);
            workbook.close();

//            ******************************************************
            File fileEX = new File(pathEX);
            int length = 0;
            ServletOutputStream outStream = response.getOutputStream();
            ServletContext context = getServletConfig().getServletContext();
            String mimetype = context.getMimeType(pathEX);

            // sets response content type
            if (mimetype == null) {
                mimetype = "application/octet-stream";
            }
            response.setContentType(mimetype);
            response.setContentLength((int) fileEX.length());
            String fileNameEX = (new File(pathEX)).getName();

            // sets HTTP header
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileNameEX + "\"");

            byte[] byteBuffer = new byte[BUFSIZE];
            DataInputStream in = new DataInputStream(new FileInputStream(fileEX));

            // reads the file's bytes and writes them to the response stream
            while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                outStream.write(byteBuffer, 0, length);
            }

            in.close();
            outStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        response.setHeader(
                "Refresh", "0;/IMS2/IMS740/edit?ym=" + ym + "&imc=" + imc);
    }

}
