/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSIMCDao;
import com.twc.ims.dao.IMSRP770Dao;
import com.twc.ims.entity.IMSRP770;
import com.twc.ims.entity.IMSSHC;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class DisplayControllerIMS770 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/displayIMS770.jsp";

    public DisplayControllerIMS770() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "IMS770");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Estimate Warehouse Date. Process");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        if (request.getParameter("ym") != null) {
            String ym = request.getParameter("ym");
            String ym2 = request.getParameter("ym2");
            String imc = request.getParameter("imc");
            String date = request.getParameter("date");
            String transport = request.getParameter("transport");

            String y = ym.substring(0, 4);
            String m = ym.substring(4);

            if (m.length() < 2) {
                m = "0" + m;
            }

            ym = y + m;

            String y2 = ym2.substring(0, 4);
            String m2 = ym2.substring(4);

            if (m2.length() < 2) {
                m2 = "0" + m2;
            }

            ym2 = y2 + m2;

            request.setAttribute("ym", ym);
            request.setAttribute("ym2", ym2);
            request.setAttribute("imc", imc);
            request.setAttribute("date", date);
            request.setAttribute("transport", transport);

            IMSIMCDao dao1 = new IMSIMCDao();
            IMSSHC imcOP = dao1.findByCode(imc);
            request.setAttribute("imcName", imcOP.getName());

            IMSRP770 tsp = new IMSRP770Dao().getTransportByCode(transport);
            request.setAttribute("transportName", tsp.getTransportName());

        } else {

            Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH) + 1;

            String y = Integer.toString(year);

            String m = Integer.toString(month - 1);
            if (m.length() < 2) {
                m = "0" + m;
            }

            String m2 = Integer.toString(month);
            if (m2.length() < 2) {
                m2 = "0" + m2;
            }

            request.setAttribute("ym", y + m);
            request.setAttribute("ym2", y + m2);

            List<IMSRP770> tspList = new IMSRP770Dao().getTransportList();
            request.setAttribute("date", tspList.get(0).getDate());
        }

        IMSIMCDao dao = new IMSIMCDao();
        List<IMSSHC> imcList = dao.findAll();
        request.setAttribute("imcList", imcList);

        List<IMSRP770> tspList = new IMSRP770Dao().getTransportList();
        request.setAttribute("transportList", tspList);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "IMS770");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Estimate Warehouse Date. Process");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String userid = request.getParameter("userid");
        String ym = request.getParameter("ym");
        String ym2 = request.getParameter("ym2");
        String imc = request.getParameter("imc");
        String date = request.getParameter("date");
        String transport = request.getParameter("transport");

        request.setAttribute("date", date);
        request.setAttribute("transport", transport);

        IMSRP770 tsp = new IMSRP770Dao().getTransportByCode(transport);
        request.setAttribute("transportName", tsp.getTransportName());

        List<IMSRP770> tspList = new IMSRP770Dao().getTransportList();
        request.setAttribute("transportList", tspList);

        String y = ym.substring(0, 4);
        String m = ym.substring(4);

        if (m.length() < 2) {
            m = "0" + m;
        }

        ym = y + m;

        String y2 = ym2.substring(0, 4);
        String m2 = ym2.substring(4);

        if (m2.length() < 2) {
            m2 = "0" + m2;
        }

        ym2 = y2 + m2;

        request.setAttribute("ym", ym);
        request.setAttribute("ym2", ym2);

        request.setAttribute("imc", imc);

        IMSIMCDao dao = new IMSIMCDao();
        List<IMSSHC> imcList = dao.findAll();
        request.setAttribute("imcList", imcList);

        IMSIMCDao dao1 = new IMSIMCDao();
        IMSSHC imcOP = dao1.findByCode(imc);
        request.setAttribute("imcName", imcOP.getName());

        IMSRP770Dao daodel = new IMSRP770Dao();
        daodel.delete(ym, ym2, imc);

        IMSRP770Dao daoadd = new IMSRP770Dao();
        if (daoadd.add(ym, ym2, userid, imc)) {
            request.setAttribute("result", "<b style=\" font-size: xx-large; color: #009623;\">Success !</b>");

        } else {
            IMSRP770Dao daocnt = new IMSRP770Dao();
            int cnt = 0;
            cnt = daocnt.countByPeriod(ym, ym2, imc);

            if (cnt == 0) {
                request.setAttribute("result", "<b style=\" font-size: xx-large; color: #c10000\">No data in Year / Month : " + ym + " - " + ym2 + " and Import Company : " + imc + " !</b>");
            } else {
                request.setAttribute("result", "<b style=\" font-size: xx-large; color: #c10000\">Fail !</b>");
            }

        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

}
