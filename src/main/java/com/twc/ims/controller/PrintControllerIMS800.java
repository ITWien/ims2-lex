/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSIMCDao;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import com.twc.ims.dao.IMSRP800Dao;
import com.twc.ims.dao.IMSRP800Dao;
import com.twc.ims.entity.IMSRP800;
import com.twc.ims.entity.IMSSHC;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;

/**
 *
 * @author wien
 */
public class PrintControllerIMS800 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String FILE_PATH = "/report/";
    private static final int BUFSIZE = 4096;
    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public PrintControllerIMS800() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String ym = request.getParameter("ym");
        String imc = request.getParameter("imc");

        IMSSHC imcOP = new IMSIMCDao().findByCode(imc);

//        ***************EXCEL*************************************
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Summary Guarantee Monthly");

        Map<String, Object[]> data = new TreeMap<String, Object[]>();

        int ni = 1001;
        data.put(Integer.toString(ni++), new Object[]{"IMPORT COMPANY : " + imc + " (" + imcOP.getName() + ")"});
        data.put(Integer.toString(ni++), new Object[]{"TRANSACTION DATE : " + ym});
        data.put(Integer.toString(ni++), new Object[]{""});
        data.put(Integer.toString(ni++), new Object[]{"IMPORT NO.",
            "VENDOR",
            "INVOICE NO.",
            "ORDER NO.",
            "PRODUCT",
            "CCY",
            "FOREIGN AMOUNT",
            "INVOICE AMOUNT(�)",
            "IMPORT ENTRY NO.",
            "ENTRY DATE",
            "L/G GID NO.",
            "L/G AMOUNT",
            "L/G FEE",
            "TAX NO."});

        List<IMSRP800> rp800List = new IMSRP800Dao().findForPrint(ym, imc);

        for (int i = 0; i < rp800List.size(); i++) {

            String impno = "";

            if (i != 0) {
                if (rp800List.get(i).getIMSRPIMPNO().contains("<head>")) {
//                    data.put(Integer.toString(ni++), new Object[]{""});
//                    data.put(Integer.toString(ni++), new Object[]{""});

                    impno = rp800List.get(i).getIMSRPIMPNO();
                }
            } else {
                impno = rp800List.get(i).getIMSRPIMPNO();
            }

            data.put(Integer.toString(ni++), new Object[]{
                impno,
                rp800List.get(i).getIMSRPVENDEPT(),
                rp800List.get(i).getIMSRPINVNO(),
                rp800List.get(i).getIMSRPORDERNO(),
                rp800List.get(i).getIMSRPPROD(),
                rp800List.get(i).getIMSRPCCY(),
                rp800List.get(i).getIMSRPFAMT(),
                rp800List.get(i).getIMSRPAMT(),
                rp800List.get(i).getIMSRPENTNO(),
                rp800List.get(i).getIMSRPENTDATE(),
                rp800List.get(i).getIMSRPLGGIDNO(),
                rp800List.get(i).getIMSRPLGAMT(),
                rp800List.get(i).getIMSRPLGFEE(),
                rp800List.get(i).getIMSRPTAXNO()});

        }

        data.put(Integer.toString(ni++), new Object[]{"<foot>", "<foot>", "<foot>", "<foot>", "<foot>",
            "<foot>", "<foot>", "<foot>", "<foot>", "<foot>", "<foot>", "<foot>", "<foot>", "<foot>"});

        List<IMSRP800> rp800ListSum = new IMSRP800Dao().findForPrintSum(ym, imc);

        for (int i = 0; i < rp800ListSum.size(); i++) {
            data.put(Integer.toString(ni++), new Object[]{
                rp800ListSum.get(i).getIMSRPIMPNO() + "<sum>",
                "<sum>",
                "<sum>",
                "<sum>",
                "<sum>",
                rp800ListSum.get(i).getIMSRPCCY() + "<sum>",
                rp800ListSum.get(i).getIMSRPFAMT() + "<sum>",
                rp800ListSum.get(i).getIMSRPAMT() + "<sum>",
                "<sum>",
                "<sum>",
                "<sum>",
                rp800ListSum.get(i).getIMSRPLGAMT() + "<sum>",
                rp800ListSum.get(i).getIMSRPLGFEE() + "<sum>",
                "<sum>"});
        }

        data.put(Integer.toString(ni++), new Object[]{"<foot>", "<foot>", "<foot>", "<foot>", "<foot>",
            "<foot>", "<foot>", "<foot>", "<foot>", "<foot>", "<foot>", "<foot>", "<foot>", "<foot>"});

        //Set Column Width
        sheet.setColumnWidth(0, 4500);
        sheet.setColumnWidth(1, 12000);
        sheet.setColumnWidth(2, 4000);
        sheet.setColumnWidth(3, 4000);
        sheet.setColumnWidth(4, 3000);
        sheet.setColumnWidth(5, 2000);
        sheet.setColumnWidth(6, 5000);
        sheet.setColumnWidth(7, 5000);
        sheet.setColumnWidth(8, 5000);
        sheet.setColumnWidth(9, 5000);
        sheet.setColumnWidth(10, 5000);
        sheet.setColumnWidth(11, 5000);
        sheet.setColumnWidth(12, 5000);
        sheet.setColumnWidth(13, 5000);
//
        XSSFFont my_font = workbook.createFont();
//        my_font.setBold(true);
        my_font.setFontHeight(12);
//
        // Iterate over data and write to sheet 
        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset) {
            // this creates a new row in the sheet 
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                // this line creates a cell in the next column of that row 
                Cell cell = row.createCell(cellnum++);
                cell.setCellValue((String) obj);

                if (rownum == 4) {
                    CellStyle style_tmp = workbook.createCellStyle();
                    style_tmp.setAlignment(HorizontalAlignment.CENTER);
                    style_tmp.setBorderTop(BorderStyle.THICK);
                    style_tmp.setBorderBottom(BorderStyle.THICK);
                    cell.setCellStyle(style_tmp);
                } else if (rownum > 4) {

                    CellStyle style_tmp = workbook.createCellStyle();

                    if (cell.getStringCellValue().contains("<head>")) {
                        style_tmp.setBorderTop(BorderStyle.THIN);
                    } else {
                        if (cellnum >= 9) {
                            int u = 0;
                            if (cell.getStringCellValue().contains("<foot>")) {
                                u++;
                            }
                            if (cell.getStringCellValue().contains("<sum>")) {
                                u++;
                            }
                            if (u == 0) {
                                cell.setCellValue("");
                            }
                        }
                    }

                    if (cell.getStringCellValue().contains("<foot>")) {
                        style_tmp.setBorderTop(BorderStyle.THIN);
                    }

                    if (cellnum >= 7) {
                        style_tmp.setAlignment(HorizontalAlignment.RIGHT);
                    } else if (cellnum == 5 || cellnum == 6) {
                        style_tmp.setAlignment(HorizontalAlignment.CENTER);
                    }

                    style_tmp.setFont(my_font);

                    cell.setCellStyle(style_tmp);
                    cell.setCellValue(cell.getStringCellValue().replace("<head>", "").replace("<foot>", "").replace("<sum>", "").replace("null", ""));

                }
            }
        }

        try {
            ServletContext servletContextEX = getServletContext();    // new by ji
            String realPathEX = servletContextEX.getRealPath(FILE_PATH);        // new by ji
            String saperateEX = realPathEX.contains(":") ? "\\" : "/";
            String pathEX = realPathEX + saperateEX + "Summary_Guarantee_Monthly_".toUpperCase() + ym + "_" + imc + ".xlsx";

            FileOutputStream outputStream = new FileOutputStream(pathEX);
            workbook.write(outputStream);
            workbook.close();

//            ******************************************************
            File fileEX = new File(pathEX);
            int length = 0;
            ServletOutputStream outStream = response.getOutputStream();
            ServletContext context = getServletConfig().getServletContext();
            String mimetype = context.getMimeType(pathEX);

            // sets response content type
            if (mimetype == null) {
                mimetype = "application/octet-stream";
            }
            response.setContentType(mimetype);
            response.setContentLength((int) fileEX.length());
            String fileNameEX = (new File(pathEX)).getName();

            // sets HTTP header
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileNameEX + "\"");

            byte[] byteBuffer = new byte[BUFSIZE];
            DataInputStream in = new DataInputStream(new FileInputStream(fileEX));

            // reads the file's bytes and writes them to the response stream
            while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                outStream.write(byteBuffer, 0, length);
            }

            in.close();
            outStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        response.setHeader("Refresh", "0;/IMS2/IMS800/edit?ym=" + ym + "&imc=" + imc);
    }

}
