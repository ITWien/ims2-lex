/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSIMCDao;
import com.twc.ims.dao.IMSRP800Dao;
import com.twc.ims.entity.IMSSHC;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class DisplayControllerIMS800 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/displayIMS800.jsp";

    public DisplayControllerIMS800() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "IMS800");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Summary Guarantee Monthly. Process");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH) + 1;

        String y = Integer.toString(year);
        String m = Integer.toString(month);

        if (m.length() < 2) {
            m = "0" + m;
        }

        request.setAttribute("ym", y + m);

        IMSIMCDao dao = new IMSIMCDao();
        List<IMSSHC> imcList = dao.findAll();
        request.setAttribute("imcList", imcList);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "IMS800");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Summary Guarantee Monthly. Process");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String userid = request.getParameter("userid");
        String ym = request.getParameter("ym");
        String imc = request.getParameter("imc");

        String pym = "";
        String y = ym.substring(0, 4);
        String m = ym.substring(4);

        if (m.length() < 2) {
            m = "0" + m;
        }

        ym = y + m;

        int yy = Integer.parseInt(y);
        int mm = Integer.parseInt(m);

        if (mm == 1) {
            yy--;
            mm = 12;
        } else {
            mm--;
        }

        y = Integer.toString(yy);
        m = Integer.toString(mm);

        if (m.length() < 2) {
            m = "0" + m;
        }

        pym = y + m;

        request.setAttribute("ym", ym);
//        request.setAttribute("pym", pym);
        request.setAttribute("imc", imc);

        IMSIMCDao dao = new IMSIMCDao();
        List<IMSSHC> imcList = dao.findAll();
        request.setAttribute("imcList", imcList);

        IMSIMCDao dao1 = new IMSIMCDao();
        IMSSHC imcOP = dao1.findByCode(imc);
        request.setAttribute("imcName", imcOP.getName());

        IMSRP800Dao daodel = new IMSRP800Dao();
        daodel.delete(ym, imc);

        IMSRP800Dao daoadd = new IMSRP800Dao();
        if (daoadd.add(ym, userid, imc)) {
            request.setAttribute("result", "<b style=\" font-size: xx-large; color: #009623;\">Success !</b>");

        } else {
            IMSRP800Dao daocnt = new IMSRP800Dao();
            int cnt = 0;
            cnt = daocnt.countByPeriod(ym, imc);

            if (cnt == 0) {
                request.setAttribute("result", "<b style=\" font-size: xx-large; color: #c10000\">No data in Year / Month : " + ym + " and Import Company : " + imc + " !</b>");
            } else {
                request.setAttribute("result", "<b style=\" font-size: xx-large; color: #c10000\">Fail !</b>");
            }

        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

}
