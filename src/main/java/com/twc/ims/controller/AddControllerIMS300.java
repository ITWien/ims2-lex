/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSADETAILDao;
import com.twc.ims.dao.IMSAHEADDao;
import com.twc.ims.dao.IMSAPDETAILDao;
import com.twc.ims.dao.IMSAPHEADDao;
import com.twc.ims.dao.IMSBAMDao;
import com.twc.ims.dao.IMSCURDao;
import com.twc.ims.dao.IMSIMCDao;
import com.twc.ims.dao.IMSPDETAILDao;
import com.twc.ims.dao.IMSPHEADDao;
import com.twc.ims.dao.IMSSDETAILDao;
import com.twc.ims.dao.IMSSHCDao;
import com.twc.ims.dao.IMSSHEADDao;
import com.twc.ims.dao.IMSVENDao;
import com.twc.ims.dao.QNBSERDao;
import com.twc.ims.entity.IMSAHEAD;
import com.twc.ims.entity.IMSSDETAIL;
import com.twc.ims.entity.IMSSHC;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class AddControllerIMS300 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/subCreateIMS300.jsp";

    public AddControllerIMS300() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String totalEstPaidAmount = request.getParameter("totalEstPaidAmount");
        String ccy = request.getParameter("ccy");
        String paidBy = request.getParameter("paidBy");
        String paidDate = request.getParameter("paidDate");
        String bank = request.getParameter("bank");
        String vendor = request.getParameter("vendor");
        String remainingAmount = request.getParameter("remainingAmount");
        String importPaidNo = request.getParameter("importPaidNo");
        String sumIA = request.getParameter("sumIA");
        String sumFIA = request.getParameter("sumFIA");
        String[] selectCk = request.getParameterValues("selectCk");
        String[] importNoL = request.getParameterValues("importNoL");
        String[] invoiceNoL = request.getParameterValues("invoiceNoL");
        String[] ccyL = request.getParameterValues("ccyL");
        String[] productL = request.getParameterValues("productL");
        String[] oia = request.getParameterValues("oia");
        String[] ofia = request.getParameterValues("ofia");
        String[] ia = request.getParameterValues("ia");
        String[] fia = request.getParameterValues("fia");
        String fee = request.getParameter("fee");

        if (fee.trim().equals("") || fee.trim().equals("NaN")) {
            fee = "0.00";
        }

        String userid = request.getParameter("userid");

        if (sumFIA.trim().equals("")) {
            sumFIA = "0";
        }
        if (remainingAmount.trim().equals("")) {
            remainingAmount = totalEstPaidAmount;
        }

        if (selectCk != null) {
            String head = "'TWC', '" + importNoL[0] + "', '" + importPaidNo + "', " + (paidDate.trim().equals("") ? "null" : "'" + paidDate + "'")
                    + ", null, " + (bank.trim().equals("") ? "null" : "'" + bank + "'") + ", '" + vendor + "', "
                    + totalEstPaidAmount.replace(",", "") + ", " + sumFIA.replace(",", "") + ", "
                    + remainingAmount.replace(",", "") + ", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '" + userid + "', '" + paidBy + "', '" + fee.replace(",", "") + "', 'S'";
            IMSPHEADDao daock = new IMSPHEADDao();
            if (daock.check(importPaidNo).equals("t")) {
                IMSPHEADDao daoaddh = new IMSPHEADDao();
                if (daoaddh.add(head)) {
                    QNBSERDao daonb = new QNBSERDao();
                    daonb.editCurrent(importPaidNo.trim().substring(4), importPaidNo.trim().substring(0, 4), "I3", "01");

                    int line = 1;
                    for (String ii : selectCk) {
                        IMSSDETAILDao daoedit = new IMSSDETAILDao();
                        daoedit.editPaidNo(importPaidNo, ii.split("-")[1], ii.split("-")[2]);

                        ii = ii.split("-")[0];
                        int i = Integer.parseInt(ii);
                        String detail = "'TWC', '" + importNoL[i] + "', '" + importPaidNo + "', " + line + ", '" + invoiceNoL[i] + "', '" + vendor + "', '" + ccyL[i]
                                + "', '" + productL[i] + "', " + oia[i].replace(",", "") + ", " + ofia[i].replace(",", "")
                                + ", null, null, null, " + (bank.trim().equals("") ? "null" : "'" + bank + "'") + ", " + (paidDate.trim().equals("") ? "null" : "'" + paidDate + "'") + ", 'S', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '" + userid + "', '" + fee.replace(",", "") + "'";

                        line++;

                        IMSPDETAILDao daoaddd = new IMSPDETAILDao();
                        if (daoaddd.add(detail)) {
                            IMSSDETAILDao daosts = new IMSSDETAILDao();
                            daosts.setStatus("IMSSDSTS300", importNoL[i], invoiceNoL[i].trim(), "'Y'");
                        }
                    }

                }
            }

        }

        request.setAttribute("PROGRAMNAME", "IMS300/C");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Import Payment. Entry");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        request.setAttribute("totalEstPaidAmount", totalEstPaidAmount);
        request.setAttribute("remainingAmount", remainingAmount);
        request.setAttribute("ccy", ccy);
        request.setAttribute("paidDate", paidDate);

        IMSBAMDao dao2 = new IMSBAMDao();
        IMSSHC Bank = dao2.findByCode(bank);

        IMSVENDao dao1 = new IMSVENDao();
        IMSSHC Vendor = dao1.findByCode(vendor);

        IMSIMCDao dao8 = new IMSIMCDao();
        IMSSHC pb = dao8.findByCode(paidBy);

        request.setAttribute("bankcode", Bank.getCode());
        request.setAttribute("bankname", Bank.getName());
        request.setAttribute("vendorcode", Vendor.getCode());
        request.setAttribute("vendorname", Vendor.getName());
        request.setAttribute("importPaidNo", importPaidNo);
        request.setAttribute("pbcode", pb.getCode());
        request.setAttribute("pbname", pb.getName());
        request.setAttribute("fee", fee);

        request.setAttribute("hide", " display: none;");

        IMSBAMDao dao4 = new IMSBAMDao();
        List<IMSSHC> bankList = dao4.findAllPay();
        request.setAttribute("bankList", bankList);

        IMSCURDao dao5 = new IMSCURDao();
        List<IMSSHC> ccyList = dao5.findAll();
        request.setAttribute("ccyList", ccyList);

        IMSVENDao dao6 = new IMSVENDao();
        List<IMSSHC> vendorList = dao6.findAll();
        request.setAttribute("vendorList", vendorList);

        IMSIMCDao dao7 = new IMSIMCDao();
        List<IMSSHC> pbList = dao7.findAll();
        request.setAttribute("pbList", pbList);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

}
