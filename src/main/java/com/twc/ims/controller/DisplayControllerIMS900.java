/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSIMCDao;
import com.twc.ims.dao.IMSSDETAILDao;
import com.twc.ims.dao.IMSSHEADDao;
import com.twc.ims.dao.IMSVENDao;
import com.twc.ims.entity.IMSSDETAIL;
import com.twc.ims.entity.IMSSHC;
import com.twc.ims.entity.IMSSHEAD;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class DisplayControllerIMS900 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/displayIMS900.jsp";

    public DisplayControllerIMS900() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "IMS900");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Import Information. Tracking");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String ym = request.getParameter("ym");
        String ym2 = request.getParameter("ym2");

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH) + 1;

        String y = Integer.toString(year);
        String m = Integer.toString(month - 1);
        String m2 = Integer.toString(month);

        if (m.length() < 2) {
            m = "0" + m;
        }
        if (m2.length() < 2) {
            m2 = "0" + m2;
        }

        if (ym == null) {
            ym = y + m;
        } else if (ym.trim().equals("")) {
            ym = y + m;
        }

        if (ym2 == null) {
            ym2 = y + m2;
        } else if (ym2.trim().equals("")) {
            ym2 = y + m2;
        }

        request.setAttribute("ym", ym);
        request.setAttribute("ym2", ym2);

        IMSSHEADDao daoImpCom = new IMSSHEADDao();
        List<IMSSHC> ImpComList = daoImpCom.findImpComIMS900(ym, ym2);
        request.setAttribute("ImpComList", ImpComList);

        IMSSHEADDao daoVenCom = new IMSSHEADDao();
        List<IMSSHC> VenComList = daoVenCom.findVenComIMS900(ym, ym2);
        request.setAttribute("VenComList", VenComList);

        IMSSDETAILDao daoVenDep = new IMSSDETAILDao();
        List<IMSSHC> VenDepList = daoVenDep.findVenDepIMS900(ym, ym2);
        request.setAttribute("VenDepList", VenDepList);

        IMSSHEADDao daoShipCom = new IMSSHEADDao();
        List<IMSSHC> ShipComList = daoShipCom.findShipComIMS900(ym, ym2);
        request.setAttribute("ShipComList", ShipComList);

        IMSSDETAILDao daoProGrp = new IMSSDETAILDao();
        List<IMSSHC> ProGrpList = daoProGrp.findProGrpIMS900(ym, ym2);
        request.setAttribute("ProGrpList", ProGrpList);

        IMSSHEADDao daoPrv = new IMSSHEADDao();
        List<IMSSHC> PrvList = daoPrv.findPrvIMS900(ym, ym2);
        request.setAttribute("PrvList", PrvList);

        String[] StsCode = {"IMS100", "IMS101", "IMS110", "IMS200", "IMS300", "IMS301", "ALL"};
        String[] StsName = {"IMPORT ENTRY", "COST CALCULATION", "ADVANCE SETTING", "ADVANCE PAYMENT", "IMPORT SETTING", "IMPORT PAYMENT", "ALL STATUS"};

        List<IMSSHC> StsList = new ArrayList<IMSSHC>();
        for (int i = 0; i < StsCode.length; i++) {
            IMSSHC p = new IMSSHC();
            p.setCode(StsCode[i]);
            p.setName(StsName[i]);
            StsList.add(p);
        }
        request.setAttribute("StsList", StsList);

//        ***************************Main List**********************************
        List<IMSSHC> MainList = new ArrayList<IMSSHC>();
        request.setAttribute("MainList", MainList);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "IMS900");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Import Information. Tracking");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String ym = request.getParameter("ym");
        String ym2 = request.getParameter("ym2");
        String sortBy = request.getParameter("sortBy");
        String data = request.getParameter("data");

        String dataU = "";

        if (data.contains(" : ")) {
            dataU = data.split(" : ")[0];
        }

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH) + 1;

        String y = Integer.toString(year);
        String m = Integer.toString(month - 1);
        String m2 = Integer.toString(month);

        if (m.length() < 2) {
            m = "0" + m;
        }
        if (m2.length() < 2) {
            m2 = "0" + m2;
        }

        if (ym.trim().equals("")) {
            ym = ym2;
        }

        if (ym2.trim().equals("")) {
            ym2 = ym;
        }

        if (ym.trim().equals("") && ym2.trim().equals("")) {
            ym = y + m;
            ym2 = y + m2;
        }

        List<IMSSHC> MainList = new ArrayList<IMSSHC>();

        IMSSHEADDao daoImpCom = new IMSSHEADDao();
        List<IMSSHC> ImpComList = daoImpCom.findImpComIMS900(ym, ym2);
        request.setAttribute("ImpComList", ImpComList);

        IMSSHEADDao daoVenCom = new IMSSHEADDao();
        List<IMSSHC> VenComList = daoVenCom.findVenComIMS900(ym, ym2);
        request.setAttribute("VenComList", VenComList);

        IMSSDETAILDao daoVenDep = new IMSSDETAILDao();
        List<IMSSHC> VenDepList = daoVenDep.findVenDepIMS900(ym, ym2);
        request.setAttribute("VenDepList", VenDepList);

        IMSSHEADDao daoShipCom = new IMSSHEADDao();
        List<IMSSHC> ShipComList = daoShipCom.findShipComIMS900(ym, ym2);
        request.setAttribute("ShipComList", ShipComList);

        IMSSDETAILDao daoProGrp = new IMSSDETAILDao();
        List<IMSSHC> ProGrpList = daoProGrp.findProGrpIMS900(ym, ym2);
        request.setAttribute("ProGrpList", ProGrpList);

        IMSSHEADDao daoPrv = new IMSSHEADDao();
        List<IMSSHC> PrvList = daoPrv.findPrvIMS900(ym, ym2);
        request.setAttribute("PrvList", PrvList);

        String[] StsCode = {"IMS100", "IMS101", "IMS110", "IMS200", "IMS300", "IMS301", "ALL"};
        String[] StsName = {"IMPORT ENTRY", "COST CALCULATION", "ADVANCE SETTING", "ADVANCE PAYMENT", "IMPORT SETTING", "IMPORT PAYMENT", "ALL STATUS"};

        List<IMSSHC> StsList = new ArrayList<IMSSHC>();
        for (int i = 0; i < StsCode.length; i++) {
            IMSSHC p = new IMSSHC();
            p.setCode(StsCode[i]);
            p.setName(StsName[i]);
            StsList.add(p);
        }
        request.setAttribute("StsList", StsList);

        if (sortBy.equals("impCom")) {
            request.setAttribute("sortBy", "1 : IMPORT COMPANY");
            request.setAttribute("sortByValue", "impCom");
            MainList = ImpComList;

        } else if (sortBy.equals("venCom")) {
            request.setAttribute("sortBy", "2 : VENDOR COMPANY");
            request.setAttribute("sortByValue", "venCom");
            MainList = VenComList;

        } else if (sortBy.equals("venDep")) {
            request.setAttribute("sortBy", "3 : VENDOR DEPARTMENT");
            request.setAttribute("sortByValue", "venDep");
            MainList = VenDepList;

        } else if (sortBy.equals("shipCom")) {
            request.setAttribute("sortBy", "4 : SHIPPING COMPANY");
            request.setAttribute("sortByValue", "shipCom");
            MainList = ShipComList;

        } else if (sortBy.equals("proGrp")) {
            request.setAttribute("sortBy", "5 : PRODUCT GROUP");
            request.setAttribute("sortByValue", "proGrp");
            MainList = ProGrpList;

        } else if (sortBy.equals("prv")) {
            request.setAttribute("sortBy", "6 : PRIVILEGE");
            request.setAttribute("sortByValue", "prv");
            MainList = PrvList;

        } else if (sortBy.equals("sts")) {
            request.setAttribute("sortBy", "7 : STATUS");
            request.setAttribute("sortByValue", "sts");
            MainList = StsList;

        }

        request.setAttribute("ym", ym);
        request.setAttribute("ym2", ym2);
        request.setAttribute("data", new String(data.getBytes("iso-8859-1"), "UTF-8"));

//        System.out.println(ym);
//        System.out.println(ym2);
//        System.out.println(sortBy);
//        System.out.println(dataU);
        IMSSHEADDao daoH = new IMSSHEADDao();
        List<IMSSHEAD> headList = daoH.findSHead(ym, ym2, sortBy, dataU);

        for (int i = 0; i < headList.size(); i++) {
            IMSSDETAILDao daoD = new IMSSDETAILDao();
            List<IMSSDETAIL> detailList = daoD.findSDetail(headList.get(i).getPimNo(), sortBy, dataU);

            headList.get(i).setDetailList(detailList);
        }

        request.setAttribute("headList", headList);

//        ***************************Main List**********************************
        request.setAttribute("MainList", MainList);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

}
