/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSIMCDao;
import com.twc.ims.dao.IMSRP700Dao;
import com.twc.ims.dao.IMSSDETAILDao;
import com.twc.ims.dao.IMSSHEADDao;
import com.twc.ims.dao.IMSVENDao;
import com.twc.ims.entity.IMSSDETAIL;
import com.twc.ims.entity.IMSSHC;
import com.twc.ims.entity.IMSSHEAD;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class DisplayControllerIMS700 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/displayIMS700.jsp";

    public DisplayControllerIMS700() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("PROGRAMNAME", "IMS700");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Summary Payment Report. Process");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH) + 1;

        String y = Integer.toString(year);
        String m = Integer.toString(month);

        if (m.length() < 2) {
            m = "0" + m;
        }

        request.setAttribute("ym", y + m);

        IMSIMCDao dao = new IMSIMCDao();
        List<IMSSHC> imcList = dao.findAll();
        request.setAttribute("imcList", imcList);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "IMS700");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Summary Payment Report. Process");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String userid = request.getParameter("userid");
        String ym = request.getParameter("ym");
        String imc = request.getParameter("imc");

        String pym = "";
        String y = ym.substring(0, 4);
        String m = ym.substring(4);

        if (m.length() < 2) {
            m = "0" + m;
        }

        ym = y + m;

        int yy = Integer.parseInt(y);
        int mm = Integer.parseInt(m);

        if (mm == 1) {
            yy--;
            mm = 12;
        } else {
            mm--;
        }

        y = Integer.toString(yy);
        m = Integer.toString(mm);

        if (m.length() < 2) {
            m = "0" + m;
        }

        pym = y + m;

        request.setAttribute("ym", ym);
//        request.setAttribute("pym", pym);
        request.setAttribute("imc", imc);

        IMSIMCDao dao = new IMSIMCDao();
        List<IMSSHC> imcList = dao.findAll();
        request.setAttribute("imcList", imcList);

        IMSIMCDao dao1 = new IMSIMCDao();
        IMSSHC imcOP = dao1.findByCode(imc);
        request.setAttribute("imcName", imcOP.getName());

        IMSRP700Dao daodel = new IMSRP700Dao();
        daodel.delete(ym, imc);

        IMSRP700Dao daoadd = new IMSRP700Dao();
        if (daoadd.add(ym, pym, userid, imc)) {
            request.setAttribute("result", "<b style=\" font-size: xx-large; color: #009623;\">Success !</b>");

        } else {
            request.setAttribute("result", "<b style=\" font-size: xx-large; color: #c10000\">Fail !</b>");

        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

}
