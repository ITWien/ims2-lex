/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSADETAILDao;
import com.twc.ims.dao.IMSAHEADDao;
import com.twc.ims.dao.IMSAPDETAILDao;
import com.twc.ims.dao.IMSAPHEADDao;
import com.twc.ims.dao.IMSBAMDao;
import com.twc.ims.dao.IMSSHCDao;
import com.twc.ims.dao.IMSSHEADDao;
import com.twc.ims.dao.QNBSERDao;
import com.twc.ims.entity.IMSAHEAD;
import com.twc.ims.entity.IMSAPDETAIL;
import com.twc.ims.entity.IMSAPHEAD;
import com.twc.ims.entity.IMSSDETAIL;
import com.twc.ims.entity.IMSSHC;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class SDisplayControllerIMS200 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/sdisplayIMS200.jsp";
    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public SDisplayControllerIMS200() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "IMS200/D");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Advance Import Payment. Entry");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********     
        IMSSHCDao dao1 = new IMSSHCDao();
        List<IMSSHC> shipList = dao1.findAll();
        request.setAttribute("shipList", shipList);

        String advPaidNo = request.getParameter("advPaidNo");

        request.setAttribute("cur", advPaidNo);

        IMSAPHEADDao daoh = new IMSAPHEADDao();
        IMSAPHEAD head = daoh.findByCode(advPaidNo);

        IMSSHCDao dao2 = new IMSSHCDao();
        IMSSHC ship = dao2.findByCode(head.getShipCom());

        request.setAttribute("shipcode", ship.getCode());
        request.setAttribute("shipname", ship.getName());
        request.setAttribute("shipnamet", ship.getNamet());

        IMSAPDETAILDao dao3 = new IMSAPDETAILDao();
        List<IMSAPDETAIL> detList = dao3.findByCode(advPaidNo);
        request.setAttribute("detList", detList);

        if (!detList.isEmpty()) {
            double sumadv = 0;
            double sumestAdv = 0;
            double sumbillAmt = 0;
            double sumtax3 = 0;
            double sumtax1 = 0;
            double sumamount = 0;
            double sumtotAdv = 0;
            double sumbalance = 0;

            double sumtotAdvTMP = 0;
            double sumbalanceTMP = 0;

            String checkno = "";
            boolean isOne = true;

            for (int i = 0; i < detList.size(); i++) {
                String sa = "0";
                if (detList.get(i).getAdvance() != null) {
                    sa = detList.get(i).getAdvance().replace(",", "");
                }

                sumadv += Double.parseDouble(sa);
                sumestAdv += Double.parseDouble(detList.get(i).getEstAdv().replace(",", ""));
                sumbillAmt += Double.parseDouble(detList.get(i).getBillAmt().replace(",", ""));
                sumtax3 += Double.parseDouble(detList.get(i).getTax3().replace(",", ""));
                sumtax1 += Double.parseDouble(detList.get(i).getTax1().replace(",", ""));
                sumamount += Double.parseDouble(detList.get(i).getAmount().replace(",", ""));

                if (checkno.equals("")) {
                    sumtotAdvTMP += Double.parseDouble(detList.get(i).getTotAdv().replace(",", ""));
                    sumbalanceTMP += Double.parseDouble(detList.get(i).getBalance().replace(",", "").replace("(", "").replace(")", ""));

                    checkno = detList.get(i).getCheckNoD();
                } else if (checkno.equals(detList.get(i).getCheckNoD())) {
                    sumtotAdvTMP = 0;
                    sumbalanceTMP = 0;

                    sumtotAdvTMP += Double.parseDouble(detList.get(i).getTotAdv().replace(",", ""));
                    sumbalanceTMP += Double.parseDouble(detList.get(i).getBalance().replace(",", "").replace("(", "").replace(")", ""));

                    checkno = detList.get(i).getCheckNoD();
                } else {
                    isOne = false;

                    sumtotAdv += sumtotAdvTMP;
                    sumbalance += sumbalanceTMP;

                    checkno = detList.get(i).getCheckNoD();
                }
            }

            request.setAttribute("grt", "Grand Total");
            request.setAttribute("sumadv", formatDou.format(sumadv));
            request.setAttribute("sumestAdv", formatDou.format(sumestAdv));
            request.setAttribute("sumbillAmt", formatDou.format(sumbillAmt));
            request.setAttribute("sumtax3", formatDou.format(sumtax3));
            request.setAttribute("sumtax1", formatDou.format(sumtax1));
            request.setAttribute("sumamount", formatDou.format(sumamount));

            if (isOne) {
                request.setAttribute("sumtotAdv", formatDou.format(sumtotAdvTMP));
                request.setAttribute("sumbalance", formatDou.format(sumbalanceTMP));
            } else {
                request.setAttribute("sumtotAdv", formatDou.format(sumtotAdv));
                request.setAttribute("sumbalance", formatDou.format(sumbalance));
            }

        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
