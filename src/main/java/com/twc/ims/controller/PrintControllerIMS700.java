/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import com.twc.ims.dao.IMSADETAILDao;
import com.twc.ims.dao.IMSAHEADDao;
import com.twc.ims.dao.IMSRP700Dao;
import com.twc.ims.entity.IMSAHEAD;
import com.twc.ims.entity.IMSRP700;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import net.sf.jasperreports.engine.*;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFFont;

/**
 *
 * @author wien
 */
public class PrintControllerIMS700 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String FILE_PATH = "/report/";
    private static final int BUFSIZE = 4096;

    public PrintControllerIMS700() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String ym = request.getParameter("ym");
        String imc = request.getParameter("imc");
        String[] ccy = request.getParameterValues("ccy");
        String[] rate = request.getParameterValues("rate");
        String[] totalImpAmtB = request.getParameterValues("totalImpAmtB");
        String[] accInvAmt = request.getParameterValues("accInvAmt");
        String[] noPaymentAmt = request.getParameterValues("noPaymentAmt");

        if (ccy != null) {
            for (int i = 0; i < ccy.length; i++) {
                String rt = rate[i].trim().equals("") ? "null" : "'" + rate[i].trim() + "'";
                String tiab = totalImpAmtB[i].trim().equals("") ? "null" : "'" + totalImpAmtB[i].trim() + "'";
                String acc = accInvAmt[i].trim().equals("") ? "null" : "'" + accInvAmt[i].trim() + "'";
                String nop = noPaymentAmt[i].trim().equals("") ? "null" : "'" + noPaymentAmt[i].trim() + "'";

                IMSRP700Dao daoedt = new IMSRP700Dao();
                daoedt.edit(ym, ccy[i], rt, tiab, acc, nop, imc);

            }

//        ***************EXCEL*************************************
            Calendar c = Calendar.getInstance();
            int yearC = c.get(Calendar.YEAR);
            int monthC = c.get(Calendar.MONTH) + 1;
            int dayC = c.get(Calendar.DATE);

            String yC = Integer.toString(yearC);
            String mC = Integer.toString(monthC);
            String dC = Integer.toString(dayC);

            if (mC.length() < 2) {
                mC = "0" + mC;
            }
            if (dC.length() < 2) {
                dC = "0" + dC;
            }

            String year = ym.substring(0, 4);
            String mt = ym.substring(4);
            String ml = "01";
            if (mt.equals(ml)) {
                ml = "M.1";
            } else {
                ml = "M.1-M." + Integer.parseInt(mt);
            }

            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet sheet = workbook.createSheet("PAYMENT " + ml + " " + year + " " + dayC + "." + monthC + "." + yC.substring(2));

            Map<String, Object[]> data = new TreeMap<String, Object[]>();

            data.put(Integer.toString(101), new Object[]{"Month", "CCY", "AMOUNT IMPORT",
                "AMOUNT IMPORT", "Amount Import", "Exchange Rate", "Amount Import",
                "Balance", "Total (3)", "ALL BANK (CCY)", "SUMITOMO", "HSB", "SCB (CCY)", "BBL (CCY)",
                "MIZUHO", "KRUNGSRI-TOKYO", "TOTAL PAYMENT (11)", "No Payment"});
            data.put(Integer.toString(102), new Object[]{"", "", "MACHINE", "MATERIALS",
                "(CCY) (1)", "Customs", "Baht", "(CCY) (2)", "(CCY) (1+2)", "Balance this Month (4)",
                "Payment this Month (5)", "Payment this Month (6)", "Payment this Month (7)",
                "Payment this Month (8)", "Payment this Month (9)", "Payment this Month (10)",
                "(4+5+6+7+8+9+10)", "(3-11)"});

            IMSRP700Dao daofind = new IMSRP700Dao();
            List<IMSRP700> rp700List = daofind.findByYear(year, imc);

            IMSRP700Dao daofind2 = new IMSRP700Dao();
            List<IMSRP700> rp700List2 = daofind2.findByYearTotal(year, imc);

            int ni = 103;
            for (int i = 0; i < rp700List.size(); i++) {
                String y = rp700List.get(i).getPeriod().substring(0, 4);
                String m = rp700List.get(i).getPeriod().substring(4);

                if (i == 0) {
                    data.put(Integer.toString(ni++), new Object[]{"M" + Integer.parseInt(m) + "/" + y,
                        rp700List.get(i).getCcy(), rp700List.get(i).getMacImpAmt(), rp700List.get(i).getMatImpAmt(),
                        rp700List.get(i).getTotalImpAmt(), rp700List.get(i).getRate(),
                        rp700List.get(i).getTotalImpAmtB(), rp700List.get(i).getBalAmtPrevMonth(),
                        rp700List.get(i).getAccInvAmt(), rp700List.get(i).getAllBankBalPayment(),
                        rp700List.get(i).getSumitomo(), rp700List.get(i).getHsb(), rp700List.get(i).getScb(),
                        rp700List.get(i).getBbl(), rp700List.get(i).getMizuho(), rp700List.get(i).getK_tokyo(),
                        rp700List.get(i).getTotal(), rp700List.get(i).getNoPaymentAmt()});

                } else {
                    if (!rp700List.get(i).getPeriod().equals(rp700List.get(i - 1).getPeriod())) {
                        data.put(Integer.toString(ni++), new Object[]{"M" + Integer.parseInt(m) + "/" + y,
                            rp700List.get(i).getCcy(), rp700List.get(i).getMacImpAmt(), rp700List.get(i).getMatImpAmt(),
                            rp700List.get(i).getTotalImpAmt(), rp700List.get(i).getRate(),
                            rp700List.get(i).getTotalImpAmtB(), rp700List.get(i).getBalAmtPrevMonth(),
                            rp700List.get(i).getAccInvAmt(), rp700List.get(i).getAllBankBalPayment(),
                            rp700List.get(i).getSumitomo(), rp700List.get(i).getHsb(), rp700List.get(i).getScb(),
                            rp700List.get(i).getBbl(), rp700List.get(i).getMizuho(), rp700List.get(i).getK_tokyo(),
                            rp700List.get(i).getTotal(), rp700List.get(i).getNoPaymentAmt()});
                    } else {
                        data.put(Integer.toString(ni++), new Object[]{"",
                            rp700List.get(i).getCcy(), rp700List.get(i).getMacImpAmt(), rp700List.get(i).getMatImpAmt(),
                            rp700List.get(i).getTotalImpAmt(), rp700List.get(i).getRate(),
                            rp700List.get(i).getTotalImpAmtB(), rp700List.get(i).getBalAmtPrevMonth(),
                            rp700List.get(i).getAccInvAmt(), rp700List.get(i).getAllBankBalPayment(),
                            rp700List.get(i).getSumitomo(), rp700List.get(i).getHsb(), rp700List.get(i).getScb(),
                            rp700List.get(i).getBbl(), rp700List.get(i).getMizuho(), rp700List.get(i).getK_tokyo(),
                            rp700List.get(i).getTotal(), rp700List.get(i).getNoPaymentAmt()});
                    }
                }
//************Total Month***************
                if (i != rp700List.size() - 1) {
                    if (!rp700List.get(i).getPeriod().equals(rp700List.get(i + 1).getPeriod())) {
                        data.put(Integer.toString(ni++), new Object[]{"TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT-" + rp700List.get(i).getSumTiab(), "TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT"});
                    }
                } else {
                    data.put(Integer.toString(ni++), new Object[]{"TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT-" + rp700List.get(i).getSumTiab(), "TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT"});
                }
            }

            for (int i = 0; i < rp700List2.size(); i++) {
                if (i == 0) {
                    data.put(Integer.toString(ni++), new Object[]{"TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT"});

                    data.put(Integer.toString(ni++), new Object[]{"G.TOTAL",
                        rp700List2.get(i).getCcy(), rp700List2.get(i).getMacImpAmt(), rp700List2.get(i).getMatImpAmt(),
                        rp700List2.get(i).getTotalImpAmt(), rp700List2.get(i).getRate(),
                        rp700List2.get(i).getTotalImpAmtB(), rp700List2.get(i).getBalAmtPrevMonth(),
                        rp700List2.get(i).getAccInvAmt(), rp700List2.get(i).getAllBankBalPayment(),
                        rp700List2.get(i).getSumitomo(), rp700List2.get(i).getHsb(), rp700List2.get(i).getScb(),
                        rp700List2.get(i).getBbl(), rp700List2.get(i).getMizuho(), rp700List2.get(i).getK_tokyo(),
                        rp700List2.get(i).getTotal(), rp700List2.get(i).getNoPaymentAmt()});

                } else {
                    if (!rp700List2.get(i).getPeriod().equals(rp700List2.get(i - 1).getPeriod())) {
                        data.put(Integer.toString(ni++), new Object[]{"G.TOTAL",
                            rp700List2.get(i).getCcy(), rp700List2.get(i).getMacImpAmt(), rp700List2.get(i).getMatImpAmt(),
                            rp700List2.get(i).getTotalImpAmt(), rp700List2.get(i).getRate(),
                            rp700List2.get(i).getTotalImpAmtB(), rp700List2.get(i).getBalAmtPrevMonth(),
                            rp700List2.get(i).getAccInvAmt(), rp700List2.get(i).getAllBankBalPayment(),
                            rp700List2.get(i).getSumitomo(), rp700List2.get(i).getHsb(), rp700List2.get(i).getScb(),
                            rp700List2.get(i).getBbl(), rp700List2.get(i).getMizuho(), rp700List2.get(i).getK_tokyo(),
                            rp700List2.get(i).getTotal(), rp700List2.get(i).getNoPaymentAmt()});
                    } else {
                        data.put(Integer.toString(ni++), new Object[]{"",
                            rp700List2.get(i).getCcy(), rp700List2.get(i).getMacImpAmt(), rp700List2.get(i).getMatImpAmt(),
                            rp700List2.get(i).getTotalImpAmt(), rp700List2.get(i).getRate(),
                            rp700List2.get(i).getTotalImpAmtB(), rp700List2.get(i).getBalAmtPrevMonth(),
                            rp700List2.get(i).getAccInvAmt(), rp700List2.get(i).getAllBankBalPayment(),
                            rp700List2.get(i).getSumitomo(), rp700List2.get(i).getHsb(), rp700List2.get(i).getScb(),
                            rp700List2.get(i).getBbl(), rp700List2.get(i).getMizuho(), rp700List2.get(i).getK_tokyo(),
                            rp700List2.get(i).getTotal(), rp700List2.get(i).getNoPaymentAmt()});
                    }
                }
//************Total Month***************
                if (i != rp700List2.size() - 1) {
                    if (!rp700List2.get(i).getPeriod().equals(rp700List2.get(i + 1).getPeriod())) {
                        data.put(Integer.toString(ni++), new Object[]{"TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT-" + rp700List2.get(i).getSumTiab(), "TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT"});
                    }
                } else {
                    data.put(Integer.toString(ni++), new Object[]{"TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT-" + rp700List2.get(i).getSumTiab(), "TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT", "TOT"});
                }
            }

            //Set Column Width
            sheet.setColumnWidth(0, 3000);
            sheet.setColumnWidth(1, 2000);
            sheet.setColumnWidth(2, 5000);
            sheet.setColumnWidth(3, 5000);
            sheet.setColumnWidth(4, 5000);
            sheet.setColumnWidth(5, 5000);
            sheet.setColumnWidth(6, 5000);
            sheet.setColumnWidth(7, 5000);
            sheet.setColumnWidth(8, 5000);
            sheet.setColumnWidth(9, 6000);
            sheet.setColumnWidth(10, 6000);
            sheet.setColumnWidth(11, 6000);
            sheet.setColumnWidth(12, 6000);
            sheet.setColumnWidth(13, 6000);
            sheet.setColumnWidth(14, 6000);
            sheet.setColumnWidth(15, 6000);
            sheet.setColumnWidth(16, 6000);
            sheet.setColumnWidth(17, 5000);

            CellStyle styleF = workbook.createCellStyle();
            styleF.setAlignment(HorizontalAlignment.CENTER);
            styleF.setBorderRight(BorderStyle.THIN);
            styleF.setBorderLeft(BorderStyle.THIN);
//        styleF.setFillBackgroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
//        styleF.setFillPattern(FillPatternType.LESS_DOTS);

            CellStyle style = workbook.createCellStyle();
            style.setAlignment(HorizontalAlignment.CENTER);
            style.setBorderBottom(BorderStyle.THICK);
            style.setBorderRight(BorderStyle.THIN);
            style.setBorderLeft(BorderStyle.THIN);
//        style.setFillBackgroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
//        style.setFillPattern(FillPatternType.BIG_SPOTS);

            // Styling border and alignment of cell.  
            CellStyle style_ct = workbook.createCellStyle();
            style_ct.setAlignment(HorizontalAlignment.CENTER);

            // Styling border and alignment of cell.
            CellStyle style_rt = workbook.createCellStyle();
            style_rt.setAlignment(HorizontalAlignment.RIGHT);

            // Styling border and alignment of cell.  
            CellStyle style_bd = workbook.createCellStyle();
            style_bd.setBorderBottom(BorderStyle.THIN);
            style_bd.setBorderTop(BorderStyle.THIN);
            style_bd.setBorderRight(BorderStyle.THIN);
            style_bd.setBorderLeft(BorderStyle.THIN);

            // Styling border and alignment of cell.  
            CellStyle style_bd_u = workbook.createCellStyle();
            style_bd_u.setBorderBottom(BorderStyle.THICK);
            style_bd_u.setBorderTop(BorderStyle.THIN);
            style_bd_u.setBorderRight(BorderStyle.THIN);
            style_bd_u.setBorderLeft(BorderStyle.THIN);

            // Styling border and alignment of cell.  
            CellStyle style_bd_ct = workbook.createCellStyle();
            style_bd_ct.setAlignment(HorizontalAlignment.CENTER);
            style_bd_ct.setBorderBottom(BorderStyle.THIN);
            style_bd_ct.setBorderTop(BorderStyle.THIN);
            style_bd_ct.setBorderRight(BorderStyle.THIN);
            style_bd_ct.setBorderLeft(BorderStyle.THIN);

            // Styling border and alignment of cell.  
            CellStyle style_bd_rt = workbook.createCellStyle();
            style_bd_rt.setAlignment(HorizontalAlignment.RIGHT);
            style_bd_rt.setBorderBottom(BorderStyle.THIN);
            style_bd_rt.setBorderTop(BorderStyle.THIN);
            style_bd_rt.setBorderRight(BorderStyle.THIN);
            style_bd_rt.setBorderLeft(BorderStyle.THIN);

            // Styling border and alignment of cell.  
            CellStyle style_bd_rt_u = workbook.createCellStyle();
            style_bd_rt_u.setAlignment(HorizontalAlignment.RIGHT);
            style_bd_rt_u.setBorderBottom(BorderStyle.THICK);
            style_bd_rt_u.setBorderTop(BorderStyle.THIN);
            style_bd_rt_u.setBorderRight(BorderStyle.THIN);
            style_bd_rt_u.setBorderLeft(BorderStyle.THIN);
//        XSSFFont my_font = workbook.createFont();
//        my_font.setBold(true);
//        style_bd_rt_u.setFont(my_font);
//
            // Iterate over data and write to sheet 
            Set<String> keyset = data.keySet();
            int rownum = 0;
            for (String key : keyset) {
                // this creates a new row in the sheet 
                Row row = sheet.createRow(rownum++);
                Object[] objArr = data.get(key);
                int cellnum = 0;
                for (Object obj : objArr) {
                    // this line creates a cell in the next column of that row 
                    Cell cell = row.createCell(cellnum++);
                    cell.setCellValue((String) obj);

                    if (rownum == 1) {
                        cell.setCellStyle(styleF);

                    } else if (rownum == 2) {
                        cell.setCellStyle(style);

                    } else {
                        if (cellnum > 2) {
                            if (cell.getStringCellValue().equals("TOT")) {
                                cell.setCellValue("");
                                cell.setCellStyle(style_bd_rt_u);
                            } else if (cell.getStringCellValue().split("-")[0].equals("TOT")) {
                                cell.setCellValue(cell.getStringCellValue().split("-")[1]);
                                cell.setCellStyle(style_bd_rt_u);
                            } else {
                                cell.setCellStyle(style_bd_rt);
                            }

                        } else {
                            if (cell.getStringCellValue().equals("TOT")) {
                                cell.setCellValue("");
                                cell.setCellStyle(style_bd_u);
                            } else {
                                cell.setCellStyle(style_bd);
                            }
                        }
                    }
                }
            }

            try {
                ServletContext servletContextEX = getServletContext();    // new by ji
                String realPathEX = servletContextEX.getRealPath(FILE_PATH);        // new by ji
                String saperateEX = realPathEX.contains(":") ? "\\" : "/";
                String pathEX = realPathEX + saperateEX + "SUMMARY_PAYMENT_REPORT_" + year + "_" + imc + ".xlsx";

                FileOutputStream outputStream = new FileOutputStream(pathEX);
                workbook.write(outputStream);
                workbook.close();

//            ******************************************************
                File fileEX = new File(pathEX);
                int length = 0;
                ServletOutputStream outStream = response.getOutputStream();
                ServletContext context = getServletConfig().getServletContext();
                String mimetype = context.getMimeType(pathEX);

                // sets response content type
                if (mimetype == null) {
                    mimetype = "application/octet-stream";
                }
                response.setContentType(mimetype);
                response.setContentLength((int) fileEX.length());
                String fileNameEX = (new File(pathEX)).getName();

                // sets HTTP header
                response.setHeader("Content-Disposition", "attachment; filename=\"" + fileNameEX + "\"");

                byte[] byteBuffer = new byte[BUFSIZE];
                DataInputStream in = new DataInputStream(new FileInputStream(fileEX));

                // reads the file's bytes and writes them to the response stream
                while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                    outStream.write(byteBuffer, 0, length);
                }

                in.close();
                outStream.close();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        response.setHeader("Refresh", "0;/IMS2/IMS700/edit?ym=" + ym + "&imc=" + imc);
    }

}
