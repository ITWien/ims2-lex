/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSADETAILDao;
import com.twc.ims.dao.IMSBAMDao;
import com.twc.ims.dao.IMSCURDao;
import com.twc.ims.dao.IMSIMCDao;
import com.twc.ims.dao.IMSSDETAILDao;
import com.twc.ims.dao.IMSSHCDao;
import com.twc.ims.dao.IMSSHEADDao;
import com.twc.ims.dao.IMSVENDao;
import com.twc.ims.dao.QNBSERDao;
import com.twc.ims.entity.IMSAPDETAIL;
import com.twc.ims.entity.IMSPDETAIL;
import com.twc.ims.entity.IMSSDETAIL;
import com.twc.ims.entity.IMSSHC;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wien
 */
public class CreateControllerIMS30A extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/createIMS30A.jsp";
    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public CreateControllerIMS30A() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "IMS30A/C");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Import Payment. Entry");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        request.setAttribute("hide", " display: none;");

        IMSBAMDao dao4 = new IMSBAMDao();
        List<IMSSHC> bankList = dao4.findAllPay();
        request.setAttribute("bankList", bankList);

        IMSCURDao dao5 = new IMSCURDao();
        List<IMSSHC> ccyList = dao5.findAll();
        request.setAttribute("ccyList", ccyList);

        IMSVENDao dao6 = new IMSVENDao();
        List<IMSSHC> vendorList = dao6.findAll();
        request.setAttribute("vendorList", vendorList);

        IMSIMCDao dao7 = new IMSIMCDao();
        List<IMSSHC> pbList = dao7.findAll();
        request.setAttribute("pbList", pbList);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "IMS30A/C");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Import Payment. Entry");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        QNBSERDao dao = new QNBSERDao();
        String cur = dao.findCurrent("I3", "01");
        request.setAttribute("importPaidNo", cur);

        IMSBAMDao dao4 = new IMSBAMDao();
        List<IMSSHC> bankList = dao4.findAllPay();
        request.setAttribute("bankList", bankList);

        IMSCURDao dao5 = new IMSCURDao();
        List<IMSSHC> ccyList = dao5.findAll();
        request.setAttribute("ccyList", ccyList);

        IMSVENDao dao6 = new IMSVENDao();
        List<IMSSHC> vendorList = dao6.findAll();
        request.setAttribute("vendorList", vendorList);

        IMSIMCDao dao7 = new IMSIMCDao();
        List<IMSSHC> pbList = dao7.findAll();
        request.setAttribute("pbList", pbList);

        String totalEstPaidAmount = request.getParameter("totalEstPaidAmountTMP");
        String ccy = request.getParameter("ccyTMP");
        String paidBy = request.getParameter("paidByTMP");
        String paidDate = request.getParameter("paidDateTMP");
        String bank = request.getParameter("bankTMP");
        String vendor = request.getParameter("vendorTMP");
        String fee = request.getParameter("feeTMP");

        if (fee.trim().equals("") || fee.trim().equals("NaN")) {
            fee = "0.00";
        }

        if (vendor.contains(" : ")) {
            vendor = vendor.split(" : ")[0];
        }

        request.setAttribute("totalEstPaidAmount", totalEstPaidAmount);
        request.setAttribute("ccy", ccy);
        request.setAttribute("fee", fee);
        request.setAttribute("paidDate", paidDate);

        IMSBAMDao dao2 = new IMSBAMDao();
        IMSSHC Bank = dao2.findByCode(bank);

        IMSVENDao dao1 = new IMSVENDao();
        IMSSHC Vendor = dao1.findByCode(vendor);

        IMSIMCDao dao8 = new IMSIMCDao();
        IMSSHC pb = dao8.findByCode(paidBy);

        request.setAttribute("bankcode", Bank.getCode());
        request.setAttribute("bankname", Bank.getName());
        request.setAttribute("vendorcode", Vendor.getCode());
        request.setAttribute("vendorname", Vendor.getName());
        request.setAttribute("pbcode", pb.getCode());
        request.setAttribute("pbname", pb.getName());

        IMSSDETAILDao dao3 = new IMSSDETAILDao();
        List<IMSPDETAIL> detList = dao3.findByCode(vendor, ccy, paidBy);
        request.setAttribute("detList", detList);

        if (detList.isEmpty()) {
            request.setAttribute("hide", " display: none;");
        } else {
            request.setAttribute("show", " display: none;");
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

}
