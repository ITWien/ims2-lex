/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.controller;

import com.twc.ims.dao.IMSRP750Dao;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import com.twc.ims.dao.IMSRP740Dao;
import com.twc.ims.entity.IMSRP750;
import com.twc.ims.entity.IMSRP740;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;

/**
 *
 * @author wien
 */
public class PrintControllerIMS750 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String FILE_PATH = "/report/";
    private static final int BUFSIZE = 4096;
    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public PrintControllerIMS750() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String ym = request.getParameter("ym");
        String imc = request.getParameter("imc");

//        ***************EXCEL*************************************
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Summary Charge Amount");

        Map<String, Object[]> data = new TreeMap<String, Object[]>();

        String y = ym.substring(0, 4);
        String m = ym.substring(4);

        int ni = 101;

        data.put(Integer.toString(ni++), new Object[]{"PRODUCT", "MONTH", "VENDER COMPANY", "INVOICE NO.",
            "TERM", "CCY", "FOREIGN AMOUNT", "IMPORT CHARGE"});

        IMSRP750Dao daofind = new IMSRP750Dao();
        List<IMSRP750> rp750List = daofind.findByCodePT(ym, imc);

        for (int i = 0; i < rp750List.size(); i++) {
            data.put(Integer.toString(ni++), new Object[]{rp750List.get(i).getProd(),
                rp750List.get(i).getMonth(), rp750List.get(i).getCo(), rp750List.get(i).getInvNo(),
                rp750List.get(i).getTerm(), rp750List.get(i).getCcy(), rp750List.get(i).getAmount(),
                rp750List.get(i).getCharge()});
        }

        //Set Column Width
        sheet.setColumnWidth(0, 7000);
        sheet.setColumnWidth(1, 4000);
        sheet.setColumnWidth(2, 7000);
        sheet.setColumnWidth(3, 5000);
        sheet.setColumnWidth(4, 2000);
        sheet.setColumnWidth(5, 2000);
        sheet.setColumnWidth(6, 5000);
        sheet.setColumnWidth(7, 5000);

        XSSFFont my_font = workbook.createFont();
        my_font.setBold(true);

        // Iterate over data and write to sheet 
        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset) {
            // this creates a new row in the sheet 
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                // this line creates a cell in the next column of that row 
                Cell cell = row.createCell(cellnum++);
                cell.setCellValue((String) obj);

                if (rownum == 1) {
                    CellStyle style_tmp = workbook.createCellStyle();
                    style_tmp.setAlignment(HorizontalAlignment.CENTER);
                    style_tmp.setBorderBottom(BorderStyle.THIN);
                    style_tmp.setBorderTop(BorderStyle.THIN);
                    style_tmp.setBorderRight(BorderStyle.THIN);
                    style_tmp.setBorderLeft(BorderStyle.THIN);
                    style_tmp.setFont(my_font);
                    cell.setCellStyle(style_tmp);

                } else {
                    CellStyle style_tmp = workbook.createCellStyle();
                    if (cellnum >= 7) {
                        style_tmp.setAlignment(HorizontalAlignment.RIGHT);
                        if (cell.getStringCellValue().contains("+ULINE")) {
                            cell.setCellValue(cell.getStringCellValue().replace("+ULINE", ""));
                            style_tmp.setBorderBottom(BorderStyle.THIN);
                        }
                    } else {
                        if (cell.getStringCellValue().contains("+ULINE")) {
                            cell.setCellValue(cell.getStringCellValue().replace("+ULINE", ""));
                            style_tmp.setBorderBottom(BorderStyle.THIN);
                        }
                    }
                    cell.setCellStyle(style_tmp);

                }
            }
        }

        try {
            ServletContext servletContextEX = getServletContext();    // new by ji
            String realPathEX = servletContextEX.getRealPath(FILE_PATH);        // new by ji
            String saperateEX = realPathEX.contains(":") ? "\\" : "/";
            String pathEX = realPathEX + saperateEX + "SUMMARY_CHARGE_AMOUNT_" + ym + "_" + imc + ".xlsx";

            FileOutputStream outputStream = new FileOutputStream(pathEX);
            workbook.write(outputStream);
            workbook.close();

//            ******************************************************
            File fileEX = new File(pathEX);
            int length = 0;
            ServletOutputStream outStream = response.getOutputStream();
            ServletContext context = getServletConfig().getServletContext();
            String mimetype = context.getMimeType(pathEX);

            // sets response content type
            if (mimetype == null) {
                mimetype = "application/octet-stream";
            }
            response.setContentType(mimetype);
            response.setContentLength((int) fileEX.length());
            String fileNameEX = (new File(pathEX)).getName();

            // sets HTTP header
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileNameEX + "\"");

            byte[] byteBuffer = new byte[BUFSIZE];
            DataInputStream in = new DataInputStream(new FileInputStream(fileEX));

            // reads the file's bytes and writes them to the response stream
            while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                outStream.write(byteBuffer, 0, length);
            }

            in.close();
            outStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        response.setHeader("Refresh", "0;/IMS2/IMS750/edit?ym=" + ym + "&imc=" + imc);
    }

}
