/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSPHEAD {

    private String impPaidNo;
    private String vendor;
    private String ccy;
    private String fee;
    private String bank;
    private String paidDate;
    private String paidBy;
    private String totalEstPaidAmount;
    private String totalPaidAmount;
    private String remainingAmount;
    private List<IMSPDETAIL> detailList;

    public IMSPHEAD() {

    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getPaidBy() {
        return paidBy;
    }

    public void setPaidBy(String paidBy) {
        this.paidBy = paidBy;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getImpPaidNo() {
        return impPaidNo;
    }

    public void setImpPaidNo(String impPaidNo) {
        this.impPaidNo = impPaidNo;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(String paidDate) {
        this.paidDate = paidDate;
    }

    public String getTotalEstPaidAmount() {
        return totalEstPaidAmount;
    }

    public void setTotalEstPaidAmount(String totalEstPaidAmount) {
        this.totalEstPaidAmount = totalEstPaidAmount;
    }

    public String getTotalPaidAmount() {
        return totalPaidAmount;
    }

    public void setTotalPaidAmount(String totalPaidAmount) {
        this.totalPaidAmount = totalPaidAmount;
    }

    public String getRemainingAmount() {
        return remainingAmount;
    }

    public void setRemainingAmount(String remainingAmount) {
        this.remainingAmount = remainingAmount;
    }

    public List<IMSPDETAIL> getDetailList() {
        return detailList;
    }

    public void setDetailList(List<IMSPDETAIL> detailList) {
        this.detailList = detailList;
    }
}
