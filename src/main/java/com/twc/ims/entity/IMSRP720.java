/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSRP720 {

    private String period;
    private String prod;
    private String ccy;
    private String yam;
    private String forAmt;
    private String invAmt;
    private String freight;
    private String impDuty;
    private String vatAmt;
    private String totAmtEV;
    private String totAmtEVF;
    private String exp;

    private String FreightM;
    private String Insur;
    private String Rent;
    private String Clearing;
    private String Custfee;
    private String Custcash;
    private String Custvat;
    private String SumExp;
    private String SumExpCash;
    private String ExpN;
    private String Exp0;

    public IMSRP720() {

    }

    public String getYam() {
        return yam;
    }

    public void setYam(String yam) {
        this.yam = yam;
    }

    public String getProd() {
        return prod;
    }

    public void setProd(String prod) {
        this.prod = prod;
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getForAmt() {
        return forAmt;
    }

    public void setForAmt(String forAmt) {
        this.forAmt = forAmt;
    }

    public String getInvAmt() {
        return invAmt;
    }

    public void setInvAmt(String invAmt) {
        this.invAmt = invAmt;
    }

    public String getFreight() {
        return freight;
    }

    public void setFreight(String freight) {
        this.freight = freight;
    }

    public String getImpDuty() {
        return impDuty;
    }

    public void setImpDuty(String impDuty) {
        this.impDuty = impDuty;
    }

    public String getVatAmt() {
        return vatAmt;
    }

    public void setVatAmt(String vatAmt) {
        this.vatAmt = vatAmt;
    }

    public String getTotAmtEV() {
        return totAmtEV;
    }

    public void setTotAmtEV(String totAmtEV) {
        this.totAmtEV = totAmtEV;
    }

    public String getTotAmtEVF() {
        return totAmtEVF;
    }

    public void setTotAmtEVF(String totAmtEVF) {
        this.totAmtEVF = totAmtEVF;
    }

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

    public String getFreightM() {
        return FreightM;
    }

    public void setFreightM(String FreightM) {
        this.FreightM = FreightM;
    }

    public String getInsur() {
        return Insur;
    }

    public void setInsur(String Insur) {
        this.Insur = Insur;
    }

    public String getRent() {
        return Rent;
    }

    public void setRent(String Rent) {
        this.Rent = Rent;
    }

    public String getClearing() {
        return Clearing;
    }

    public void setClearing(String Clearing) {
        this.Clearing = Clearing;
    }

    public String getCustfee() {
        return Custfee;
    }

    public void setCustfee(String Custfee) {
        this.Custfee = Custfee;
    }

    public String getCustcash() {
        return Custcash;
    }

    public void setCustcash(String Custcash) {
        this.Custcash = Custcash;
    }

    public String getCustvat() {
        return Custvat;
    }

    public void setCustvat(String Custvat) {
        this.Custvat = Custvat;
    }

    public String getSumExp() {
        return SumExp;
    }

    public void setSumExp(String SumExp) {
        this.SumExp = SumExp;
    }

    public String getSumExpCash() {
        return SumExpCash;
    }

    public void setSumExpCash(String SumExpCash) {
        this.SumExpCash = SumExpCash;
    }

    public String getExpN() {
        return ExpN;
    }

    public void setExpN(String ExpN) {
        this.ExpN = ExpN;
    }

    public String getExp0() {
        return Exp0;
    }

    public void setExp0(String Exp0) {
        this.Exp0 = Exp0;
    }

}
