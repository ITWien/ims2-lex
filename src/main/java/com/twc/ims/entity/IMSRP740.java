/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSRP740 {

    private String period;
    private String impNo;
    private String venDept;
    private String venDeptName;
    private String invNo;
    private String prod;
    private String transport;
    private String invDate;
    private String arrDate;
    private String debitDate;

    public IMSRP740() {

    }

    public String getVenDeptName() {
        return venDeptName;
    }

    public void setVenDeptName(String venDeptName) {
        this.venDeptName = venDeptName;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getImpNo() {
        return impNo;
    }

    public void setImpNo(String impNo) {
        this.impNo = impNo;
    }

    public String getVenDept() {
        return venDept;
    }

    public void setVenDept(String venDept) {
        this.venDept = venDept;
    }

    public String getInvNo() {
        return invNo;
    }

    public void setInvNo(String invNo) {
        this.invNo = invNo;
    }

    public String getProd() {
        return prod;
    }

    public void setProd(String prod) {
        this.prod = prod;
    }

    public String getTransport() {
        return transport;
    }

    public void setTransport(String transport) {
        this.transport = transport;
    }

    public String getInvDate() {
        return invDate;
    }

    public void setInvDate(String invDate) {
        this.invDate = invDate;
    }

    public String getArrDate() {
        return arrDate;
    }

    public void setArrDate(String arrDate) {
        this.arrDate = arrDate;
    }

    public String getDebitDate() {
        return debitDate;
    }

    public void setDebitDate(String debitDate) {
        this.debitDate = debitDate;
    }

}
