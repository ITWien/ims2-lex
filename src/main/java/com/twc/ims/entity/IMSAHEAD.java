/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSAHEAD {

    private String advNo;
    private String dateF;
    private String dateT;
    private String shipCode;
    private String checkDate;
    private String amount;
    private String user;
    private String checkNo;
    private String bnkCode;
    private List<IMSSDETAIL> detailList;

    public IMSAHEAD() {

    }

    public List<IMSSDETAIL> getDetailList() {
        return detailList;
    }

    public void setDetailList(List<IMSSDETAIL> detailList) {
        this.detailList = detailList;
    }

    public String getCheckNo() {
        return checkNo;
    }

    public void setCheckNo(String checkNo) {
        this.checkNo = checkNo;
    }

    public String getBnkCode() {
        return bnkCode;
    }

    public void setBnkCode(String bnkCode) {
        this.bnkCode = bnkCode;
    }

    public String getDateF() {
        return dateF;
    }

    public void setDateF(String dateF) {
        this.dateF = dateF;
    }

    public String getDateT() {
        return dateT;
    }

    public void setDateT(String dateT) {
        this.dateT = dateT;
    }

    public String getAdvNo() {
        return advNo;
    }

    public void setAdvNo(String advNo) {
        this.advNo = advNo;
    }

    public String getShipCode() {
        return shipCode;
    }

    public void setShipCode(String shipCode) {
        this.shipCode = shipCode;
    }

    public String getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(String checkDate) {
        this.checkDate = checkDate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

}
