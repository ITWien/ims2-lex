/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSSHEAD {

    private String pimNo;
    private String impNo;
    private String imc;
    private String prv;
    private String ven;
    private String ship;
    private String user;
    private String imcD;
    private String prvD;
    private String venD;
    private String shipD;
    private String userD;
    private String awb;
    private String enno;
    private String dbno;
    private List<IMSSDETAIL> detailList;

    public IMSSHEAD() {

    }

    public IMSSHEAD(String pimNo, String impNo, String imc, String prv, String ven, String ship, String user, String imcD, String prvD, String venD, String shipD, String userD, String awb, String enno, String dbno, List<IMSSDETAIL> detailList) {
        this.pimNo = pimNo;
        this.impNo = impNo;
        this.imc = imc;
        this.prv = prv;
        this.ven = ven;
        this.ship = ship;
        this.user = user;
        this.imcD = imcD;
        this.prvD = prvD;
        this.venD = venD;
        this.shipD = shipD;
        this.userD = userD;
        this.awb = awb;
        this.enno = enno;
        this.dbno = dbno;
        this.detailList = detailList;
    }

    public String getPimNo() {
        return pimNo;
    }

    public void setPimNo(String pimNo) {
        this.pimNo = pimNo;
    }

    public String getImpNo() {
        return impNo;
    }

    public void setImpNo(String impNo) {
        this.impNo = impNo;
    }

    public String getImc() {
        return imc;
    }

    public void setImc(String imc) {
        this.imc = imc;
    }

    public String getPrv() {
        return prv;
    }

    public void setPrv(String prv) {
        this.prv = prv;
    }

    public String getVen() {
        return ven;
    }

    public void setVen(String ven) {
        this.ven = ven;
    }

    public String getShip() {
        return ship;
    }

    public void setShip(String ship) {
        this.ship = ship;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getImcD() {
        return imcD;
    }

    public void setImcD(String imcD) {
        this.imcD = imcD;
    }

    public String getPrvD() {
        return prvD;
    }

    public void setPrvD(String prvD) {
        this.prvD = prvD;
    }

    public String getVenD() {
        return venD;
    }

    public void setVenD(String venD) {
        this.venD = venD;
    }

    public String getShipD() {
        return shipD;
    }

    public void setShipD(String shipD) {
        this.shipD = shipD;
    }

    public String getUserD() {
        return userD;
    }

    public void setUserD(String userD) {
        this.userD = userD;
    }

    public String getAwb() {
        return awb;
    }

    public void setAwb(String awb) {
        this.awb = awb;
    }

    public String getEnno() {
        return enno;
    }

    public void setEnno(String enno) {
        this.enno = enno;
    }

    public String getDbno() {
        return dbno;
    }

    public void setDbno(String dbno) {
        this.dbno = dbno;
    }

    public List<IMSSDETAIL> getDetailList() {
        return detailList;
    }

    public void setDetailList(List<IMSSDETAIL> detailList) {
        this.detailList = detailList;
    }

}
