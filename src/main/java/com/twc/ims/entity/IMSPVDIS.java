/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSPVDIS {

    private String impPaidNo;
    private String line;
    private String famt;
    private String rate;
    private String amt;

    public IMSPVDIS() {

    }

    public String getImpPaidNo() {
        return impPaidNo;
    }

    public void setImpPaidNo(String impPaidNo) {
        this.impPaidNo = impPaidNo;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getFamt() {
        return famt;
    }

    public void setFamt(String famt) {
        this.famt = famt;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

}
