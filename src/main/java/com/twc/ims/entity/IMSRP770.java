/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSRP770 {

    private String IMSRPIMC;
    private String IMSRPPERIOD;
    private String IMSRPPIMPNO;
    private String GRPIMPNO;
    private String IMSRPIMPNO;
    private String IMSRPVENDEPT;
    private String IMSRPINVNO;
    private String IMSRPPROD;
    private String IMSRPEST;
    private String IMSRPINCOTERM;
    private String IMSRPCCY;
    private String IMSRPFAMT;
    private String IMSRPRATE;
    private String IMSRPAMT;
    private String IMSRPBOX;
    private String IMSRPCARRIER;
    private String IMSRPTRANSPORT;
    private String IMSRPINVDATE;
    private String IMSRPDEPARTDATE;
    private String IMSRPARRIVALDATE;
    private String IMSRPBL;
    private String IMSRPNOENTRY;
    private String IMSRPENTRYDATE;
    private String IMSRPLGAMT;
    private String IMSRPIMPDUTY;
    private String IMSRPDEBITNOTEDATE;
    private String IMSRPREMARK;
    private String transportCode;
    private String transportName;
    private String date;

    public IMSRP770() {

    }

    public String getTransportCode() {
        return transportCode;
    }

    public void setTransportCode(String transportCode) {
        this.transportCode = transportCode;
    }

    public String getTransportName() {
        return transportName;
    }

    public void setTransportName(String transportName) {
        this.transportName = transportName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getGRPIMPNO() {
        return GRPIMPNO;
    }

    public void setGRPIMPNO(String GRPIMPNO) {
        this.GRPIMPNO = GRPIMPNO;
    }

    public String getIMSRPIMC() {
        return IMSRPIMC;
    }

    public void setIMSRPIMC(String IMSRPIMC) {
        this.IMSRPIMC = IMSRPIMC;
    }

    public String getIMSRPPERIOD() {
        return IMSRPPERIOD;
    }

    public void setIMSRPPERIOD(String IMSRPPERIOD) {
        this.IMSRPPERIOD = IMSRPPERIOD;
    }

    public String getIMSRPPIMPNO() {
        return IMSRPPIMPNO;
    }

    public void setIMSRPPIMPNO(String IMSRPPIMPNO) {
        this.IMSRPPIMPNO = IMSRPPIMPNO;
    }

    public String getIMSRPIMPNO() {
        return IMSRPIMPNO;
    }

    public void setIMSRPIMPNO(String IMSRPIMPNO) {
        this.IMSRPIMPNO = IMSRPIMPNO;
    }

    public String getIMSRPVENDEPT() {
        return IMSRPVENDEPT;
    }

    public void setIMSRPVENDEPT(String IMSRPVENDEPT) {
        this.IMSRPVENDEPT = IMSRPVENDEPT;
    }

    public String getIMSRPINVNO() {
        return IMSRPINVNO;
    }

    public void setIMSRPINVNO(String IMSRPINVNO) {
        this.IMSRPINVNO = IMSRPINVNO;
    }

    public String getIMSRPPROD() {
        return IMSRPPROD;
    }

    public void setIMSRPPROD(String IMSRPPROD) {
        this.IMSRPPROD = IMSRPPROD;
    }

    public String getIMSRPEST() {
        return IMSRPEST;
    }

    public void setIMSRPEST(String IMSRPEST) {
        this.IMSRPEST = IMSRPEST;
    }

    public String getIMSRPINCOTERM() {
        return IMSRPINCOTERM;
    }

    public void setIMSRPINCOTERM(String IMSRPINCOTERM) {
        this.IMSRPINCOTERM = IMSRPINCOTERM;
    }

    public String getIMSRPCCY() {
        return IMSRPCCY;
    }

    public void setIMSRPCCY(String IMSRPCCY) {
        this.IMSRPCCY = IMSRPCCY;
    }

    public String getIMSRPFAMT() {
        return IMSRPFAMT;
    }

    public void setIMSRPFAMT(String IMSRPFAMT) {
        this.IMSRPFAMT = IMSRPFAMT;
    }

    public String getIMSRPRATE() {
        return IMSRPRATE;
    }

    public void setIMSRPRATE(String IMSRPRATE) {
        this.IMSRPRATE = IMSRPRATE;
    }

    public String getIMSRPAMT() {
        return IMSRPAMT;
    }

    public void setIMSRPAMT(String IMSRPAMT) {
        this.IMSRPAMT = IMSRPAMT;
    }

    public String getIMSRPBOX() {
        return IMSRPBOX;
    }

    public void setIMSRPBOX(String IMSRPBOX) {
        this.IMSRPBOX = IMSRPBOX;
    }

    public String getIMSRPCARRIER() {
        return IMSRPCARRIER;
    }

    public void setIMSRPCARRIER(String IMSRPCARRIER) {
        this.IMSRPCARRIER = IMSRPCARRIER;
    }

    public String getIMSRPTRANSPORT() {
        return IMSRPTRANSPORT;
    }

    public void setIMSRPTRANSPORT(String IMSRPTRANSPORT) {
        this.IMSRPTRANSPORT = IMSRPTRANSPORT;
    }

    public String getIMSRPINVDATE() {
        return IMSRPINVDATE;
    }

    public void setIMSRPINVDATE(String IMSRPINVDATE) {
        this.IMSRPINVDATE = IMSRPINVDATE;
    }

    public String getIMSRPDEPARTDATE() {
        return IMSRPDEPARTDATE;
    }

    public void setIMSRPDEPARTDATE(String IMSRPDEPARTDATE) {
        this.IMSRPDEPARTDATE = IMSRPDEPARTDATE;
    }

    public String getIMSRPARRIVALDATE() {
        return IMSRPARRIVALDATE;
    }

    public void setIMSRPARRIVALDATE(String IMSRPARRIVALDATE) {
        this.IMSRPARRIVALDATE = IMSRPARRIVALDATE;
    }

    public String getIMSRPBL() {
        return IMSRPBL;
    }

    public void setIMSRPBL(String IMSRPBL) {
        this.IMSRPBL = IMSRPBL;
    }

    public String getIMSRPNOENTRY() {
        return IMSRPNOENTRY;
    }

    public void setIMSRPNOENTRY(String IMSRPNOENTRY) {
        this.IMSRPNOENTRY = IMSRPNOENTRY;
    }

    public String getIMSRPENTRYDATE() {
        return IMSRPENTRYDATE;
    }

    public void setIMSRPENTRYDATE(String IMSRPENTRYDATE) {
        this.IMSRPENTRYDATE = IMSRPENTRYDATE;
    }

    public String getIMSRPLGAMT() {
        return IMSRPLGAMT;
    }

    public void setIMSRPLGAMT(String IMSRPLGAMT) {
        this.IMSRPLGAMT = IMSRPLGAMT;
    }

    public String getIMSRPIMPDUTY() {
        return IMSRPIMPDUTY;
    }

    public void setIMSRPIMPDUTY(String IMSRPIMPDUTY) {
        this.IMSRPIMPDUTY = IMSRPIMPDUTY;
    }

    public String getIMSRPDEBITNOTEDATE() {
        return IMSRPDEBITNOTEDATE;
    }

    public void setIMSRPDEBITNOTEDATE(String IMSRPDEBITNOTEDATE) {
        this.IMSRPDEBITNOTEDATE = IMSRPDEBITNOTEDATE;
    }

    public String getIMSRPREMARK() {
        return IMSRPREMARK;
    }

    public void setIMSRPREMARK(String IMSRPREMARK) {
        this.IMSRPREMARK = IMSRPREMARK;
    }

}
