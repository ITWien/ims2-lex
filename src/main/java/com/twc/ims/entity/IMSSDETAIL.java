/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSSDETAIL {

    private String advImpNo;
    private String no;
    private String pImpNo;
    private String invNoList;
    private String transDate;
    private String invAmt;
    private String taxDutyP;
    private String advImpAmt;
    private String taxDuty;
    private String TaxBase;
    private String vat7;
    private String totAmt;
    private String checkBox;
    private String invNo;
    private String sts100;
    private String sts101;
    private String sts110;
    private String sts200;
    private String sts300;
    private String sts301;
    private String imcD;
    private String prod;

    public IMSSDETAIL() {

    }

    public String getProd() {
        return prod;
    }

    public void setProd(String prod) {
        this.prod = prod;
    }

    public String getImcD() {
        return imcD;
    }

    public void setImcD(String imcD) {
        this.imcD = imcD;
    }

    public String getInvNo() {
        return invNo;
    }

    public void setInvNo(String invNo) {
        this.invNo = invNo;
    }

    public String getSts100() {
        return sts100;
    }

    public void setSts100(String sts100) {
        this.sts100 = sts100;
    }

    public String getSts101() {
        return sts101;
    }

    public void setSts101(String sts101) {
        this.sts101 = sts101;
    }

    public String getSts110() {
        return sts110;
    }

    public void setSts110(String sts110) {
        this.sts110 = sts110;
    }

    public String getSts200() {
        return sts200;
    }

    public void setSts200(String sts200) {
        this.sts200 = sts200;
    }

    public String getSts300() {
        return sts300;
    }

    public void setSts300(String sts300) {
        this.sts300 = sts300;
    }

    public String getSts301() {
        return sts301;
    }

    public void setSts301(String sts301) {
        this.sts301 = sts301;
    }

    public String getCheckBox() {
        return checkBox;
    }

    public void setCheckBox(String checkBox) {
        this.checkBox = checkBox;
    }

    public String getAdvImpNo() {
        return advImpNo;
    }

    public void setAdvImpNo(String advImpNo) {
        this.advImpNo = advImpNo;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getpImpNo() {
        return pImpNo;
    }

    public void setpImpNo(String pImpNo) {
        this.pImpNo = pImpNo;
    }

    public String getInvNoList() {
        return invNoList;
    }

    public void setInvNoList(String invNoList) {
        this.invNoList = invNoList;
    }

    public String getTransDate() {
        return transDate;
    }

    public void setTransDate(String transDate) {
        this.transDate = transDate;
    }

    public String getInvAmt() {
        return invAmt;
    }

    public void setInvAmt(String invAmt) {
        this.invAmt = invAmt;
    }

    public String getTaxDutyP() {
        return taxDutyP;
    }

    public void setTaxDutyP(String taxDutyP) {
        this.taxDutyP = taxDutyP;
    }

    public String getAdvImpAmt() {
        return advImpAmt;
    }

    public void setAdvImpAmt(String advImpAmt) {
        this.advImpAmt = advImpAmt;
    }

    public String getTaxDuty() {
        return taxDuty;
    }

    public void setTaxDuty(String taxDuty) {
        this.taxDuty = taxDuty;
    }

    public String getTaxBase() {
        return TaxBase;
    }

    public void setTaxBase(String TaxBase) {
        this.TaxBase = TaxBase;
    }

    public String getVat7() {
        return vat7;
    }

    public void setVat7(String vat7) {
        this.vat7 = vat7;
    }

    public String getTotAmt() {
        return totAmt;
    }

    public void setTotAmt(String totAmt) {
        this.totAmt = totAmt;
    }

}
