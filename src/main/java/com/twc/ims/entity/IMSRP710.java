/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSRP710 {

    private String period;
    private String dept;
    private String bank;
    private String payDate;
    private String ccy;
    private String payRate;
    private String impRate;
    private String forAmt;
    private String invAmt;
    private String payAmt;
    private String diffAmt;

    public IMSRP710() {

    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getPayDate() {
        return payDate;
    }

    public void setPayDate(String payDate) {
        this.payDate = payDate;
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getPayRate() {
        return payRate;
    }

    public void setPayRate(String payRate) {
        this.payRate = payRate;
    }

    public String getImpRate() {
        return impRate;
    }

    public void setImpRate(String impRate) {
        this.impRate = impRate;
    }

    public String getForAmt() {
        return forAmt;
    }

    public void setForAmt(String forAmt) {
        this.forAmt = forAmt;
    }

    public String getInvAmt() {
        return invAmt;
    }

    public void setInvAmt(String invAmt) {
        this.invAmt = invAmt;
    }

    public String getPayAmt() {
        return payAmt;
    }

    public void setPayAmt(String payAmt) {
        this.payAmt = payAmt;
    }

    public String getDiffAmt() {
        return diffAmt;
    }

    public void setDiffAmt(String diffAmt) {
        this.diffAmt = diffAmt;
    }

}
