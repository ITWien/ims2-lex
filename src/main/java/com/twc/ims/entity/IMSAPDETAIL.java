/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSAPDETAIL {

    private String pimno;
    private String no;
    private String checkNo;
    private String bank;
    private String checkDate;
    private String advance;
    private String estAdv;
    private String dnNo;
    private String dnDate;
    private String invNoList;
    private String billAmt;
    private String tax3;
    private String tax1;
    private String amount;
    private String totAdv;
    private String balance;
    private String checkBox;
    private String checkNoD;
    private String bankD;
    private String checkDateD;
    private String advanceD;
    private String imcD;
    private String sumAdv;
    private String sumEstAdv;
    private String sumBillAmt;
    private String sumTax3;
    private String sumTax1;
    private String sumAmt;
    private String sumTotAdv;
    private String sumBal;
    private String dateTime;
    private String IMSADCOM;
    private String IMSADPDNO;
    private String IMSADLINO;
    private String IMSADCQNO;
    private String IMSADCQDT;
    private String IMSADBCODE;
    private String IMSADADAMT;
    private String IMSADESAMT;
    private String IMSADDNNO;
    private String IMSADDNDT;
    private String IMSADINVNO;
    private String IMSADBLAMT;
    private String IMSADTX3AMT;
    private String IMSADTX1AMT;
    private String IMSADNETAMT;
    private String IMSADTNTAMT;
    private String IMSADBALAMT;
    private String IMSADEDT;
    private String IMSADCDT;
    private String IMSADUSER;
    private String IMSADIMC;
    private String INDB;

    public String getINDB() {
        return INDB;
    }

    public void setINDB(String INDB) {
        this.INDB = INDB;
    }

    public IMSAPDETAIL() {

    }

    public String getIMSADCOM() {
        return IMSADCOM;
    }

    public void setIMSADCOM(String IMSADCOM) {
        this.IMSADCOM = IMSADCOM;
    }

    public String getIMSADPDNO() {
        return IMSADPDNO;
    }

    public void setIMSADPDNO(String IMSADPDNO) {
        this.IMSADPDNO = IMSADPDNO;
    }

    public String getIMSADLINO() {
        return IMSADLINO;
    }

    public void setIMSADLINO(String IMSADLINO) {
        this.IMSADLINO = IMSADLINO;
    }

    public String getIMSADCQNO() {
        return IMSADCQNO;
    }

    public void setIMSADCQNO(String IMSADCQNO) {
        this.IMSADCQNO = IMSADCQNO;
    }

    public String getIMSADCQDT() {
        return IMSADCQDT;
    }

    public void setIMSADCQDT(String IMSADCQDT) {
        this.IMSADCQDT = IMSADCQDT;
    }

    public String getIMSADBCODE() {
        return IMSADBCODE;
    }

    public void setIMSADBCODE(String IMSADBCODE) {
        this.IMSADBCODE = IMSADBCODE;
    }

    public String getIMSADADAMT() {
        return IMSADADAMT;
    }

    public void setIMSADADAMT(String IMSADADAMT) {
        this.IMSADADAMT = IMSADADAMT;
    }

    public String getIMSADESAMT() {
        return IMSADESAMT;
    }

    public void setIMSADESAMT(String IMSADESAMT) {
        this.IMSADESAMT = IMSADESAMT;
    }

    public String getIMSADDNNO() {
        return IMSADDNNO;
    }

    public void setIMSADDNNO(String IMSADDNNO) {
        this.IMSADDNNO = IMSADDNNO;
    }

    public String getIMSADDNDT() {
        return IMSADDNDT;
    }

    public void setIMSADDNDT(String IMSADDNDT) {
        this.IMSADDNDT = IMSADDNDT;
    }

    public String getIMSADINVNO() {
        return IMSADINVNO;
    }

    public void setIMSADINVNO(String IMSADINVNO) {
        this.IMSADINVNO = IMSADINVNO;
    }

    public String getIMSADBLAMT() {
        return IMSADBLAMT;
    }

    public void setIMSADBLAMT(String IMSADBLAMT) {
        this.IMSADBLAMT = IMSADBLAMT;
    }

    public String getIMSADTX3AMT() {
        return IMSADTX3AMT;
    }

    public void setIMSADTX3AMT(String IMSADTX3AMT) {
        this.IMSADTX3AMT = IMSADTX3AMT;
    }

    public String getIMSADTX1AMT() {
        return IMSADTX1AMT;
    }

    public void setIMSADTX1AMT(String IMSADTX1AMT) {
        this.IMSADTX1AMT = IMSADTX1AMT;
    }

    public String getIMSADNETAMT() {
        return IMSADNETAMT;
    }

    public void setIMSADNETAMT(String IMSADNETAMT) {
        this.IMSADNETAMT = IMSADNETAMT;
    }

    public String getIMSADTNTAMT() {
        return IMSADTNTAMT;
    }

    public void setIMSADTNTAMT(String IMSADTNTAMT) {
        this.IMSADTNTAMT = IMSADTNTAMT;
    }

    public String getIMSADBALAMT() {
        return IMSADBALAMT;
    }

    public void setIMSADBALAMT(String IMSADBALAMT) {
        this.IMSADBALAMT = IMSADBALAMT;
    }

    public String getIMSADEDT() {
        return IMSADEDT;
    }

    public void setIMSADEDT(String IMSADEDT) {
        this.IMSADEDT = IMSADEDT;
    }

    public String getIMSADCDT() {
        return IMSADCDT;
    }

    public void setIMSADCDT(String IMSADCDT) {
        this.IMSADCDT = IMSADCDT;
    }

    public String getIMSADUSER() {
        return IMSADUSER;
    }

    public void setIMSADUSER(String IMSADUSER) {
        this.IMSADUSER = IMSADUSER;
    }

    public String getIMSADIMC() {
        return IMSADIMC;
    }

    public void setIMSADIMC(String IMSADIMC) {
        this.IMSADIMC = IMSADIMC;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getSumEstAdv() {
        return sumEstAdv;
    }

    public void setSumEstAdv(String sumEstAdv) {
        this.sumEstAdv = sumEstAdv;
    }

    public String getSumBillAmt() {
        return sumBillAmt;
    }

    public void setSumBillAmt(String sumBillAmt) {
        this.sumBillAmt = sumBillAmt;
    }

    public String getSumTax3() {
        return sumTax3;
    }

    public void setSumTax3(String sumTax3) {
        this.sumTax3 = sumTax3;
    }

    public String getSumTax1() {
        return sumTax1;
    }

    public void setSumTax1(String sumTax1) {
        this.sumTax1 = sumTax1;
    }

    public String getSumAmt() {
        return sumAmt;
    }

    public void setSumAmt(String sumAmt) {
        this.sumAmt = sumAmt;
    }

    public String getSumTotAdv() {
        return sumTotAdv;
    }

    public void setSumTotAdv(String sumTotAdv) {
        this.sumTotAdv = sumTotAdv;
    }

    public String getSumBal() {
        return sumBal;
    }

    public void setSumBal(String sumBal) {
        this.sumBal = sumBal;
    }

    public String getSumAdv() {
        return sumAdv;
    }

    public void setSumAdv(String sumAdv) {
        this.sumAdv = sumAdv;
    }

    public String getImcD() {
        return imcD;
    }

    public void setImcD(String imcD) {
        this.imcD = imcD;
    }

    public String getPimno() {
        return pimno;
    }

    public void setPimno(String pimno) {
        this.pimno = pimno;
    }

    public String getCheckNoD() {
        return checkNoD;
    }

    public void setCheckNoD(String checkNoD) {
        this.checkNoD = checkNoD;
    }

    public String getBankD() {
        return bankD;
    }

    public void setBankD(String bankD) {
        this.bankD = bankD;
    }

    public String getCheckDateD() {
        return checkDateD;
    }

    public void setCheckDateD(String checkDateD) {
        this.checkDateD = checkDateD;
    }

    public String getAdvanceD() {
        return advanceD;
    }

    public void setAdvanceD(String advanceD) {
        this.advanceD = advanceD;
    }

    public String getCheckBox() {
        return checkBox;
    }

    public void setCheckBox(String checkBox) {
        this.checkBox = checkBox;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getCheckNo() {
        return checkNo;
    }

    public void setCheckNo(String checkNo) {
        this.checkNo = checkNo;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(String checkDate) {
        this.checkDate = checkDate;
    }

    public String getAdvance() {
        return advance;
    }

    public void setAdvance(String advance) {
        this.advance = advance;
    }

    public String getEstAdv() {
        return estAdv;
    }

    public void setEstAdv(String estAdv) {
        this.estAdv = estAdv;
    }

    public String getDnNo() {
        return dnNo;
    }

    public void setDnNo(String dnNo) {
        this.dnNo = dnNo;
    }

    public String getDnDate() {
        return dnDate;
    }

    public void setDnDate(String dnDate) {
        this.dnDate = dnDate;
    }

    public String getInvNoList() {
        return invNoList;
    }

    public void setInvNoList(String invNoList) {
        this.invNoList = invNoList;
    }

    public String getBillAmt() {
        return billAmt;
    }

    public void setBillAmt(String billAmt) {
        this.billAmt = billAmt;
    }

    public String getTax3() {
        return tax3;
    }

    public void setTax3(String tax3) {
        this.tax3 = tax3;
    }

    public String getTax1() {
        return tax1;
    }

    public void setTax1(String tax1) {
        this.tax1 = tax1;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTotAdv() {
        return totAdv;
    }

    public void setTotAdv(String totAdv) {
        this.totAdv = totAdv;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

}
