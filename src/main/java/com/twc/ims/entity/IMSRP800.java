/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSRP800 {

    private String IMSRPIMC;
    private String IMSRPPERIOD;
    private String IMSRPPIMPNO;
    private String IMSRPIMPNO;
    private String IMSRPVENDEPT;
    private String IMSRPINVNO;
    private String IMSRPORDERNO;
    private String IMSRPPROD;
    private String IMSRPCCY;
    private String IMSRPFAMT;
    private String IMSRPAMT;
    private String IMSRPENTNO;
    private String IMSRPENTDATE;
    private String IMSRPLGGIDNO;
    private String IMSRPLGAMT;
    private String IMSRPLGFEE;
    private String IMSRPTAXNO;

    public IMSRP800() {

    }

    public String getIMSRPIMC() {
        return IMSRPIMC;
    }

    public void setIMSRPIMC(String IMSRPIMC) {
        this.IMSRPIMC = IMSRPIMC;
    }

    public String getIMSRPPERIOD() {
        return IMSRPPERIOD;
    }

    public void setIMSRPPERIOD(String IMSRPPERIOD) {
        this.IMSRPPERIOD = IMSRPPERIOD;
    }

    public String getIMSRPPIMPNO() {
        return IMSRPPIMPNO;
    }

    public void setIMSRPPIMPNO(String IMSRPPIMPNO) {
        this.IMSRPPIMPNO = IMSRPPIMPNO;
    }

    public String getIMSRPIMPNO() {
        return IMSRPIMPNO;
    }

    public void setIMSRPIMPNO(String IMSRPIMPNO) {
        this.IMSRPIMPNO = IMSRPIMPNO;
    }

    public String getIMSRPVENDEPT() {
        return IMSRPVENDEPT;
    }

    public void setIMSRPVENDEPT(String IMSRPVENDEPT) {
        this.IMSRPVENDEPT = IMSRPVENDEPT;
    }

    public String getIMSRPINVNO() {
        return IMSRPINVNO;
    }

    public void setIMSRPINVNO(String IMSRPINVNO) {
        this.IMSRPINVNO = IMSRPINVNO;
    }

    public String getIMSRPORDERNO() {
        return IMSRPORDERNO;
    }

    public void setIMSRPORDERNO(String IMSRPORDERNO) {
        this.IMSRPORDERNO = IMSRPORDERNO;
    }

    public String getIMSRPPROD() {
        return IMSRPPROD;
    }

    public void setIMSRPPROD(String IMSRPPROD) {
        this.IMSRPPROD = IMSRPPROD;
    }

    public String getIMSRPCCY() {
        return IMSRPCCY;
    }

    public void setIMSRPCCY(String IMSRPCCY) {
        this.IMSRPCCY = IMSRPCCY;
    }

    public String getIMSRPFAMT() {
        return IMSRPFAMT;
    }

    public void setIMSRPFAMT(String IMSRPFAMT) {
        this.IMSRPFAMT = IMSRPFAMT;
    }

    public String getIMSRPAMT() {
        return IMSRPAMT;
    }

    public void setIMSRPAMT(String IMSRPAMT) {
        this.IMSRPAMT = IMSRPAMT;
    }

    public String getIMSRPENTNO() {
        return IMSRPENTNO;
    }

    public void setIMSRPENTNO(String IMSRPENTNO) {
        this.IMSRPENTNO = IMSRPENTNO;
    }

    public String getIMSRPENTDATE() {
        return IMSRPENTDATE;
    }

    public void setIMSRPENTDATE(String IMSRPENTDATE) {
        this.IMSRPENTDATE = IMSRPENTDATE;
    }

    public String getIMSRPLGGIDNO() {
        return IMSRPLGGIDNO;
    }

    public void setIMSRPLGGIDNO(String IMSRPLGGIDNO) {
        this.IMSRPLGGIDNO = IMSRPLGGIDNO;
    }

    public String getIMSRPLGAMT() {
        return IMSRPLGAMT;
    }

    public void setIMSRPLGAMT(String IMSRPLGAMT) {
        this.IMSRPLGAMT = IMSRPLGAMT;
    }

    public String getIMSRPLGFEE() {
        return IMSRPLGFEE;
    }

    public void setIMSRPLGFEE(String IMSRPLGFEE) {
        this.IMSRPLGFEE = IMSRPLGFEE;
    }

    public String getIMSRPTAXNO() {
        return IMSRPTAXNO;
    }

    public void setIMSRPTAXNO(String IMSRPTAXNO) {
        this.IMSRPTAXNO = IMSRPTAXNO;
    }

}
