/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSRP700 {

    private String period;
    private String ccy;
    private String macImpAmt;
    private String matImpAmt;
    private String totalImpAmt;
    private String rate;
    private String totalImpAmtB;
    private String balAmtPrevMonth;
    private String accInvAmt;
    private String allBankBalPayment;
    private String sumitomo;
    private String hsb;
    private String scb;
    private String bbl;
    private String mizuho;
    private String k_tokyo;
    private String total;
    private String noPaymentAmt;
    private String sumTiab;

    public IMSRP700() {

    }

    public String getSumTiab() {
        return sumTiab;
    }

    public void setSumTiab(String sumTiab) {
        this.sumTiab = sumTiab;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getMacImpAmt() {
        return macImpAmt;
    }

    public void setMacImpAmt(String macImpAmt) {
        this.macImpAmt = macImpAmt;
    }

    public String getMatImpAmt() {
        return matImpAmt;
    }

    public void setMatImpAmt(String matImpAmt) {
        this.matImpAmt = matImpAmt;
    }

    public String getTotalImpAmt() {
        return totalImpAmt;
    }

    public void setTotalImpAmt(String totalImpAmt) {
        this.totalImpAmt = totalImpAmt;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getTotalImpAmtB() {
        return totalImpAmtB;
    }

    public void setTotalImpAmtB(String totalImpAmtB) {
        this.totalImpAmtB = totalImpAmtB;
    }

    public String getBalAmtPrevMonth() {
        return balAmtPrevMonth;
    }

    public void setBalAmtPrevMonth(String balAmtPrevMonth) {
        this.balAmtPrevMonth = balAmtPrevMonth;
    }

    public String getAccInvAmt() {
        return accInvAmt;
    }

    public void setAccInvAmt(String accInvAmt) {
        this.accInvAmt = accInvAmt;
    }

    public String getAllBankBalPayment() {
        return allBankBalPayment;
    }

    public void setAllBankBalPayment(String allBankBalPayment) {
        this.allBankBalPayment = allBankBalPayment;
    }

    public String getSumitomo() {
        return sumitomo;
    }

    public void setSumitomo(String sumitomo) {
        this.sumitomo = sumitomo;
    }

    public String getHsb() {
        return hsb;
    }

    public void setHsb(String hsb) {
        this.hsb = hsb;
    }

    public String getScb() {
        return scb;
    }

    public void setScb(String scb) {
        this.scb = scb;
    }

    public String getBbl() {
        return bbl;
    }

    public void setBbl(String bbl) {
        this.bbl = bbl;
    }

    public String getMizuho() {
        return mizuho;
    }

    public void setMizuho(String mizuho) {
        this.mizuho = mizuho;
    }

    public String getK_tokyo() {
        return k_tokyo;
    }

    public void setK_tokyo(String k_tokyo) {
        this.k_tokyo = k_tokyo;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getNoPaymentAmt() {
        return noPaymentAmt;
    }

    public void setNoPaymentAmt(String noPaymentAmt) {
        this.noPaymentAmt = noPaymentAmt;
    }

}
