/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.dao;

import com.twc.ims.database.database;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class QNBSERDao extends database {

    public String findCurrent(String type, String group) {

        String cur = "";

        String sql = "SELECT TOP 1 RIGHT('000'+CAST((QNBLAST + 1) AS VARCHAR(3)),3) AS QNBLAST, QNBYEAR "
                + "FROM QNBSER "
                + "WHERE QNBTYPE = '" + type + "' "
                + "AND QNBGROUP = '" + group + "' "
                + "AND [QNBYEAR] = format(CURRENT_TIMESTAMP,'yyyy') "
                + "ORDER BY QNBYEAR DESC";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                cur = result.getString("QNBYEAR") + result.getString("QNBLAST");
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return cur;

    }

    public boolean editCurrent(String runno, String year, String type, String group) {

        boolean result = false;

        String sql = "UPDATE QNBSER "
                + "SET QNBLAST = '" + Integer.toString(Integer.parseInt(runno)) + "', QNBEDT = CURRENT_TIMESTAMP "
                + "WHERE QNBTYPE = '" + type + "' "
                + "AND QNBGROUP = '" + group + "' "
                + "AND QNBYEAR = '" + year + "' ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

}
