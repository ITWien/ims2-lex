/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.dao;

import com.twc.ims.database.database;
import com.twc.ims.entity.IMSAHEAD;
import com.twc.ims.entity.IMSPDETAIL;
import com.twc.ims.entity.IMSSDETAIL;
import com.twc.ims.entity.IMSSHC;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSSDETAILDao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public List<IMSSDETAIL> findSDetail(String pimno, String sortBy, String data) {

        List<IMSSDETAIL> UAList = new ArrayList<IMSSDETAIL>();

        String con = "";

        if (sortBy.equals("venDep")) {
            con = "AND IMSSDVCODE = '" + data + "'\n";

        } else if (sortBy.equals("proGrp")) {
            con = "AND IMSSDPROD = '" + data + "'\n";

        } else if (sortBy.equals("sts")) {
            if (data.equals("IMS100")) {
                con = "AND IMSSDSTS100 = 'Y'\n";

            } else if (data.equals("IMS101")) {
                con = "AND IMSSDSTS101 = 'Y'\n";

            } else if (data.equals("IMS110")) {
                con = "AND IMSSDSTS110 = 'Y'\n";

            } else if (data.equals("IMS200")) {
                con = "AND IMSSDSTS200 = 'Y'\n";

            } else if (data.equals("IMS300")) {
                con = "AND IMSSDSTS300 = 'Y'\n";

            } else if (data.equals("IMS301")) {
                con = "AND IMSSDSTS301 = 'Y'\n";

            }

        }

        String sql = "SELECT IMSSDPIMNO, IMSSDINVNO, IMSSDPROD,\n"
                + "ISNULL(IMSSDSTS100,'N') AS IMSSDSTS100, \n"
                + "ISNULL(IMSSDSTS101,'N') AS IMSSDSTS101, \n"
                + "ISNULL(IMSSDSTS110,'N') AS IMSSDSTS110, \n"
                + "ISNULL(IMSSDSTS200,'N') AS IMSSDSTS200, \n"
                + "ISNULL(IMSSDSTS300,'N') AS IMSSDSTS300, \n"
                + "ISNULL(IMSSDSTS301,'N') AS IMSSDSTS301\n"
                + "FROM IMSSDETAIL\n"
                + "WHERE IMSSDPIMNO = '" + pimno + "'\n"
                + con;
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSSDETAIL p = new IMSSDETAIL();

                p.setpImpNo(result.getString("IMSSDPIMNO"));
                p.setInvNo(result.getString("IMSSDINVNO"));
                p.setProd(result.getString("IMSSDPROD"));

                p.setSts100(result.getString("IMSSDSTS100").equals("N") ? "display: none;" : "");
                p.setSts101(result.getString("IMSSDSTS101").equals("N") ? "display: none;" : "");
                p.setSts110(result.getString("IMSSDSTS110").equals("N") ? "display: none;" : "");
                p.setSts200(result.getString("IMSSDSTS200").equals("N") ? "display: none;" : "");
                p.setSts300(result.getString("IMSSDSTS300").equals("N") ? "display: none;" : "");
                p.setSts301(result.getString("IMSSDSTS301").equals("N") ? "display: none;" : "");

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<IMSSHC> findProGrpIMS900(String ym, String ym2) {

        List<IMSSHC> UAList = new ArrayList<IMSSHC>();

        String sql = "SELECT DISTINCT IMSSDPROD, IMSGPDESC\n"
                + "FROM IMSSDETAIL\n"
                + "LEFT JOIN IMSGRP ON IMSGRP.IMSGPCODE = IMSSDETAIL.IMSSDPROD\n"
                + "LEFT JOIN IMSSHEAD ON IMSSHEAD.IMSHPIMNO = IMSSDETAIL.IMSSDPIMNO\n"
                + "WHERE FORMAT(IMSHTRDT,'yyyyMM') BETWEEN '" + ym + "' AND '" + ym2 + "'\n";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSSHC p = new IMSSHC();

                p.setCode(result.getString("IMSSDPROD"));

                p.setName(result.getString("IMSGPDESC"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<IMSSHC> findVenDepIMS900(String ym, String ym2) {

        List<IMSSHC> UAList = new ArrayList<IMSSHC>();

        String sql = "SELECT DISTINCT IMSSDVCODE, IMSVDNAME\n"
                + "FROM IMSSDETAIL\n"
                + "LEFT JOIN IMSVEN ON IMSVEN.IMSVDCODE = IMSSDETAIL.IMSSDVCODE\n"
                + "LEFT JOIN IMSSHEAD ON IMSSHEAD.IMSHPIMNO = IMSSDETAIL.IMSSDPIMNO\n"
                + "WHERE FORMAT(IMSHTRDT,'yyyyMM') BETWEEN '" + ym + "' AND '" + ym2 + "'\n";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSSHC p = new IMSSHC();

                p.setCode(result.getString("IMSSDVCODE"));

                p.setName(result.getString("IMSVDNAME"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public boolean setStatus(String STS, String pImpNo, String invNo, String sts) {

        boolean result = false;

        String sql = "UPDATE IMSSDETAIL "
                + "SET " + STS + " = " + sts + ", IMSSDEDT = CURRENT_TIMESTAMP "
                + "WHERE IMSSDPIMNO = '" + pImpNo + "' "
                + "AND IMSSDINVNO = '" + invNo + "' ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }
        return result;

    }

    public boolean clearPaidNo(String importPaidNo) {

        boolean result = false;

        String sql = "UPDATE IMSSDETAIL "
                + "SET IMSSDPDNO = null, IMSSDSTS300 = null, IMSSDSTS301 = null, IMSSDEDT = CURRENT_TIMESTAMP "
                + "WHERE IMSSDPDNO = '" + importPaidNo + "' ";
//                + "AND IMSSDVCODE = '" + vcode + "' ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean editPaidNo(String importPaidNo, String imno, String line) {

        boolean result = false;

        String sql = "UPDATE IMSSDETAIL "
                + "SET IMSSDPDNO = '" + importPaidNo + "', IMSSDEDT = CURRENT_TIMESTAMP "
                + "WHERE IMSSDPIMNO = '" + imno + "' "
                + "AND IMSSDLINE = '" + line + "' ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean editPaidNo2(String importPaidNo, String imno, String inv) {

        boolean result = false;

        String sql = "UPDATE IMSSDETAIL "
                + "SET IMSSDPDNO = '" + importPaidNo + "', IMSSDEDT = CURRENT_TIMESTAMP "
                + "WHERE IMSSDPIMNO = '" + imno + "' "
                + "AND IMSSDINVNO = '" + inv + "' ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public List<IMSPDETAIL> findByCode(String vendor, String ccy, String paidBy) {

        List<IMSPDETAIL> UAList = new ArrayList<IMSPDETAIL>();

        String VEN = "IMSSDVCODE";

        if (vendor.trim().equals("X0100001") || vendor.trim().equals("X0300002")) {
            VEN = "IMSHVDCODE";
        }

        String sql = "SELECT IMSHVDCODE, IMSSDVCODE, IMSSDPIMNO, IMSSDLINE, IMSSDINVNO, IMSSDCUR, IMSSDPROD, IMSSDINVAMT, IMSSDFINVAMT, IMSHIMC\n"
                + "FROM IMSSDETAIL\n"
                + "LEFT JOIN IMSSHEAD ON IMSSHEAD.IMSHPIMNO = IMSSDETAIL.IMSSDPIMNO\n"
                + "WHERE " + VEN + " = '" + vendor + "'\n"
                + "AND IMSSDCUR = '" + ccy + "'\n"
                + "AND IMSHIMC = '" + paidBy + "'\n"
                + "AND (IMSSDPDNO IS NULL OR IMSSDPDNO = '')\n"
                + "AND isnull(IMSSDINVAMT,0) != 0\n"
                + "ORDER BY IMSSDPIMNO";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String impNo = "";

            while (result.next()) {

                IMSPDETAIL p = new IMSPDETAIL();

                if (impNo.equals("")) {
                    p.setImportNo("<b class=\"impBold\">" + result.getString("IMSSDPIMNO") + "</b>");
                    impNo = result.getString("IMSSDPIMNO");
                } else {
                    if (!impNo.equals(result.getString("IMSSDPIMNO"))) {
                        p.setImportNo("<b class=\"impBold\">" + result.getString("IMSSDPIMNO") + "</b>");
                        impNo = result.getString("IMSSDPIMNO");
                    } else {
                        p.setImportNo("<b class=\"impBold\" style=\"opacity: 0;\">" + result.getString("IMSSDPIMNO") + "</b>");
                        impNo = result.getString("IMSSDPIMNO");
                    }
                }

                p.setImp(result.getString("IMSSDPIMNO"));

                p.setLine(result.getString("IMSSDPIMNO") + "-" + result.getString("IMSSDLINE"));
                p.setInvoiceNo(result.getString("IMSSDINVNO"));
                p.setCcy(result.getString("IMSSDCUR"));
                p.setProduct(result.getString("IMSSDPROD"));

                p.setInvAmount(formatDou.format(Double.parseDouble(result.getString("IMSSDINVAMT"))));
                p.setForeignInvAmount(formatDou.format(Double.parseDouble(result.getString("IMSSDFINVAMT"))));
//                p.setPaidInvAmount(formatDou.format(Double.parseDouble(result.getString("PAID_INV_AMT"))));
//                p.setPaidForeignInvAmount(formatDou.format(Double.parseDouble(result.getString("PAID_FOR_INV_AMT"))));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public boolean clearFlag(String importPaidNo) {

        boolean result = false;

        String sql = "update [RMShipment].[dbo].[IMSSDETAIL]\n"
                + "set [IMSSDPDNO]=null\n"
                + "      ,[IMSSDSTS300]=null\n"
                + "      ,[IMSSDSTS301]=null\n"
                + "  where IMSSDPDNO='" + importPaidNo + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

}
