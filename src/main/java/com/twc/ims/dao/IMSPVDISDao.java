/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.dao;

import com.twc.ims.database.database;
import com.twc.ims.entity.IMSAPHEAD;
import com.twc.ims.entity.IMSPVDIS;
import com.twc.ims.entity.IMSSHC;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSPVDISDao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public List<IMSPVDIS> findByCode(String code) {

        List<IMSPVDIS> list = new ArrayList<IMSPVDIS>();

        String sql = "SELECT [IMSPVDPDNO]\n"
                + "      ,[IMSPVDLINE]\n"
                + "      ,FORMAT([IMSPVDFAMT],'#,#0.00') AS [IMSPVDFAMT]\n"
                + "      ,[IMSPVDRATE]\n"
                + "      ,FORMAT([IMSPVDAMT],'#,#0.00') AS [IMSPVDAMT]\n"
                + "  FROM [RMShipment].[dbo].[IMSPVDIS]\n"
                + "  WHERE [IMSPVDPDNO] = '" + code + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSPVDIS p = new IMSPVDIS();

                p.setImpPaidNo(result.getString("IMSPVDPDNO"));
                p.setLine(result.getString("IMSPVDLINE"));
                p.setFamt(result.getString("IMSPVDFAMT"));
                p.setRate(result.getString("IMSPVDRATE"));
                p.setAmt(result.getString("IMSPVDAMT"));

                list.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return list;

    }

    public boolean add(String impPaidNo, String line, String famt, String rate, String amt, String userid) {

        boolean result = false;

        String sql = "INSERT INTO [IMSPVDIS]"
                + " ([IMSPVDCOM]\n"
                + "      ,[IMSPVDPDNO]\n"
                + "      ,[IMSPVDLINE]\n"
                + "      ,[IMSPVDFAMT]\n"
                + "      ,[IMSPVDRATE]\n"
                + "      ,[IMSPVDAMT]\n"
                + "      ,[IMSPVDEDT]\n"
                + "      ,[IMSPVDCDT]\n"
                + "      ,[IMSPVDUSER])"
                + " VALUES(?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, ?)";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, "TWC");

            ps.setString(2, impPaidNo);

            ps.setString(3, line);

            ps.setString(4, famt);

            ps.setString(5, rate);

            ps.setString(6, amt);

            ps.setString(7, userid);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String code) {

        String sql = "DELETE FROM [IMSPVDIS] WHERE [IMSPVDPDNO] = '" + code + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
