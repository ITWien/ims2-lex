/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.dao;

import com.twc.ims.database.database;
import com.twc.ims.entity.IMSAPHEAD;
import com.twc.ims.entity.IMSRP700;
import com.twc.ims.entity.IMSRP710;
import com.twc.ims.entity.IMSRP750;
import com.twc.ims.entity.IMSSHC;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSRP750Dao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public int countByPeriod(String ym, String imc) {

        int cnt = 0;

        String sql = "SELECT COUNT(*) AS CNT\n"
                + "  FROM IMSSHEAD\n"
                + "  WHERE FORMAT(IMSHTRDT,'yyyyMM') = '" + ym + "' AND IMSHIMC = '" + imc + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                cnt = result.getInt("CNT");

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return cnt;

    }

    public String findTotImpAmt(String ym) {

        String TotImpAmt = "";

        String sql = "SELECT ISNULL(SUM([IMSRPBAHT]),0) as SUMTIAB\n"
                + "  FROM [RMShipment].[dbo].[IMSRP700]\n"
                + "  WHERE [IMSRPPERIOD] = '" + ym + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                TotImpAmt = result.getString("SUMTIAB");

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return TotImpAmt;

    }

    public List<IMSRP750> findByCode(String ym, String imc) {

        List<IMSRP750> ul = new ArrayList<IMSRP750>();

        String sql = "SELECT ISNULL([IMSRPPERIOD],'') AS IMSRPPERIOD\n"
                + "      ,ISNULL([IMSRPPROD]+' : '+[IMSGPDESC],'') AS [IMSRPPROD]\n"
                + "      ,ISNULL([IMSRPMONTH],'') AS IMSRPMONTH\n"
                + "      ,ISNULL([IMSVDNAME],'') AS [IMSRPCO]\n"
                + "      ,[IMSRPINVNO]\n"
                + "      ,[IMSRPTERM]\n"
                + "      ,[IMSRPCCY]\n"
                + "      ,FORMAT(ISNULL([IMSRPAMT],0),'#,#0.00') AS [IMSRPAMT]\n"
                + "      ,FORMAT(ISNULL([IMSRPCHARGE],0),'#,#0.00') AS [IMSRPCHARGE]\n"
                + "  FROM [IMSRP750]\n"
                + "  LEFT JOIN IMSVEN ON IMSVEN.IMSVDCODE = [IMSRP750].[IMSRPCO]\n"
                + "  LEFT JOIN [IMSGRP] ON [IMSGRP].[IMSGPCODE] = [IMSRP750].[IMSRPPROD]\n"
                + "  WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + "  AND [IMSRPIMC] = '" + imc + "'\n"
                + "  ORDER BY [IMSRPPERIOD], [IMSRPPROD], [IMSRPMONTH], [IMSRPCO], [IMSRPINVNO]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String prod = "ST";
            String month = "ST";
            String co = "ST";

            Double sumAMTm = 0.00;
            Double sumCHAm = 0.00;

            Double sumAMTp = 0.00;
            Double sumCHAp = 0.00;

            Double sumAMTa = 0.00;
            Double sumCHAa = 0.00;

            while (result.next()) {

                IMSRP750 p = new IMSRP750();

                p.setPeriod(result.getString("IMSRPPERIOD"));

                if (prod.equals(result.getString("IMSRPPROD"))) {
                    prod = result.getString("IMSRPPROD");
                    p.setProd("<b style=\"opacity: 0; font-size: 1px;\">" + result.getString("IMSRPPROD") + "</b>");

                    if (month.equals(result.getString("IMSRPMONTH"))) {
                        month = result.getString("IMSRPMONTH");
                        p.setMonth("<b style=\"opacity: 0; font-size: 1px;\">" + result.getString("IMSRPMONTH") + "</b>");

                        if (co.equals(result.getString("IMSRPCO"))) {
                            co = result.getString("IMSRPCO");
                            p.setCo("<b style=\"opacity: 0; font-size: 1px;\">" + result.getString("IMSRPCO") + "</b>");

                        } else {
                            co = result.getString("IMSRPCO");
                            p.setCo(result.getString("IMSRPCO"));
                        }
                    } else {
                        if (!month.equals("ST")) {
                            IMSRP750 sp = new IMSRP750();
                            sp.setMonth("<b>" + month + " Total</b>");
                            sp.setAmount("<b>" + formatDou.format(sumAMTm) + "</b>");
                            sp.setCharge("<b>" + formatDou.format(sumCHAm) + "</b>");
                            ul.add(sp);

                            sumAMTm = 0.00;
                            sumCHAm = 0.00;
                        }

                        month = result.getString("IMSRPMONTH");
                        p.setMonth(result.getString("IMSRPMONTH"));
                        co = result.getString("IMSRPCO");
                        p.setCo(result.getString("IMSRPCO"));

                    }

                } else {
                    if (!prod.equals("ST")) {
                        IMSRP750 sp = new IMSRP750();
                        sp.setMonth("<b>" + month + " Total</b>");
                        sp.setAmount("<b>" + formatDou.format(sumAMTm) + "</b>");
                        sp.setCharge("<b>" + formatDou.format(sumCHAm) + "</b>");
                        ul.add(sp);

                        sumAMTm = 0.00;
                        sumCHAm = 0.00;

                        IMSRP750 sp2 = new IMSRP750();
                        sp2.setProd("<b>" + prod + " Total</b>");
                        sp2.setAmount("<b>" + formatDou.format(sumAMTp) + "</b>");
                        sp2.setCharge("<b>" + formatDou.format(sumCHAp) + "</b>");
                        ul.add(sp2);

                        sumAMTp = 0.00;
                        sumCHAp = 0.00;
                    }

                    prod = result.getString("IMSRPPROD");
                    p.setProd(result.getString("IMSRPPROD"));
                    month = result.getString("IMSRPMONTH");
                    p.setMonth(result.getString("IMSRPMONTH"));
                    co = result.getString("IMSRPCO");
                    p.setCo(result.getString("IMSRPCO"));
                }

                p.setInvNo(result.getString("IMSRPINVNO"));
                p.setTerm(result.getString("IMSRPTERM"));
                p.setCcy(result.getString("IMSRPCCY"));
                p.setAmount(result.getString("IMSRPAMT"));
                p.setCharge(result.getString("IMSRPCHARGE"));

                sumAMTm += Double.parseDouble(result.getString("IMSRPAMT").replace(",", ""));
                sumCHAm += Double.parseDouble(result.getString("IMSRPCHARGE").replace(",", ""));

                sumAMTp += Double.parseDouble(result.getString("IMSRPAMT").replace(",", ""));
                sumCHAp += Double.parseDouble(result.getString("IMSRPCHARGE").replace(",", ""));

                sumAMTa += Double.parseDouble(result.getString("IMSRPAMT").replace(",", ""));
                sumCHAa += Double.parseDouble(result.getString("IMSRPCHARGE").replace(",", ""));

                ul.add(p);

            }

            IMSRP750 sp = new IMSRP750();
            sp.setMonth("<b>" + month + " Total</b>");
            sp.setAmount("<b>" + formatDou.format(sumAMTm) + "</b>");
            sp.setCharge("<b>" + formatDou.format(sumCHAm) + "</b>");
            ul.add(sp);

            IMSRP750 sp2 = new IMSRP750();
            sp2.setProd("<b>" + prod + " Total</b>");
            sp2.setAmount("<b>" + formatDou.format(sumAMTp) + "</b>");
            sp2.setCharge("<b>" + formatDou.format(sumCHAp) + "</b>");
            ul.add(sp2);

            IMSRP750 sp3 = new IMSRP750();
            sp3.setProd("<b>Grand Total</b>");
            sp3.setAmount("<b>" + formatDou.format(sumAMTa) + "</b>");
            sp3.setCharge("<b>" + formatDou.format(sumCHAa) + "</b>");
            ul.add(sp3);

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP750> findByCodePT(String ym, String imc) {

        List<IMSRP750> ul = new ArrayList<IMSRP750>();

        String sql = "SELECT ISNULL([IMSRPPERIOD],'') AS IMSRPPERIOD\n"
                + "      ,ISNULL([IMSRPPROD]+' : '+[IMSGPDESC],'') AS [IMSRPPROD]\n"
                + "      ,ISNULL([IMSRPMONTH],'') AS IMSRPMONTH\n"
                + "      ,ISNULL([IMSVDNAME],'') AS [IMSRPCO]\n"
                + "      ,[IMSRPINVNO]\n"
                + "      ,[IMSRPTERM]\n"
                + "      ,[IMSRPCCY]\n"
                + "      ,FORMAT(ISNULL([IMSRPAMT],0),'#,#0.00') AS [IMSRPAMT]\n"
                + "      ,FORMAT(ISNULL([IMSRPCHARGE],0),'#,#0.00') AS [IMSRPCHARGE]\n"
                + "  FROM [IMSRP750]\n"
                + "  LEFT JOIN IMSVEN ON IMSVEN.IMSVDCODE = [IMSRP750].[IMSRPCO]\n"
                + "  LEFT JOIN [IMSGRP] ON [IMSGRP].[IMSGPCODE] = [IMSRP750].[IMSRPPROD]\n"
                + "  WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + "  AND [IMSRPIMC] = '" + imc + "'\n"
                + "  ORDER BY [IMSRPPERIOD], [IMSRPPROD], [IMSRPMONTH], [IMSRPCO], [IMSRPINVNO]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String prod = "ST";
            String month = "ST";
            String co = "ST";

            Double sumAMTm = 0.00;
            Double sumCHAm = 0.00;

            Double sumAMTp = 0.00;
            Double sumCHAp = 0.00;

            Double sumAMTa = 0.00;
            Double sumCHAa = 0.00;

            while (result.next()) {

                IMSRP750 p = new IMSRP750();

                p.setPeriod(result.getString("IMSRPPERIOD"));

                if (prod.equals(result.getString("IMSRPPROD"))) {
                    prod = result.getString("IMSRPPROD");
                    p.setProd("");

                    if (month.equals(result.getString("IMSRPMONTH"))) {
                        month = result.getString("IMSRPMONTH");
                        p.setMonth("");

                        if (co.equals(result.getString("IMSRPCO"))) {
                            co = result.getString("IMSRPCO");
                            p.setCo("");

                        } else {
                            co = result.getString("IMSRPCO");
                            p.setCo(result.getString("IMSRPCO"));
                        }
                    } else {
                        if (!month.equals("ST")) {
                            IMSRP750 sp = new IMSRP750();
                            sp.setMonth(month + " Total" + "+ULINE");
                            sp.setCo("+ULINE");
                            sp.setInvNo("+ULINE");
                            sp.setTerm("+ULINE");
                            sp.setCcy("+ULINE");
                            sp.setAmount(formatDou.format(sumAMTm) + "+ULINE");
                            sp.setCharge(formatDou.format(sumCHAm) + "+ULINE");
                            ul.add(sp);

                            sumAMTm = 0.00;
                            sumCHAm = 0.00;
                        }

                        month = result.getString("IMSRPMONTH");
                        p.setMonth(result.getString("IMSRPMONTH"));
                        co = result.getString("IMSRPCO");
                        p.setCo(result.getString("IMSRPCO"));

                    }

                } else {
                    if (!prod.equals("ST")) {
                        IMSRP750 sp = new IMSRP750();
                        sp.setMonth(month + " Total" + "+ULINE");
                        sp.setCo("+ULINE");
                        sp.setInvNo("+ULINE");
                        sp.setTerm("+ULINE");
                        sp.setCcy("+ULINE");
                        sp.setAmount(formatDou.format(sumAMTm) + "+ULINE");
                        sp.setCharge(formatDou.format(sumCHAm) + "+ULINE");
                        ul.add(sp);

                        sumAMTm = 0.00;
                        sumCHAm = 0.00;

                        IMSRP750 sp2 = new IMSRP750();
                        sp2.setProd(prod + " Total" + "+ULINE");
                        sp2.setMonth("+ULINE");
                        sp2.setCo("+ULINE");
                        sp2.setInvNo("+ULINE");
                        sp2.setTerm("+ULINE");
                        sp2.setCcy("+ULINE");
                        sp2.setAmount(formatDou.format(sumAMTp) + "+ULINE");
                        sp2.setCharge(formatDou.format(sumCHAp) + "+ULINE");
                        ul.add(sp2);

                        sumAMTp = 0.00;
                        sumCHAp = 0.00;
                    }

                    prod = result.getString("IMSRPPROD");
                    p.setProd(result.getString("IMSRPPROD"));
                    month = result.getString("IMSRPMONTH");
                    p.setMonth(result.getString("IMSRPMONTH"));
                    co = result.getString("IMSRPCO");
                    p.setCo(result.getString("IMSRPCO"));
                }

                p.setInvNo(result.getString("IMSRPINVNO"));
                p.setTerm(result.getString("IMSRPTERM"));
                p.setCcy(result.getString("IMSRPCCY"));
                p.setAmount(result.getString("IMSRPAMT"));
                p.setCharge(result.getString("IMSRPCHARGE"));

                sumAMTm += Double.parseDouble(result.getString("IMSRPAMT").replace(",", ""));
                sumCHAm += Double.parseDouble(result.getString("IMSRPCHARGE").replace(",", ""));

                sumAMTp += Double.parseDouble(result.getString("IMSRPAMT").replace(",", ""));
                sumCHAp += Double.parseDouble(result.getString("IMSRPCHARGE").replace(",", ""));

                sumAMTa += Double.parseDouble(result.getString("IMSRPAMT").replace(",", ""));
                sumCHAa += Double.parseDouble(result.getString("IMSRPCHARGE").replace(",", ""));

                ul.add(p);

            }

            IMSRP750 sp = new IMSRP750();
            sp.setMonth(month + " Total" + "+ULINE");
            sp.setCo("+ULINE");
            sp.setInvNo("+ULINE");
            sp.setTerm("+ULINE");
            sp.setCcy("+ULINE");
            sp.setAmount(formatDou.format(sumAMTm) + "+ULINE");
            sp.setCharge(formatDou.format(sumCHAm) + "+ULINE");
            ul.add(sp);

            IMSRP750 sp2 = new IMSRP750();
            sp2.setProd(prod + " Total" + "+ULINE");
            sp2.setMonth("+ULINE");
            sp2.setCo("+ULINE");
            sp2.setInvNo("+ULINE");
            sp2.setTerm("+ULINE");
            sp2.setCcy("+ULINE");
            sp2.setAmount(formatDou.format(sumAMTp) + "+ULINE");
            sp2.setCharge(formatDou.format(sumCHAp) + "+ULINE");
            ul.add(sp2);

            IMSRP750 sp3 = new IMSRP750();
            sp3.setProd("Grand Total" + "+ULINE");
            sp3.setMonth("+ULINE");
            sp3.setCo("+ULINE");
            sp3.setInvNo("+ULINE");
            sp3.setTerm("+ULINE");
            sp3.setCcy("+ULINE");
            sp3.setAmount(formatDou.format(sumAMTa) + "+ULINE");
            sp3.setCharge(formatDou.format(sumCHAa) + "+ULINE");
            ul.add(sp3);

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findForPrint2(String year) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT (SELECT TOP 1 RIGHT(IMSRPPERIOD,2) FROM IMSRP710 WHERE LEFT(IMSRPPERIOD,4) = '2019' ORDER BY IMSRPPERIOD) AS MF\n"
                + ",(SELECT TOP 1 RIGHT(IMSRPPERIOD,2) FROM IMSRP710 WHERE LEFT(IMSRPPERIOD,4) = '2019' ORDER BY IMSRPPERIOD DESC) AS MT\n"
                + ",*\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPFAMT) FROM IMSRP710 WHERE LEFT(IMSRPPERIOD,4) = '" + year + "' AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPINVAMT) FROM IMSRP710 WHERE LEFT(IMSRPPERIOD,4) = '" + year + "' AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMIA\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPPAYAMT) FROM IMSRP710 WHERE LEFT(IMSRPPERIOD,4) = '" + year + "' AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMPA\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPDIFFAMT) FROM IMSRP710 WHERE LEFT(IMSRPPERIOD,4) = '" + year + "' AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPCCY AS CCY\n"
                + "FROM IMSRP710\n"
                + "WHERE LEFT(IMSRPPERIOD,4) = '" + year + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setPeriod("M." + result.getString("MF") + "-M." + result.getString("MT"));
                p.setCcy(result.getString("CCY"));

                p.setForAmt(result.getString("SUMFA"));
                p.setInvAmt(result.getString("SUMIA"));
                p.setPayAmt(result.getString("SUMPA"));
                p.setDiffAmt(result.getString("SUMDA"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findTotalByDept(String ym) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT *\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPINVAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0),'#,#0.00') AS SUMIA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPPAYAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0),'#,#0.00') AS SUMPA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPDIFFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPDEPT AS DEPT\n"
                + "FROM IMSRP710\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setDept("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("DEPT") + "3</font><b>TOTAL BY DEPT : </b>");
                p.setBank("<b>" + result.getString("DEPT") + "</b>");

                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setInvAmt("<b>" + result.getString("SUMIA") + "</b>");
                p.setPayAmt("<b>" + result.getString("SUMPA") + "</b>");
                p.setDiffAmt("<b>" + result.getString("SUMDA") + "</b>");

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findTotalByBank(String ym) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT *\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPINVAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK),0),'#,#0.00') AS SUMIA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPPAYAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK),0),'#,#0.00') AS SUMPA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPDIFFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK),0),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPDEPT AS DEPT, IMSRPBCODE AS BANK\n"
                + "FROM IMSRP710\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setDept("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("DEPT") + "2</font><font size=\"1\" style=\"opacity: 0;\">TOTAL BY DEPT : " + result.getString("DEPT") + "</font>");
                p.setBank("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("BANK") + "2</font><b>TOTAL BY BANK : </b>");
                p.setPayDate("<b>" + result.getString("BANK") + "</b>");

                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setInvAmt("<b>" + result.getString("SUMIA") + "</b>");
                p.setPayAmt("<b>" + result.getString("SUMPA") + "</b>");
                p.setDiffAmt("<b>" + result.getString("SUMDA") + "</b>");

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findTotalByCcy(String ym) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT *\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPINVAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMIA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPPAYAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMPA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPDIFFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPDEPT AS DEPT, IMSRPBCODE AS BANK, IMSRPCCY AS CCY\n"
                + "FROM IMSRP710\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setDept("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("DEPT") + "1</font><font size=\"1\" style=\"opacity: 0;\">TOTAL BY DEPT : " + result.getString("DEPT") + "</font>");
                p.setBank("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("BANK") + "1</font><font size=\"1\" style=\"opacity: 0;\">TOTAL BY BANK : " + result.getString("BANK") + "</font>");
                p.setCcy("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("CCY") + "</font><b>TOTAL BY CCY : </b>");
                p.setPayRate("<b>" + result.getString("CCY") + "</b>");

                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setInvAmt("<b>" + result.getString("SUMIA") + "</b>");
                p.setPayAmt("<b>" + result.getString("SUMPA") + "</b>");
                p.setDiffAmt("<b>" + result.getString("SUMDA") + "</b>");

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findTotalByMonth(String ym) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT FORMAT(SUM(SUMFA),'#,#0.00') AS SUMFA, \n"
                + "FORMAT(SUM(SUMIA),'#,#0.00') AS SUMIA, \n"
                + "FORMAT(SUM(SUMPA),'#,#0.00') AS SUMPA, \n"
                + "FORMAT(SUM(SUMDA),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT *\n"
                + ",ISNULL((SELECT SUM([IMSRPFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0) AS SUMFA\n"
                + ",ISNULL((SELECT SUM([IMSRPINVAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0) AS SUMIA\n"
                + ",ISNULL((SELECT SUM([IMSRPPAYAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0) AS SUMPA\n"
                + ",ISNULL((SELECT SUM([IMSRPDIFFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0) AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPPERIOD, IMSRPDEPT AS DEPT\n"
                + "FROM IMSRP710\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + ")TMP\n"
                + ")TMP2";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setDept("<font size=\"1\" style=\"opacity: 0;\">Z</font><b>GRAND TOTAL </b>");
                p.setBank("<b>BY MONTH : " + ym + "</b>");

                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setInvAmt("<b>" + result.getString("SUMIA") + "</b>");
                p.setPayAmt("<b>" + result.getString("SUMPA") + "</b>");
                p.setDiffAmt("<b>" + result.getString("SUMDA") + "</b>");

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP700> findByYear(String year) {

        List<IMSRP700> ul = new ArrayList<IMSRP700>();

        String sql = "SELECT [IMSRPPERIOD]\n"
                + "      ,[IMSRPCCY]\n"
                + "      ,FORMAT([IMSRPMACHINE],'#,#0.00') AS [IMSRPMACHINE]\n"
                + "      ,FORMAT([IMSRPOTHER],'#,#0.00') AS [IMSRPOTHER]\n"
                + "      ,FORMAT([IMSRPTOTAL],'#,#0.00') AS [IMSRPTOTAL]\n"
                + "      ,FORMAT([IMSRPRATE],'#,#0.000000') AS [IMSRPRATE]\n"
                + "      ,FORMAT([IMSRPBAHT],'#,#0.00') AS [IMSRPBAHT]\n"
                + "      ,FORMAT([IMSRPBALCCY],'#,#0.00') AS [IMSRPBALCCY]\n"
                + "      ,FORMAT([IMSRPTOTCCY],'#,#0.00') AS [IMSRPTOTCCY]\n"
                + "      ,FORMAT([IMSRPALLCCY],'#,#0.00') AS [IMSRPALLCCY]\n"
                + "      ,FORMAT([IMSRPSUM],'#,#0.00') AS [IMSRPSUM]\n"
                + "      ,FORMAT([IMSRPHSB],'#,#0.00') AS [IMSRPHSB]\n"
                + "      ,FORMAT([IMSRPSCB],'#,#0.00') AS [IMSRPSCB]\n"
                + "      ,FORMAT([IMSRPBBL],'#,#0.00') AS [IMSRPBBL]\n"
                + "      ,FORMAT([IMSRPMIZ],'#,#0.00') AS [IMSRPMIZ]\n"
                + "      ,FORMAT([IMSRPKRT],'#,#0.00') AS [IMSRPKRT]\n"
                + "      ,FORMAT([IMSRPTOTPAY],'#,#0.00') AS [IMSRPTOTPAY]\n"
                + "      ,FORMAT([IMSRPNOPAY],'#,#0.00') AS [IMSRPNOPAY]\n"
                + "	 ,FORMAT(ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE IMSRPPERIOD = MN.IMSRPPERIOD),0),'#,#0.00')  AS SUMTIAB\n"
                + "  FROM [RMShipment].[dbo].[IMSRP700] MN\n"
                + "  WHERE LEFT([IMSRPPERIOD],4) = '" + year + "'\n"
                + "  ORDER BY IMSRPPERIOD ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP700 p = new IMSRP700();

                p.setPeriod(result.getString("IMSRPPERIOD"));
                p.setCcy(result.getString("IMSRPCCY"));
                p.setMacImpAmt(result.getString("IMSRPMACHINE"));
                p.setMatImpAmt(result.getString("IMSRPOTHER"));
                p.setTotalImpAmt(result.getString("IMSRPTOTAL"));
                p.setRate(result.getString("IMSRPRATE"));
                p.setTotalImpAmtB(result.getString("IMSRPBAHT"));
                p.setBalAmtPrevMonth(result.getString("IMSRPBALCCY"));
                p.setAccInvAmt(result.getString("IMSRPTOTCCY"));
                p.setAllBankBalPayment(result.getString("IMSRPALLCCY"));
                p.setSumitomo(result.getString("IMSRPSUM"));
                p.setHsb(result.getString("IMSRPHSB"));
                p.setScb(result.getString("IMSRPSCB"));
                p.setBbl(result.getString("IMSRPBBL"));
                p.setMizuho(result.getString("IMSRPMIZ"));
                p.setK_tokyo(result.getString("IMSRPKRT"));
                p.setTotal(result.getString("IMSRPTOTPAY"));
                p.setNoPaymentAmt(result.getString("IMSRPNOPAY"));

                p.setSumTiab(result.getString("SUMTIAB"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP700> findByYearTotal(String year) {

        List<IMSRP700> ul = new ArrayList<IMSRP700>();

        String sql = "DECLARE @YEAR NVARCHAR(6) = '" + year + "'\n"
                + "\n"
                + "SELECT 'GTOTAL' AS IMSRPPERIOD, CCY AS IMSRPCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPMACHINE) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPMACHINE\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPOTHER) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPOTHER\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPTOTAL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPTOTAL\n"
                + ",FORMAT(ISNULL((CASE WHEN (ISNULL((SELECT SUM(IMSRPTOTAL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0))=0 THEN 0 \n"
                + "ELSE (ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0)\n"
                + "/ISNULL((SELECT SUM(IMSRPTOTAL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0))\n"
                + "END),0),'#,#0.000000') AS IMSRPRATE\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPBAHT\n"
                + ",FORMAT(ISNULL((SELECT TOP 1 IMSRPNOPAY FROM IMSRP700 \n"
                + "WHERE IMSRPPERIOD = (SELECT TOP 1 IMSRPPERIOD FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY ORDER BY IMSRPPERIOD DESC) \n"
                + "AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPBALCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPTOTCCY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPTOTCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPALLCCY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPALLCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPSUM) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPSUM\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPHSB) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPHSB\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPSCB) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPSCB\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBBL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPBBL\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPMIZ) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPMIZ\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPKRT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPKRT\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPTOTPAY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPTOTPAY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPNOPAY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPNOPAY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR),0),'#,#0.00') AS SUMTIAB\n"
                + "FROM(\n"
                + "SELECT IMSCUCODE AS CCY\n"
                + "FROM IMSCUR\n"
                + "WHERE IMSCUCODE != 'THB'\n"
                + ")CUR";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP700 p = new IMSRP700();

                p.setPeriod(result.getString("IMSRPPERIOD"));
                p.setCcy(result.getString("IMSRPCCY"));
                p.setMacImpAmt(result.getString("IMSRPMACHINE"));
                p.setMatImpAmt(result.getString("IMSRPOTHER"));
                p.setTotalImpAmt(result.getString("IMSRPTOTAL"));
                p.setRate(result.getString("IMSRPRATE"));
                p.setTotalImpAmtB(result.getString("IMSRPBAHT"));
                p.setBalAmtPrevMonth(result.getString("IMSRPBALCCY"));
                p.setAccInvAmt(result.getString("IMSRPTOTCCY"));
                p.setAllBankBalPayment(result.getString("IMSRPALLCCY"));
                p.setSumitomo(result.getString("IMSRPSUM"));
                p.setHsb(result.getString("IMSRPHSB"));
                p.setScb(result.getString("IMSRPSCB"));
                p.setBbl(result.getString("IMSRPBBL"));
                p.setMizuho(result.getString("IMSRPMIZ"));
                p.setK_tokyo(result.getString("IMSRPKRT"));
                p.setTotal(result.getString("IMSRPTOTPAY"));
                p.setNoPaymentAmt(result.getString("IMSRPNOPAY"));

                p.setSumTiab(result.getString("SUMTIAB"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public boolean add(String ym, String userid, String imc) {

        boolean result = false;

        String sql = "INSERT INTO IMSRP750 ([IMSRPCOM]\n"
                + "      ,[IMSRPPERIOD]\n"
                + "      ,[IMSRPPROD]\n"
                + "      ,[IMSRPMONTH]\n"
                + "      ,[IMSRPCO]\n"
                + "      ,[IMSRPINVNO]\n"
                + "      ,[IMSRPTERM]\n"
                + "      ,[IMSRPCCY]\n"
                + "      ,[IMSRPAMT]\n"
                + "      ,[IMSRPCHARGE]\n"
                + "      ,[IMSRPEDT]\n"
                + "      ,[IMSRPCDT]\n"
                + "      ,[IMSRPUSER]\n"
                + "      ,[IMSRPIMC])\n"
                + "SELECT 'TWC', FORMAT(IMSHTRDT,'yyyyMM'), IMSSDPROD, FORMAT(IMSHTRDT,'yyyy/MM'), IMSSDVCODE\n"
                + ",IMSSDINVNO, IMSSDINC, IMSSDCUR, IMSSDFINVAMT, (IMSSHEAD.[IMSHTCHARGE]/(SELECT COUNT(*) FROM IMSSDETAIL WHERE [IMSSDPIMNO] = DET.IMSSDPIMNO)) AS IMSSDCHARGE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '" + userid + "', IMSHIMC\n"
                + "FROM IMSSDETAIL DET\n"
                + "LEFT JOIN IMSSHEAD ON IMSSHEAD.IMSHPIMNO = DET.IMSSDPIMNO\n"
                + "WHERE FORMAT(IMSHTRDT,'yyyyMM') = '" + ym + "' AND IMSHIMC = '" + imc + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String ym, String ccy, String rate, String totalImpAmtB, String accInvAmt, String noPaymentAmt) {

        boolean result = false;

        String sql = "UPDATE IMSRP700 "
                + "SET IMSRPRATE = " + rate + ""
                + ", IMSRPBAHT = " + totalImpAmtB + ""
                + ", IMSRPTOTCCY = " + accInvAmt + ""
                + ", IMSRPNOPAY = " + noPaymentAmt + ""
                + ", IMSRPEDT = CURRENT_TIMESTAMP"
                + " WHERE IMSRPPERIOD = '" + ym + "' AND IMSRPCCY = '" + ccy + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String code, String imc) {

        String sql = "DELETE FROM IMSRP750 WHERE IMSRPPERIOD = '" + code + "' AND IMSRPIMC = '" + imc + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
