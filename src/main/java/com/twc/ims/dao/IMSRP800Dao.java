/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.dao;

import com.twc.ims.database.database;
import com.twc.ims.entity.IMSAPHEAD;
import com.twc.ims.entity.IMSRP700;
import com.twc.ims.entity.IMSRP710;
import com.twc.ims.entity.IMSRP770;
import com.twc.ims.entity.IMSRP800;
import com.twc.ims.entity.IMSSHC;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSRP800Dao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public int countByPeriod(String ym, String imc) {

        int cnt = 0;

        String sql = "SELECT COUNT(*) AS CNT\n"
                + "  FROM IMSSHEAD\n"
                + "  WHERE FORMAT(IMSHTRDT,'yyyyMM') = '" + ym + "' AND IMSHIMC = '" + imc + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                cnt = result.getInt("CNT");

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return cnt;

    }

    public String findTotImpAmt(String ym) {

        String TotImpAmt = "";

        String sql = "SELECT ISNULL(SUM([IMSRPBAHT]),0) as SUMTIAB\n"
                + "  FROM [RMShipment].[dbo].[IMSRP700]\n"
                + "  WHERE [IMSRPPERIOD] = '" + ym + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                TotImpAmt = result.getString("SUMTIAB");

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return TotImpAmt;

    }

    public List<IMSRP800> findByCode(String ym, String imc) {

        List<IMSRP800> ul = new ArrayList<IMSRP800>();

        String sql = "SELECT [IMSRPIMC]\n"
                + "      ,[IMSRPPERIOD]\n"
                + "	  ,ROW_NUMBER() OVER (PARTITION BY [IMSRPIMPNO] ORDER BY [IMSRPIMPNO]) AS GRPIMPNO\n"
                + "      ,[IMSRPPIMPNO]\n"
                + "      ,[IMSRPIMPNO]\n"
                + "      ,[IMSRPVENDEPT] +' : '+ [IMSVDNAME] AS [IMSRPVENDEPT]\n"
                + "      ,[IMSRPINVNO]\n"
                + "      ,[IMSRPORDERNO]\n"
                + "      ,[IMSRPPROD]\n"
                + "      ,[IMSRPCCY]\n"
                + "      ,FORMAT([IMSRPFAMT],'#,#0.00') AS [IMSRPFAMT]\n"
                + "      ,FORMAT([IMSRPAMT],'#,#0.00') AS [IMSRPAMT]\n"
                + "      ,[IMSRPENTNO]\n"
                + "      ,FORMAT([IMSRPENTDATE],'dd-MM-yyyy') AS [IMSRPENTDATE]\n"
                + "      ,[IMSRPLGGIDNO]\n"
                + "      ,FORMAT([IMSRPLGAMT],'#,#0.00') AS [IMSRPLGAMT]\n"
                + "      ,FORMAT([IMSRPLGFEE],'#,#0.00') AS [IMSRPLGFEE]\n"
                + "      ,[IMSRPTAXNO]\n"
                + "  FROM [RMShipment].[dbo].[IMSRP800]\n"
                + "  LEFT JOIN IMSVEN ON IMSVEN.IMSVDCODE = [IMSRP800].[IMSRPVENDEPT]\n"
                + "  WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + "  AND [IMSRPIMC] = '" + imc + "'\n"
                + "  ORDER BY [IMSRPIMPNO]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP800 p = new IMSRP800();

                p.setIMSRPIMC(result.getString("IMSRPIMC"));
                p.setIMSRPPERIOD(result.getString("IMSRPPERIOD"));

                p.setIMSRPIMPNO(result.getString("IMSRPIMPNO"));

                if (result.getString("GRPIMPNO") != null) {
                    if (result.getString("GRPIMPNO").trim().equals("1")) {
                        p.setIMSRPIMPNO(result.getString("IMSRPIMPNO"));
                    } else {
                        p.setIMSRPIMPNO("<b style=\"opacity: 0;\">" + result.getString("IMSRPIMPNO") + "</b>");
                    }
                }

                p.setIMSRPVENDEPT(result.getString("IMSRPVENDEPT"));
                p.setIMSRPINVNO(result.getString("IMSRPINVNO"));
                p.setIMSRPORDERNO(result.getString("IMSRPORDERNO"));
                p.setIMSRPPROD(result.getString("IMSRPPROD"));
                p.setIMSRPCCY(result.getString("IMSRPCCY"));
                p.setIMSRPFAMT(result.getString("IMSRPFAMT"));
                p.setIMSRPAMT(result.getString("IMSRPAMT"));
                p.setIMSRPENTNO(result.getString("IMSRPENTNO"));
                p.setIMSRPENTDATE(result.getString("IMSRPENTDATE"));
                p.setIMSRPLGGIDNO(result.getString("IMSRPLGGIDNO"));
                p.setIMSRPLGAMT(result.getString("IMSRPLGAMT"));
                p.setIMSRPLGFEE(result.getString("IMSRPLGFEE"));
                p.setIMSRPTAXNO(result.getString("IMSRPTAXNO"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP800> findForPrint(String ym, String imc) {

        List<IMSRP800> ul = new ArrayList<IMSRP800>();

        String sql = "SELECT [IMSRPIMC]\n"
                + "      ,[IMSRPPERIOD]\n"
                + "	  ,ROW_NUMBER() OVER (PARTITION BY [IMSRPIMPNO] ORDER BY [IMSRPIMPNO]) AS GRPIMPNO\n"
                + "      ,[IMSRPPIMPNO]\n"
                + "      ,[IMSRPIMPNO]\n"
                + "      ,[IMSRPVENDEPT] +' : '+ [IMSVDNAME] AS [IMSRPVENDEPT]\n"
                + "      ,[IMSRPINVNO]\n"
                + "      ,[IMSRPORDERNO]\n"
                + "      ,[IMSRPPROD]\n"
                + "      ,[IMSRPCCY]\n"
                + "      ,FORMAT([IMSRPFAMT],'#,#0.00') AS [IMSRPFAMT]\n"
                + "      ,FORMAT([IMSRPAMT],'#,#0.00') AS [IMSRPAMT]\n"
                + "      ,[IMSRPENTNO]\n"
                + "      ,FORMAT([IMSRPENTDATE],'dd-MM-yyyy') AS [IMSRPENTDATE]\n"
                + "      ,[IMSRPLGGIDNO]\n"
                + "      ,FORMAT([IMSRPLGAMT],'#,#0.00') AS [IMSRPLGAMT]\n"
                + "      ,FORMAT([IMSRPLGFEE],'#,#0.00') AS [IMSRPLGFEE]\n"
                + "      ,[IMSRPTAXNO]\n"
                + "  FROM [RMShipment].[dbo].[IMSRP800]\n"
                + "  LEFT JOIN IMSVEN ON IMSVEN.IMSVDCODE = [IMSRP800].[IMSRPVENDEPT]\n"
                + "  WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + "  AND [IMSRPIMC] = '" + imc + "'\n"
                + "  ORDER BY [IMSRPIMPNO]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP800 p = new IMSRP800();

                p.setIMSRPIMC(result.getString("IMSRPIMC"));
                p.setIMSRPPERIOD(result.getString("IMSRPPERIOD"));

                p.setIMSRPIMPNO(result.getString("IMSRPIMPNO"));

                if (result.getString("GRPIMPNO") != null) {
                    if (result.getString("GRPIMPNO").trim().equals("1")) {
                        p.setIMSRPIMPNO(result.getString("IMSRPIMPNO") + "<head>");
                        p.setIMSRPVENDEPT(result.getString("IMSRPVENDEPT") + "<head>");
                        p.setIMSRPINVNO(result.getString("IMSRPINVNO") + "<head>");
                        p.setIMSRPORDERNO(result.getString("IMSRPORDERNO") + "<head>");
                        p.setIMSRPPROD(result.getString("IMSRPPROD") + "<head>");
                        p.setIMSRPCCY(result.getString("IMSRPCCY") + "<head>");
                        p.setIMSRPFAMT(result.getString("IMSRPFAMT") + "<head>");
                        p.setIMSRPAMT(result.getString("IMSRPAMT") + "<head>");
                        p.setIMSRPENTNO(result.getString("IMSRPENTNO") + "<head>");
                        p.setIMSRPENTDATE(result.getString("IMSRPENTDATE") + "<head>");
                        p.setIMSRPLGGIDNO(result.getString("IMSRPLGGIDNO") + "<head>");
                        p.setIMSRPLGAMT(result.getString("IMSRPLGAMT") + "<head>");
                        p.setIMSRPLGFEE(result.getString("IMSRPLGFEE") + "<head>");
                        p.setIMSRPTAXNO(result.getString("IMSRPTAXNO") + "<head>");
                    } else {
                        p.setIMSRPIMPNO(result.getString("IMSRPIMPNO"));
                        p.setIMSRPVENDEPT(result.getString("IMSRPVENDEPT"));
                        p.setIMSRPINVNO(result.getString("IMSRPINVNO"));
                        p.setIMSRPORDERNO(result.getString("IMSRPORDERNO"));
                        p.setIMSRPPROD(result.getString("IMSRPPROD"));
                        p.setIMSRPCCY(result.getString("IMSRPCCY"));
                        p.setIMSRPFAMT(result.getString("IMSRPFAMT"));
                        p.setIMSRPAMT(result.getString("IMSRPAMT"));
                        p.setIMSRPENTNO(result.getString("IMSRPENTNO"));
                        p.setIMSRPENTDATE(result.getString("IMSRPENTDATE"));
                        p.setIMSRPLGGIDNO(result.getString("IMSRPLGGIDNO"));
                        p.setIMSRPLGAMT(result.getString("IMSRPLGAMT"));
                        p.setIMSRPLGFEE(result.getString("IMSRPLGFEE"));
                        p.setIMSRPTAXNO(result.getString("IMSRPTAXNO"));
                    }
                } else {
                    p.setIMSRPVENDEPT(result.getString("IMSRPVENDEPT"));
                    p.setIMSRPINVNO(result.getString("IMSRPINVNO"));
                    p.setIMSRPORDERNO(result.getString("IMSRPORDERNO"));
                    p.setIMSRPPROD(result.getString("IMSRPPROD"));
                    p.setIMSRPCCY(result.getString("IMSRPCCY"));
                    p.setIMSRPFAMT(result.getString("IMSRPFAMT"));
                    p.setIMSRPAMT(result.getString("IMSRPAMT"));
                    p.setIMSRPENTNO(result.getString("IMSRPENTNO"));
                    p.setIMSRPENTDATE(result.getString("IMSRPENTDATE"));
                    p.setIMSRPLGGIDNO(result.getString("IMSRPLGGIDNO"));
                    p.setIMSRPLGAMT(result.getString("IMSRPLGAMT"));
                    p.setIMSRPLGFEE(result.getString("IMSRPLGFEE"));
                    p.setIMSRPTAXNO(result.getString("IMSRPTAXNO"));
                }

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP800> findForPrintSum(String ym, String imc) {

        List<IMSRP800> ul = new ArrayList<IMSRP800>();

        String sql = "SELECT NULL AS IMPNO,[IMSRPCCY]\n"
                + "      ,FORMAT(SUM([IMSRPFAMT]),'#,#0.00') AS [IMSRPFAMT]\n"
                + "      ,FORMAT(SUM([IMSRPAMT]),'#,#0.00') AS [IMSRPAMT]\n"
                + "      ,FORMAT(SUM([IMSRPLGAMT]),'#,#0.00') AS [IMSRPLGAMT]\n"
                + "      ,FORMAT(SUM([IMSRPLGFEE]),'#,#0.00') AS [IMSRPLGFEE]\n"
                + "  FROM [RMShipment].[dbo].[IMSRP800]\n"
                + "  WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + "  AND [IMSRPIMC] = '" + imc + "'\n"
                + "  GROUP BY [IMSRPCCY]\n"
                + "UNION ALL\n"
                + "SELECT 'SUM AMOUNT BAHT',NULL,NULL,NULL\n"
                + "      ,FORMAT(SUM([IMSRPLGAMT]),'#,#0.00') AS [IMSRPLGAMT]\n"
                + "      ,FORMAT(SUM([IMSRPLGFEE]),'#,#0.00') AS [IMSRPLGFEE]\n"
                + "  FROM [RMShipment].[dbo].[IMSRP800]\n"
                + "  WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + "  AND [IMSRPIMC] = '" + imc + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP800 p = new IMSRP800();

                p.setIMSRPIMPNO(result.getString("IMPNO"));
                p.setIMSRPCCY(result.getString("IMSRPCCY"));
                p.setIMSRPFAMT(result.getString("IMSRPFAMT"));
                p.setIMSRPAMT(result.getString("IMSRPAMT"));
                p.setIMSRPLGAMT(result.getString("IMSRPLGAMT"));
                p.setIMSRPLGFEE(result.getString("IMSRPLGFEE"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP770> findForPrint2(String ym, String ym2, String imc) {

        List<IMSRP770> ul = new ArrayList<IMSRP770>();

        String sql = "SELECT [IMSRPCCY]\n"
                + "      ,FORMAT(SUM([IMSRPFAMT]),'#,#0.00') as [IMSRPFAMT]\n"
                + "      ,FORMAT(SUM([IMSRPAMT]),'#,#0.00') as [IMSRPAMT]\n"
                + "  FROM [RMShipment].[dbo].[IMSRP770]\n"
                + "  WHERE [IMSRPPERIOD] BETWEEN '" + ym + "' AND '" + ym2 + "'\n"
                + "  AND [IMSRPIMC] = '" + imc + "'\n"
                + "  AND [IMSRPCCY] is not null\n"
                + "  AND [IMSRPCCY] != ''\n"
                + "  GROUP BY [IMSRPCCY]\n"
                + "  ORDER BY [IMSRPCCY]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP770 p = new IMSRP770();

                p.setIMSRPCCY(result.getString("IMSRPCCY"));
                p.setIMSRPFAMT(result.getString("IMSRPFAMT"));
                p.setIMSRPAMT(result.getString("IMSRPAMT"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findTotalByDept(String ym) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT *\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPINVAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0),'#,#0.00') AS SUMIA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPPAYAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0),'#,#0.00') AS SUMPA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPDIFFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPDEPT AS DEPT\n"
                + "FROM IMSRP710\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setDept("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("DEPT") + "3</font><b>TOTAL BY DEPT : </b>");
                p.setBank("<b>" + result.getString("DEPT") + "</b>");

                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setInvAmt("<b>" + result.getString("SUMIA") + "</b>");
                p.setPayAmt("<b>" + result.getString("SUMPA") + "</b>");
                p.setDiffAmt("<b>" + result.getString("SUMDA") + "</b>");

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findTotalByBank(String ym) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT *\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPINVAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK),0),'#,#0.00') AS SUMIA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPPAYAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK),0),'#,#0.00') AS SUMPA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPDIFFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK),0),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPDEPT AS DEPT, IMSRPBCODE AS BANK\n"
                + "FROM IMSRP710\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setDept("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("DEPT") + "2</font><font size=\"1\" style=\"opacity: 0;\">TOTAL BY DEPT : " + result.getString("DEPT") + "</font>");
                p.setBank("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("BANK") + "2</font><b>TOTAL BY BANK : </b>");
                p.setPayDate("<b>" + result.getString("BANK") + "</b>");

                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setInvAmt("<b>" + result.getString("SUMIA") + "</b>");
                p.setPayAmt("<b>" + result.getString("SUMPA") + "</b>");
                p.setDiffAmt("<b>" + result.getString("SUMDA") + "</b>");

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findTotalByCcy(String ym) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT *\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPINVAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMIA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPPAYAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMPA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPDIFFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPDEPT AS DEPT, IMSRPBCODE AS BANK, IMSRPCCY AS CCY\n"
                + "FROM IMSRP710\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setDept("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("DEPT") + "1</font><font size=\"1\" style=\"opacity: 0;\">TOTAL BY DEPT : " + result.getString("DEPT") + "</font>");
                p.setBank("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("BANK") + "1</font><font size=\"1\" style=\"opacity: 0;\">TOTAL BY BANK : " + result.getString("BANK") + "</font>");
                p.setCcy("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("CCY") + "</font><b>TOTAL BY CCY : </b>");
                p.setPayRate("<b>" + result.getString("CCY") + "</b>");

                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setInvAmt("<b>" + result.getString("SUMIA") + "</b>");
                p.setPayAmt("<b>" + result.getString("SUMPA") + "</b>");
                p.setDiffAmt("<b>" + result.getString("SUMDA") + "</b>");

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findTotalByMonth(String ym) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT FORMAT(SUM(SUMFA),'#,#0.00') AS SUMFA, \n"
                + "FORMAT(SUM(SUMIA),'#,#0.00') AS SUMIA, \n"
                + "FORMAT(SUM(SUMPA),'#,#0.00') AS SUMPA, \n"
                + "FORMAT(SUM(SUMDA),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT *\n"
                + ",ISNULL((SELECT SUM([IMSRPFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0) AS SUMFA\n"
                + ",ISNULL((SELECT SUM([IMSRPINVAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0) AS SUMIA\n"
                + ",ISNULL((SELECT SUM([IMSRPPAYAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0) AS SUMPA\n"
                + ",ISNULL((SELECT SUM([IMSRPDIFFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0) AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPPERIOD, IMSRPDEPT AS DEPT\n"
                + "FROM IMSRP710\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + ")TMP\n"
                + ")TMP2";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setDept("<font size=\"1\" style=\"opacity: 0;\">Z</font><b>GRAND TOTAL </b>");
                p.setBank("<b>BY MONTH : " + ym + "</b>");

                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setInvAmt("<b>" + result.getString("SUMIA") + "</b>");
                p.setPayAmt("<b>" + result.getString("SUMPA") + "</b>");
                p.setDiffAmt("<b>" + result.getString("SUMDA") + "</b>");

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP700> findByYear(String year) {

        List<IMSRP700> ul = new ArrayList<IMSRP700>();

        String sql = "SELECT [IMSRPPERIOD]\n"
                + "      ,[IMSRPCCY]\n"
                + "      ,FORMAT([IMSRPMACHINE],'#,#0.00') AS [IMSRPMACHINE]\n"
                + "      ,FORMAT([IMSRPOTHER],'#,#0.00') AS [IMSRPOTHER]\n"
                + "      ,FORMAT([IMSRPTOTAL],'#,#0.00') AS [IMSRPTOTAL]\n"
                + "      ,FORMAT([IMSRPRATE],'#,#0.000000') AS [IMSRPRATE]\n"
                + "      ,FORMAT([IMSRPBAHT],'#,#0.00') AS [IMSRPBAHT]\n"
                + "      ,FORMAT([IMSRPBALCCY],'#,#0.00') AS [IMSRPBALCCY]\n"
                + "      ,FORMAT([IMSRPTOTCCY],'#,#0.00') AS [IMSRPTOTCCY]\n"
                + "      ,FORMAT([IMSRPALLCCY],'#,#0.00') AS [IMSRPALLCCY]\n"
                + "      ,FORMAT([IMSRPSUM],'#,#0.00') AS [IMSRPSUM]\n"
                + "      ,FORMAT([IMSRPHSB],'#,#0.00') AS [IMSRPHSB]\n"
                + "      ,FORMAT([IMSRPSCB],'#,#0.00') AS [IMSRPSCB]\n"
                + "      ,FORMAT([IMSRPBBL],'#,#0.00') AS [IMSRPBBL]\n"
                + "      ,FORMAT([IMSRPMIZ],'#,#0.00') AS [IMSRPMIZ]\n"
                + "      ,FORMAT([IMSRPKRT],'#,#0.00') AS [IMSRPKRT]\n"
                + "      ,FORMAT([IMSRPTOTPAY],'#,#0.00') AS [IMSRPTOTPAY]\n"
                + "      ,FORMAT([IMSRPNOPAY],'#,#0.00') AS [IMSRPNOPAY]\n"
                + "	 ,FORMAT(ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE IMSRPPERIOD = MN.IMSRPPERIOD),0),'#,#0.00')  AS SUMTIAB\n"
                + "  FROM [RMShipment].[dbo].[IMSRP700] MN\n"
                + "  WHERE LEFT([IMSRPPERIOD],4) = '" + year + "'\n"
                + "  ORDER BY IMSRPPERIOD ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP700 p = new IMSRP700();

                p.setPeriod(result.getString("IMSRPPERIOD"));
                p.setCcy(result.getString("IMSRPCCY"));
                p.setMacImpAmt(result.getString("IMSRPMACHINE"));
                p.setMatImpAmt(result.getString("IMSRPOTHER"));
                p.setTotalImpAmt(result.getString("IMSRPTOTAL"));
                p.setRate(result.getString("IMSRPRATE"));
                p.setTotalImpAmtB(result.getString("IMSRPBAHT"));
                p.setBalAmtPrevMonth(result.getString("IMSRPBALCCY"));
                p.setAccInvAmt(result.getString("IMSRPTOTCCY"));
                p.setAllBankBalPayment(result.getString("IMSRPALLCCY"));
                p.setSumitomo(result.getString("IMSRPSUM"));
                p.setHsb(result.getString("IMSRPHSB"));
                p.setScb(result.getString("IMSRPSCB"));
                p.setBbl(result.getString("IMSRPBBL"));
                p.setMizuho(result.getString("IMSRPMIZ"));
                p.setK_tokyo(result.getString("IMSRPKRT"));
                p.setTotal(result.getString("IMSRPTOTPAY"));
                p.setNoPaymentAmt(result.getString("IMSRPNOPAY"));

                p.setSumTiab(result.getString("SUMTIAB"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP700> findByYearTotal(String year) {

        List<IMSRP700> ul = new ArrayList<IMSRP700>();

        String sql = "DECLARE @YEAR NVARCHAR(6) = '" + year + "'\n"
                + "\n"
                + "SELECT 'GTOTAL' AS IMSRPPERIOD, CCY AS IMSRPCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPMACHINE) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPMACHINE\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPOTHER) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPOTHER\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPTOTAL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPTOTAL\n"
                + ",FORMAT(ISNULL((CASE WHEN (ISNULL((SELECT SUM(IMSRPTOTAL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0))=0 THEN 0 \n"
                + "ELSE (ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0)\n"
                + "/ISNULL((SELECT SUM(IMSRPTOTAL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0))\n"
                + "END),0),'#,#0.000000') AS IMSRPRATE\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPBAHT\n"
                + ",FORMAT(ISNULL((SELECT TOP 1 IMSRPNOPAY FROM IMSRP700 \n"
                + "WHERE IMSRPPERIOD = (SELECT TOP 1 IMSRPPERIOD FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY ORDER BY IMSRPPERIOD DESC) \n"
                + "AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPBALCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPTOTCCY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPTOTCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPALLCCY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPALLCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPSUM) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPSUM\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPHSB) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPHSB\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPSCB) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPSCB\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBBL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPBBL\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPMIZ) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPMIZ\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPKRT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPKRT\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPTOTPAY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPTOTPAY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPNOPAY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPNOPAY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR),0),'#,#0.00') AS SUMTIAB\n"
                + "FROM(\n"
                + "SELECT IMSCUCODE AS CCY\n"
                + "FROM IMSCUR\n"
                + "WHERE IMSCUCODE != 'THB'\n"
                + ")CUR";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP700 p = new IMSRP700();

                p.setPeriod(result.getString("IMSRPPERIOD"));
                p.setCcy(result.getString("IMSRPCCY"));
                p.setMacImpAmt(result.getString("IMSRPMACHINE"));
                p.setMatImpAmt(result.getString("IMSRPOTHER"));
                p.setTotalImpAmt(result.getString("IMSRPTOTAL"));
                p.setRate(result.getString("IMSRPRATE"));
                p.setTotalImpAmtB(result.getString("IMSRPBAHT"));
                p.setBalAmtPrevMonth(result.getString("IMSRPBALCCY"));
                p.setAccInvAmt(result.getString("IMSRPTOTCCY"));
                p.setAllBankBalPayment(result.getString("IMSRPALLCCY"));
                p.setSumitomo(result.getString("IMSRPSUM"));
                p.setHsb(result.getString("IMSRPHSB"));
                p.setScb(result.getString("IMSRPSCB"));
                p.setBbl(result.getString("IMSRPBBL"));
                p.setMizuho(result.getString("IMSRPMIZ"));
                p.setK_tokyo(result.getString("IMSRPKRT"));
                p.setTotal(result.getString("IMSRPTOTPAY"));
                p.setNoPaymentAmt(result.getString("IMSRPNOPAY"));

                p.setSumTiab(result.getString("SUMTIAB"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public boolean add(String ym, String userid, String imc) {

        boolean result = false;

        String sql = "INSERT INTO IMSRP800 ([IMSRPCOM]\n"
                + "      ,[IMSRPIMC]\n"
                + "      ,[IMSRPPERIOD]\n"
                + "      ,[IMSRPPIMPNO]\n"
                + "      ,[IMSRPIMPNO]\n"
                + "      ,[IMSRPVENDEPT]\n"
                + "      ,[IMSRPINVNO]\n"
                + "      ,[IMSRPORDERNO]\n"
                + "      ,[IMSRPPROD]\n"
                + "      ,[IMSRPCCY]\n"
                + "      ,[IMSRPFAMT]\n"
                + "      ,[IMSRPAMT]\n"
                + "      ,[IMSRPENTNO]\n"
                + "      ,[IMSRPENTDATE]\n"
                + "      ,[IMSRPLGGIDNO]\n"
                + "      ,[IMSRPLGAMT]\n"
                + "      ,[IMSRPLGFEE]\n"
                + "      ,[IMSRPTAXNO]\n"
                + "      ,[IMSRPEDT]\n"
                + "      ,[IMSRPCDT]\n"
                + "      ,[IMSRPUSER])\n"
                + "SELECT 'TWC' AS COM, IMSHIMC, FORMAT(IMSCUT.IMSCUETDATE,'yyyyMM') AS SHIPDATE, [IMSSDPIMNO], IMSSDIMNO\n"
                + ", [IMSSDVCODE], IMSSDINVNO, [IMSSDORDNO]\n"
                + ",IMSSDPROD, [IMSSDCUR], [IMSSDFINVAMT], [IMSSDINVAMT]\n"
                + ",[IMSCUT].[IMSCUETNO], [IMSCUT].[IMSCUETDATE], [IMSCUT].[IMSCULGGID]\n"
                + ",[IMSCUT].[IMSCULGAMT], [IMSCUT].[IMSCUFEEAMT], [IMSCUT].[IMSCUTXNO]\n"
                + ",CURRENT_TIMESTAMP AS EDT, CURRENT_TIMESTAMP AS CDT, '" + userid + "' AS USERID\n"
                + "FROM IMSSDETAIL\n"
                + "LEFT JOIN IMSSHEAD ON IMSSHEAD.IMSHPIMNO = IMSSDETAIL.IMSSDPIMNO\n"
                + "LEFT JOIN [IMSCUT] ON [IMSCUT].[IMSCUPIMNO] = IMSSDETAIL.IMSSDPIMNO\n"
                + "WHERE FORMAT(IMSCUT.IMSCUETDATE,'yyyyMM') = '" + ym + "'\n"
                + "AND IMSHIMC = '" + imc + "'\n"
                + "AND IMSSDIMNO != ''\n"
                + "ORDER BY IMSSDIMNO";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String ym, String ccy, String rate, String totalImpAmtB, String accInvAmt, String noPaymentAmt) {

        boolean result = false;

        String sql = "UPDATE IMSRP700 "
                + "SET IMSRPRATE = " + rate + ""
                + ", IMSRPBAHT = " + totalImpAmtB + ""
                + ", IMSRPTOTCCY = " + accInvAmt + ""
                + ", IMSRPNOPAY = " + noPaymentAmt + ""
                + ", IMSRPEDT = CURRENT_TIMESTAMP"
                + " WHERE IMSRPPERIOD = '" + ym + "' AND IMSRPCCY = '" + ccy + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String ym, String imc) {

        String sql = "DELETE FROM IMSRP800 WHERE IMSRPPERIOD = '" + ym + "' AND IMSRPIMC = '" + imc + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
