/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.dao;

import com.twc.ims.database.database;
import com.twc.ims.entity.IMSAPHEAD;
import com.twc.ims.entity.IMSRP700;
import com.twc.ims.entity.IMSRP710;
import com.twc.ims.entity.IMSRP770;
import com.twc.ims.entity.IMSSHC;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSRP770Dao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public List<IMSRP770> getTransportList() {

        List<IMSRP770> ul = new ArrayList<IMSRP770>();

        String sql = "SELECT [IMSTRCODE]\n"
                + "      ,[IMSTRNAME]\n"
                + "      ,format(CURRENT_TIMESTAMP,'yyyy-MM-dd') as [DATE]\n"
                + "  FROM [RMShipment].[dbo].[IMSTRP]\n"
                + "ORDER BY IMSTRCODE\n";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP770 p = new IMSRP770();

                p.setTransportCode(result.getString("IMSTRCODE"));
                p.setTransportName(result.getString("IMSTRNAME"));
                p.setDate(result.getString("DATE"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public IMSRP770 getTransportByCode(String code) {

        IMSRP770 p = new IMSRP770();

        String sql = "SELECT [IMSTRCODE]\n"
                + "      ,[IMSTRNAME]\n"
                + "      ,format(CURRENT_TIMESTAMP,'yyyy-MM-dd') as [DATE]\n"
                + "  FROM [RMShipment].[dbo].[IMSTRP]\n"
                + "  WHERE IMSTRCODE = '" + code + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setTransportCode(result.getString("IMSTRCODE"));
                p.setTransportName(result.getString("IMSTRNAME"));
                p.setDate(result.getString("DATE"));

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public int countByPeriod(String ym, String ym2, String imc) {

        int cnt = 0;

        String sql = "SELECT COUNT(*) AS CNT\n"
                + "  FROM IMSSHEAD\n"
                + "  WHERE FORMAT(IMSHTRDT,'yyyyMM') BETWEEN '" + ym + "' AND '" + ym2 + "' AND IMSHIMC = '" + imc + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                cnt = result.getInt("CNT");

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return cnt;

    }

    public String findTotImpAmt(String ym) {

        String TotImpAmt = "";

        String sql = "SELECT ISNULL(SUM([IMSRPBAHT]),0) as SUMTIAB\n"
                + "  FROM [RMShipment].[dbo].[IMSRP700]\n"
                + "  WHERE [IMSRPPERIOD] = '" + ym + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                TotImpAmt = result.getString("SUMTIAB");

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return TotImpAmt;

    }

    public List<IMSRP770> findByCode(String ym, String ym2, String imc, String date, String transport) {

        List<IMSRP770> ul = new ArrayList<IMSRP770>();

        String DATE = "";
        if (!date.equals("")) {
            DATE = "  AND format(IMSRPARRIVALDATE,'yyyy-MM-dd') = '" + date + "'\n";
        }

        String TSP = "";
        if (!transport.equals("ALL")) {
            TSP = "  AND [IMSRPTRANSPORT] = '" + transport + "'\n";
        }

        String sql = "SELECT [IMSRPIMC]\n"
                + "      ,[IMSRPPERIOD]\n"
                + "      ,[IMSRPPIMPNO]\n"
                + "	 ,ROW_NUMBER() OVER (PARTITION BY [IMSRPPIMPNO] ORDER BY [IMSRPPERIOD],[IMSRPPIMPNO],IMSVDNAME,[IMSRPINVNO],[IMSRPPROD]) AS GRPIMPNO\n"
                + "      ,[IMSRPIMPNO]\n"
                + "	  ,IMSVDNAME as [IMSRPVENDEPT]\n"
                + "      ,[IMSRPINVNO]\n"
                + "      ,[IMSRPPROD]\n"
                + "      ,[IMSRPEST]\n"
                + "      ,[IMSRPINCOTERM]\n"
                + "      ,[IMSRPCCY]\n"
                + "      ,FORMAT([IMSRPFAMT],'#,#0.00') as [IMSRPFAMT]\n"
                + "      ,FORMAT([IMSRPRATE],'#,#0.000000') as [IMSRPRATE]\n"
                + "      ,FORMAT([IMSRPAMT],'#,#0.00') as [IMSRPAMT]\n"
                + "      ,[IMSRPBOX]\n"
                + "      ,[IMSRPCARRIER]\n"
                + "	  ,IMSTRNAME as [IMSRPTRANSPORT]\n"
                + "      ,FORMAT([IMSRPINVDATE],'dd-MM-yyy') as [IMSRPINVDATE]\n"
                + "      ,FORMAT([IMSRPDEPARTDATE],'dd-MM-yyy') as [IMSRPDEPARTDATE]\n"
                + "      ,FORMAT([IMSRPARRIVALDATE],'dd-MM-yyy') as [IMSRPARRIVALDATE]\n"
                + "      ,[IMSRPBL]\n"
                + "      ,[IMSRPNOENTRY]\n"
                + "      ,FORMAT([IMSRPENTRYDATE],'dd-MM-yyy') as [IMSRPENTRYDATE]\n"
                + "      ,FORMAT([IMSRPLGAMT],'#,#0.00') as [IMSRPLGAMT]\n"
                + "      ,FORMAT([IMSRPIMPDUTY],'#,#0.00') as [IMSRPIMPDUTY]\n"
                + "      ,FORMAT([IMSRPDEBITNOTEDATE],'dd-MM-yyy') as [IMSRPDEBITNOTEDATE]\n"
                + "      ,[IMSRPREMARK]\n"
                + "  FROM [RMShipment].[dbo].[IMSRP770]\n"
                + "  LEFT JOIN IMSVEN ON IMSVEN.IMSVDCODE = [IMSRP770].[IMSRPVENDEPT]\n"
                + "  LEFT JOIN [IMSTRP] ON [IMSTRP].[IMSTRCODE] = [IMSRP770].[IMSRPTRANSPORT]\n"
                + "  WHERE [IMSRPPERIOD] BETWEEN '" + ym + "' AND '" + ym2 + "'\n"
                + DATE
                + TSP
                + "  AND [IMSRPIMC] = '" + imc + "'\n"
                + "  ORDER BY [IMSRPPERIOD],[IMSRPPIMPNO],IMSVDNAME,[IMSRPINVNO],[IMSRPPROD]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP770 p = new IMSRP770();

                p.setIMSRPIMC(result.getString("IMSRPIMC"));
                p.setIMSRPPERIOD(result.getString("IMSRPPERIOD"));

                p.setGRPIMPNO(result.getString("GRPIMPNO"));

                p.setIMSRPPIMPNO(result.getString("IMSRPPIMPNO"));
                p.setIMSRPIMPNO(result.getString("IMSRPIMPNO"));

                if (result.getString("GRPIMPNO") != null) {
                    if (result.getString("GRPIMPNO").trim().equals("1")) {
                        p.setIMSRPPIMPNO(result.getString("IMSRPPIMPNO"));
                        p.setIMSRPIMPNO(result.getString("IMSRPIMPNO"));
                    } else {
                        p.setIMSRPPIMPNO("<b style=\"opacity: 0;\">" + result.getString("IMSRPPIMPNO") + "</b>");
                        p.setIMSRPIMPNO("<b style=\"opacity: 0;\">" + result.getString("IMSRPIMPNO") + "</b>");
                    }
                }

                p.setIMSRPVENDEPT(result.getString("IMSRPVENDEPT"));
                p.setIMSRPINVNO(result.getString("IMSRPINVNO"));
                p.setIMSRPPROD(result.getString("IMSRPPROD"));
                p.setIMSRPEST(result.getString("IMSRPEST"));
                p.setIMSRPINCOTERM(result.getString("IMSRPINCOTERM"));
                p.setIMSRPCCY(result.getString("IMSRPCCY"));
                p.setIMSRPFAMT(result.getString("IMSRPFAMT"));
                p.setIMSRPRATE(result.getString("IMSRPRATE"));
                p.setIMSRPAMT(result.getString("IMSRPAMT"));
                p.setIMSRPBOX(result.getString("IMSRPBOX"));
                p.setIMSRPCARRIER(result.getString("IMSRPCARRIER"));
                p.setIMSRPTRANSPORT(result.getString("IMSRPTRANSPORT"));
                p.setIMSRPINVDATE(result.getString("IMSRPINVDATE"));
                p.setIMSRPDEPARTDATE(result.getString("IMSRPDEPARTDATE"));
                p.setIMSRPARRIVALDATE(result.getString("IMSRPARRIVALDATE"));
                p.setIMSRPBL(result.getString("IMSRPBL"));
                p.setIMSRPNOENTRY(result.getString("IMSRPNOENTRY"));
                p.setIMSRPENTRYDATE(result.getString("IMSRPENTRYDATE"));
                p.setIMSRPLGAMT(result.getString("IMSRPLGAMT"));
                p.setIMSRPIMPDUTY(result.getString("IMSRPIMPDUTY"));
                p.setIMSRPDEBITNOTEDATE(result.getString("IMSRPDEBITNOTEDATE"));
                p.setIMSRPREMARK(result.getString("IMSRPREMARK"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP770> findForPrint(String ym, String ym2, String imc, String date, String transport) {

        List<IMSRP770> ul = new ArrayList<IMSRP770>();

        String DATE = "";
        if (!date.equals("")) {
            DATE = "  AND format(IMSRPARRIVALDATE,'yyyy-MM-dd') = '" + date + "'\n";
        }

        String TSP = "";
        if (!transport.equals("ALL")) {
            TSP = "  AND [IMSRPTRANSPORT] = '" + transport + "'\n";
        }

        String sql = "SELECT [IMSRPIMC]\n"
                + "      ,[IMSRPPERIOD]\n"
                + "      ,[IMSRPPIMPNO]\n"
                + "	 ,ROW_NUMBER() OVER (PARTITION BY [IMSRPPIMPNO] ORDER BY [IMSRPPERIOD],[IMSRPPIMPNO],IMSVDNAME,[IMSRPINVNO],[IMSRPPROD]) AS GRPIMPNO\n"
                + "      ,[IMSRPIMPNO]\n"
                + "	  ,IMSVDNAME as [IMSRPVENDEPT]\n"
                + "      ,[IMSRPINVNO]\n"
                + "      ,[IMSRPPROD]\n"
                + "      ,[IMSRPEST]\n"
                + "      ,[IMSRPINCOTERM]\n"
                + "      ,[IMSRPCCY]\n"
                + "      ,FORMAT([IMSRPFAMT],'#,#0.00') as [IMSRPFAMT]\n"
                + "      ,FORMAT([IMSRPRATE],'#,#0.000000') as [IMSRPRATE]\n"
                + "      ,FORMAT([IMSRPAMT],'#,#0.00') as [IMSRPAMT]\n"
                + "      ,[IMSRPBOX]\n"
                + "      ,[IMSRPCARRIER]\n"
                + "	  ,IMSTRNAME as [IMSRPTRANSPORT]\n"
                + "      ,FORMAT([IMSRPINVDATE],'dd-MM-yyy') as [IMSRPINVDATE]\n"
                + "      ,FORMAT([IMSRPDEPARTDATE],'dd-MM-yyy') as [IMSRPDEPARTDATE]\n"
                + "      ,FORMAT([IMSRPARRIVALDATE],'dd-MM-yyy') as [IMSRPARRIVALDATE]\n"
                + "      ,[IMSRPBL]\n"
                + "      ,[IMSRPNOENTRY]\n"
                + "      ,FORMAT([IMSRPENTRYDATE],'dd-MM-yyy') as [IMSRPENTRYDATE]\n"
                + "      ,FORMAT([IMSRPLGAMT],'#,#0.00') as [IMSRPLGAMT]\n"
                + "      ,FORMAT([IMSRPIMPDUTY],'#,#0.00') as [IMSRPIMPDUTY]\n"
                + "      ,FORMAT([IMSRPDEBITNOTEDATE],'dd-MM-yyy') as [IMSRPDEBITNOTEDATE]\n"
                + "      ,[IMSRPREMARK]\n"
                + "  FROM [RMShipment].[dbo].[IMSRP770]\n"
                + "  LEFT JOIN IMSVEN ON IMSVEN.IMSVDCODE = [IMSRP770].[IMSRPVENDEPT]\n"
                + "  LEFT JOIN [IMSTRP] ON [IMSTRP].[IMSTRCODE] = [IMSRP770].[IMSRPTRANSPORT]\n"
                + "  WHERE [IMSRPPERIOD] BETWEEN '" + ym + "' AND '" + ym2 + "'\n"
                + DATE
                + TSP
                + "  AND [IMSRPIMC] = '" + imc + "'\n"
                + "  ORDER BY [IMSRPPERIOD],[IMSRPPIMPNO],IMSVDNAME,[IMSRPINVNO],[IMSRPPROD]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP770 p = new IMSRP770();

                p.setIMSRPIMC(result.getString("IMSRPIMC"));
                p.setIMSRPPERIOD(result.getString("IMSRPPERIOD"));
                p.setIMSRPPIMPNO(result.getString("IMSRPPIMPNO"));
                p.setGRPIMPNO(result.getString("GRPIMPNO"));
                p.setIMSRPIMPNO(result.getString("IMSRPIMPNO"));
                p.setIMSRPVENDEPT(result.getString("IMSRPVENDEPT"));
                p.setIMSRPINVNO(result.getString("IMSRPINVNO"));
                p.setIMSRPPROD(result.getString("IMSRPPROD"));
                p.setIMSRPEST(result.getString("IMSRPEST"));
                p.setIMSRPINCOTERM(result.getString("IMSRPINCOTERM"));
                p.setIMSRPCCY(result.getString("IMSRPCCY"));
                p.setIMSRPFAMT(result.getString("IMSRPFAMT"));
                p.setIMSRPRATE(result.getString("IMSRPRATE"));
                p.setIMSRPAMT(result.getString("IMSRPAMT"));
                p.setIMSRPBOX(result.getString("IMSRPBOX"));
                p.setIMSRPCARRIER(result.getString("IMSRPCARRIER"));
                p.setIMSRPTRANSPORT(result.getString("IMSRPTRANSPORT"));
                p.setIMSRPINVDATE(result.getString("IMSRPINVDATE"));
                p.setIMSRPDEPARTDATE(result.getString("IMSRPDEPARTDATE"));
                p.setIMSRPARRIVALDATE(result.getString("IMSRPARRIVALDATE"));
                p.setIMSRPBL(result.getString("IMSRPBL"));
                p.setIMSRPNOENTRY(result.getString("IMSRPNOENTRY"));
                p.setIMSRPENTRYDATE(result.getString("IMSRPENTRYDATE"));
                p.setIMSRPLGAMT(result.getString("IMSRPLGAMT"));
                p.setIMSRPIMPDUTY(result.getString("IMSRPIMPDUTY"));
                p.setIMSRPDEBITNOTEDATE(result.getString("IMSRPDEBITNOTEDATE"));
                p.setIMSRPREMARK(result.getString("IMSRPREMARK"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP770> findForPrint2(String ym, String ym2, String imc, String date, String transport) {

        List<IMSRP770> ul = new ArrayList<IMSRP770>();

        String DATE = "";
        if (!date.equals("")) {
            DATE = "  AND format(IMSRPARRIVALDATE,'yyyy-MM-dd') = '" + date + "'\n";
        }

        String TSP = "";
        if (!transport.equals("ALL")) {
            TSP = "  AND [IMSRPTRANSPORT] = '" + transport + "'\n";
        }

        String sql = "SELECT [IMSRPCCY]\n"
                + "      ,FORMAT(SUM([IMSRPFAMT]),'#,#0.00') as [IMSRPFAMT]\n"
                + "      ,FORMAT(SUM([IMSRPAMT]),'#,#0.00') as [IMSRPAMT]\n"
                + "  FROM [RMShipment].[dbo].[IMSRP770]\n"
                + "  WHERE [IMSRPPERIOD] BETWEEN '" + ym + "' AND '" + ym2 + "'\n"
                + DATE
                + TSP
                + "  AND [IMSRPIMC] = '" + imc + "'\n"
                + "  AND [IMSRPCCY] is not null\n"
                + "  AND [IMSRPCCY] != ''\n"
                + "  GROUP BY [IMSRPCCY]\n"
                + "  ORDER BY [IMSRPCCY]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP770 p = new IMSRP770();

                p.setIMSRPCCY(result.getString("IMSRPCCY"));
                p.setIMSRPFAMT(result.getString("IMSRPFAMT"));
                p.setIMSRPAMT(result.getString("IMSRPAMT"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findTotalByDept(String ym) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT *\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPINVAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0),'#,#0.00') AS SUMIA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPPAYAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0),'#,#0.00') AS SUMPA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPDIFFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPDEPT AS DEPT\n"
                + "FROM IMSRP710\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setDept("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("DEPT") + "3</font><b>TOTAL BY DEPT : </b>");
                p.setBank("<b>" + result.getString("DEPT") + "</b>");

                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setInvAmt("<b>" + result.getString("SUMIA") + "</b>");
                p.setPayAmt("<b>" + result.getString("SUMPA") + "</b>");
                p.setDiffAmt("<b>" + result.getString("SUMDA") + "</b>");

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findTotalByBank(String ym) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT *\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPINVAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK),0),'#,#0.00') AS SUMIA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPPAYAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK),0),'#,#0.00') AS SUMPA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPDIFFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK),0),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPDEPT AS DEPT, IMSRPBCODE AS BANK\n"
                + "FROM IMSRP710\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setDept("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("DEPT") + "2</font><font size=\"1\" style=\"opacity: 0;\">TOTAL BY DEPT : " + result.getString("DEPT") + "</font>");
                p.setBank("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("BANK") + "2</font><b>TOTAL BY BANK : </b>");
                p.setPayDate("<b>" + result.getString("BANK") + "</b>");

                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setInvAmt("<b>" + result.getString("SUMIA") + "</b>");
                p.setPayAmt("<b>" + result.getString("SUMPA") + "</b>");
                p.setDiffAmt("<b>" + result.getString("SUMDA") + "</b>");

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findTotalByCcy(String ym) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT *\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPINVAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMIA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPPAYAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMPA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPDIFFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPDEPT AS DEPT, IMSRPBCODE AS BANK, IMSRPCCY AS CCY\n"
                + "FROM IMSRP710\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setDept("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("DEPT") + "1</font><font size=\"1\" style=\"opacity: 0;\">TOTAL BY DEPT : " + result.getString("DEPT") + "</font>");
                p.setBank("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("BANK") + "1</font><font size=\"1\" style=\"opacity: 0;\">TOTAL BY BANK : " + result.getString("BANK") + "</font>");
                p.setCcy("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("CCY") + "</font><b>TOTAL BY CCY : </b>");
                p.setPayRate("<b>" + result.getString("CCY") + "</b>");

                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setInvAmt("<b>" + result.getString("SUMIA") + "</b>");
                p.setPayAmt("<b>" + result.getString("SUMPA") + "</b>");
                p.setDiffAmt("<b>" + result.getString("SUMDA") + "</b>");

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findTotalByMonth(String ym) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT FORMAT(SUM(SUMFA),'#,#0.00') AS SUMFA, \n"
                + "FORMAT(SUM(SUMIA),'#,#0.00') AS SUMIA, \n"
                + "FORMAT(SUM(SUMPA),'#,#0.00') AS SUMPA, \n"
                + "FORMAT(SUM(SUMDA),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT *\n"
                + ",ISNULL((SELECT SUM([IMSRPFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0) AS SUMFA\n"
                + ",ISNULL((SELECT SUM([IMSRPINVAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0) AS SUMIA\n"
                + ",ISNULL((SELECT SUM([IMSRPPAYAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0) AS SUMPA\n"
                + ",ISNULL((SELECT SUM([IMSRPDIFFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0) AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPPERIOD, IMSRPDEPT AS DEPT\n"
                + "FROM IMSRP710\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + ")TMP\n"
                + ")TMP2";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setDept("<font size=\"1\" style=\"opacity: 0;\">Z</font><b>GRAND TOTAL </b>");
                p.setBank("<b>BY MONTH : " + ym + "</b>");

                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setInvAmt("<b>" + result.getString("SUMIA") + "</b>");
                p.setPayAmt("<b>" + result.getString("SUMPA") + "</b>");
                p.setDiffAmt("<b>" + result.getString("SUMDA") + "</b>");

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP700> findByYear(String year) {

        List<IMSRP700> ul = new ArrayList<IMSRP700>();

        String sql = "SELECT [IMSRPPERIOD]\n"
                + "      ,[IMSRPCCY]\n"
                + "      ,FORMAT([IMSRPMACHINE],'#,#0.00') AS [IMSRPMACHINE]\n"
                + "      ,FORMAT([IMSRPOTHER],'#,#0.00') AS [IMSRPOTHER]\n"
                + "      ,FORMAT([IMSRPTOTAL],'#,#0.00') AS [IMSRPTOTAL]\n"
                + "      ,FORMAT([IMSRPRATE],'#,#0.000000') AS [IMSRPRATE]\n"
                + "      ,FORMAT([IMSRPBAHT],'#,#0.00') AS [IMSRPBAHT]\n"
                + "      ,FORMAT([IMSRPBALCCY],'#,#0.00') AS [IMSRPBALCCY]\n"
                + "      ,FORMAT([IMSRPTOTCCY],'#,#0.00') AS [IMSRPTOTCCY]\n"
                + "      ,FORMAT([IMSRPALLCCY],'#,#0.00') AS [IMSRPALLCCY]\n"
                + "      ,FORMAT([IMSRPSUM],'#,#0.00') AS [IMSRPSUM]\n"
                + "      ,FORMAT([IMSRPHSB],'#,#0.00') AS [IMSRPHSB]\n"
                + "      ,FORMAT([IMSRPSCB],'#,#0.00') AS [IMSRPSCB]\n"
                + "      ,FORMAT([IMSRPBBL],'#,#0.00') AS [IMSRPBBL]\n"
                + "      ,FORMAT([IMSRPMIZ],'#,#0.00') AS [IMSRPMIZ]\n"
                + "      ,FORMAT([IMSRPKRT],'#,#0.00') AS [IMSRPKRT]\n"
                + "      ,FORMAT([IMSRPTOTPAY],'#,#0.00') AS [IMSRPTOTPAY]\n"
                + "      ,FORMAT([IMSRPNOPAY],'#,#0.00') AS [IMSRPNOPAY]\n"
                + "	 ,FORMAT(ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE IMSRPPERIOD = MN.IMSRPPERIOD),0),'#,#0.00')  AS SUMTIAB\n"
                + "  FROM [RMShipment].[dbo].[IMSRP700] MN\n"
                + "  WHERE LEFT([IMSRPPERIOD],4) = '" + year + "'\n"
                + "  ORDER BY IMSRPPERIOD ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP700 p = new IMSRP700();

                p.setPeriod(result.getString("IMSRPPERIOD"));
                p.setCcy(result.getString("IMSRPCCY"));
                p.setMacImpAmt(result.getString("IMSRPMACHINE"));
                p.setMatImpAmt(result.getString("IMSRPOTHER"));
                p.setTotalImpAmt(result.getString("IMSRPTOTAL"));
                p.setRate(result.getString("IMSRPRATE"));
                p.setTotalImpAmtB(result.getString("IMSRPBAHT"));
                p.setBalAmtPrevMonth(result.getString("IMSRPBALCCY"));
                p.setAccInvAmt(result.getString("IMSRPTOTCCY"));
                p.setAllBankBalPayment(result.getString("IMSRPALLCCY"));
                p.setSumitomo(result.getString("IMSRPSUM"));
                p.setHsb(result.getString("IMSRPHSB"));
                p.setScb(result.getString("IMSRPSCB"));
                p.setBbl(result.getString("IMSRPBBL"));
                p.setMizuho(result.getString("IMSRPMIZ"));
                p.setK_tokyo(result.getString("IMSRPKRT"));
                p.setTotal(result.getString("IMSRPTOTPAY"));
                p.setNoPaymentAmt(result.getString("IMSRPNOPAY"));

                p.setSumTiab(result.getString("SUMTIAB"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP700> findByYearTotal(String year) {

        List<IMSRP700> ul = new ArrayList<IMSRP700>();

        String sql = "DECLARE @YEAR NVARCHAR(6) = '" + year + "'\n"
                + "\n"
                + "SELECT 'GTOTAL' AS IMSRPPERIOD, CCY AS IMSRPCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPMACHINE) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPMACHINE\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPOTHER) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPOTHER\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPTOTAL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPTOTAL\n"
                + ",FORMAT(ISNULL((CASE WHEN (ISNULL((SELECT SUM(IMSRPTOTAL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0))=0 THEN 0 \n"
                + "ELSE (ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0)\n"
                + "/ISNULL((SELECT SUM(IMSRPTOTAL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0))\n"
                + "END),0),'#,#0.000000') AS IMSRPRATE\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPBAHT\n"
                + ",FORMAT(ISNULL((SELECT TOP 1 IMSRPNOPAY FROM IMSRP700 \n"
                + "WHERE IMSRPPERIOD = (SELECT TOP 1 IMSRPPERIOD FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY ORDER BY IMSRPPERIOD DESC) \n"
                + "AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPBALCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPTOTCCY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPTOTCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPALLCCY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPALLCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPSUM) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPSUM\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPHSB) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPHSB\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPSCB) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPSCB\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBBL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPBBL\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPMIZ) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPMIZ\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPKRT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPKRT\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPTOTPAY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPTOTPAY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPNOPAY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPNOPAY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR),0),'#,#0.00') AS SUMTIAB\n"
                + "FROM(\n"
                + "SELECT IMSCUCODE AS CCY\n"
                + "FROM IMSCUR\n"
                + "WHERE IMSCUCODE != 'THB'\n"
                + ")CUR";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP700 p = new IMSRP700();

                p.setPeriod(result.getString("IMSRPPERIOD"));
                p.setCcy(result.getString("IMSRPCCY"));
                p.setMacImpAmt(result.getString("IMSRPMACHINE"));
                p.setMatImpAmt(result.getString("IMSRPOTHER"));
                p.setTotalImpAmt(result.getString("IMSRPTOTAL"));
                p.setRate(result.getString("IMSRPRATE"));
                p.setTotalImpAmtB(result.getString("IMSRPBAHT"));
                p.setBalAmtPrevMonth(result.getString("IMSRPBALCCY"));
                p.setAccInvAmt(result.getString("IMSRPTOTCCY"));
                p.setAllBankBalPayment(result.getString("IMSRPALLCCY"));
                p.setSumitomo(result.getString("IMSRPSUM"));
                p.setHsb(result.getString("IMSRPHSB"));
                p.setScb(result.getString("IMSRPSCB"));
                p.setBbl(result.getString("IMSRPBBL"));
                p.setMizuho(result.getString("IMSRPMIZ"));
                p.setK_tokyo(result.getString("IMSRPKRT"));
                p.setTotal(result.getString("IMSRPTOTPAY"));
                p.setNoPaymentAmt(result.getString("IMSRPNOPAY"));

                p.setSumTiab(result.getString("SUMTIAB"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public boolean add(String ym, String ym2, String userid, String imc) {

        boolean result = false;

        String sql = "INSERT INTO IMSRP770 ([IMSRPCOM]\n"
                + "      ,[IMSRPIMC]\n"
                + "      ,[IMSRPPERIOD]\n"
                + "      ,[IMSRPPIMPNO]\n"
                + "      ,[IMSRPIMPNO]\n"
                + "      ,[IMSRPVENDEPT]\n"
                + "      ,[IMSRPINVNO]\n"
                + "      ,[IMSRPPROD]\n"
                + "      ,[IMSRPEST]\n"
                + "      ,[IMSRPINCOTERM]\n"
                + "      ,[IMSRPCCY]\n"
                + "      ,[IMSRPFAMT]\n"
                + "      ,[IMSRPRATE]\n"
                + "      ,[IMSRPAMT]\n"
                + "      ,[IMSRPBOX]\n"
                + "      ,[IMSRPCARRIER]\n"
                + "      ,[IMSRPTRANSPORT]\n"
                + "      ,[IMSRPINVDATE]\n"
                + "      ,[IMSRPDEPARTDATE]\n"
                + "      ,[IMSRPARRIVALDATE]\n"
                + "      ,[IMSRPBL]\n"
                + "      ,[IMSRPNOENTRY]\n"
                + "      ,[IMSRPENTRYDATE]\n"
                + "      ,[IMSRPLGAMT]\n"
                + "      ,[IMSRPIMPDUTY]\n"
                + "      ,[IMSRPDEBITNOTEDATE]\n"
                + "      ,[IMSRPREMARK]\n"
                + "      ,[IMSRPEDT]\n"
                + "      ,[IMSRPCDT]\n"
                + "      ,[IMSRPUSER])\n"
                + "SELECT 'TWC', IMSHIMC, FORMAT(IMSHTRDT,'yyyyMM'), [IMSSDPIMNO], IMSSDIMNO, [IMSSDVCODE], IMSSDINVNO\n"
                + ",IMSSDPROD, [IMSSDESTEXP], [IMSSDINC], [IMSSDCUR], [IMSSDFINVAMT]\n"
                + ",CASE WHEN [IMSHCURRATE] IS NULL THEN NULL WHEN RTRIM(LTRIM([IMSHCURRATE])) = '' THEN NULL ELSE ([dbo].[GET_RATE_IMSSHEAD]([IMSHCURRATE],[IMSSDCUR])) END AS RATE\n"
                + ",[IMSSDINVAMT], [IMSSBOX], [IMSHCARRIER], [IMSHTPCODE], [IMSSDINVDATE], [IMSHDPDATE], [IMSHARDATE]\n"
                + ",[IMSFRBL], [IMSCUETNO], [IMSCUETDATE], [IMSCULGAMT], [IMSCUCASHAMT], [IMSHDBDATE], null\n"
                + ",CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '" + userid + "'\n"
                + "FROM IMSSDETAIL\n"
                + "LEFT JOIN IMSSHEAD ON IMSSHEAD.IMSHPIMNO = IMSSDETAIL.IMSSDPIMNO\n"
                + "LEFT JOIN [IMSFRG] ON [IMSFRG].[IMSFRPIMNO] = IMSSDETAIL.IMSSDPIMNO\n"
                + "LEFT JOIN [IMSCUT] ON [IMSCUT].[IMSCUPIMNO] = IMSSDETAIL.IMSSDPIMNO\n"
                + "WHERE FORMAT(IMSHTRDT,'yyyyMM') BETWEEN '" + ym + "' AND '" + ym2 + "'\n"
                + "AND IMSHIMC = '" + imc + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String ym, String ccy, String rate, String totalImpAmtB, String accInvAmt, String noPaymentAmt) {

        boolean result = false;

        String sql = "UPDATE IMSRP700 "
                + "SET IMSRPRATE = " + rate + ""
                + ", IMSRPBAHT = " + totalImpAmtB + ""
                + ", IMSRPTOTCCY = " + accInvAmt + ""
                + ", IMSRPNOPAY = " + noPaymentAmt + ""
                + ", IMSRPEDT = CURRENT_TIMESTAMP"
                + " WHERE IMSRPPERIOD = '" + ym + "' AND IMSRPCCY = '" + ccy + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String ym, String ym2, String imc) {

        String sql = "DELETE FROM IMSRP770 WHERE IMSRPPERIOD BETWEEN '" + ym + "' AND '" + ym2 + "' AND IMSRPIMC = '" + imc + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
