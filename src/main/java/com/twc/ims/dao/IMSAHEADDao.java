/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.dao;

import com.twc.ims.database.database;
import com.twc.ims.entity.IMSAHEAD;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSAHEADDao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public ResultSet reportIMS110(String advImpNo) {

        String sql = "SELECT IMSAHAVNO, IMSAHSHCD, IMSSCNAME, IMSAHINVAMT, [dbo].[NUM2THAI](IMSAHINVAMT) AS IMSAHINVAMTTB\n"
                + "FROM IMSAHEAD HD\n"
                + "LEFT JOIN IMSSHC HC ON HD.IMSAHSHCD = HC.IMSSCCODE \n"
                + "WHERE IMSAHAVNO = '" + advImpNo + "'";
        ResultSet result = null;
        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            result = ps.executeQuery();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public List<IMSAHEAD> findAll() {

        List<IMSAHEAD> UAList = new ArrayList<IMSAHEAD>();

        String sql = "SELECT IMSAHAVNO, IMSAHFRDT, IMSAHTODT, IMSAHSHCD + ' : ' + IMSSCNAME AS IMSAHSHCD, "
                + "IMSAHCQDT, IMSAHINVAMT, IMSAHUSER + ' : ' + USERS AS IMSAHUSER, IMSAHCQNO, IMSAHBCODE + ' : ' + IMSBADESC AS IMSAHBCODE "
                + "FROM IMSAHEAD "
                + "LEFT JOIN MSSUSER ON MSSUSER.USERID = IMSAHEAD.IMSAHUSER "
                + "LEFT JOIN IMSSHC ON IMSSHC.IMSSCCODE = IMSAHEAD.IMSAHSHCD "
                + "LEFT JOIN IMSBMAS ON IMSBMAS.IMSBACODE = IMSAHEAD.IMSAHBCODE "
                + "ORDER BY IMSAHAVNO ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSAHEAD p = new IMSAHEAD();

                p.setAdvNo(result.getString("IMSAHAVNO"));
                p.setDateF(result.getString("IMSAHFRDT"));
                p.setDateT(result.getString("IMSAHTODT"));
                p.setShipCode(result.getString("IMSAHSHCD"));

                p.setCheckNo(result.getString("IMSAHCQNO"));
                p.setBnkCode(result.getString("IMSAHBCODE"));

                if (result.getString("IMSAHCQDT") != null) {
                    p.setCheckDate(result.getString("IMSAHCQDT").split(" ")[0]);
                }

                double tot = Double.parseDouble(result.getString("IMSAHINVAMT"));
                String tqt = formatDou.format(tot);

                p.setAmount(tqt);
                p.setUser(result.getString("IMSAHUSER"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public IMSAHEAD findByCode(String code) {

        IMSAHEAD p = new IMSAHEAD();

        String sql = "SELECT IMSAHAVNO, IMSAHFRDT, IMSAHTODT, IMSAHSHCD, IMSAHCQDT, IMSAHINVAMT, IMSAHUSER, IMSAHCQNO, IMSAHBCODE "
                + "FROM IMSAHEAD "
                + "WHERE IMSAHAVNO = '" + code + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setAdvNo(result.getString("IMSAHAVNO"));
                p.setCheckNo(result.getString("IMSAHCQNO") == null ? "" : result.getString("IMSAHCQNO"));
                p.setBnkCode(result.getString("IMSAHBCODE") == null ? "" : result.getString("IMSAHBCODE"));

                if (result.getString("IMSAHFRDT") != null) {
                    p.setDateF(result.getString("IMSAHFRDT").split(" ")[0]);
                }
                if (result.getString("IMSAHTODT") != null) {
                    p.setDateT(result.getString("IMSAHTODT").split(" ")[0]);
                }

                p.setShipCode(result.getString("IMSAHSHCD"));

                if (result.getString("IMSAHCQDT") != null) {
                    p.setCheckDate(result.getString("IMSAHCQDT").split(" ")[0]);
                }

                double tot = Double.parseDouble(result.getString("IMSAHINVAMT"));
                String tqt = formatDou.format(tot);

                p.setAmount(tqt);
                p.setUser(result.getString("IMSAHUSER"));

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public String check(String code) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "Select * FROM [RMShipment].[dbo].[IMSAHEAD] where [IMSAHAVNO] = '" + code + "' ";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(String advImpNo, String dateF, String dateT, String shipCom, String checkDate, String finalTotalAmtData, String userid, String checkNo, String bnkCode) {

        boolean result = false;

        String sql = "INSERT INTO IMSAHEAD"
                + " (IMSAHCOM, IMSAHAVNO, IMSAHFRDT, IMSAHTODT, IMSAHSHCD, IMSAHCQDT, IMSAHINVAMT, IMSAHEDT, IMSAHCDT, IMSAHUSER, IMSAHCQNO, IMSAHBCODE)"
                + " VALUES(?, ?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, ?, ?, ?)";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, "TWC");

            ps.setString(2, advImpNo);

            ps.setString(3, dateF);

            ps.setString(4, dateT);

            ps.setString(5, shipCom);

            ps.setString(6, checkDate);

            ps.setString(7, finalTotalAmtData);

            ps.setString(8, userid);

            ps.setString(9, checkNo);

            ps.setString(10, bnkCode);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String advImpNo, String dateF, String dateT, String shipCom, String checkDate, String finalTotalAmtData, String userid, String checkNo, String bnkCode) {

        boolean result = false;

        String sql = "UPDATE IMSAHEAD"
                + " SET IMSAHFRDT = ?, IMSAHTODT = ?, IMSAHSHCD = ?,"
                + " IMSAHCQDT = ?, IMSAHINVAMT = ?, IMSAHEDT = CURRENT_TIMESTAMP, IMSAHUSER = ?,"
                + " IMSAHCQNO = ?, IMSAHBCODE = ?"
                + " WHERE IMSAHAVNO = ? ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(9, advImpNo);

            ps.setString(1, dateF);

            ps.setString(2, dateT);

            ps.setString(3, shipCom);

            ps.setString(4, checkDate);

            ps.setString(5, finalTotalAmtData);

            ps.setString(6, userid);

            ps.setString(7, checkNo);

            ps.setString(8, bnkCode);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean editTB(String advImpNo, String thaiBaht) {

        boolean result = false;

        String sql = "UPDATE IMSAHEAD"
                + " SET IMSAHINVAMTTB = ?"
                + " WHERE IMSAHAVNO = ? ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(2, advImpNo);

            ps.setString(1, thaiBaht);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean delete(String code) {

        boolean result = false;

        String sql = "DELETE FROM IMSAHEAD WHERE IMSAHAVNO = '" + code + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

}
