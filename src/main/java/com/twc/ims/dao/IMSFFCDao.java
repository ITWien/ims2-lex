/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.dao;

import com.twc.ims.database.database;
import com.twc.ims.entity.IMSAPHEAD;
import com.twc.ims.entity.IMSSHC;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSFFCDao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public List<IMSSHC> findAll() {

        List<IMSSHC> UAList = new ArrayList<IMSSHC>();

        String sql = "SELECT [IMSFFCODE]\n"
                + "      ,[IMSFFDESC]\n"
                + "      ,[IMSFFDEST]\n"
                + "  FROM [RMShipment].[dbo].[IMSFFC]";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSSHC p = new IMSSHC();
//
                p.setCode(result.getString("IMSFFCODE"));
                p.setName(result.getString("IMSFFDESC"));
                p.setNamet(result.getString("IMSFFDEST"));

//
                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public IMSSHC findByCode(String code) {

        IMSSHC p = new IMSSHC();

        String sql = "SELECT [IMSFFCODE]\n"
                + "      ,[IMSFFDESC]\n"
                + "      ,[IMSFFDEST]\n"
                + "  FROM [RMShipment].[dbo].[IMSFFC]"
                + "  WHERE IMSFFCODE = '" + code + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

//
                p.setCode(result.getString("IMSFFCODE"));
                p.setName(result.getString("IMSFFDESC"));
                p.setNamet(result.getString("IMSFFDEST"));

//
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public String check(String code) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "Select * FROM [RMShipment].[dbo].[IMSFFC] where [IMSFFCODE] = '" + code + "' ";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(String code, String desc, String desc2, String userid) {

        boolean result = false;

        String sql = "INSERT INTO IMSFFC"
                + " ([IMSFFCOM]\n"
                + "      ,[IMSFFCODE]\n"
                + "      ,[IMSFFDESC]\n"
                + "      ,[IMSFFDEST]\n"
                + "      ,[IMSFFEDT]\n"
                + "      ,[IMSFFCDT]\n"
                + "      ,[IMSFFUSER])"
                + " VALUES(?, ?, ?, ?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, ?)";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, "TWC");

            ps.setString(2, code);

            ps.setString(3, desc);

            ps.setString(4, desc2);

            ps.setString(5, userid);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String code, String desc, String desc2, String uid) {

        boolean result = false;

        String sql = "UPDATE IMSFFC SET IMSFFDESC = '" + desc + "', IMSFFDEST = '" + desc2 + "', IMSFFEDT = CURRENT_TIMESTAMP"
                + ", IMSFFUSER = '" + uid + "'"
                + " WHERE IMSFFCODE = '" + code + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String code) {

        String sql = "DELETE FROM IMSFFC WHERE IMSFFCODE = '" + code + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
