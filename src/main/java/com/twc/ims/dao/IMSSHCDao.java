/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.dao;

import com.twc.ims.database.database;
import com.twc.ims.entity.IMSSHC;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSSHCDao extends database {

    public List<IMSSHC> findAll() {

        List<IMSSHC> UAList = new ArrayList<IMSSHC>();

        String sql = "SELECT IMSSCCODE, IMSSCNAME, IMSSCNAMT "
                + "FROM IMSSHC "
                + "ORDER BY IMSSCCODE ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSSHC p = new IMSSHC();

                p.setCode(result.getString("IMSSCCODE"));

                p.setName(result.getString("IMSSCNAME"));

                p.setNamet(result.getString("IMSSCNAMT"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public IMSSHC findByCode(String code) {

        IMSSHC p = new IMSSHC();

        String sql = "SELECT IMSSCCODE, IMSSCNAME, IMSSCNAMT "
                + "FROM IMSSHC "
                + "WHERE IMSSCCODE = '" + code + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setCode(result.getString("IMSSCCODE"));

                p.setName(result.getString("IMSSCNAME"));

                p.setNamet(result.getString("IMSSCNAMT"));

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

}
