/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.dao;

import com.twc.ims.database.database;
import com.twc.ims.entity.IMSAHEAD;
import com.twc.ims.entity.IMSAPDETAIL;
import com.twc.ims.entity.IMSSDETAIL;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSINSDao extends database {

    public ResultSet reportIMS670D(String ym, String imc) {

        String sql = "SELECT IMSINPIMNO, IMSHIMC, IMSININV, IMSINCODE, IMSFFDEST, IMSINPLCNO, IMSINIMNO, (ISNULL(IMSINAMT,0) + ISNULL(IMSINSAMT,0)) AS IMSINAMT, IMSINVAT\n"
                + "FROM IMSINS\n"
                + "LEFT JOIN IMSFFC ON IMSFFC.IMSFFCODE = IMSINS.IMSINCODE\n"
                + "LEFT JOIN IMSSHEAD ON IMSSHEAD.IMSHPIMNO = IMSINS.IMSINPIMNO\n"
                + "WHERE FORMAT(IMSINDATE,'yyyyMM') = '" + ym + "'\n"
                + "AND IMSHIMC = '" + imc + "'\n"
                + "ORDER BY IMSININV, IMSINCODE, IMSINPLCNO, IMSINIMNO";
        ResultSet result = null;
        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            result = ps.executeQuery();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public ResultSet reportIMS670JV(String ym, String imc) {

        String sql = "SELECT DISTINCT IMSHIMC, INSINV, INS, IMSFFDEST, TOT_PR, TOT_VA, TOT, TOTAL_THAI, ALL_INV_NO\n"
                + "FROM(\n"
                + "SELECT IMSINPIMNO, IMSHIMC, INSINV, INS, IMSFFDEST\n"
                + ",(SELECT SUM((ISNULL(IMSINAMT,0) + ISNULL(IMSINSAMT,0))) FROM IMSINS WHERE IMSININV = INSINV AND IMSINCODE = INS) AS TOT_PR\n"
                + ",(SELECT SUM(IMSINVAT) FROM IMSINS WHERE IMSININV = INSINV AND IMSINCODE = INS) AS TOT_VA\n"
                + ",(\n"
                + "ISNULL((SELECT SUM((ISNULL(IMSINAMT,0) + ISNULL(IMSINSAMT,0))) FROM IMSINS WHERE IMSININV = INSINV AND IMSINCODE = INS),0)\n"
                + "+\n"
                + "ISNULL((SELECT SUM(IMSINVAT) FROM IMSINS WHERE IMSININV = INSINV AND IMSINCODE = INS),0)\n"
                + ") AS TOT\n"
                + ",DBO.NUM2THAI((\n"
                + "ISNULL((SELECT SUM((ISNULL(IMSINAMT,0) + ISNULL(IMSINSAMT,0))) FROM IMSINS WHERE IMSININV = INSINV AND IMSINCODE = INS),0)\n"
                + "+\n"
                + "ISNULL((SELECT SUM(IMSINVAT) FROM IMSINS WHERE IMSININV = INSINV AND IMSINCODE = INS),0)\n"
                + ")) AS TOTAL_THAI\n"
                + ",(\n"
                + "	      SELECT STUFF((SELECT ', ' + IMSSDINVNO\n"
                + "          FROM IMSSDETAIL\n"
                + "          WHERE IMSSDPIMNO = IMSINPIMNO\n"
                + "		  FOR XML PATH('')), 1, 1, '')\n"
                + "	   ) AS ALL_INV_NO\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSINPIMNO, IMSININV AS INSINV, IMSINCODE AS INS\n"
                + "FROM IMSINS\n"
                + "WHERE FORMAT(IMSINDATE,'yyyyMM') = '" + ym + "'\n"
                + ")TMP\n"
                + "LEFT JOIN IMSFFC ON IMSFFC.IMSFFCODE = TMP.INS\n"
                + "LEFT JOIN IMSSHEAD ON IMSSHEAD.IMSHPIMNO = TMP.IMSINPIMNO\n"
                + "WHERE IMSHIMC = '" + imc + "'\n"
                + ")TTT";
        ResultSet result = null;
        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            result = ps.executeQuery();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

}
