/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.dao;

import com.twc.ims.database.database;
import com.twc.ims.entity.IMSAPHEAD;
import com.twc.ims.entity.IMSRP700;
import com.twc.ims.entity.IMSRP710;
import com.twc.ims.entity.IMSRP770;
import com.twc.ims.entity.IMSRP780;
import com.twc.ims.entity.IMSSHC;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSRP780Dao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public int countByPeriod(String ym, String ym2, String imc) {

        int cnt = 0;

        String sql = "SELECT COUNT(*) AS CNT\n"
                + "  FROM IMSSHEAD\n"
                + "  WHERE FORMAT(IMSHTRDT,'yyyyMM') BETWEEN '" + ym + "' AND '" + ym2 + "' AND IMSHIMC = '" + imc + "' AND [IMSHVDCODE] IN ('X0100001','X0300002')";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                cnt = result.getInt("CNT");

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return cnt;

    }

    public String findTotImpAmt(String ym) {

        String TotImpAmt = "";

        String sql = "SELECT ISNULL(SUM([IMSRPBAHT]),0) as SUMTIAB\n"
                + "  FROM [RMShipment].[dbo].[IMSRP700]\n"
                + "  WHERE [IMSRPPERIOD] = '" + ym + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                TotImpAmt = result.getString("SUMTIAB");

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return TotImpAmt;

    }

    public List<IMSRP780> findByCode(String ym, String ym2, String imc) {

        List<IMSRP780> ul = new ArrayList<IMSRP780>();

        String sql = "SELECT [IMSRPIMC]\n"
                + "      ,[IMSRPPERIOD]\n"
                + "      ,[IMSRPORDERNO]\n"
                + "	  ,ROW_NUMBER() OVER (PARTITION BY [IMSRPORDERNO] ORDER BY [IMSRPORDERNO],[IMSRPPIMPNO],IMSVDNAME,[IMSRPINVNO],[IMSRPPROD]) AS GRPORDER\n"
                + "      ,[IMSRPPIMPNO]\n"
                + "      ,[IMSRPIMPNO]\n"
                + "      ,[IMSVDNAME] as [IMSRPVENDEPT]\n"
                + "      ,[IMSRPINVNO]\n"
                + "      ,[IMSRPPROD]\n"
                + "      ,[IMSRPINCOTERM]\n"
                + "      ,[IMSRPCCY]\n"
                + "      ,FORMAT([IMSRPFAMT],'#,#0.00') AS [IMSRPFAMT]\n"
                + "      ,FORMAT([IMSRPRATE],'#,#0.000000') AS [IMSRPRATE]\n"
                + "      ,FORMAT([IMSRPAMT],'#,#0.00') AS [IMSRPAMT]\n"
                + "      ,FORMAT([IMSRPHANDERING],'#,#0.00') AS [IMSRPHANDERING]\n"
                + "      ,FORMAT([IMSRPINSUR],'#,#0.00') AS [IMSRPINSUR]\n"
                + "      ,FORMAT([IMSRPFEE],'#,#0.00') AS [IMSRPFEE]\n"
                + "      ,FORMAT([IMSRPRENT],'#,#0.00') AS [IMSRPRENT]\n"
                + "      ,FORMAT([IMSRPCLEARING],'#,#0.00') AS [IMSRPCLEARING]\n"
                + "      ,FORMAT([IMSRPCASH],'#,#0.00') AS [IMSRPCASH]\n"
                + "      ,FORMAT([IMSRPVAT],'#,#0.00') AS [IMSRPVAT]\n"
                + "      ,FORMAT([IMSRPSUMEXPENSE],'#,#0.00') AS [IMSRPSUMEXPENSE]\n"
                + "  FROM [RMShipment].[dbo].[IMSRP780]\n"
                + "  LEFT JOIN IMSVEN ON IMSVEN.IMSVDCODE = [IMSRP780].[IMSRPVENDEPT]\n"
                + "  WHERE [IMSRPPERIOD] BETWEEN '" + ym + "' AND '" + ym2 + "'\n"
                + "  AND [IMSRPIMC] = '" + imc + "'\n"
                + "  ORDER BY [IMSRPORDERNO],[IMSRPPIMPNO],IMSVDNAME,[IMSRPINVNO],[IMSRPPROD]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP780 p = new IMSRP780();

                p.setIMSRPIMC(result.getString("IMSRPIMC"));
                p.setIMSRPPERIOD(result.getString("IMSRPPERIOD"));

                p.setIMSRPORDERNO(result.getString("IMSRPORDERNO"));

                if (result.getString("GRPORDER") != null) {
                    if (result.getString("GRPORDER").trim().equals("1")) {
                        p.setIMSRPORDERNO(result.getString("IMSRPORDERNO"));
                    } else {
                        p.setIMSRPORDERNO("<b style=\"opacity: 0;\">" + result.getString("IMSRPORDERNO") + "</b>");
                    }
                }

                p.setIMSRPPIMPNO(result.getString("IMSRPPIMPNO"));
                p.setIMSRPIMPNO(result.getString("IMSRPIMPNO"));
                p.setIMSRPVENDEPT(result.getString("IMSRPVENDEPT"));
                p.setIMSRPINVNO(result.getString("IMSRPINVNO"));
                p.setIMSRPPROD(result.getString("IMSRPPROD"));
                p.setIMSRPINCOTERM(result.getString("IMSRPINCOTERM"));
                p.setIMSRPCCY(result.getString("IMSRPCCY"));
                p.setIMSRPFAMT(result.getString("IMSRPFAMT"));
                p.setIMSRPRATE(result.getString("IMSRPRATE"));
                p.setIMSRPAMT(result.getString("IMSRPAMT"));
                p.setIMSRPHANDERING(result.getString("IMSRPHANDERING"));
                p.setIMSRPINSUR(result.getString("IMSRPINSUR"));
                p.setIMSRPFEE(result.getString("IMSRPFEE"));
                p.setIMSRPRENT(result.getString("IMSRPRENT"));
                p.setIMSRPCLEARING(result.getString("IMSRPCLEARING"));
                p.setIMSRPCASH(result.getString("IMSRPCASH"));
                p.setIMSRPVAT(result.getString("IMSRPVAT"));
                p.setIMSRPSUMEXPENSE(result.getString("IMSRPSUMEXPENSE"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP780> findForPrint(String ym, String ym2, String imc) {

        List<IMSRP780> ul = new ArrayList<IMSRP780>();

        String sql = "SELECT [IMSRPIMC]\n"
                + "      ,[IMSRPPERIOD]\n"
                + "      ,[IMSRPORDERNO]\n"
                + "	  ,ROW_NUMBER() OVER (PARTITION BY [IMSRPORDERNO] ORDER BY [IMSRPORDERNO],[IMSRPPIMPNO],IMSVDNAME,[IMSRPINVNO],[IMSRPPROD]) AS GRPORDER\n"
                + "      ,[IMSRPPIMPNO]\n"
                + "      ,[IMSRPIMPNO]\n"
                + "      ,[IMSVDNAME] as [IMSRPVENDEPT]\n"
                + "      ,[IMSRPINVNO]\n"
                + "      ,[IMSRPPROD]\n"
                + "      ,[IMSRPINCOTERM]\n"
                + "      ,[IMSRPCCY]\n"
                + "      ,FORMAT([IMSRPFAMT],'#,#0.00') AS [IMSRPFAMT]\n"
                + "      ,FORMAT([IMSRPRATE],'#,#0.000000') AS [IMSRPRATE]\n"
                + "      ,FORMAT([IMSRPAMT],'#,#0.00') AS [IMSRPAMT]\n"
                + "      ,FORMAT([IMSRPHANDERING],'#,#0.00') AS [IMSRPHANDERING]\n"
                + "      ,FORMAT([IMSRPINSUR],'#,#0.00') AS [IMSRPINSUR]\n"
                + "      ,FORMAT([IMSRPFEE],'#,#0.00') AS [IMSRPFEE]\n"
                + "      ,FORMAT([IMSRPRENT],'#,#0.00') AS [IMSRPRENT]\n"
                + "      ,FORMAT([IMSRPCLEARING],'#,#0.00') AS [IMSRPCLEARING]\n"
                + "      ,FORMAT([IMSRPCASH],'#,#0.00') AS [IMSRPCASH]\n"
                + "      ,FORMAT([IMSRPVAT],'#,#0.00') AS [IMSRPVAT]\n"
                + "      ,FORMAT([IMSRPSUMEXPENSE],'#,#0.00') AS [IMSRPSUMEXPENSE]\n"
                + "  FROM [RMShipment].[dbo].[IMSRP780]\n"
                + "  LEFT JOIN IMSVEN ON IMSVEN.IMSVDCODE = [IMSRP780].[IMSRPVENDEPT]\n"
                + "  WHERE [IMSRPPERIOD] BETWEEN '" + ym + "' AND '" + ym2 + "'\n"
                + "  AND [IMSRPIMC] = '" + imc + "'\n"
                + "  ORDER BY [IMSRPORDERNO],[IMSRPPIMPNO],IMSVDNAME,[IMSRPINVNO],[IMSRPPROD]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP780 p = new IMSRP780();

                p.setIMSRPIMC(result.getString("IMSRPIMC"));
                p.setIMSRPPERIOD(result.getString("IMSRPPERIOD"));

                p.setGRPORDER(result.getString("GRPORDER"));
                p.setIMSRPORDERNO(result.getString("IMSRPORDERNO"));

                if (result.getString("GRPORDER") != null) {
                    if (result.getString("GRPORDER").trim().equals("1")) {
                        p.setIMSRPORDERNO(result.getString("IMSRPORDERNO") + "<head>");
                    } else {
                        p.setIMSRPORDERNO(result.getString("IMSRPORDERNO"));
                    }
                }

                p.setIMSRPPIMPNO(result.getString("IMSRPPIMPNO"));
                p.setIMSRPIMPNO(result.getString("IMSRPIMPNO"));
                p.setIMSRPVENDEPT(result.getString("IMSRPVENDEPT"));
                p.setIMSRPINVNO(result.getString("IMSRPINVNO"));
                p.setIMSRPPROD(result.getString("IMSRPPROD"));
                p.setIMSRPINCOTERM(result.getString("IMSRPINCOTERM"));
                p.setIMSRPCCY(result.getString("IMSRPCCY"));
                p.setIMSRPFAMT(result.getString("IMSRPFAMT"));
                p.setIMSRPRATE(result.getString("IMSRPRATE"));
                p.setIMSRPAMT(result.getString("IMSRPAMT"));
                p.setIMSRPHANDERING(result.getString("IMSRPHANDERING"));
                p.setIMSRPINSUR(result.getString("IMSRPINSUR"));
                p.setIMSRPFEE(result.getString("IMSRPFEE"));
                p.setIMSRPRENT(result.getString("IMSRPRENT"));
                p.setIMSRPCLEARING(result.getString("IMSRPCLEARING"));
                p.setIMSRPCASH(result.getString("IMSRPCASH"));
                p.setIMSRPVAT(result.getString("IMSRPVAT"));
                p.setIMSRPSUMEXPENSE(result.getString("IMSRPSUMEXPENSE"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP770> findForPrint2(String ym, String ym2, String imc) {

        List<IMSRP770> ul = new ArrayList<IMSRP770>();

        String sql = "SELECT [IMSRPCCY]\n"
                + "      ,FORMAT(SUM([IMSRPFAMT]),'#,#0.00') as [IMSRPFAMT]\n"
                + "      ,FORMAT(SUM([IMSRPAMT]),'#,#0.00') as [IMSRPAMT]\n"
                + "  FROM [RMShipment].[dbo].[IMSRP770]\n"
                + "  WHERE [IMSRPPERIOD] BETWEEN '" + ym + "' AND '" + ym2 + "'\n"
                + "  AND [IMSRPIMC] = '" + imc + "'\n"
                + "  AND [IMSRPCCY] is not null\n"
                + "  AND [IMSRPCCY] != ''\n"
                + "  GROUP BY [IMSRPCCY]\n"
                + "  ORDER BY [IMSRPCCY]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP770 p = new IMSRP770();

                p.setIMSRPCCY(result.getString("IMSRPCCY"));
                p.setIMSRPFAMT(result.getString("IMSRPFAMT"));
                p.setIMSRPAMT(result.getString("IMSRPAMT"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findTotalByDept(String ym) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT *\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPINVAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0),'#,#0.00') AS SUMIA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPPAYAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0),'#,#0.00') AS SUMPA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPDIFFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPDEPT AS DEPT\n"
                + "FROM IMSRP710\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setDept("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("DEPT") + "3</font><b>TOTAL BY DEPT : </b>");
                p.setBank("<b>" + result.getString("DEPT") + "</b>");

                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setInvAmt("<b>" + result.getString("SUMIA") + "</b>");
                p.setPayAmt("<b>" + result.getString("SUMPA") + "</b>");
                p.setDiffAmt("<b>" + result.getString("SUMDA") + "</b>");

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findTotalByBank(String ym) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT *\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPINVAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK),0),'#,#0.00') AS SUMIA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPPAYAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK),0),'#,#0.00') AS SUMPA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPDIFFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK),0),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPDEPT AS DEPT, IMSRPBCODE AS BANK\n"
                + "FROM IMSRP710\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setDept("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("DEPT") + "2</font><font size=\"1\" style=\"opacity: 0;\">TOTAL BY DEPT : " + result.getString("DEPT") + "</font>");
                p.setBank("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("BANK") + "2</font><b>TOTAL BY BANK : </b>");
                p.setPayDate("<b>" + result.getString("BANK") + "</b>");

                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setInvAmt("<b>" + result.getString("SUMIA") + "</b>");
                p.setPayAmt("<b>" + result.getString("SUMPA") + "</b>");
                p.setDiffAmt("<b>" + result.getString("SUMDA") + "</b>");

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findTotalByCcy(String ym) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT *\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPINVAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMIA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPPAYAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMPA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPDIFFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPDEPT AS DEPT, IMSRPBCODE AS BANK, IMSRPCCY AS CCY\n"
                + "FROM IMSRP710\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setDept("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("DEPT") + "1</font><font size=\"1\" style=\"opacity: 0;\">TOTAL BY DEPT : " + result.getString("DEPT") + "</font>");
                p.setBank("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("BANK") + "1</font><font size=\"1\" style=\"opacity: 0;\">TOTAL BY BANK : " + result.getString("BANK") + "</font>");
                p.setCcy("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("CCY") + "</font><b>TOTAL BY CCY : </b>");
                p.setPayRate("<b>" + result.getString("CCY") + "</b>");

                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setInvAmt("<b>" + result.getString("SUMIA") + "</b>");
                p.setPayAmt("<b>" + result.getString("SUMPA") + "</b>");
                p.setDiffAmt("<b>" + result.getString("SUMDA") + "</b>");

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findTotalByMonth(String ym) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT FORMAT(SUM(SUMFA),'#,#0.00') AS SUMFA, \n"
                + "FORMAT(SUM(SUMIA),'#,#0.00') AS SUMIA, \n"
                + "FORMAT(SUM(SUMPA),'#,#0.00') AS SUMPA, \n"
                + "FORMAT(SUM(SUMDA),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT *\n"
                + ",ISNULL((SELECT SUM([IMSRPFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0) AS SUMFA\n"
                + ",ISNULL((SELECT SUM([IMSRPINVAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0) AS SUMIA\n"
                + ",ISNULL((SELECT SUM([IMSRPPAYAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0) AS SUMPA\n"
                + ",ISNULL((SELECT SUM([IMSRPDIFFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0) AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPPERIOD, IMSRPDEPT AS DEPT\n"
                + "FROM IMSRP710\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + ")TMP\n"
                + ")TMP2";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setDept("<font size=\"1\" style=\"opacity: 0;\">Z</font><b>GRAND TOTAL </b>");
                p.setBank("<b>BY MONTH : " + ym + "</b>");

                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setInvAmt("<b>" + result.getString("SUMIA") + "</b>");
                p.setPayAmt("<b>" + result.getString("SUMPA") + "</b>");
                p.setDiffAmt("<b>" + result.getString("SUMDA") + "</b>");

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP700> findByYear(String year) {

        List<IMSRP700> ul = new ArrayList<IMSRP700>();

        String sql = "SELECT [IMSRPPERIOD]\n"
                + "      ,[IMSRPCCY]\n"
                + "      ,FORMAT([IMSRPMACHINE],'#,#0.00') AS [IMSRPMACHINE]\n"
                + "      ,FORMAT([IMSRPOTHER],'#,#0.00') AS [IMSRPOTHER]\n"
                + "      ,FORMAT([IMSRPTOTAL],'#,#0.00') AS [IMSRPTOTAL]\n"
                + "      ,FORMAT([IMSRPRATE],'#,#0.000000') AS [IMSRPRATE]\n"
                + "      ,FORMAT([IMSRPBAHT],'#,#0.00') AS [IMSRPBAHT]\n"
                + "      ,FORMAT([IMSRPBALCCY],'#,#0.00') AS [IMSRPBALCCY]\n"
                + "      ,FORMAT([IMSRPTOTCCY],'#,#0.00') AS [IMSRPTOTCCY]\n"
                + "      ,FORMAT([IMSRPALLCCY],'#,#0.00') AS [IMSRPALLCCY]\n"
                + "      ,FORMAT([IMSRPSUM],'#,#0.00') AS [IMSRPSUM]\n"
                + "      ,FORMAT([IMSRPHSB],'#,#0.00') AS [IMSRPHSB]\n"
                + "      ,FORMAT([IMSRPSCB],'#,#0.00') AS [IMSRPSCB]\n"
                + "      ,FORMAT([IMSRPBBL],'#,#0.00') AS [IMSRPBBL]\n"
                + "      ,FORMAT([IMSRPMIZ],'#,#0.00') AS [IMSRPMIZ]\n"
                + "      ,FORMAT([IMSRPKRT],'#,#0.00') AS [IMSRPKRT]\n"
                + "      ,FORMAT([IMSRPTOTPAY],'#,#0.00') AS [IMSRPTOTPAY]\n"
                + "      ,FORMAT([IMSRPNOPAY],'#,#0.00') AS [IMSRPNOPAY]\n"
                + "	 ,FORMAT(ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE IMSRPPERIOD = MN.IMSRPPERIOD),0),'#,#0.00')  AS SUMTIAB\n"
                + "  FROM [RMShipment].[dbo].[IMSRP700] MN\n"
                + "  WHERE LEFT([IMSRPPERIOD],4) = '" + year + "'\n"
                + "  ORDER BY IMSRPPERIOD ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP700 p = new IMSRP700();

                p.setPeriod(result.getString("IMSRPPERIOD"));
                p.setCcy(result.getString("IMSRPCCY"));
                p.setMacImpAmt(result.getString("IMSRPMACHINE"));
                p.setMatImpAmt(result.getString("IMSRPOTHER"));
                p.setTotalImpAmt(result.getString("IMSRPTOTAL"));
                p.setRate(result.getString("IMSRPRATE"));
                p.setTotalImpAmtB(result.getString("IMSRPBAHT"));
                p.setBalAmtPrevMonth(result.getString("IMSRPBALCCY"));
                p.setAccInvAmt(result.getString("IMSRPTOTCCY"));
                p.setAllBankBalPayment(result.getString("IMSRPALLCCY"));
                p.setSumitomo(result.getString("IMSRPSUM"));
                p.setHsb(result.getString("IMSRPHSB"));
                p.setScb(result.getString("IMSRPSCB"));
                p.setBbl(result.getString("IMSRPBBL"));
                p.setMizuho(result.getString("IMSRPMIZ"));
                p.setK_tokyo(result.getString("IMSRPKRT"));
                p.setTotal(result.getString("IMSRPTOTPAY"));
                p.setNoPaymentAmt(result.getString("IMSRPNOPAY"));

                p.setSumTiab(result.getString("SUMTIAB"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP700> findByYearTotal(String year) {

        List<IMSRP700> ul = new ArrayList<IMSRP700>();

        String sql = "DECLARE @YEAR NVARCHAR(6) = '" + year + "'\n"
                + "\n"
                + "SELECT 'GTOTAL' AS IMSRPPERIOD, CCY AS IMSRPCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPMACHINE) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPMACHINE\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPOTHER) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPOTHER\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPTOTAL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPTOTAL\n"
                + ",FORMAT(ISNULL((CASE WHEN (ISNULL((SELECT SUM(IMSRPTOTAL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0))=0 THEN 0 \n"
                + "ELSE (ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0)\n"
                + "/ISNULL((SELECT SUM(IMSRPTOTAL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0))\n"
                + "END),0),'#,#0.000000') AS IMSRPRATE\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPBAHT\n"
                + ",FORMAT(ISNULL((SELECT TOP 1 IMSRPNOPAY FROM IMSRP700 \n"
                + "WHERE IMSRPPERIOD = (SELECT TOP 1 IMSRPPERIOD FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY ORDER BY IMSRPPERIOD DESC) \n"
                + "AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPBALCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPTOTCCY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPTOTCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPALLCCY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPALLCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPSUM) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPSUM\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPHSB) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPHSB\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPSCB) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPSCB\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBBL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPBBL\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPMIZ) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPMIZ\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPKRT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPKRT\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPTOTPAY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPTOTPAY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPNOPAY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPNOPAY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR),0),'#,#0.00') AS SUMTIAB\n"
                + "FROM(\n"
                + "SELECT IMSCUCODE AS CCY\n"
                + "FROM IMSCUR\n"
                + "WHERE IMSCUCODE != 'THB'\n"
                + ")CUR";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP700 p = new IMSRP700();

                p.setPeriod(result.getString("IMSRPPERIOD"));
                p.setCcy(result.getString("IMSRPCCY"));
                p.setMacImpAmt(result.getString("IMSRPMACHINE"));
                p.setMatImpAmt(result.getString("IMSRPOTHER"));
                p.setTotalImpAmt(result.getString("IMSRPTOTAL"));
                p.setRate(result.getString("IMSRPRATE"));
                p.setTotalImpAmtB(result.getString("IMSRPBAHT"));
                p.setBalAmtPrevMonth(result.getString("IMSRPBALCCY"));
                p.setAccInvAmt(result.getString("IMSRPTOTCCY"));
                p.setAllBankBalPayment(result.getString("IMSRPALLCCY"));
                p.setSumitomo(result.getString("IMSRPSUM"));
                p.setHsb(result.getString("IMSRPHSB"));
                p.setScb(result.getString("IMSRPSCB"));
                p.setBbl(result.getString("IMSRPBBL"));
                p.setMizuho(result.getString("IMSRPMIZ"));
                p.setK_tokyo(result.getString("IMSRPKRT"));
                p.setTotal(result.getString("IMSRPTOTPAY"));
                p.setNoPaymentAmt(result.getString("IMSRPNOPAY"));

                p.setSumTiab(result.getString("SUMTIAB"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public boolean add(String ym, String ym2, String userid, String imc) {

        boolean result = false;

        String sql = "INSERT INTO IMSRP780 ([IMSRPCOM]\n"
                + "      ,[IMSRPIMC]\n"
                + "      ,[IMSRPPERIOD]\n"
                + "      ,[IMSRPORDERNO]\n"
                + "      ,[IMSRPPIMPNO]\n"
                + "      ,[IMSRPIMPNO]\n"
                + "      ,[IMSRPVENDEPT]\n"
                + "      ,[IMSRPINVNO]\n"
                + "      ,[IMSRPPROD]\n"
                + "      ,[IMSRPINCOTERM]\n"
                + "      ,[IMSRPCCY]\n"
                + "      ,[IMSRPFAMT]\n"
                + "      ,[IMSRPRATE]\n"
                + "      ,[IMSRPAMT]\n"
                + "      ,[IMSRPHANDERING]\n"
                + "      ,[IMSRPINSUR]\n"
                + "      ,[IMSRPFEE]\n"
                + "      ,[IMSRPRENT]\n"
                + "      ,[IMSRPCLEARING]\n"
                + "      ,[IMSRPCASH]\n"
                + "      ,[IMSRPVAT]\n"
                + "      ,[IMSRPSUMEXPENSE]\n"
                + "      ,[IMSRPEDT]\n"
                + "      ,[IMSRPCDT]\n"
                + "      ,[IMSRPUSER])\n"
                + "SELECT *, (ISNULL(TMP.IMSFRHDAMT,0) + ISNULL(TMP.INSUR,0) + ISNULL(TMP.IMSCUFEEAMT,0) + ISNULL(TMP.RENT,0) + ISNULL(TMP.CLEARING,0) + ISNULL(TMP.IMSCUCASHAMT,0)) AS SUMEPENSE\n"
                + ",CURRENT_TIMESTAMP AS EDT, CURRENT_TIMESTAMP AS CDT, '" + userid + "' AS USERID\n"
                + "FROM(\n"
                + "SELECT 'TWC' AS COM, IMSHIMC, FORMAT(IMSHTRDT,'yyyyMM') AS SHIPDATE, [IMSSDORDNO], [IMSSDPIMNO], IMSSDIMNO\n"
                + ", [IMSSDVCODE], IMSSDINVNO\n"
                + ",IMSSDPROD, [IMSSDINC], [IMSSDCUR], [IMSSDFINVAMT]\n"
                + ",CASE WHEN [IMSHCURRATE] IS NULL THEN NULL WHEN RTRIM(LTRIM([IMSHCURRATE])) = '' THEN NULL ELSE ([dbo].[GET_RATE_IMSSHEAD]([IMSHCURRATE],[IMSSDCUR])) END AS RATE\n"
                + ",[IMSSDINVAMT], [IMSFRHDAMT], ([IMSINAMT]+[IMSINSAMT]) AS INSUR, [IMSCUFEEAMT]\n"
                + ",(SELECT SUM([IMSRNAMT])*([IMSSDESTEXP]/100) FROM [IMSRNT] WHERE [IMSRNPIMNO] = [IMSSDPIMNO]) AS RENT\n"
                + ",([IMSCLTOT]-([IMSCLVAT]+[IMSCLTAX3]+[IMSCLTAX1]))*([IMSSDESTEXP]/100) AS CLEARING\n"
                + ",[IMSCUCASHAMT], [IMSCUVATAMT]\n"
                + "FROM IMSSDETAIL\n"
                + "LEFT JOIN IMSSHEAD ON IMSSHEAD.IMSHPIMNO = IMSSDETAIL.IMSSDPIMNO\n"
                + "LEFT JOIN [IMSFRG] ON [IMSFRG].[IMSFRPIMNO] = IMSSDETAIL.IMSSDPIMNO\n"
                + "LEFT JOIN [IMSCUT] ON [IMSCUT].[IMSCUPIMNO] = IMSSDETAIL.IMSSDPIMNO\n"
                + "LEFT JOIN [IMSINS] ON [IMSINS].[IMSINPIMNO] = IMSSDETAIL.IMSSDPIMNO\n"
                + "LEFT JOIN [IMSCLR] ON [IMSCLR].[IMSCLPIMNO] = IMSSDETAIL.IMSSDPIMNO\n"
                + "WHERE FORMAT(IMSHTRDT,'yyyyMM') BETWEEN '" + ym + "' AND '" + ym2 + "'\n"
                + "AND IMSHIMC = '" + imc + "'\n"
                + "AND [IMSHVDCODE] IN ('X0100001','X0300002')\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String ym, String ccy, String rate, String totalImpAmtB, String accInvAmt, String noPaymentAmt) {

        boolean result = false;

        String sql = "UPDATE IMSRP700 "
                + "SET IMSRPRATE = " + rate + ""
                + ", IMSRPBAHT = " + totalImpAmtB + ""
                + ", IMSRPTOTCCY = " + accInvAmt + ""
                + ", IMSRPNOPAY = " + noPaymentAmt + ""
                + ", IMSRPEDT = CURRENT_TIMESTAMP"
                + " WHERE IMSRPPERIOD = '" + ym + "' AND IMSRPCCY = '" + ccy + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String ym, String ym2, String imc) {

        String sql = "DELETE FROM IMSRP780 WHERE IMSRPPERIOD BETWEEN '" + ym + "' AND '" + ym2 + "' AND IMSRPIMC = '" + imc + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
