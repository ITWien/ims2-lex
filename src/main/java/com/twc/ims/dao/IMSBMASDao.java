/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.dao;

import com.twc.ims.database.database;
import com.twc.ims.entity.IMSAPHEAD;
import com.twc.ims.entity.IMSSHC;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSBMASDao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public List<IMSSHC> findAll() {

        List<IMSSHC> UAList = new ArrayList<IMSSHC>();

        String sql = "SELECT [IMSBACODE]\n"
                + "      ,[IMSBADESC]\n"
                + "      ,[IMSBADEST]\n"
                + "      ,[IMSBTYPE]\n"
                + "  FROM [RMShipment].[dbo].[IMSBMAS]";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSSHC p = new IMSSHC();
//
                p.setCode(result.getString("IMSBACODE"));
                p.setName(result.getString("IMSBADESC"));
                p.setNamet(result.getString("IMSBADEST"));
                p.setType(result.getString("IMSBTYPE"));

//
                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public IMSSHC findByCode(String code) {

        IMSSHC p = new IMSSHC();

        String sql = "SELECT [IMSBACODE]\n"
                + "      ,[IMSBADESC]\n"
                + "      ,[IMSBADEST]\n"
                + "      ,[IMSBTYPE]\n"
                + "  FROM [RMShipment].[dbo].[IMSBMAS]"
                + "  WHERE IMSBACODE = '" + code + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

//
                p.setCode(result.getString("IMSBACODE"));
                p.setName(result.getString("IMSBADESC"));
                p.setNamet(result.getString("IMSBADEST"));
                p.setType(result.getString("IMSBTYPE"));

//
            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public String check(String code) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "Select * FROM [RMShipment].[dbo].[IMSBMAS] where [IMSBACODE] = '" + code + "' ";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(String code, String desc, String desc2, String type, String userid) {

        boolean result = false;

        String sql = "INSERT INTO IMSBMAS"
                + " ([IMSBACOM]\n"
                + "      ,[IMSBACODE]\n"
                + "      ,[IMSBADESC]\n"
                + "      ,[IMSBADEST]\n"
                + "      ,[IMSBTYPE]\n"
                + "      ,[IMSBAEDT]\n"
                + "      ,[IMSBACDT]\n"
                + "      ,[IMSBAUSER])"
                + " VALUES(?, ?, ?, ?, ?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, ?)";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, "TWC");

            ps.setString(2, code);

            ps.setString(3, desc);

            ps.setString(4, desc2);

            ps.setString(5, type);

            ps.setString(6, userid);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String code, String desc, String desc2, String type, String uid) {

        boolean result = false;

        String sql = "UPDATE IMSBMAS "
                + "SET IMSBADESC = '" + desc + "'"
                + ", IMSBADEST = '" + desc2 + "'"
                + ", IMSBTYPE = '" + type + "'"
                + ", IMSBAEDT = CURRENT_TIMESTAMP"
                + ", IMSBAUSER = '" + uid + "'"
                + " WHERE IMSBACODE = '" + code + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String code) {

        String sql = "DELETE FROM IMSBMAS WHERE IMSBACODE = '" + code + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
