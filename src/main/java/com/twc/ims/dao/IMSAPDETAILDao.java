/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.dao;

import com.twc.ims.database.database;
import com.twc.ims.entity.IMSAHEAD;
import com.twc.ims.entity.IMSAPDETAIL;
import com.twc.ims.entity.IMSSDETAIL;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSAPDETAILDao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public ResultSet reportIMS200(String advPaidNo) {

        String sql = "SELECT [IMSADPDNO]\n"
                + "      ,[IMSAPSHCD]\n"
                + "	  ,[IMSSCNAME]\n"
                + "      ,[IMSADLINO]\n"
                + "      ,[IMSADCQNO]\n"
                + "      ,[IMSADBCODE]\n"
                + "	  ,[IMSADCQDT]\n"
                + "      ,[IMSADADAMT]\n"
                + "      ,[IMSADESAMT]\n"
                + "      ,[IMSADDNNO]\n"
                + "      ,[IMSADDNDT]\n"
                + "      ,[IMSADINVNO]\n"
                + "      ,[IMSADBLAMT]\n"
                + "      ,[IMSADTX3AMT]\n"
                + "      ,[IMSADTX1AMT]\n"
                + "      ,[IMSADNETAMT]\n"
                + "      ,[IMSADTNTAMT]\n"
                + "      ,[IMSADBALAMT]\n"
                + "	  ,(SELECT SUM([IMSADADAMT])\n"
                + "  FROM(\n"
                + "	SELECT DISTINCT [IMSADCQNO], [IMSADADAMT]\n"
                + "    FROM [IMSAPDETAIL]\n"
                + "    WHERE [IMSADPDNO] = '" + advPaidNo + "'\n"
                + "  )TMP) AS SUM_ADV\n"
                + "  FROM [IMSAPDETAIL]\n"
                + "  LEFT JOIN [IMSAPHEAD] ON [IMSAPHEAD].[IMSAPPDNO] = [IMSAPDETAIL].[IMSADPDNO]\n"
                + "  LEFT JOIN [IMSSHC] ON [IMSSHC].[IMSSCCODE] = [IMSAPHEAD].[IMSAPSHCD]\n"
                + "  WHERE [IMSADPDNO] = '" + advPaidNo + "'\n"
                + "  ORDER BY [IMSADPDNO], [IMSADLINO], [IMSADCQNO]";
//        System.out.println(sql);
        ResultSet result = null;
        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            result = ps.executeQuery();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public List<IMSAPDETAIL> reportExcelIMS200(String advPaidNo) {

        List<IMSAPDETAIL> UAList = new ArrayList<IMSAPDETAIL>();

        String sql = "SELECT [IMSADPDNO]\n"
                + "      ,[IMSAPSHCD]\n"
                + "	  ,[IMSSCNAME]\n"
                + "      ,[IMSADLINO]\n"
                + "      ,([IMSADCQNO]\n"
                + "      +[IMSADBCODE]\n"
                + "	  +format([IMSADCQDT],'dd-MM-yy')\n"
                + "      +format([IMSADADAMT],'#,#0.00')) AS GRP\n"
                + "      ,[IMSADCQNO]\n"
                + "      ,[IMSADBCODE]\n"
                + "	  ,format([IMSADCQDT],'dd-MM-yy') as [IMSADCQDT]\n"
                + "      ,format([IMSADADAMT],'#,#0.00') as [IMSADADAMT]\n"
                + "      ,format([IMSADESAMT],'#,#0.00') as [IMSADESAMT]\n"
                + "      ,[IMSADDNNO]\n"
                + "      ,format([IMSADDNDT],'dd-MM-yy') as [IMSADDNDT]\n"
                + "      ,[IMSADINVNO]\n"
                + "      ,format([IMSADBLAMT],'#,#0.00') as [IMSADBLAMT]\n"
                + "      ,format([IMSADTX3AMT],'#,#0.00') as [IMSADTX3AMT]\n"
                + "      ,format([IMSADTX1AMT],'#,#0.00') as [IMSADTX1AMT]\n"
                + "      ,format([IMSADNETAMT],'#,#0.00') as [IMSADNETAMT]\n"
                + "      ,format([IMSADTNTAMT],'#,#0.00') as [IMSADTNTAMT]\n"
                + "      ,format([IMSADBALAMT],'#,#0.00') as [IMSADBALAMT]\n"
                + "	  ,(SELECT format(SUM([IMSADADAMT]),'#,#0.00')\n"
                + "		FROM(\n"
                + "			SELECT DISTINCT [IMSADCQNO], [IMSADADAMT]\n"
                + "			FROM [IMSAPDETAIL]\n"
                + "			WHERE [IMSADPDNO] = DET.[IMSADPDNO]\n"
                + "		)TMP) AS SUM_ADV\n"
                + "	  ,(SELECT format(SUM([IMSADESAMT]),'#,#0.00')\n"
                + "		FROM [IMSAPDETAIL]\n"
                + "		WHERE [IMSADPDNO] = DET.[IMSADPDNO]\n"
                + "	   ) AS SUM_EST_ADV\n"
                + "	  ,(SELECT format(SUM([IMSADBLAMT]),'#,#0.00')\n"
                + "		FROM [IMSAPDETAIL]\n"
                + "		WHERE [IMSADPDNO] = DET.[IMSADPDNO]\n"
                + "	   ) AS SUM_BIIL_AMT\n"
                + "	  ,(SELECT format(SUM([IMSADTX3AMT]),'#,#0.00')\n"
                + "		FROM [IMSAPDETAIL]\n"
                + "		WHERE [IMSADPDNO] = DET.[IMSADPDNO]\n"
                + "	   ) AS SUM_TAX3\n"
                + "	  ,(SELECT format(SUM([IMSADTX1AMT]),'#,#0.00')\n"
                + "		FROM [IMSAPDETAIL]\n"
                + "		WHERE [IMSADPDNO] = DET.[IMSADPDNO]\n"
                + "	   ) AS SUM_TAX1\n"
                + "	  ,(SELECT format(SUM([IMSADNETAMT]),'#,#0.00')\n"
                + "		FROM [IMSAPDETAIL]\n"
                + "		WHERE [IMSADPDNO] = DET.[IMSADPDNO]\n"
                + "	   ) AS SUM_AMT\n"
                + "	  ,(SELECT format(SUM([IMSADNETAMT]),'#,#0.00')\n"
                + "		FROM [IMSAPDETAIL]\n"
                + "		WHERE [IMSADPDNO] = DET.[IMSADPDNO]\n"
                + "	   ) AS SUM_TOT_ADV\n"
                + "	  ,(SELECT format((SELECT SUM([IMSADADAMT])\n"
                + "		FROM(\n"
                + "			SELECT DISTINCT [IMSADCQNO], [IMSADADAMT]\n"
                + "			FROM [IMSAPDETAIL]\n"
                + "			WHERE [IMSADPDNO] = DET.[IMSADPDNO]\n"
                + "		)TMP)-SUM([IMSADNETAMT]),'#,#0.00')\n"
                + "		FROM [IMSAPDETAIL]\n"
                + "		WHERE [IMSADPDNO] = DET.[IMSADPDNO]\n"
                + "	   ) AS SUM_BAL\n"
                + "       ,format(current_timestamp,'dd/MM/yy HH:mm:ss') as CUR_DATETIME\n"
                + "  FROM [IMSAPDETAIL] DET\n"
                + "  LEFT JOIN [IMSAPHEAD] ON [IMSAPHEAD].[IMSAPPDNO] = DET.[IMSADPDNO]\n"
                + "  LEFT JOIN [IMSSHC] ON [IMSSHC].[IMSSCCODE] = [IMSAPHEAD].[IMSAPSHCD]\n"
                + "  WHERE [IMSADPDNO] = '" + advPaidNo + "'\n"
                + "  ORDER BY [IMSADPDNO], [IMSADLINO], [IMSADCQNO]";
//        System.out.println(sql);
        ResultSet result = null;
        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            result = ps.executeQuery();

            String grp = "first";

            while (result.next()) {

                IMSAPDETAIL p = new IMSAPDETAIL();

                if (grp.equals("first") || !grp.equals(result.getString("GRP"))) {
                    p.setCheckNo(result.getString("IMSADCQNO"));
                    p.setBank(result.getString("IMSADBCODE"));
                    p.setCheckDate(result.getString("IMSADCQDT"));
                    p.setAdvance(result.getString("IMSADADAMT"));
                } else if (grp.equals(result.getString("GRP"))) {
                    p.setCheckNo("");
                    p.setBank("");
                    p.setCheckDate("");
                    p.setAdvance("");
                }
                grp = result.getString("GRP");

                p.setPimno(result.getString("IMSADPDNO"));
                p.setImcD(result.getString("IMSSCNAME"));
                p.setNo(result.getString("IMSADLINO"));
//                p.setCheckNo(result.getString("IMSADCQNO"));
//                p.setBank(result.getString("IMSADBCODE"));
//                p.setCheckDate(result.getString("IMSADCQDT"));
//                p.setAdvance(result.getString("IMSADADAMT"));
                p.setEstAdv(result.getString("IMSADESAMT"));
                p.setDnNo(result.getString("IMSADDNNO"));
                p.setDnDate(result.getString("IMSADDNDT"));
                p.setInvNoList(result.getString("IMSADINVNO"));
                p.setBillAmt(result.getString("IMSADBLAMT"));
                p.setTax3(result.getString("IMSADTX3AMT"));
                p.setTax1(result.getString("IMSADTX1AMT"));
                p.setAmount(result.getString("IMSADNETAMT"));
                p.setTotAdv(result.getString("IMSADTNTAMT"));
                p.setBalance(result.getString("IMSADBALAMT"));
                p.setSumAdv(result.getString("SUM_ADV"));
                p.setSumEstAdv(result.getString("SUM_EST_ADV"));
                p.setSumBillAmt(result.getString("SUM_BIIL_AMT"));
                p.setSumTax3(result.getString("SUM_TAX3"));
                p.setSumTax1(result.getString("SUM_TAX1"));
                p.setSumAmt(result.getString("SUM_AMT"));
                p.setSumTotAdv(result.getString("SUM_TOT_ADV"));
                p.setSumBal(result.getString("SUM_BAL"));
//                p.setDateTime(result.getString("CUR_DATETIME"));

                UAList.add(p);

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public int findNextLine(String code) {

        int max = 1;

        String sql = "SELECT MAX([IMSADLINO]) + 1 AS MAX\n"
                + "FROM [IMSAPDETAIL] "
                + "WHERE [IMSADPDNO] = '" + code + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                String maxSt = result.getString("MAX");
                if (maxSt != null) {
                    max = Integer.parseInt(result.getString("MAX"));
                }

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return max;

    }

    public int findCNT(String ckno, String line) {

        int CNT = 1;

        String sql = "SELECT COUNT(*) AS CNT \n"
                + "FROM IMSAPDETAIL \n"
                + "WHERE IMSADCQNO = '" + ckno + "' AND IMSADLINO = '" + line + "' ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                String maxSt = result.getString("CNT");
                if (maxSt != null) {
                    CNT = Integer.parseInt(result.getString("CNT"));
                }

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return CNT;

    }

    public String findAPN(String ckno) {

        String APN = "";

        String sql = "SELECT TOP 1 IMSADPDNO \n"
                + "FROM IMSAPDETAIL \n"
                + "WHERE IMSADCQNO = '" + ckno + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                APN = result.getString("IMSADPDNO");

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return APN;

    }

    public List<IMSAPDETAIL> findByCode(String code) {

        List<IMSAPDETAIL> UAList = new ArrayList<IMSAPDETAIL>();

        String sql = "SELECT [IMSADLINO]\n"
                + "      ,[IMSADCQNO]\n"
                + "      ,[IMSADBCODE]\n"
                + "	 ,[IMSADCQDT]\n"
                + "      ,[IMSADADAMT]\n"
                + "      ,[IMSADESAMT]\n"
                + "      ,[IMSADDNNO]\n"
                + "      ,[IMSADDNDT]\n"
                + "      ,[IMSADINVNO]\n"
                + "      ,[IMSADBLAMT]\n"
                + "      ,[IMSADTX3AMT]\n"
                + "      ,[IMSADTX1AMT]\n"
                + "      ,[IMSADNETAMT]\n"
                + "      ,[IMSADTNTAMT]\n"
                + "      ,[IMSADBALAMT]\n"
                + "      ,(select [IMSIMCODE]+' : '+[IMSIMNAME] from [IMSIMC] where [IMSIMCODE] = [IMSADIMC]) as[IMSADIMC]\n"
                + "  FROM [IMSAPDETAIL] \n"
                + "  WHERE [IMSADPDNO] = '" + code + "'\n"
                + "  ORDER BY IMSADPDNO, IMSADLINO, IMSADCQNO";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String current_checkNo = "";
            IMSAPDETAIL current_p = null;

            while (result.next()) {

                IMSAPDETAIL p = new IMSAPDETAIL();

                p.setNo(result.getString("IMSADLINO"));
                p.setImcD(result.getString("IMSADIMC"));

                double tot = Double.parseDouble(result.getString("IMSADADAMT"));
                String tqt = formatDou.format(tot);

                String checkbox = "";

                if (!current_checkNo.equals(result.getString("IMSADCQNO"))) {

                    if (current_p != null) {
                        String rem = current_p.getBalance();
                        current_p.setBalance(rem);
                        UAList.add(current_p);
                    }

                    p.setCheckNo(result.getString("IMSADCQNO"));
                    p.setBank(result.getString("IMSADBCODE"));

                    if (result.getString("IMSADCQDT") != null) {
                        p.setCheckDate(result.getString("IMSADCQDT").split(" ")[0]);
                    }

                    p.setAdvance(tqt);

                    p.setCheckNoD(result.getString("IMSADCQNO"));
                    p.setBankD(result.getString("IMSADBCODE"));

                    if (result.getString("IMSADCQDT") != null) {
                        p.setCheckDateD(result.getString("IMSADCQDT").split(" ")[0]);
                    }

                    p.setAdvanceD(tqt);

                    current_checkNo = result.getString("IMSADCQNO");
                } else {
                    p.setCheckNoD(result.getString("IMSADCQNO"));
                    p.setBankD(result.getString("IMSADBCODE"));

                    if (result.getString("IMSADCQDT") != null) {
                        p.setCheckDateD(result.getString("IMSADCQDT").split(" ")[0]);
                    }

                    p.setAdvanceD(tqt);

                    UAList.add(current_p);
                }

                checkbox = "<input style=\"width: 20px; height: 20px;\" type=\"checkbox\" name=\"selectCk\" value=\"" + result.getString("IMSADLINO") + "+INDB-" + result.getString("IMSADCQNO") + "\">";
                p.setCheckBox(checkbox);

                double tot1 = Double.parseDouble(result.getString("IMSADESAMT"));
                String tqt1 = formatDou.format(tot1);
                p.setEstAdv(tqt1);

                p.setDnNo(result.getString("IMSADDNNO"));

                if (result.getString("IMSADDNDT") != null) {
                    p.setDnDate(result.getString("IMSADDNDT").split(" ")[0]);
                }

                p.setInvNoList(result.getString("IMSADINVNO"));

                double tot2 = Double.parseDouble(result.getString("IMSADBLAMT"));
                String tqt2 = formatDou.format(tot2);
                p.setBillAmt(tqt2);

                double tot3 = Double.parseDouble(result.getString("IMSADTX3AMT"));
                String tqt3 = formatDou.format(tot3);
                p.setTax3(tqt3);

                double tot4 = Double.parseDouble(result.getString("IMSADTX1AMT"));
                String tqt4 = formatDou.format(tot4);
                p.setTax1(tqt4);

                double tot5 = Double.parseDouble(result.getString("IMSADNETAMT"));
                String tqt5 = formatDou.format(tot5);
                p.setAmount(tqt5);

                double tot6 = Double.parseDouble(result.getString("IMSADTNTAMT"));
                String tqt6 = formatDou.format(tot6);
                p.setTotAdv(tqt6);

                double tot7 = Double.parseDouble(result.getString("IMSADBALAMT"));
                String tqt7 = formatDou.format(tot7);
                p.setBalance(tqt7);

                current_p = p;

            }

            if (current_p != null) {
                String rem = current_p.getBalance();
                current_p.setBalance(rem);
                UAList.add(current_p);
            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<IMSAPDETAIL> findByCodeReCal(String code) {

        List<IMSAPDETAIL> UAList = new ArrayList<IMSAPDETAIL>();

        String sql = "SELECT [IMSADCOM]\n"
                + "      ,[IMSADPDNO]\n"
                + "      ,[IMSADLINO]\n"
                + "      ,[IMSADCQNO]\n"
                + "      ,[IMSADCQDT]\n"
                + "      ,[IMSADBCODE]\n"
                + "      ,[IMSADADAMT]\n"
                + "      ,[IMSADESAMT]\n"
                + "      ,[IMSADDNNO]\n"
                + "      ,[IMSADDNDT]\n"
                + "      ,[IMSADINVNO]\n"
                + "      ,[IMSADBLAMT]\n"
                + "      ,[IMSADTX3AMT]\n"
                + "      ,[IMSADTX1AMT]\n"
                + "      ,[IMSADNETAMT]\n"
                + "      ,[IMSADTNTAMT]\n"
                + "      ,[IMSADBALAMT]\n"
                + "      ,[IMSADEDT]\n"
                + "      ,[IMSADCDT]\n"
                + "      ,[IMSADUSER]\n"
                + "      ,[IMSADIMC]\n"
                + "  FROM [RMShipment].[dbo].[IMSAPDETAIL]\n"
                + "  WHERE [IMSADPDNO] = '" + code + "'\n"
                + "  ORDER BY [IMSADCQNO],[IMSADBCODE],[IMSADCQDT],[IMSADADAMT],[IMSADLINO]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSAPDETAIL p = new IMSAPDETAIL();

                p.setIMSADCOM(result.getString("IMSADCOM"));
                p.setIMSADPDNO(result.getString("IMSADPDNO"));
                p.setIMSADLINO(result.getString("IMSADLINO"));
                p.setIMSADCQNO(result.getString("IMSADCQNO"));
                p.setIMSADCQDT(result.getString("IMSADCQDT"));
                p.setIMSADBCODE(result.getString("IMSADBCODE"));
                p.setIMSADADAMT(result.getString("IMSADADAMT"));
                p.setIMSADESAMT(result.getString("IMSADESAMT"));
                p.setIMSADDNNO(result.getString("IMSADDNNO"));
                p.setIMSADDNDT(result.getString("IMSADDNDT"));
                p.setIMSADINVNO(result.getString("IMSADINVNO"));
                p.setIMSADBLAMT(result.getString("IMSADBLAMT"));
                p.setIMSADTX3AMT(result.getString("IMSADTX3AMT"));
                p.setIMSADTX1AMT(result.getString("IMSADTX1AMT"));
                p.setIMSADNETAMT(result.getString("IMSADNETAMT"));
                p.setIMSADTNTAMT(result.getString("IMSADTNTAMT"));
                p.setIMSADBALAMT(result.getString("IMSADBALAMT"));
                p.setIMSADEDT(result.getString("IMSADEDT"));
                p.setIMSADCDT(result.getString("IMSADCDT"));
                p.setIMSADUSER(result.getString("IMSADUSER"));
                p.setIMSADIMC(result.getString("IMSADIMC"));

                UAList.add(p);
            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public String check(String code, String line) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "Select * FROM [RMShipment].[dbo].[IMSAPDETAIL] where [IMSADPDNO] = '" + code + "' "
                    + "AND [IMSADLINO] = '" + line + "' ";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(String advPaidNo, String no, String checkNo, String checkDate,
            String bank, String advance, String estAdv, String dnNo, String dnDate,
            String invNoList, String billAmt, String tax3, String tax1, String amount, String totAdv,
            String balance, String userid, String imcD) {

        boolean result = false;

        String sql = "INSERT INTO IMSAPDETAIL ([IMSADCOM]\n"
                + "      ,[IMSADPDNO]\n"
                + "      ,[IMSADLINO]\n"
                + "      ,[IMSADCQNO]\n"
                + "      ,[IMSADCQDT]\n"
                + "      ,[IMSADBCODE]\n"
                + "      ,[IMSADADAMT]\n"
                + "      ,[IMSADESAMT]\n"
                + "      ,[IMSADDNNO]\n"
                + "      ,[IMSADDNDT]\n"
                + "      ,[IMSADINVNO]\n"
                + "      ,[IMSADBLAMT]\n"
                + "      ,[IMSADTX3AMT]\n"
                + "      ,[IMSADTX1AMT]\n"
                + "      ,[IMSADNETAMT]\n"
                + "      ,[IMSADTNTAMT]\n"
                + "      ,[IMSADBALAMT]\n"
                + "      ,[IMSADEDT]\n"
                + "      ,[IMSADCDT]\n"
                + "      ,[IMSADUSER]\n"
                + "      ,[IMSADIMC]) "
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, ?, ?)";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, "TWC");

            ps.setString(2, advPaidNo);

            ps.setString(3, no);

            ps.setString(4, checkNo);

            ps.setString(5, checkDate);

            ps.setString(6, bank);

            ps.setString(7, advance);

            ps.setString(8, estAdv);

            ps.setString(9, dnNo);

            ps.setString(10, dnDate);

            ps.setString(11, invNoList);

            ps.setString(12, billAmt);

            ps.setString(13, tax3);

            ps.setString(14, tax1);

            ps.setString(15, amount);

            ps.setString(16, totAdv);

            ps.setString(17, balance);

            ps.setString(18, userid);

            ps.setString(19, imcD);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean addReCal(IMSAPDETAIL det) {

        boolean result = false;

        String sql = "INSERT INTO [IMSAPDETAIL] ([IMSADCOM]\n"
                + "      ,[IMSADPDNO]\n"
                + "      ,[IMSADLINO]\n"
                + "      ,[IMSADCQNO]\n"
                + "      ,[IMSADCQDT]\n"
                + "      ,[IMSADBCODE]\n"
                + "      ,[IMSADADAMT]\n"
                + "      ,[IMSADESAMT]\n"
                + "      ,[IMSADDNNO]\n"
                + "      ,[IMSADDNDT]\n"
                + "      ,[IMSADINVNO]\n"
                + "      ,[IMSADBLAMT]\n"
                + "      ,[IMSADTX3AMT]\n"
                + "      ,[IMSADTX1AMT]\n"
                + "      ,[IMSADNETAMT]\n"
                + "      ,[IMSADTNTAMT]\n"
                + "      ,[IMSADBALAMT]\n"
                + "      ,[IMSADEDT]\n"
                + "      ,[IMSADCDT]\n"
                + "      ,[IMSADUSER]\n"
                + "      ,[IMSADIMC]) "
                + "VALUES("
                + "'" + det.getIMSADCOM() + "', "
                + "'" + det.getIMSADPDNO() + "', "
                + "'" + det.getIMSADLINO() + "', "
                + "'" + det.getIMSADCQNO() + "', "
                + "'" + det.getIMSADCQDT() + "', "
                + "'" + det.getIMSADBCODE() + "', "
                + "'" + det.getIMSADADAMT() + "', "
                + "'" + det.getIMSADESAMT() + "', "
                + "'" + det.getIMSADDNNO() + "', "
                + "'" + det.getIMSADDNDT() + "', "
                + "'" + det.getIMSADINVNO() + "', "
                + "'" + det.getIMSADBLAMT() + "', "
                + "'" + det.getIMSADTX3AMT() + "', "
                + "'" + det.getIMSADTX1AMT() + "', "
                + "'" + det.getIMSADNETAMT() + "', "
                + "'" + det.getIMSADTNTAMT() + "', "
                + "'" + det.getIMSADBALAMT() + "', "
                + "'" + det.getIMSADEDT() + "', "
                + "'" + det.getIMSADCDT() + "', "
                + "'" + det.getIMSADUSER() + "', "
                + "'" + det.getIMSADIMC() + "' "
                + ")";

//        System.out.println(sql);
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String advPaidNo, String no, String checkNo, String checkDate,
            String bank, String advance, String estAdv, String dnNo, String dnDate,
            String invNoList, String billAmt, String tax3, String tax1, String amount, String totAdv,
            String balance, String userid, String imcD) {

        boolean result = false;

        String sql = "UPDATE IMSAPDETAIL "
                + "SET [IMSADCQNO] = ?\n"
                + "      ,[IMSADCQDT] = ?\n"
                + "      ,[IMSADBCODE] = ?\n"
                + "      ,[IMSADADAMT] = ?\n"
                + "      ,[IMSADESAMT] = ?\n"
                + "      ,[IMSADDNNO] = ?\n"
                + "      ,[IMSADDNDT] = ?\n"
                + "      ,[IMSADINVNO] = ?\n"
                + "      ,[IMSADBLAMT] = ?\n"
                + "      ,[IMSADTX3AMT] = ?\n"
                + "      ,[IMSADTX1AMT] = ?\n"
                + "      ,[IMSADNETAMT] = ?\n"
                + "      ,[IMSADTNTAMT] = ?\n"
                + "      ,[IMSADBALAMT] = ?\n"
                + "      ,[IMSADEDT] = CURRENT_TIMESTAMP\n"
                + "      ,[IMSADUSER] = ?\n"
                + "      ,[IMSADIMC] = '" + imcD + "'\n"
                + "WHERE IMSADPDNO = ? "
                + "AND IMSADLINO = ? ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(16, advPaidNo);

            ps.setString(17, no);

            ps.setString(1, checkNo);

            ps.setString(2, checkDate);

            ps.setString(3, bank);

            ps.setString(4, advance);

            ps.setString(5, estAdv);

            ps.setString(6, dnNo);

            ps.setString(7, dnDate);

            ps.setString(8, invNoList);

            ps.setString(9, billAmt);

            ps.setString(10, tax3);

            ps.setString(11, tax1);

            ps.setString(12, amount);

            ps.setString(13, totAdv);

            ps.setString(14, balance);

            ps.setString(15, userid);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean delete(String advPaidNo, String checkNo, String line) {

        boolean result = false;

        String sql = "DELETE FROM IMSAPDETAIL WHERE IMSADPDNO = '" + advPaidNo + "' AND IMSADCQNO = '" + checkNo + "' AND IMSADLINO = '" + line + "' ";
//        System.out.println(sql);
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean deleteReCal(String advPaidNo) {

        boolean result = false;

        String sql = "DELETE FROM [IMSAPDETAIL] WHERE [IMSADPDNO] = '" + advPaidNo + "' ";
//        System.out.println(sql);
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean updateEstReCal(String advPaidNo) {

        boolean result = false;

        String sql = "UPDATE DDT\n"
                + "SET [IMSADESAMT]=isnull((SELECT top 1 DT.[IMSADADVAMT]\n"
                + "FROM [IMSADETAIL] DT\n"
                + "LEFT JOIN [IMSAHEAD] HD on HD.[IMSAHAVNO] = DT.[IMSADAVNO]\n"
                + "where HD.[IMSAHCQNO]=DDT.[IMSADCQNO]\n"
                + "and DT.[IMSADINVNO]=DDT.[IMSADINVNO]),DDT.[IMSADINVNO])\n"
                + "FROM [IMSAPDETAIL] DDT\n"
                + "where [IMSADPDNO]='" + advPaidNo + "' ";
//        System.out.println(sql);
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }
}
