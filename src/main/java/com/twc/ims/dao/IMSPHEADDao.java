/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.dao;

import com.twc.ims.database.database;
import com.twc.ims.entity.IMSPHEAD;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSPHEADDao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public boolean updateAmt(String importPaidNo, String sumFIA, String remainingAmount, String bank, String paidDate, String fee) {

        boolean result = false;

        String sql = "UPDATE IMSPHEAD "
                + "SET IMSPHPDAMT = '" + sumFIA + "'\n"
                + "   ,IMSPHRMAMT = '" + remainingAmount + "'\n"
                + "   ,IMSPHEDT = CURRENT_TIMESTAMP "
                + "   ,IMSPHSTS = 'P' "
                + "   ,IMSPHPDDT = '" + paidDate + "' "
                + "   ,IMSPHBCODE = '" + bank + "' "
                + "   ,IMSPHFEE = '" + fee + "' "
                + "WHERE IMSPHPDNO = '" + importPaidNo + "' ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public IMSPHEAD findByCode(String code) {

        IMSPHEAD p = new IMSPHEAD();

        String sql = "SELECT IMSPHESTAMT, (SELECT TOP 1 IMSPDCUR FROM IMSPDETAIL WHERE IMSPDPDNO = IMSPHPDNO) AS CCY, IMSPHVCODE, "
                + "IMSPHBCODE, IMSPHPDDT, IMSPHIMC, IMSPHFEE, IMSPHRMAMT "
                + "FROM IMSPHEAD "
                + "WHERE IMSPHPDNO = '" + code + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {
//
                p.setTotalEstPaidAmount(formatDou.format(Double.parseDouble(result.getString("IMSPHESTAMT"))));
                p.setRemainingAmount(formatDou.format(Double.parseDouble(result.getString("IMSPHRMAMT"))));
                p.setCcy(result.getString("CCY"));
                p.setPaidBy(result.getString("IMSPHIMC"));
                p.setFee(formatDou.format(Double.parseDouble(result.getString("IMSPHFEE"))));
                p.setVendor(result.getString("IMSPHVCODE"));
                p.setBank(result.getString("IMSPHBCODE"));

                if (result.getString("IMSPHPDDT") != null) {
                    p.setPaidDate(result.getString("IMSPHPDDT").split(" ")[0]);
                }
//

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public boolean updateSTS(String importPaidNo, String sts) {

        boolean result = false;

        String sql = "UPDATE IMSPHEAD "
                + "SET IMSPHSTS = '" + sts + "'\n"
                + "   ,IMSPHEDT = CURRENT_TIMESTAMP "
                + "WHERE IMSPHPDNO = '" + importPaidNo + "' ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean update(String paidDate, String bank, String vendor, String totalEstPaidAmount, String sumFIA, String remainingAmount, String importPaidNo, String fee) {

        boolean result = false;

        String sql = "UPDATE IMSPHEAD "
                + "SET [IMSPHPDDT] = '" + paidDate + "'\n"
                + "      ,[IMSPHBCODE] = '" + bank + "'\n"
                + "      ,[IMSPHFEE] = '" + fee + "'\n"
                + "      ,[IMSPHVCODE] = '" + vendor + "'\n"
                + "      ,[IMSPHESTAMT] = '" + totalEstPaidAmount + "'\n"
                + "      ,[IMSPHPDAMT] = " + totalEstPaidAmount + " - " + remainingAmount + "\n"
                + "      ,[IMSPHRMAMT] = '" + remainingAmount + "', IMSPHEDT = CURRENT_TIMESTAMP "
                + "WHERE IMSPHPDNO = '" + importPaidNo + "' ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public String check(String code) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "Select * FROM [RMShipment].[dbo].[IMSPHEAD] where [IMSPHPDNO] = '" + code + "' ";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(String head) {

        boolean result = false;

        String sql = "INSERT INTO IMSPHEAD "
                + "([IMSPHCOM]\n"
                + "      ,[IMSPHIMNO]\n"
                + "      ,[IMSPHPDNO]\n"
                + "      ,[IMSPHPDDT]\n"
                + "      ,[IMSPHEXRATE]\n"
                + "      ,[IMSPHBCODE]\n"
                + "      ,[IMSPHVCODE]\n"
                + "      ,[IMSPHESTAMT]\n"
                + "      ,[IMSPHPDAMT]\n"
                + "      ,[IMSPHRMAMT]\n"
                + "      ,[IMSPHEDT]\n"
                + "      ,[IMSPHCDT]\n"
                + "      ,[IMSPHUSER]\n"
                + "      ,[IMSPHIMC]\n"
                + "      ,[IMSPHFEE]\n"
                + "      ,[IMSPHSTS]) "
                + " VALUES(" + head + ")";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public List<IMSPHEAD> findAll() {

        List<IMSPHEAD> UAList = new ArrayList<IMSPHEAD>();

        String sql = "SELECT IMSPHPDNO, IMSPHVCODE + ' : ' + IMSVDNAME AS IMSPHVCODE, IMSPHPDDT, IMSPHESTAMT, IMSPHPDAMT, IMSPHRMAMT, [IMSPHBCODE]\n"
                + "FROM IMSPHEAD\n"
                + "LEFT JOIN IMSVEN ON IMSVEN.IMSVDCODE = IMSPHEAD.IMSPHVCODE\n"
                //                + "WHERE IMSPHSTS != 'D'\n"
                + "ORDER BY IMSPHPDNO DESC";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSPHEAD p = new IMSPHEAD();

                p.setImpPaidNo(result.getString("IMSPHPDNO"));
                p.setVendor(result.getString("IMSPHVCODE"));
                p.setBank(result.getString("IMSPHBCODE"));

                if (result.getString("IMSPHPDDT") != null) {
                    p.setPaidDate(result.getString("IMSPHPDDT").split(" ")[0]);
                }

                double tot1 = Double.parseDouble(result.getString("IMSPHESTAMT"));
                String tqt1 = formatDou.format(tot1);
                p.setTotalEstPaidAmount(tqt1);

                double tot2 = Double.parseDouble(result.getString("IMSPHPDAMT"));
                String tqt2 = formatDou.format(tot2);
                p.setTotalPaidAmount(tqt2);

                double tot3 = Double.parseDouble(result.getString("IMSPHRMAMT"));
                String tqt3 = formatDou.format(tot3);
                p.setRemainingAmount(tqt3);

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }
}
