/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.dao;

import com.twc.ims.database.database;
import com.twc.ims.entity.IMSAPHEAD;
import com.twc.ims.entity.IMSRP700;
import com.twc.ims.entity.IMSRP710;
import com.twc.ims.entity.IMSRP720;
import com.twc.ims.entity.IMSRP730;
import com.twc.ims.entity.IMSRP740;
import com.twc.ims.entity.IMSSHC;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSRP720Dao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public int countByPeriod(String ym, String imc) {

        int cnt = 0;

        String sql = "SELECT COUNT(*) AS CNT\n"
                + "  FROM IMSSHEAD\n"
                + "  WHERE FORMAT(IMSHTRDT,'yyyy') = '" + ym + "' AND IMSHIMC = '" + imc + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                cnt = result.getInt("CNT");

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return cnt;

    }

    public String findTotImpAmt(String ym) {

        String TotImpAmt = "";

        String sql = "SELECT ISNULL(SUM([IMSRPBAHT]),0) as SUMTIAB\n"
                + "  FROM [RMShipment].[dbo].[IMSRP700]\n"
                + "  WHERE [IMSRPPERIOD] = '" + ym + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                TotImpAmt = result.getString("SUMTIAB");

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return TotImpAmt;

    }

    public List<IMSRP720> findByCode(String ym, String imc) {

        List<IMSRP720> ul = new ArrayList<IMSRP720>();

        String sql = "SELECT [IMSRPPERIOD]\n"
                + "      ,[IMSRPPROD]\n"
                + "      ,[IMSRPCUR]\n"
                + "      ,[IMSRPYAM]\n"
                + "      ,FORMAT(ISNULL([IMSRPFORAMT],0),'#,#0.00') AS [IMSRPFORAMT]\n"
                + "      ,FORMAT(ISNULL([IMSRPINVAMT],0),'#,#0.00') AS [IMSRPINVAMT]\n"
                + "      ,FORMAT(ISNULL([IMSRPFREIGHT],0),'#,#0.00') AS [IMSRPFREIGHT]\n"
                + "      ,FORMAT(ISNULL([IMSRPIMPDUTY],0),'#,#0.00') AS [IMSRPIMPDUTY]\n"
                + "      ,FORMAT(ISNULL([IMSRPVATAMT],0),'#,#0.00') AS [IMSRPVATAMT]\n"
                + "      ,FORMAT(ISNULL([IMSRPTOTAMTEV],0),'#,#0.00') AS [IMSRPTOTAMTEV]\n"
                + "      ,FORMAT(ISNULL([IMSRPTOTAMTEVF],0),'#,#0.00') AS [IMSRPTOTAMTEVF]\n"
                + "      ,FORMAT(ISNULL([IMSRPEXPENSE],0),'#,#0.00') AS [IMSRPEXPENSE]\n"
                + "  FROM [RMShipment].[dbo].[IMSRP720]\n"
                + "  WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + "  AND [IMSRPIMC] = '" + imc + "'\n"
                + "  ORDER BY [IMSRPPROD],[IMSRPCUR],[IMSRPYAM]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String prod = "ST";
            String ccy = "ST";
            String yam = "ST";

            Double FA = 0.00;
            Double IA = 0.00;
            Double FR = 0.00;
            Double ID = 0.00;
            Double VA = 0.00;
            Double TA = 0.00;
            Double TA2 = 0.00;

            Double cFA = 0.00;
            Double cIA = 0.00;
            Double cFR = 0.00;
            Double cID = 0.00;
            Double cVA = 0.00;
            Double cTA = 0.00;
            Double cTA2 = 0.00;

            Double aFA = 0.00;
            Double aIA = 0.00;
            Double aFR = 0.00;
            Double aID = 0.00;
            Double aVA = 0.00;
            Double aTA = 0.00;
            Double aTA2 = 0.00;

            while (result.next()) {

                IMSRP720 p = new IMSRP720();

                p.setPeriod(result.getString("IMSRPPERIOD"));
                ym = result.getString("IMSRPPERIOD");

                if (prod.equals(result.getString("IMSRPPROD"))) {
                    prod = result.getString("IMSRPPROD");
                    p.setProd("<b style=\"opacity: 0; font-size: 1px;\">" + result.getString("IMSRPPROD") + "</b>");

                    if (ccy.equals(result.getString("IMSRPCUR"))) {
                        ccy = result.getString("IMSRPCUR");
                        p.setCcy("<b style=\"opacity: 0; font-size: 1px;\">" + result.getString("IMSRPCUR") + "</b>");

                        if (yam.equals(result.getString("IMSRPYAM"))) {
                            yam = result.getString("IMSRPYAM");
                            p.setYam("<b style=\"opacity: 0; font-size: 1px;\">" + result.getString("IMSRPYAM") + "</b>");

                        } else {
                            yam = result.getString("IMSRPYAM");
                            p.setYam(result.getString("IMSRPYAM"));
                        }
                    } else {
                        if (!ccy.equals("ST")) {
                            IMSRP720 sp = new IMSRP720();
                            sp.setCcy("<b>TOTAL BY CCY : " + ccy + "</b>");
                            sp.setForAmt("<b>" + formatDou.format(cFA) + "</b>");
                            sp.setInvAmt("<b>" + formatDou.format(cIA) + "</b>");
                            sp.setFreight("<b>" + formatDou.format(cFR) + "</b>");
                            sp.setImpDuty("<b>" + formatDou.format(cID) + "</b>");
                            sp.setVatAmt("<b>" + formatDou.format(cVA) + "</b>");
                            sp.setTotAmtEV("<b>" + formatDou.format(cTA) + "</b>");
                            sp.setTotAmtEVF("<b>" + formatDou.format(cTA2) + "</b>");
                            sp.setExp("<b>" + formatDou.format((cIA == 0 ? 100 : (cTA / cIA) * 100)) + "%</b>");
                            ul.add(sp);

                            cFA = 0.00;
                            cIA = 0.00;
                            cFR = 0.00;
                            cID = 0.00;
                            cVA = 0.00;
                            cTA = 0.00;
                            cTA2 = 0.00;
                        }

                        ccy = result.getString("IMSRPCUR");
                        p.setCcy(result.getString("IMSRPCUR"));
                        yam = result.getString("IMSRPYAM");
                        p.setYam(result.getString("IMSRPYAM"));
                    }

                } else {
                    if (!prod.equals("ST")) {
                        IMSRP720 sp = new IMSRP720();
                        sp.setCcy("<b>TOTAL BY CCY : " + ccy + "</b>");
                        sp.setForAmt("<b>" + formatDou.format(cFA) + "</b>");
                        sp.setInvAmt("<b>" + formatDou.format(cIA) + "</b>");
                        sp.setFreight("<b>" + formatDou.format(cFR) + "</b>");
                        sp.setImpDuty("<b>" + formatDou.format(cID) + "</b>");
                        sp.setVatAmt("<b>" + formatDou.format(cVA) + "</b>");
                        sp.setTotAmtEV("<b>" + formatDou.format(cTA) + "</b>");
                        sp.setTotAmtEVF("<b>" + formatDou.format(cTA2) + "</b>");
                        sp.setExp("<b>" + formatDou.format((cIA == 0 ? 100 : (cTA / cIA) * 100)) + "%</b>");
                        ul.add(sp);

                        cFA = 0.00;
                        cIA = 0.00;
                        cFR = 0.00;
                        cID = 0.00;
                        cVA = 0.00;
                        cTA = 0.00;
                        cTA2 = 0.00;

                        IMSRP720 sp2 = new IMSRP720();
                        sp2.setProd("<b>TOTAL BY PRODUCT : " + prod + "</b>");
                        sp2.setForAmt("<b>" + formatDou.format(FA) + "</b>");
                        sp2.setInvAmt("<b>" + formatDou.format(IA) + "</b>");
                        sp2.setFreight("<b>" + formatDou.format(FR) + "</b>");
                        sp2.setImpDuty("<b>" + formatDou.format(ID) + "</b>");
                        sp2.setVatAmt("<b>" + formatDou.format(VA) + "</b>");
                        sp2.setTotAmtEV("<b>" + formatDou.format(TA) + "</b>");
                        sp2.setTotAmtEVF("<b>" + formatDou.format(TA2) + "</b>");
                        sp2.setExp("<b>" + formatDou.format((IA == 0 ? 100 : (TA / IA) * 100)) + "%</b>");
                        ul.add(sp2);

                        FA = 0.00;
                        IA = 0.00;
                        FR = 0.00;
                        ID = 0.00;
                        VA = 0.00;
                        TA = 0.00;
                        TA2 = 0.00;
                    }

                    prod = result.getString("IMSRPPROD");
                    p.setProd(result.getString("IMSRPPROD"));
                    ccy = result.getString("IMSRPCUR");
                    p.setCcy(result.getString("IMSRPCUR"));
                    yam = result.getString("IMSRPYAM");
                    p.setYam(result.getString("IMSRPYAM"));

                }

                p.setForAmt(result.getString("IMSRPFORAMT"));
                p.setInvAmt(result.getString("IMSRPINVAMT"));
                p.setFreight(result.getString("IMSRPFREIGHT"));
                p.setImpDuty(result.getString("IMSRPIMPDUTY"));
                p.setVatAmt(result.getString("IMSRPVATAMT"));
                p.setTotAmtEV(result.getString("IMSRPTOTAMTEV"));
                p.setTotAmtEVF(result.getString("IMSRPTOTAMTEVF"));
                p.setExp(result.getString("IMSRPEXPENSE") + "%");

                FA += Double.parseDouble(result.getString("IMSRPFORAMT").replace(",", ""));
                IA += Double.parseDouble(result.getString("IMSRPINVAMT").replace(",", ""));
                FR += Double.parseDouble(result.getString("IMSRPFREIGHT").replace(",", ""));
                ID += Double.parseDouble(result.getString("IMSRPIMPDUTY").replace(",", ""));
                VA += Double.parseDouble(result.getString("IMSRPVATAMT").replace(",", ""));
                TA += Double.parseDouble(result.getString("IMSRPTOTAMTEV").replace(",", ""));
                TA2 += Double.parseDouble(result.getString("IMSRPTOTAMTEVF").replace(",", ""));

                cFA += Double.parseDouble(result.getString("IMSRPFORAMT").replace(",", ""));
                cIA += Double.parseDouble(result.getString("IMSRPINVAMT").replace(",", ""));
                cFR += Double.parseDouble(result.getString("IMSRPFREIGHT").replace(",", ""));
                cID += Double.parseDouble(result.getString("IMSRPIMPDUTY").replace(",", ""));
                cVA += Double.parseDouble(result.getString("IMSRPVATAMT").replace(",", ""));
                cTA += Double.parseDouble(result.getString("IMSRPTOTAMTEV").replace(",", ""));
                cTA2 += Double.parseDouble(result.getString("IMSRPTOTAMTEVF").replace(",", ""));

                aFA += Double.parseDouble(result.getString("IMSRPFORAMT").replace(",", ""));
                aIA += Double.parseDouble(result.getString("IMSRPINVAMT").replace(",", ""));
                aFR += Double.parseDouble(result.getString("IMSRPFREIGHT").replace(",", ""));
                aID += Double.parseDouble(result.getString("IMSRPIMPDUTY").replace(",", ""));
                aVA += Double.parseDouble(result.getString("IMSRPVATAMT").replace(",", ""));
                aTA += Double.parseDouble(result.getString("IMSRPTOTAMTEV").replace(",", ""));
                aTA2 += Double.parseDouble(result.getString("IMSRPTOTAMTEVF").replace(",", ""));

                ul.add(p);

            }

            IMSRP720 sp = new IMSRP720();
            sp.setCcy("<b>TOTAL BY CCY : " + ccy + "</b>");
            sp.setForAmt("<b>" + formatDou.format(cFA) + "</b>");
            sp.setInvAmt("<b>" + formatDou.format(cIA) + "</b>");
            sp.setFreight("<b>" + formatDou.format(cFR) + "</b>");
            sp.setImpDuty("<b>" + formatDou.format(cID) + "</b>");
            sp.setVatAmt("<b>" + formatDou.format(cVA) + "</b>");
            sp.setTotAmtEV("<b>" + formatDou.format(cTA) + "</b>");
            sp.setTotAmtEVF("<b>" + formatDou.format(cTA2) + "</b>");
            sp.setExp("<b>" + formatDou.format((cIA == 0 ? 100 : (cTA / cIA) * 100)) + "%</b>");
            ul.add(sp);

            IMSRP720 sp2 = new IMSRP720();
            sp2.setProd("<b>TOTAL BY PRODUCT : " + prod + "</b>");
            sp2.setForAmt("<b>" + formatDou.format(FA) + "</b>");
            sp2.setInvAmt("<b>" + formatDou.format(IA) + "</b>");
            sp2.setFreight("<b>" + formatDou.format(FR) + "</b>");
            sp2.setImpDuty("<b>" + formatDou.format(ID) + "</b>");
            sp2.setVatAmt("<b>" + formatDou.format(VA) + "</b>");
            sp2.setTotAmtEV("<b>" + formatDou.format(TA) + "</b>");
            sp2.setTotAmtEVF("<b>" + formatDou.format(TA2) + "</b>");
            sp2.setExp("<b>" + formatDou.format((IA == 0 ? 100 : (TA / IA) * 100)) + "%</b>");
            ul.add(sp2);

            IMSRP720 sp3 = new IMSRP720();
            sp3.setProd("<b>GRAND TOTAL BY YEAR : " + ym + "</b>");
            sp3.setForAmt("<b>" + formatDou.format(aFA) + "</b>");
            sp3.setInvAmt("<b>" + formatDou.format(aIA) + "</b>");
            sp3.setFreight("<b>" + formatDou.format(aFR) + "</b>");
            sp3.setImpDuty("<b>" + formatDou.format(aID) + "</b>");
            sp3.setVatAmt("<b>" + formatDou.format(aVA) + "</b>");
            sp3.setTotAmtEV("<b>" + formatDou.format(aTA) + "</b>");
            sp3.setTotAmtEVF("<b>" + formatDou.format(aTA2) + "</b>");
            sp3.setExp("<b>" + formatDou.format((aIA == 0 ? 100 : (aTA / aIA) * 100)) + "%</b>");
            ul.add(sp3);

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP720> findByCodePT(String ym, String imc) {

        List<IMSRP720> ul = new ArrayList<IMSRP720>();

        String sql = "SELECT TMP.[IMSRPPERIOD]\n"
                + "      ,TMP.[IMSRPPROD]\n"
                + "      ,TMP.[IMSRPCUR]\n"
                + "      ,TMP.[IMSRPYAM]\n"
                + "      ,FORMAT(TMP.[IMSRPFORAMT],'#,#0.00') AS [IMSRPFORAMT]\n"
                + "      ,FORMAT(TMP.[IMSRPINVAMT],'#,#0.00') AS [IMSRPINVAMT]\n"
                + "      ,FORMAT(TMP.[IMSRPFREIGHT],'#,#0.00') AS [IMSRPFREIGHT]\n"
                + "      ,FORMAT(TMP.FREIGHTM,'#,#0.00') AS FREIGHTM\n"
                + "	  ,FORMAT(TMP.INSUR,'#,#0.00') AS INSUR\n"
                + "	  ,FORMAT(TMP.RENT,'#,#0.00') AS RENT\n"
                + "	  ,FORMAT(TMP.CLEARING,'#,#0.00') AS CLEARING\n"
                + "	  ,FORMAT(TMP.CUSTFEE,'#,#0.00') AS CUSTFEE\n"
                + "	  ,FORMAT(TMP.CUSTCASH,'#,#0.00') AS CUSTCASH\n"
                + "	  ,FORMAT(TMP.CUSTVAT,'#,#0.00') AS CUSTVAT\n"
                + "      ,FORMAT(TMP.[IMSRPIMPDUTY],'#,#0.00') AS [IMSRPIMPDUTY]\n"
                + "      ,FORMAT(TMP.[IMSRPVATAMT],'#,#0.00') AS [IMSRPVATAMT]\n"
                + "      ,FORMAT(TMP.[IMSRPTOTAMTEV],'#,#0.00') AS [IMSRPTOTAMTEV]\n"
                + "      ,FORMAT(TMP.[IMSRPTOTAMTEVF],'#,#0.00') AS [IMSRPTOTAMTEVF]\n"
                + "      ,FORMAT(TMP.[IMSRPEXPENSE],'#,#0.00') AS [IMSRPEXPENSE]\n"
                + "	  ,FORMAT(TMP.SUMEXP,'#,#0.00') AS SUMEXP\n"
                + "	  ,FORMAT(TMP.SUMEXPCASH,'#,#0.00') AS SUMEXPCASH\n"
                + "	  ,FORMAT((CASE WHEN ISNULL(TMP.[IMSRPINVAMT],0) = 0 THEN 100 ELSE (ISNULL(TMP.SUMEXP,0)/ISNULL(TMP.[IMSRPINVAMT],0)*100) END),'#,#0.00') AS EXPN\n"
                + "	  ,FORMAT((CASE WHEN ISNULL(TMP.[IMSRPINVAMT],0) = 0 THEN 100 ELSE (ISNULL(TMP.SUMEXPCASH,0)/ISNULL(TMP.[IMSRPINVAMT],0)*100) END),'#,#0.00') AS EXP0\n"
                + "FROM (\n"
                + "SELECT [IMSRPPERIOD]\n"
                + "      ,[IMSRPPROD]\n"
                + "      ,[IMSRPCUR]\n"
                + "      ,[IMSRPYAM]\n"
                + "      ,ISNULL([IMSRPFORAMT],0) AS [IMSRPFORAMT]\n"
                + "      ,ISNULL([IMSRPINVAMT],0) AS [IMSRPINVAMT]\n"
                + "      ,ISNULL([IMSRPFREIGHT],0) AS [IMSRPFREIGHT]\n"
                + "      ,ISNULL([IMSRPFREIGHTM],0) AS FREIGHTM\n"
                + "	  ,ISNULL([IMSRPINSUR],0) AS INSUR\n"
                + "	  ,ISNULL([IMSRPRENT],0) AS RENT\n"
                + "	  ,ISNULL([IMSRPCLEAR],0) AS CLEARING\n"
                + "	  ,ISNULL([IMSRPCUSTFEE],0) AS CUSTFEE\n"
                + "	  ,ISNULL([IMSRPCUSTCASH],0) AS CUSTCASH\n"
                + "	  ,ISNULL([IMSRPCUSTVAT],0) AS CUSTVAT\n"
                + "      ,ISNULL([IMSRPIMPDUTY],0) AS [IMSRPIMPDUTY]\n"
                + "      ,ISNULL([IMSRPVATAMT],0) AS [IMSRPVATAMT]\n"
                + "      ,ISNULL([IMSRPTOTAMTEV],0) AS [IMSRPTOTAMTEV]\n"
                + "      ,ISNULL([IMSRPTOTAMTEVF],0) AS [IMSRPTOTAMTEVF]\n"
                + "      ,ISNULL([IMSRPEXPENSE],0) AS [IMSRPEXPENSE]\n"
                + "	  ,ISNULL([IMSRPFREIGHT]+[IMSRPFREIGHTM]+[IMSRPINSUR]+[IMSRPRENT]+[IMSRPCLEAR]+[IMSRPCUSTFEE]+[IMSRPCUSTCASH],0) AS SUMEXP\n"
                + "	  ,ISNULL([IMSRPFREIGHT]+[IMSRPFREIGHTM]+[IMSRPINSUR]+[IMSRPRENT]+[IMSRPCLEAR]+[IMSRPCUSTFEE],0) AS SUMEXPCASH\n"
                + "\n"
                + "  FROM [RMShipment].[dbo].[IMSRP720]\n"
                + "  WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + "  AND [IMSRPIMC] = '" + imc + "'\n"
                + "  ) TMP\n"
                + "  ORDER BY [IMSRPPROD],[IMSRPCUR],[IMSRPYAM]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String prod = "ST";
            String ccy = "ST";
            String yam = "ST";

            Double FA = 0.00;
            Double IA = 0.00;
            Double FR = 0.00;
            Double ID = 0.00;
            Double VA = 0.00;
            Double TA = 0.00;
            Double TA2 = 0.00;

            Double FAM = 0.00;
            Double INS = 0.00;
            Double RNT = 0.00;
            Double CLE = 0.00;
            Double CFEE = 0.00;
            Double CCASH = 0.00;
            Double CVAT = 0.00;

            Double SumExp = 0.00;
            Double SumExpCash = 0.00;

            Double cFA = 0.00;
            Double cIA = 0.00;
            Double cFR = 0.00;
            Double cID = 0.00;
            Double cVA = 0.00;
            Double cTA = 0.00;
            Double cTA2 = 0.00;

            Double cFAM = 0.00;
            Double cINS = 0.00;
            Double cRNT = 0.00;
            Double cCLE = 0.00;
            Double cCFEE = 0.00;
            Double cCCASH = 0.00;
            Double cCVAT = 0.00;

            Double cSumExp = 0.00;
            Double cSumExpCash = 0.00;

            Double aFA = 0.00;
            Double aIA = 0.00;
            Double aFR = 0.00;
            Double aID = 0.00;
            Double aVA = 0.00;
            Double aTA = 0.00;
            Double aTA2 = 0.00;

            Double aFAM = 0.00;
            Double aINS = 0.00;
            Double aRNT = 0.00;
            Double aCLE = 0.00;
            Double aCFEE = 0.00;
            Double aCCASH = 0.00;
            Double aCVAT = 0.00;

            Double aSumExp = 0.00;
            Double aSumExpCash = 0.00;

            while (result.next()) {

                IMSRP720 p = new IMSRP720();

                p.setPeriod(result.getString("IMSRPPERIOD"));
                ym = result.getString("IMSRPPERIOD");

                if (prod.equals(result.getString("IMSRPPROD"))) {
                    prod = result.getString("IMSRPPROD");
                    p.setProd("");

                    if (ccy.equals(result.getString("IMSRPCUR"))) {
                        ccy = result.getString("IMSRPCUR");
                        p.setCcy("");

                        if (yam.equals(result.getString("IMSRPYAM"))) {
                            yam = result.getString("IMSRPYAM");
                            p.setYam("");

                        } else {
                            yam = result.getString("IMSRPYAM");
                            p.setYam(result.getString("IMSRPYAM"));
                        }
                    } else {
                        if (!ccy.equals("ST")) {
                            IMSRP720 sp = new IMSRP720();
                            sp.setCcy(ccy + " Total+ULINE");
                            sp.setYam("+ULINE");
                            sp.setForAmt(formatDou.format(cFA) + "+ULINE");
                            sp.setInvAmt(formatDou.format(cIA) + "+ULINE");
                            sp.setFreight(formatDou.format(cFR) + "+ULINE");
                            sp.setImpDuty(formatDou.format(cID) + "+ULINE");
                            sp.setVatAmt(formatDou.format(cVA) + "+ULINE");
                            sp.setTotAmtEV(formatDou.format(cTA) + "+ULINE");
                            sp.setTotAmtEVF(formatDou.format(cTA2) + "+ULINE");
                            sp.setExp(formatDou.format((cIA == 0 ? 100 : (cTA / cIA) * 100)) + "%+ULINE");

                            sp.setFreightM(formatDou.format(cFAM) + "+ULINE");
                            sp.setInsur(formatDou.format(cINS) + "+ULINE");
                            sp.setRent(formatDou.format(cRNT) + "+ULINE");
                            sp.setClearing(formatDou.format(cCLE) + "+ULINE");
                            sp.setCustfee(formatDou.format(cCFEE) + "+ULINE");
                            sp.setCustcash(formatDou.format(cCCASH) + "+ULINE");
                            sp.setCustvat(formatDou.format(cCVAT) + "+ULINE");

                            sp.setSumExp(formatDou.format(cSumExp) + "+ULINE");
                            sp.setSumExpCash(formatDou.format(cSumExpCash) + "+ULINE");
                            sp.setExpN(formatDou.format((cIA == 0 ? 100 : (cSumExp / cIA) * 100)) + "%+ULINE");
                            sp.setExp0(formatDou.format((cIA == 0 ? 100 : (cSumExpCash / cIA) * 100)) + "%+ULINE");

                            ul.add(sp);

                            cFA = 0.00;
                            cIA = 0.00;
                            cFR = 0.00;
                            cID = 0.00;
                            cVA = 0.00;
                            cTA = 0.00;
                            cTA2 = 0.00;

                            cFAM = 0.00;
                            cINS = 0.00;
                            cRNT = 0.00;
                            cCLE = 0.00;
                            cCFEE = 0.00;
                            cCCASH = 0.00;
                            cCVAT = 0.00;

                            cSumExp = 0.00;
                            cSumExpCash = 0.00;

                        }

                        ccy = result.getString("IMSRPCUR");
                        p.setCcy(result.getString("IMSRPCUR"));
                        yam = result.getString("IMSRPYAM");
                        p.setYam(result.getString("IMSRPYAM"));
                    }

                } else {
                    if (!prod.equals("ST")) {
                        IMSRP720 sp = new IMSRP720();
                        sp.setCcy(ccy + " Total+ULINE");
                        sp.setYam("+ULINE");
                        sp.setForAmt(formatDou.format(cFA) + "+ULINE");
                        sp.setInvAmt(formatDou.format(cIA) + "+ULINE");
                        sp.setFreight(formatDou.format(cFR) + "+ULINE");
                        sp.setImpDuty(formatDou.format(cID) + "+ULINE");
                        sp.setVatAmt(formatDou.format(cVA) + "+ULINE");
                        sp.setTotAmtEV(formatDou.format(cTA) + "+ULINE");
                        sp.setTotAmtEVF(formatDou.format(cTA2) + "+ULINE");
                        sp.setExp(formatDou.format((cIA == 0 ? 100 : (cTA / cIA) * 100)) + "%+ULINE");

                        sp.setFreightM(formatDou.format(cFAM) + "+ULINE");
                        sp.setInsur(formatDou.format(cINS) + "+ULINE");
                        sp.setRent(formatDou.format(cRNT) + "+ULINE");
                        sp.setClearing(formatDou.format(cCLE) + "+ULINE");
                        sp.setCustfee(formatDou.format(cCFEE) + "+ULINE");
                        sp.setCustcash(formatDou.format(cCCASH) + "+ULINE");
                        sp.setCustvat(formatDou.format(cCVAT) + "+ULINE");

                        sp.setSumExp(formatDou.format(cSumExp) + "+ULINE");
                        sp.setSumExpCash(formatDou.format(cSumExpCash) + "+ULINE");
                        sp.setExpN(formatDou.format((cIA == 0 ? 100 : (cSumExp / cIA) * 100)) + "%+ULINE");
                        sp.setExp0(formatDou.format((cIA == 0 ? 100 : (cSumExpCash / cIA) * 100)) + "%+ULINE");

                        ul.add(sp);

                        cFA = 0.00;
                        cIA = 0.00;
                        cFR = 0.00;
                        cID = 0.00;
                        cVA = 0.00;
                        cTA = 0.00;
                        cTA2 = 0.00;

                        cFAM = 0.00;
                        cINS = 0.00;
                        cRNT = 0.00;
                        cCLE = 0.00;
                        cCFEE = 0.00;
                        cCCASH = 0.00;
                        cCVAT = 0.00;

                        cSumExp = 0.00;
                        cSumExpCash = 0.00;

                        IMSRP720 sp2 = new IMSRP720();
                        sp2.setProd(prod + " Total+ULINE");
                        sp2.setCcy("+ULINE");
                        sp2.setYam("+ULINE");
                        sp2.setForAmt(formatDou.format(FA) + "+ULINE");
                        sp2.setInvAmt(formatDou.format(IA) + "+ULINE");
                        sp2.setFreight(formatDou.format(FR) + "+ULINE");
                        sp2.setImpDuty(formatDou.format(ID) + "+ULINE");
                        sp2.setVatAmt(formatDou.format(VA) + "+ULINE");
                        sp2.setTotAmtEV(formatDou.format(TA) + "+ULINE");
                        sp2.setTotAmtEVF(formatDou.format(TA2) + "+ULINE");
                        sp2.setExp(formatDou.format((IA == 0 ? 100 : (TA / IA) * 100)) + "%+ULINE");

                        sp2.setFreightM(formatDou.format(FAM) + "+ULINE");
                        sp2.setInsur(formatDou.format(INS) + "+ULINE");
                        sp2.setRent(formatDou.format(RNT) + "+ULINE");
                        sp2.setClearing(formatDou.format(CLE) + "+ULINE");
                        sp2.setCustfee(formatDou.format(CFEE) + "+ULINE");
                        sp2.setCustcash(formatDou.format(CCASH) + "+ULINE");
                        sp2.setCustvat(formatDou.format(CVAT) + "+ULINE");

                        sp2.setSumExp(formatDou.format(SumExp) + "+ULINE");
                        sp2.setSumExpCash(formatDou.format(SumExpCash) + "+ULINE");
                        sp2.setExpN(formatDou.format((IA == 0 ? 100 : (SumExp / IA) * 100)) + "%+ULINE");
                        sp2.setExp0(formatDou.format((IA == 0 ? 100 : (SumExpCash / IA) * 100)) + "%+ULINE");

                        ul.add(sp2);

                        FA = 0.00;
                        IA = 0.00;
                        FR = 0.00;
                        ID = 0.00;
                        VA = 0.00;
                        TA = 0.00;
                        TA2 = 0.00;

                        FAM = 0.00;
                        INS = 0.00;
                        RNT = 0.00;
                        CLE = 0.00;
                        CFEE = 0.00;
                        CCASH = 0.00;
                        CVAT = 0.00;

                        SumExp = 0.00;
                        SumExpCash = 0.00;

                    }

                    prod = result.getString("IMSRPPROD");
                    p.setProd(result.getString("IMSRPPROD"));
                    ccy = result.getString("IMSRPCUR");
                    p.setCcy(result.getString("IMSRPCUR"));
                    yam = result.getString("IMSRPYAM");
                    p.setYam(result.getString("IMSRPYAM"));

                }

                p.setForAmt(result.getString("IMSRPFORAMT"));
                p.setInvAmt(result.getString("IMSRPINVAMT"));
                p.setFreight(result.getString("IMSRPFREIGHT"));

                p.setFreightM(result.getString("FREIGHTM"));
                p.setInsur(result.getString("INSUR"));
                p.setRent(result.getString("RENT"));
                p.setClearing(result.getString("CLEARING"));
                p.setCustfee(result.getString("CUSTFEE"));
                p.setCustcash(result.getString("CUSTCASH"));
                p.setCustvat(result.getString("CUSTVAT"));

                p.setSumExp(result.getString("SUMEXP"));
                p.setSumExpCash(result.getString("SUMEXPCASH"));
                p.setExpN(result.getString("EXPN") + "%");
                p.setExp0(result.getString("EXP0") + "%");

                p.setImpDuty(result.getString("IMSRPIMPDUTY"));
                p.setVatAmt(result.getString("IMSRPVATAMT"));
                p.setTotAmtEV(result.getString("IMSRPTOTAMTEV"));
                p.setTotAmtEVF(result.getString("IMSRPTOTAMTEVF"));
                p.setExp(result.getString("IMSRPEXPENSE") + "%");

                FA += Double.parseDouble(result.getString("IMSRPFORAMT").replace(",", ""));
                IA += Double.parseDouble(result.getString("IMSRPINVAMT").replace(",", ""));
                FR += Double.parseDouble(result.getString("IMSRPFREIGHT").replace(",", ""));
                ID += Double.parseDouble(result.getString("IMSRPIMPDUTY").replace(",", ""));
                VA += Double.parseDouble(result.getString("IMSRPVATAMT").replace(",", ""));
                TA += Double.parseDouble(result.getString("IMSRPTOTAMTEV").replace(",", ""));
                TA2 += Double.parseDouble(result.getString("IMSRPTOTAMTEVF").replace(",", ""));

                SumExp += Double.parseDouble(result.getString("SUMEXP").replace(",", ""));
                SumExpCash += Double.parseDouble(result.getString("SUMEXPCASH").replace(",", ""));

                FAM += Double.parseDouble(result.getString("FREIGHTM").replace(",", ""));
                INS += Double.parseDouble(result.getString("INSUR").replace(",", ""));
                RNT += Double.parseDouble(result.getString("RENT").replace(",", ""));
                CLE += Double.parseDouble(result.getString("CLEARING").replace(",", ""));
                CFEE += Double.parseDouble(result.getString("CUSTFEE").replace(",", ""));
                CCASH += Double.parseDouble(result.getString("CUSTCASH").replace(",", ""));
                CVAT += Double.parseDouble(result.getString("CUSTVAT").replace(",", ""));

                cFA += Double.parseDouble(result.getString("IMSRPFORAMT").replace(",", ""));
                cIA += Double.parseDouble(result.getString("IMSRPINVAMT").replace(",", ""));
                cFR += Double.parseDouble(result.getString("IMSRPFREIGHT").replace(",", ""));
                cID += Double.parseDouble(result.getString("IMSRPIMPDUTY").replace(",", ""));
                cVA += Double.parseDouble(result.getString("IMSRPVATAMT").replace(",", ""));
                cTA += Double.parseDouble(result.getString("IMSRPTOTAMTEV").replace(",", ""));
                cTA2 += Double.parseDouble(result.getString("IMSRPTOTAMTEVF").replace(",", ""));

                cFAM += Double.parseDouble(result.getString("FREIGHTM").replace(",", ""));
                cINS += Double.parseDouble(result.getString("INSUR").replace(",", ""));
                cRNT += Double.parseDouble(result.getString("RENT").replace(",", ""));
                cCLE += Double.parseDouble(result.getString("CLEARING").replace(",", ""));
                cCFEE += Double.parseDouble(result.getString("CUSTFEE").replace(",", ""));
                cCCASH += Double.parseDouble(result.getString("CUSTCASH").replace(",", ""));
                cCVAT += Double.parseDouble(result.getString("CUSTVAT").replace(",", ""));

                cSumExp += Double.parseDouble(result.getString("SUMEXP").replace(",", ""));
                cSumExpCash += Double.parseDouble(result.getString("SUMEXPCASH").replace(",", ""));

                aFA += Double.parseDouble(result.getString("IMSRPFORAMT").replace(",", ""));
                aIA += Double.parseDouble(result.getString("IMSRPINVAMT").replace(",", ""));
                aFR += Double.parseDouble(result.getString("IMSRPFREIGHT").replace(",", ""));
                aID += Double.parseDouble(result.getString("IMSRPIMPDUTY").replace(",", ""));
                aVA += Double.parseDouble(result.getString("IMSRPVATAMT").replace(",", ""));
                aTA += Double.parseDouble(result.getString("IMSRPTOTAMTEV").replace(",", ""));
                aTA2 += Double.parseDouble(result.getString("IMSRPTOTAMTEVF").replace(",", ""));

                aFAM += Double.parseDouble(result.getString("FREIGHTM").replace(",", ""));
                aINS += Double.parseDouble(result.getString("INSUR").replace(",", ""));
                aRNT += Double.parseDouble(result.getString("RENT").replace(",", ""));
                aCLE += Double.parseDouble(result.getString("CLEARING").replace(",", ""));
                aCFEE += Double.parseDouble(result.getString("CUSTFEE").replace(",", ""));
                aCCASH += Double.parseDouble(result.getString("CUSTCASH").replace(",", ""));
                aCVAT += Double.parseDouble(result.getString("CUSTVAT").replace(",", ""));

                aSumExp += Double.parseDouble(result.getString("SUMEXP").replace(",", ""));
                aSumExpCash += Double.parseDouble(result.getString("SUMEXPCASH").replace(",", ""));

                ul.add(p);

            }

            IMSRP720 sp = new IMSRP720();
            sp.setCcy(ccy + " Total+ULINE");
            sp.setYam("+ULINE");
            sp.setForAmt(formatDou.format(cFA) + "+ULINE");
            sp.setInvAmt(formatDou.format(cIA) + "+ULINE");
            sp.setFreight(formatDou.format(cFR) + "+ULINE");
            sp.setImpDuty(formatDou.format(cID) + "+ULINE");
            sp.setVatAmt(formatDou.format(cVA) + "+ULINE");
            sp.setTotAmtEV(formatDou.format(cTA) + "+ULINE");
            sp.setTotAmtEVF(formatDou.format(cTA2) + "+ULINE");
            sp.setExp(formatDou.format((cIA == 0 ? 100 : (cTA / cIA) * 100)) + "%+ULINE");

            sp.setFreightM(formatDou.format(cFAM) + "+ULINE");
            sp.setInsur(formatDou.format(cINS) + "+ULINE");
            sp.setRent(formatDou.format(cRNT) + "+ULINE");
            sp.setClearing(formatDou.format(cCLE) + "+ULINE");
            sp.setCustfee(formatDou.format(cCFEE) + "+ULINE");
            sp.setCustcash(formatDou.format(cCCASH) + "+ULINE");
            sp.setCustvat(formatDou.format(cCVAT) + "+ULINE");

            sp.setSumExp(formatDou.format(cSumExp) + "+ULINE");
            sp.setSumExpCash(formatDou.format(cSumExpCash) + "+ULINE");
            sp.setExpN(formatDou.format((cIA == 0 ? 100 : (cSumExp / cIA) * 100)) + "%+ULINE");
            sp.setExp0(formatDou.format((cIA == 0 ? 100 : (cSumExpCash / cIA) * 100)) + "%+ULINE");

            ul.add(sp);

            IMSRP720 sp2 = new IMSRP720();
            sp2.setProd(prod + " Total+ULINE");
            sp2.setCcy("+ULINE");
            sp2.setYam("+ULINE");
            sp2.setForAmt(formatDou.format(FA) + "+ULINE");
            sp2.setInvAmt(formatDou.format(IA) + "+ULINE");
            sp2.setFreight(formatDou.format(FR) + "+ULINE");
            sp2.setImpDuty(formatDou.format(ID) + "+ULINE");
            sp2.setVatAmt(formatDou.format(VA) + "+ULINE");
            sp2.setTotAmtEV(formatDou.format(TA) + "+ULINE");
            sp2.setTotAmtEVF(formatDou.format(TA2) + "+ULINE");
            sp2.setExp(formatDou.format((IA == 0 ? 100 : (TA / IA) * 100)) + "%+ULINE");

            sp2.setFreightM(formatDou.format(FAM) + "+ULINE");
            sp2.setInsur(formatDou.format(INS) + "+ULINE");
            sp2.setRent(formatDou.format(RNT) + "+ULINE");
            sp2.setClearing(formatDou.format(CLE) + "+ULINE");
            sp2.setCustfee(formatDou.format(CFEE) + "+ULINE");
            sp2.setCustcash(formatDou.format(CCASH) + "+ULINE");
            sp2.setCustvat(formatDou.format(CVAT) + "+ULINE");

            sp2.setSumExp(formatDou.format(SumExp) + "+ULINE");
            sp2.setSumExpCash(formatDou.format(SumExpCash) + "+ULINE");
            sp2.setExpN(formatDou.format((IA == 0 ? 100 : (SumExp / IA) * 100)) + "%+ULINE");
            sp2.setExp0(formatDou.format((IA == 0 ? 100 : (SumExpCash / IA) * 100)) + "%+ULINE");

            ul.add(sp2);

            IMSRP720 sp3 = new IMSRP720();
            sp3.setProd("Grand Total+ULINE");
            sp3.setCcy("+ULINE");
            sp3.setYam("+ULINE");
            sp3.setForAmt(formatDou.format(aFA) + "+ULINE");
            sp3.setInvAmt(formatDou.format(aIA) + "+ULINE");
            sp3.setFreight(formatDou.format(aFR) + "+ULINE");
            sp3.setImpDuty(formatDou.format(aID) + "+ULINE");
            sp3.setVatAmt(formatDou.format(aVA) + "+ULINE");
            sp3.setTotAmtEV(formatDou.format(aTA) + "+ULINE");
            sp3.setTotAmtEVF(formatDou.format(aTA2) + "+ULINE");
            sp3.setExp(formatDou.format((aIA == 0 ? 100 : (aTA / aIA) * 100)) + "%+ULINE");

            sp3.setFreightM(formatDou.format(aFAM) + "+ULINE");
            sp3.setInsur(formatDou.format(aINS) + "+ULINE");
            sp3.setRent(formatDou.format(aRNT) + "+ULINE");
            sp3.setClearing(formatDou.format(aCLE) + "+ULINE");
            sp3.setCustfee(formatDou.format(aCFEE) + "+ULINE");
            sp3.setCustcash(formatDou.format(aCCASH) + "+ULINE");
            sp3.setCustvat(formatDou.format(aCVAT) + "+ULINE");

            sp3.setSumExp(formatDou.format(aSumExp) + "+ULINE");
            sp3.setSumExpCash(formatDou.format(aSumExpCash) + "+ULINE");
            sp3.setExpN(formatDou.format((aIA == 0 ? 100 : (aSumExp / aIA) * 100)) + "%+ULINE");
            sp3.setExp0(formatDou.format((aIA == 0 ? 100 : (aSumExpCash / aIA) * 100)) + "%+ULINE");

            ul.add(sp3);

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP730> findByCode22Print(String ym) {

        List<IMSRP730> ul = new ArrayList<IMSRP730>();

        String sql = "SELECT CCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPFORAMT) FROM IMSRP730 WHERE [IMSRPPERIOD] = '" + ym + "' AND [IMSRPCUR] = CCY),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBILLAMT) FROM IMSRP730 WHERE [IMSRPPERIOD] = '" + ym + "' AND [IMSRPCUR] = CCY),0),'#,#0.00') AS SUMBA\n"
                + "FROM(\n"
                + "SELECT DISTINCT [IMSRPCUR] AS CCY\n"
                + "FROM [IMSRP730]\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            IMSRP730 p1 = new IMSRP730();
            p1.setCcy("");
            ul.add(p1);

            while (result.next()) {

                IMSRP730 p = new IMSRP730();

                p.setCcy(result.getString("CCY") + "+TLB");
                p.setCustRate("+TB");
                p.setSum("+TB");
                p.setForAmt(result.getString("SUMFA") + "+TLRB");
                p.setBillAmt(result.getString("SUMBA") + "+TLRB");

                ul.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP730> findByCode22(String ym) {

        List<IMSRP730> ul = new ArrayList<IMSRP730>();

        String sql = "SELECT CCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPFORAMT) FROM IMSRP730 WHERE [IMSRPPERIOD] = '" + ym + "' AND [IMSRPCUR] = CCY),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBILLAMT) FROM IMSRP730 WHERE [IMSRPPERIOD] = '" + ym + "' AND [IMSRPCUR] = CCY),0),'#,#0.00') AS SUMBA\n"
                + "FROM(\n"
                + "SELECT DISTINCT [IMSRPCUR] AS CCY\n"
                + "FROM [IMSRP730]\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            IMSRP730 p1 = new IMSRP730();
            p1.setCcy("");
            ul.add(p1);

            int i = 0;

            while (result.next()) {

                i++;

                IMSRP730 p = new IMSRP730();

                if (i == 1) {
                    p.setProd("<b>GRAND TOTAL BY CCY</b>");
                }

                p.setCcy("<b>" + result.getString("CCY") + "</b>");
                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setBillAmt("<b>" + result.getString("SUMBA") + "</b>");

                ul.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP730> findForPrint(String ym) {

        List<IMSRP730> ul = new ArrayList<IMSRP730>();

        String sql = "SELECT [IMSRPPERIOD]\n"
                + "      ,[IMSRPIMPNO]\n"
                + "      ,[IMSRPVENDEPT]\n"
                + "      ,[IMSVDNAME]\n"
                + "      ,[IMSRPINVNO]\n"
                + "      ,[IMSRPPROD]\n"
                + "      ,[IMSTRNAME] as [IMSRPTRANSPORT]\n"
                + "      ,FORMAT([IMSRPINVDATE],'dd-MM-yyy') as [IMSRPINVDATE]\n"
                + "      ,FORMAT([IMSRPARRDATE],'dd-MM-yyy') as [IMSRPARRDATE]\n"
                + "      ,FORMAT([IMSRPDEBITDATE],'dd-MM-yyy') as [IMSRPDEBITDATE]\n"
                + "  FROM [IMSRP740]\n"
                + "  LEFT JOIN IMSVEN ON IMSVEN.IMSVDCODE = [IMSRP740].[IMSRPVENDEPT]\n"
                + "  LEFT JOIN [IMSTRP] ON [IMSTRP].[IMSTRCODE] = [IMSRP740].[IMSRPTRANSPORT]\n"
                + "  WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + "  ORDER BY [IMSRPPERIOD], [IMSRPIMPNO], [IMSRPVENDEPT], [IMSRPINVNO], [IMSRPPROD], [IMSRPTRANSPORT]";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP730 p = new IMSRP730();

                p.setPeriod(result.getString("IMSRPPERIOD"));
                p.setProd(result.getString("IMSRPIMPNO"));
                p.setVen(result.getString("IMSRPVENDEPT"));
                p.setInvNo(result.getString("IMSRPINVNO"));
                p.setCcy(result.getString("IMSRPPROD"));
                p.setCustRate(result.getString("IMSRPTRANSPORT"));
                p.setForAmt(result.getString("IMSRPINVDATE"));
                p.setBillAmt(result.getString("IMSRPARRDATE"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findForPrint2(String year) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT (SELECT TOP 1 RIGHT(IMSRPPERIOD,2) FROM IMSRP710 WHERE LEFT(IMSRPPERIOD,4) = '2019' ORDER BY IMSRPPERIOD) AS MF\n"
                + ",(SELECT TOP 1 RIGHT(IMSRPPERIOD,2) FROM IMSRP710 WHERE LEFT(IMSRPPERIOD,4) = '2019' ORDER BY IMSRPPERIOD DESC) AS MT\n"
                + ",*\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPFAMT) FROM IMSRP710 WHERE LEFT(IMSRPPERIOD,4) = '" + year + "' AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPINVAMT) FROM IMSRP710 WHERE LEFT(IMSRPPERIOD,4) = '" + year + "' AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMIA\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPPAYAMT) FROM IMSRP710 WHERE LEFT(IMSRPPERIOD,4) = '" + year + "' AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMPA\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPDIFFAMT) FROM IMSRP710 WHERE LEFT(IMSRPPERIOD,4) = '" + year + "' AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPCCY AS CCY\n"
                + "FROM IMSRP710\n"
                + "WHERE LEFT(IMSRPPERIOD,4) = '" + year + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setPeriod("M." + result.getString("MF") + "-M." + result.getString("MT"));
                p.setCcy(result.getString("CCY"));

                p.setForAmt(result.getString("SUMFA"));
                p.setInvAmt(result.getString("SUMIA"));
                p.setPayAmt(result.getString("SUMPA"));
                p.setDiffAmt(result.getString("SUMDA"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findTotalByDept(String ym) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT *\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPINVAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0),'#,#0.00') AS SUMIA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPPAYAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0),'#,#0.00') AS SUMPA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPDIFFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPDEPT AS DEPT\n"
                + "FROM IMSRP710\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setDept("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("DEPT") + "3</font><b>TOTAL BY DEPT : </b>");
                p.setBank("<b>" + result.getString("DEPT") + "</b>");

                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setInvAmt("<b>" + result.getString("SUMIA") + "</b>");
                p.setPayAmt("<b>" + result.getString("SUMPA") + "</b>");
                p.setDiffAmt("<b>" + result.getString("SUMDA") + "</b>");

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findTotalByBank(String ym) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT *\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPINVAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK),0),'#,#0.00') AS SUMIA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPPAYAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK),0),'#,#0.00') AS SUMPA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPDIFFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK),0),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPDEPT AS DEPT, IMSRPBCODE AS BANK\n"
                + "FROM IMSRP710\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setDept("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("DEPT") + "2</font><font size=\"1\" style=\"opacity: 0;\">TOTAL BY DEPT : " + result.getString("DEPT") + "</font>");
                p.setBank("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("BANK") + "2</font><b>TOTAL BY BANK : </b>");
                p.setPayDate("<b>" + result.getString("BANK") + "</b>");

                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setInvAmt("<b>" + result.getString("SUMIA") + "</b>");
                p.setPayAmt("<b>" + result.getString("SUMPA") + "</b>");
                p.setDiffAmt("<b>" + result.getString("SUMDA") + "</b>");

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findTotalByCcy(String ym) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT *\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMFA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPINVAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMIA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPPAYAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMPA\n"
                + ",FORMAT(ISNULL((SELECT SUM([IMSRPDIFFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT AND IMSRPBCODE = BANK AND IMSRPCCY = CCY),0),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPDEPT AS DEPT, IMSRPBCODE AS BANK, IMSRPCCY AS CCY\n"
                + "FROM IMSRP710\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setDept("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("DEPT") + "1</font><font size=\"1\" style=\"opacity: 0;\">TOTAL BY DEPT : " + result.getString("DEPT") + "</font>");
                p.setBank("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("BANK") + "1</font><font size=\"1\" style=\"opacity: 0;\">TOTAL BY BANK : " + result.getString("BANK") + "</font>");
                p.setCcy("<font size=\"1\" style=\"opacity: 0;\">" + result.getString("CCY") + "</font><b>TOTAL BY CCY : </b>");
                p.setPayRate("<b>" + result.getString("CCY") + "</b>");

                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setInvAmt("<b>" + result.getString("SUMIA") + "</b>");
                p.setPayAmt("<b>" + result.getString("SUMPA") + "</b>");
                p.setDiffAmt("<b>" + result.getString("SUMDA") + "</b>");

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP710> findTotalByMonth(String ym) {

        List<IMSRP710> ul = new ArrayList<IMSRP710>();

        String sql = "SELECT FORMAT(SUM(SUMFA),'#,#0.00') AS SUMFA, \n"
                + "FORMAT(SUM(SUMIA),'#,#0.00') AS SUMIA, \n"
                + "FORMAT(SUM(SUMPA),'#,#0.00') AS SUMPA, \n"
                + "FORMAT(SUM(SUMDA),'#,#0.00') AS SUMDA\n"
                + "FROM(\n"
                + "SELECT *\n"
                + ",ISNULL((SELECT SUM([IMSRPFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0) AS SUMFA\n"
                + ",ISNULL((SELECT SUM([IMSRPINVAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0) AS SUMIA\n"
                + ",ISNULL((SELECT SUM([IMSRPPAYAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0) AS SUMPA\n"
                + ",ISNULL((SELECT SUM([IMSRPDIFFAMT]) FROM [IMSRP710] WHERE [IMSRPPERIOD] = '" + ym + "' AND IMSRPDEPT = DEPT),0) AS SUMDA\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSRPPERIOD, IMSRPDEPT AS DEPT\n"
                + "FROM IMSRP710\n"
                + "WHERE [IMSRPPERIOD] = '" + ym + "'\n"
                + ")TMP\n"
                + ")TMP2";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP710 p = new IMSRP710();

                p.setDept("<font size=\"1\" style=\"opacity: 0;\">Z</font><b>GRAND TOTAL </b>");
                p.setBank("<b>BY MONTH : " + ym + "</b>");

                p.setForAmt("<b>" + result.getString("SUMFA") + "</b>");
                p.setInvAmt("<b>" + result.getString("SUMIA") + "</b>");
                p.setPayAmt("<b>" + result.getString("SUMPA") + "</b>");
                p.setDiffAmt("<b>" + result.getString("SUMDA") + "</b>");

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP700> findByYear(String year) {

        List<IMSRP700> ul = new ArrayList<IMSRP700>();

        String sql = "SELECT [IMSRPPERIOD]\n"
                + "      ,[IMSRPCCY]\n"
                + "      ,FORMAT([IMSRPMACHINE],'#,#0.00') AS [IMSRPMACHINE]\n"
                + "      ,FORMAT([IMSRPOTHER],'#,#0.00') AS [IMSRPOTHER]\n"
                + "      ,FORMAT([IMSRPTOTAL],'#,#0.00') AS [IMSRPTOTAL]\n"
                + "      ,FORMAT([IMSRPRATE],'#,#0.000000') AS [IMSRPRATE]\n"
                + "      ,FORMAT([IMSRPBAHT],'#,#0.00') AS [IMSRPBAHT]\n"
                + "      ,FORMAT([IMSRPBALCCY],'#,#0.00') AS [IMSRPBALCCY]\n"
                + "      ,FORMAT([IMSRPTOTCCY],'#,#0.00') AS [IMSRPTOTCCY]\n"
                + "      ,FORMAT([IMSRPALLCCY],'#,#0.00') AS [IMSRPALLCCY]\n"
                + "      ,FORMAT([IMSRPSUM],'#,#0.00') AS [IMSRPSUM]\n"
                + "      ,FORMAT([IMSRPHSB],'#,#0.00') AS [IMSRPHSB]\n"
                + "      ,FORMAT([IMSRPSCB],'#,#0.00') AS [IMSRPSCB]\n"
                + "      ,FORMAT([IMSRPBBL],'#,#0.00') AS [IMSRPBBL]\n"
                + "      ,FORMAT([IMSRPMIZ],'#,#0.00') AS [IMSRPMIZ]\n"
                + "      ,FORMAT([IMSRPKRT],'#,#0.00') AS [IMSRPKRT]\n"
                + "      ,FORMAT([IMSRPTOTPAY],'#,#0.00') AS [IMSRPTOTPAY]\n"
                + "      ,FORMAT([IMSRPNOPAY],'#,#0.00') AS [IMSRPNOPAY]\n"
                + "	 ,FORMAT(ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE IMSRPPERIOD = MN.IMSRPPERIOD),0),'#,#0.00')  AS SUMTIAB\n"
                + "  FROM [RMShipment].[dbo].[IMSRP700] MN\n"
                + "  WHERE LEFT([IMSRPPERIOD],4) = '" + year + "'\n"
                + "  ORDER BY IMSRPPERIOD ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP700 p = new IMSRP700();

                p.setPeriod(result.getString("IMSRPPERIOD"));
                p.setCcy(result.getString("IMSRPCCY"));
                p.setMacImpAmt(result.getString("IMSRPMACHINE"));
                p.setMatImpAmt(result.getString("IMSRPOTHER"));
                p.setTotalImpAmt(result.getString("IMSRPTOTAL"));
                p.setRate(result.getString("IMSRPRATE"));
                p.setTotalImpAmtB(result.getString("IMSRPBAHT"));
                p.setBalAmtPrevMonth(result.getString("IMSRPBALCCY"));
                p.setAccInvAmt(result.getString("IMSRPTOTCCY"));
                p.setAllBankBalPayment(result.getString("IMSRPALLCCY"));
                p.setSumitomo(result.getString("IMSRPSUM"));
                p.setHsb(result.getString("IMSRPHSB"));
                p.setScb(result.getString("IMSRPSCB"));
                p.setBbl(result.getString("IMSRPBBL"));
                p.setMizuho(result.getString("IMSRPMIZ"));
                p.setK_tokyo(result.getString("IMSRPKRT"));
                p.setTotal(result.getString("IMSRPTOTPAY"));
                p.setNoPaymentAmt(result.getString("IMSRPNOPAY"));

                p.setSumTiab(result.getString("SUMTIAB"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP700> findByYearTotal(String year) {

        List<IMSRP700> ul = new ArrayList<IMSRP700>();

        String sql = "DECLARE @YEAR NVARCHAR(6) = '" + year + "'\n"
                + "\n"
                + "SELECT 'GTOTAL' AS IMSRPPERIOD, CCY AS IMSRPCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPMACHINE) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPMACHINE\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPOTHER) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPOTHER\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPTOTAL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPTOTAL\n"
                + ",FORMAT(ISNULL((CASE WHEN (ISNULL((SELECT SUM(IMSRPTOTAL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0))=0 THEN 0 \n"
                + "ELSE (ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0)\n"
                + "/ISNULL((SELECT SUM(IMSRPTOTAL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0))\n"
                + "END),0),'#,#0.000000') AS IMSRPRATE\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPBAHT\n"
                + ",FORMAT(ISNULL((SELECT TOP 1 IMSRPNOPAY FROM IMSRP700 \n"
                + "WHERE IMSRPPERIOD = (SELECT TOP 1 IMSRPPERIOD FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY ORDER BY IMSRPPERIOD DESC) \n"
                + "AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPBALCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPTOTCCY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPTOTCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPALLCCY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPALLCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPSUM) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPSUM\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPHSB) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPHSB\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPSCB) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPSCB\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBBL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPBBL\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPMIZ) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPMIZ\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPKRT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPKRT\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPTOTPAY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPTOTPAY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPNOPAY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPNOPAY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR),0),'#,#0.00') AS SUMTIAB\n"
                + "FROM(\n"
                + "SELECT IMSCUCODE AS CCY\n"
                + "FROM IMSCUR\n"
                + "WHERE IMSCUCODE != 'THB'\n"
                + ")CUR";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP700 p = new IMSRP700();

                p.setPeriod(result.getString("IMSRPPERIOD"));
                p.setCcy(result.getString("IMSRPCCY"));
                p.setMacImpAmt(result.getString("IMSRPMACHINE"));
                p.setMatImpAmt(result.getString("IMSRPOTHER"));
                p.setTotalImpAmt(result.getString("IMSRPTOTAL"));
                p.setRate(result.getString("IMSRPRATE"));
                p.setTotalImpAmtB(result.getString("IMSRPBAHT"));
                p.setBalAmtPrevMonth(result.getString("IMSRPBALCCY"));
                p.setAccInvAmt(result.getString("IMSRPTOTCCY"));
                p.setAllBankBalPayment(result.getString("IMSRPALLCCY"));
                p.setSumitomo(result.getString("IMSRPSUM"));
                p.setHsb(result.getString("IMSRPHSB"));
                p.setScb(result.getString("IMSRPSCB"));
                p.setBbl(result.getString("IMSRPBBL"));
                p.setMizuho(result.getString("IMSRPMIZ"));
                p.setK_tokyo(result.getString("IMSRPKRT"));
                p.setTotal(result.getString("IMSRPTOTPAY"));
                p.setNoPaymentAmt(result.getString("IMSRPNOPAY"));

                p.setSumTiab(result.getString("SUMTIAB"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public boolean add(String ym, String userid, String imc) {

        boolean result = false;

        String sql = "INSERT INTO IMSRP720 ([IMSRPCOM]\n"
                + "      ,[IMSRPPERIOD]\n"
                + "      ,[IMSRPPROD]\n"
                + "      ,[IMSRPCUR]\n"
                + "      ,[IMSRPYAM]\n"
                + "	  ,[IMSRPIMC]\n"
                + "      ,[IMSRPFORAMT]\n"
                + "      ,[IMSRPINVAMT]\n"
                + "      ,[IMSRPFREIGHT]\n" //Handering
                + "      ,[IMSRPFREIGHTM]\n" //freight monthly
                + "      ,[IMSRPINSUR]\n" //insurance
                + "      ,[IMSRPRENT]\n" //rent
                + "      ,[IMSRPCLEAR]\n" //clearing
                + "      ,[IMSRPCUSTFEE]\n" //custom duty fee
                + "      ,[IMSRPCUSTCASH]\n" //custom duty cash
                + "      ,[IMSRPCUSTVAT]\n" //custom duty vat

                + "      ,[IMSRPIMPDUTY]\n"
                + "      ,[IMSRPVATAMT]\n"
                + "      ,[IMSRPTOTAMTEV]\n"
                + "      ,[IMSRPTOTAMTEVF]\n"
                + "      ,[IMSRPEXPENSE]\n"
                + "      ,[IMSRPEDT]\n"
                + "      ,[IMSRPCDT]\n"
                + "      ,[IMSRPUSER])\n"
                + "SELECT *, CASE WHEN ISNULL(IMSSDINVAMT,0) = 0 THEN 100 ELSE (ISNULL(TOTALNOVAT,0)/ISNULL(IMSSDINVAMT,0)*100) END\n"
                + ",CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '93176'\n"
                + "FROM(\n"
                + "SELECT 'TWC' AS COM, YEARS, IMSSDPROD, IMSSDCUR, IMSHTRDT, IMSHIMC\n"
                + ",SUM(IMSSDFINVAMT) AS IMSSDFINVAMT, SUM(IMSSDINVAMT) AS IMSSDINVAMT\n"
                + ",SUM(HANDERING) AS HANDERING,SUM(FRMONTHLY) AS FRMONTHLY\n"
                + ",SUM(INSU) AS INSU,SUM(RENT) AS RENT,SUM(CLEARING) AS CLEARING\n"
                + ",SUM(CUSTFEE) AS CUSTFEE,SUM(CUSTCASH) AS CUSTCASH,SUM(CUSTVAT) AS CUSTVAT\n"
                + ",SUM(IMSCACTAMT) AS IMSCACTAMT\n"
                + ",SUM(VAT) AS VAT, SUM(TOTALNOVAT) AS TOTALNOVAT, SUM(TOTALNOVATNOFR) AS TOTALNOVATNOFR\n"
                + "FROM(\n"
                + "SELECT IMSSDETAIL.IMSSDPIMNO,FORMAT(IMSHTRDT,'yyyy') AS YEARS, IMSSDPROD, IMSSDCUR, FORMAT(IMSHTRDT,'yyyy/MM') AS IMSHTRDT\n"
                + ", IMSSDFINVAMT, IMSSDINVAMT, IMSCAFRAMT AS HANDERING\n"
                + ",ISNULL(ROUND((IMSFRG.IMSFRAMT*IMSSDETAIL.IMSSDESTEXP)/100,2),0) AS FRMONTHLY\n"
                + ",ISNULL(ROUND(((IMSINS.IMSINAMT + IMSINS.IMSINSAMT) * IMSSDETAIL.IMSSDESTEXP)/100,2),0) AS INSU\n"
                + ",ISNULL(ROUND(((IMSRNH.IMSRHAMT) * IMSSDETAIL.IMSSDESTEXP)/100,2),0) AS RENT\n"
                + ",ISNULL(ROUND(((IMSCLR.IMSCLSC) * IMSSDETAIL.IMSSDESTEXP)/100,2),0) AS CLEARING\n"
                + ",ISNULL(ROUND(((IMSCUT.IMSCUFEEAMT) * IMSSDETAIL.IMSSDESTEXP)/100,2),0) AS CUSTFEE\n"
                + ",ISNULL(ROUND(((IMSCUT.IMSCUCASHAMT) * IMSSDETAIL.IMSSDESTEXP)/100,2),0) AS CUSTCASH\n"
                + ",ISNULL(ROUND(((IMSCUT.IMSCUVATAMT) * IMSSDETAIL.IMSSDESTEXP)/100,2),0) AS CUSTVAT\n"
                + ",IMSCACTAMT\n"
                + ",(ISNULL(IMSCAFRVAT,0) + ISNULL(IMSCAINVAT,0) + ISNULL(IMSCAREVAT,0) + ISNULL(IMSCACLVAT,0) \n"
                + "+ ISNULL(IMSCACTVAT,0)) AS VAT\n"
                + ",(ISNULL(IMSCAFRAMT,0) + ISNULL(IMSCAINAMT,0) + ISNULL(IMSCAREAMT,0) + ISNULL(IMSCACLAMT,0) \n"
                + "+ ISNULL(IMSCABKAMT,0) + ISNULL(IMSCACTAMT,0)) AS TOTALNOVAT\n"
                + ",(ISNULL(IMSCAINAMT,0) + ISNULL(IMSCAREAMT,0) + ISNULL(IMSCACLAMT,0) \n"
                + "+ ISNULL(IMSCABKAMT,0) + ISNULL(IMSCACTAMT,0)) AS TOTALNOVATNOFR\n"
                + ",IMSHIMC\n"
                + ",IMSSDESTEXP\n"
                + "FROM IMSSDETAIL\n"
                + "LEFT JOIN IMSCAC ON IMSCAC.IMSCAPIMNO = IMSSDETAIL.IMSSDPIMNO\n"
                + "LEFT JOIN IMSSHEAD ON IMSSHEAD.IMSHPIMNO = IMSSDETAIL.IMSSDPIMNO AND IMSCAC.IMSCAINVNO = IMSSDETAIL.IMSSDINVNO AND IMSCAC.IMSCALINO = IMSSDETAIL.IMSSDLINE\n"
                + "LEFT JOIN IMSFRG ON IMSFRG.IMSFRPIMNO = IMSSDETAIL.IMSSDPIMNO\n"
                + "LEFT JOIN IMSINS ON IMSINS.IMSINPIMNO = IMSSDETAIL.IMSSDPIMNO\n"
                + "LEFT JOIN IMSRNH ON IMSRNH.IMSRHPIMNO = IMSSDETAIL.IMSSDPIMNO\n"
                + "LEFT JOIN IMSCLR ON IMSCLR.IMSCLPIMNO = IMSSDETAIL.IMSSDPIMNO\n"
                + "LEFT JOIN IMSCUT ON IMSCUT.IMSCUPIMNO = IMSSDETAIL.IMSSDPIMNO\n"
                + "WHERE FORMAT(IMSHTRDT,'yyyy') = '" + ym + "' AND IMSHIMC = '" + imc + "'\n"
                + ")TES\n"
                + "GROUP BY YEARS, IMSSDPROD, IMSSDCUR, IMSHTRDT, IMSHIMC\n"
                + ")TMP";
//                + "SELECT *, CASE WHEN ISNULL(IMSSDINVAMT,0) = 0 THEN 100 ELSE (ISNULL(TOTALNOVAT,0)/ISNULL(IMSSDINVAMT,0)*100) END\n"
//                + ",CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '" + userid + "'\n"
//                + "FROM(\n"
//                + "SELECT 'TWC' AS COM, YEARS, IMSSDPROD, IMSSDCUR, IMSHTRDT, IMSHIMC\n"
//                + ",SUM(IMSSDFINVAMT) AS IMSSDFINVAMT, SUM(IMSSDINVAMT) AS IMSSDINVAMT\n"
//                + ",SUM(IMSCAFRAMT) AS IMSCAFRAMT, SUM(IMSCACTAMT) AS IMSCACTAMT\n"
//                + ",SUM(VAT) AS VAT, SUM(TOTALNOVAT) AS TOTALNOVAT, SUM(TOTALNOVATNOFR) AS TOTALNOVATNOFR\n"
//                + "FROM(\n"
//                + "SELECT FORMAT(IMSHTRDT,'yyyy') AS YEARS, IMSSDPROD, IMSSDCUR, FORMAT(IMSHTRDT,'yyyy/MM') AS IMSHTRDT\n"
//                + ",IMSSDFINVAMT, IMSSDINVAMT, IMSCAFRAMT, IMSCACTAMT\n"
//                + ",(ISNULL(IMSCAFRVAT,0) + ISNULL(IMSCAINVAT,0) + ISNULL(IMSCAREVAT,0) + ISNULL(IMSCACLVAT,0) \n"
//                + "+ ISNULL(IMSCACTVAT,0)) AS VAT\n"
//                + ",(ISNULL(IMSCAFRAMT,0) + ISNULL(IMSCAINAMT,0) + ISNULL(IMSCAREAMT,0) + ISNULL(IMSCACLAMT,0) \n"
//                + "+ ISNULL(IMSCABKAMT,0) + ISNULL(IMSCACTAMT,0)) AS TOTALNOVAT\n"
//                + ",(ISNULL(IMSCAINAMT,0) + ISNULL(IMSCAREAMT,0) + ISNULL(IMSCACLAMT,0) \n"
//                + "+ ISNULL(IMSCABKAMT,0) + ISNULL(IMSCACTAMT,0)) AS TOTALNOVATNOFR\n"
//                + ",IMSHIMC\n"
//                + "FROM IMSSDETAIL\n"
//                + "LEFT JOIN IMSSHEAD ON IMSSHEAD.IMSHPIMNO = IMSSDETAIL.IMSSDPIMNO\n"
//                + "LEFT JOIN IMSCAC ON IMSCAC.IMSCAPIMNO = IMSSDETAIL.IMSSDPIMNO \n"
//                + "AND IMSCAC.IMSCAINVNO = IMSSDETAIL.IMSSDINVNO\n"
//                + "AND IMSCAC.IMSCALINO = IMSSDETAIL.IMSSDLINE\n"
//                + "WHERE FORMAT(IMSHTRDT,'yyyy') = '" + ym + "'\n"
//                + "AND IMSHIMC = '" + imc + "'\n"
//                + ")TES\n"
//                + "GROUP BY YEARS, IMSSDPROD, IMSSDCUR, IMSHTRDT, IMSHIMC\n"
//                + ")TMP";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String ym, String ccy, String rate, String totalImpAmtB, String accInvAmt, String noPaymentAmt) {

        boolean result = false;

        String sql = "UPDATE IMSRP700 "
                + "SET IMSRPRATE = " + rate + ""
                + ", IMSRPBAHT = " + totalImpAmtB + ""
                + ", IMSRPTOTCCY = " + accInvAmt + ""
                + ", IMSRPNOPAY = " + noPaymentAmt + ""
                + ", IMSRPEDT = CURRENT_TIMESTAMP"
                + " WHERE IMSRPPERIOD = '" + ym + "' AND IMSRPCCY = '" + ccy + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String code, String imc) {

        String sql = "";

        if (code.trim().equals("2019")) {
            sql = "DELETE FROM IMSRP720 WHERE IMSRPPERIOD = '" + code + "' AND IMSRPIMC = '" + imc + "' "
                    + "AND IMSRPYAM IN ('2019/08','2019/09','2019/10','2019/11','2019/12')";
        } else {
            sql = "DELETE FROM IMSRP720 WHERE IMSRPPERIOD = '" + code + "' AND IMSRPIMC = '" + imc + "'";
        }

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
