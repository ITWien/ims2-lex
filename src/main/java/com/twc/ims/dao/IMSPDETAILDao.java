/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.dao;

import com.twc.ims.database.database;
import com.twc.ims.entity.IMSAHEAD;
import com.twc.ims.entity.IMSPDETAIL;
import com.twc.ims.entity.IMSSDETAIL;
import com.twc.ims.entity.IMSSHC;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSPDETAILDao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public String findIMC(String code) {

        String max = "";

        String sql = "SELECT [IMSPHIMC]\n"
                + "  FROM [RMShipment].[dbo].[IMSPHEAD]\n"
                + "  where [IMSPHPDNO] = '" + code + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                String maxSt = result.getString("IMSPHIMC");
                if (maxSt != null) {
                    max = result.getString("IMSPHIMC");
                }

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return max;

    }

    public String check(String impNo, String pdNo, String inv) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "Select * FROM [RMShipment].[dbo].[IMSPDETAIL] where [IMSPDIMNO] = '" + impNo + "' "
                    + "and [IMSPDPDNO] = '" + pdNo + "' and [IMSPDINVNO] = '" + inv + "' ";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public IMSSHC findVENby(String code, String ven) {

        IMSSHC p = new IMSSHC();

        String sql = "SELECT DISTINCT IMSPDVCODE, IMSVDNAME, IMSPDFEE\n"
                + "FROM IMSPDETAIL\n"
                + "LEFT JOIN IMSVEN ON IMSVEN.IMSVDCODE = IMSPDETAIL.IMSPDVCODE\n"
                + "WHERE IMSPDPDNO = '" + code + "' "
                + "AND IMSPDVCODE = '" + ven + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setCode(result.getString("IMSPDVCODE"));

                p.setName(result.getString("IMSVDNAME"));

                p.setNamet(formatDou.format(Double.parseDouble(result.getString("IMSPDFEE"))));

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public List<IMSSHC> findAllVEN(String code) {

        List<IMSSHC> UAList = new ArrayList<IMSSHC>();

        String sql = "SELECT DISTINCT IMSPDVCODE, IMSVDNAME, IMSPDFEE\n"
                + "FROM IMSPDETAIL\n"
                + "LEFT JOIN IMSVEN ON IMSVEN.IMSVDCODE = IMSPDETAIL.IMSPDVCODE\n"
                + "WHERE IMSPDPDNO = '" + code + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSSHC p = new IMSSHC();

                p.setCode(result.getString("IMSPDVCODE"));

                p.setName(result.getString("IMSVDNAME"));

                if (result.getString("IMSPDFEE") != null) {
                    p.setNamet(formatDou.format(Double.parseDouble(result.getString("IMSPDFEE"))));
                }

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public boolean updateAmt(String importPaidNo, String line, String ia, String fia, String PRT, String bank, String paidDate, String fee, String impNo) {

        boolean result = false;

        String sql = "UPDATE IMSPDETAIL "
                + "SET IMSPDSTS = 'P'\n"
                + "   ,IMSPDPDAMT = '" + ia + "'\n"
                + "   ,IMSPDFPDAMT = '" + fia + "'\n"
                + "   ,IMSPDEXRATE = '" + PRT + "'\n"
                + "   ,IMSPDBCODE = '" + bank + "'\n"
                + "   ,IMSPDFEE = '" + fee + "'\n"
                + "   ,IMSPDPDDT = '" + paidDate + "'\n"
                + "   ,IMSPDEDT = CURRENT_TIMESTAMP "
                + "WHERE IMSPDPDNO = '" + importPaidNo + "' "
                + "AND IMSPDLINE = '" + line + "' "
                + "AND IMSPDIMNO = '" + impNo + "' ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }
        return result;

    }

    public boolean updateSTS(String importPaidNo) {

        boolean result = false;

        String sql = "UPDATE IMSPDETAIL "
                + "SET IMSPDSTS = 'D'\n"
                + "   ,IMSPDEDT = CURRENT_TIMESTAMP "
                + "WHERE IMSPDPDNO = '" + importPaidNo + "' ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public ResultSet reportIMS300(String impPaidNo) {

//        StringBuilder blank = new StringBuilder();
//        if (all <= 24) {
//            for (int i = 0; i < 24 - all; i++) {
//                blank.append("union all\n" + "select null, null, null, 'ZZZZZZZ', null, null, null, null, null, null, null, null, null, null, null, null, 'ZZZZZZZ', null\n");
//            }
//        } else if (all > 24 && all <= 48) {
//            for (int i = 0; i < 48 - all; i++) {
//                blank.append("union all\n" + "select null, null, null, 'ZZZZZZZ', null, null, null, null, null, null, null, null, null, null, null, null, 'ZZZZZZZ', null\n");
//            }
//        } else if (all > 48 && all <= 72) {
//            for (int i = 0; i < 72 - all; i++) {
//                blank.append("union all\n" + "select null, null, null, 'ZZZZZZZ', null, null, null, null, null, null, null, null, null, null, null, null, 'ZZZZZZZ', null\n");
//            }
//        }
        String sql = "SELECT IMSPHIMNO, IMSPHPDDT, IMSPDPDNO, IMSPDVCODE, IMSVDNAME, IMSVDBCODE, \n"
                + "(SELECT TOP 1 IMSBADESC FROM IMSBMAS WHERE IMSBACODE = IMSVDBCODE) AS IMSVDBCODE_NAME,\n"
                + "IMSVDSWIFT, IMSVDBACNO,\n"
                + "FORMAT((\n"
                + "SELECT SUM(IMSPDFINVAMT) FROM IMSPDETAIL\n"
                + "WHERE IMSPDPDNO = '" + impPaidNo + "'\n"
                + "AND IMSPDVCODE = TMP.IMSPDVCODE\n"
                + "), '#,#0.00') AS IMSPHPDAMT, \n"
                + "IMSPDCUR, IMSPHIMC, IMSIMNAME, \n"
                + "IMSPDBCODE, (SELECT TOP 1 IMSBADESC FROM IMSBMAS WHERE IMSBACODE = IMSPDBCODE) AS IMSPDBCODE_NAME,\n"
                + "IMCBACNO, IMSPDINVNO, \n"
                + "(SELECT SUM(IMSPDFINVAMT) FROM IMSPDETAIL WHERE IMSPDINVNO = TMP.IMSPDINVNO \n"
                + "AND IMSPDPDNO = '" + impPaidNo + "') AS AMOUNT\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSPDPDNO, IMSPDINVNO, IMSPDCUR, IMSPDBCODE, IMSPDVCODE\n"
                + "FROM IMSPDETAIL\n"
                + "WHERE IMSPDPDNO = '" + impPaidNo + "'\n"
                + ")TMP\n"
                + "LEFT JOIN IMSPHEAD ON IMSPHEAD.IMSPHPDNO = TMP.IMSPDPDNO\n"
                + "LEFT JOIN IMSVEN ON IMSVEN.IMSVDCODE = TMP.IMSPDVCODE\n"
                + "LEFT JOIN IMSSHEAD ON IMSSHEAD.IMSHIMNO = IMSPHEAD.IMSPHIMNO\n"
                + "LEFT JOIN IMSIMC ON IMSIMC.IMSIMCODE = IMSPHEAD.IMSPHIMC\n"
                + "LEFT JOIN IMSICB ON IMSICB.IMCBBCODE = TMP.IMSPDBCODE AND IMSICB.IMCBCODE = IMSPHEAD.IMSPHIMC\n"
                //                + blank
                + "ORDER BY IMSPDVCODE, IMSPDINVNO  ";
        ResultSet result = null;
        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            result = ps.executeQuery();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public ResultSet reportIMS300PVDIS(String impPaidNo) {

        String sql = "SELECT TMP.IMSPDPDNO, TMP.IMSPDVCODE, IMSVDNAME\n"
                + ",(SELECT TOP 1 IMSPDCUR FROM IMSPDETAIL WHERE IMSPDPDNO = TMP.IMSPDPDNO\n"
                + "AND IMSPDVCODE = TMP.IMSPDVCODE) AS CUR\n"
                + ",FORMAT((SELECT SUM(IMSPDFINVAMT) FROM IMSPDETAIL WHERE IMSPDPDNO = TMP.IMSPDPDNO\n"
                + "AND IMSPDVCODE = TMP.IMSPDVCODE), '#,#0.00') AS FAMT\n"
                + ",FORMAT((SELECT TOP 1 IMSPDEXRATE FROM IMSPDETAIL WHERE IMSPDPDNO = TMP.IMSPDPDNO\n"
                + "AND IMSPDVCODE = TMP.IMSPDVCODE), '#,#0.000000') AS RATE\n"
                + ",(SELECT SUM([IMSPVDAMT]) FROM [IMSPVDIS] WHERE [IMSPVDPDNO] = TMP.IMSPDPDNO) AS TAMT\n"
                + ",(SELECT TOP 1 IMSPDINVNO FROM IMSPDETAIL WHERE IMSPDPDNO = TMP.IMSPDPDNO\n"
                + "AND IMSPDVCODE = TMP.IMSPDVCODE) AS INV_NO\n"
                + ",(SELECT TOP 1 IMSPDPROD FROM IMSPDETAIL WHERE IMSPDPDNO = TMP.IMSPDPDNO\n"
                + "AND IMSPDVCODE = TMP.IMSPDVCODE) AS PROD\n"
                + ",[dbo].[NUM2THAI](\n"
                + "(SELECT SUM([IMSPVDAMT]) FROM [IMSPVDIS] WHERE [IMSPVDPDNO] = TMP.IMSPDPDNO) + IMSPDFEE\n"
                + ") AS TOTAL_THAI\n"
                + ",IMSPDFEE\n"
                + ",FORMAT(IMSPDFEE,'#,#0.00') AS IMSPDFEE2\n"
                + ",(\n"
                + "  SELECT (SELECT TOP 1 IMSPDCUR FROM IMSPDETAIL WHERE IMSPDPDNO = TMP.IMSPDPDNO\n"
                + "AND IMSPDVCODE = TMP.IMSPDVCODE)+' = '+FORMAT([IMSPVDFAMT],'#,#0.00')+' @ = '+FORMAT([IMSPVDRATE],'#,#0.000000')+' �ҷ'+'; '\n"
                + "  FROM [IMSPVDIS]\n"
                + "  WHERE [IMSPVDPDNO] = TMP.IMSPDPDNO\n"
                + "  ORDER BY [IMSPVDLINE]\n"
                + "  FOR XML PATH('')\n"
                + ") AS DISFAMTRATE\n"
                + ",(\n"
                + "  SELECT FORMAT([IMSPVDAMT],'#,#0.00')+'; '\n"
                + "  FROM [IMSPVDIS]\n"
                + "  WHERE [IMSPVDPDNO] = TMP.IMSPDPDNO\n"
                + "  ORDER BY [IMSPVDLINE]\n"
                + "  FOR XML PATH('')\n"
                + ") + FORMAT(IMSPDFEE,'#,#0.00') AS DISAMT\n"
                + ",(\n"
                + "  SELECT COUNT(*)\n"
                + "  FROM [IMSPVDIS]\n"
                + "  WHERE [IMSPVDPDNO] = TMP.IMSPDPDNO\n"
                + ") AS DISCNT\n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSPDPDNO, IMSPDVCODE, IMSPDFEE\n"
                + "FROM IMSPDETAIL\n"
                + "WHERE IMSPDPDNO = '" + impPaidNo + "'\n"
                + "AND IMSPDSTS = 'P'\n"
                + ")TMP\n"
                + "LEFT JOIN IMSVEN ON IMSVEN.IMSVDCODE = TMP.IMSPDVCODE\n"
                + "LEFT JOIN IMSPHEAD ON IMSPHEAD.IMSPHPDNO = TMP.IMSPDPDNO";
        ResultSet result = null;
        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            result = ps.executeQuery();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public ResultSet reportIMS300PV(String impPaidNo) {

        String sql = "SELECT TMP.IMSPDPDNO, TMP.IMSPDVCODE, IMSVDNAME\n"
                + ",(SELECT TOP 1 IMSPDCUR FROM IMSPDETAIL WHERE IMSPDPDNO = TMP.IMSPDPDNO \n"
                + "AND IMSPDVCODE = TMP.IMSPDVCODE) AS CUR\n"
                + ",FORMAT((SELECT SUM(IMSPDFINVAMT) FROM IMSPDETAIL WHERE IMSPDPDNO = TMP.IMSPDPDNO \n"
                + "AND IMSPDVCODE = TMP.IMSPDVCODE), '#,#0.00') AS FAMT\n"
                + ",FORMAT((SELECT TOP 1 IMSPDEXRATE FROM IMSPDETAIL WHERE IMSPDPDNO = TMP.IMSPDPDNO \n"
                + "AND IMSPDVCODE = TMP.IMSPDVCODE), '#,#0.000000') AS RATE\n"
                + ",(SELECT SUM(IMSPDPDAMT) FROM IMSPDETAIL WHERE IMSPDPDNO = TMP.IMSPDPDNO \n"
                + "AND IMSPDVCODE = TMP.IMSPDVCODE) AS TAMT\n"
                + ",(SELECT TOP 1 IMSPDINVNO FROM IMSPDETAIL WHERE IMSPDPDNO = TMP.IMSPDPDNO \n"
                + "AND IMSPDVCODE = TMP.IMSPDVCODE) AS INV_NO\n"
                + ",(SELECT TOP 1 IMSPDPROD FROM IMSPDETAIL WHERE IMSPDPDNO = TMP.IMSPDPDNO \n"
                + "AND IMSPDVCODE = TMP.IMSPDVCODE) AS PROD\n"
                + ",[dbo].[NUM2THAI](\n"
                + "(SELECT SUM(IMSPDPDAMT) FROM IMSPDETAIL WHERE IMSPDPDNO = TMP.IMSPDPDNO \n"
                + "AND IMSPDVCODE = TMP.IMSPDVCODE) + IMSPDFEE\n"
                + ") AS TOTAL_THAI\n"
                + ",IMSPDFEE "
                + ",FORMAT(IMSPDFEE,'#,#0.00') AS IMSPDFEE2 "
                + "FROM(\n"
                + "SELECT DISTINCT IMSPDPDNO, IMSPDVCODE, IMSPDFEE\n"
                + "FROM IMSPDETAIL\n"
                + "WHERE IMSPDPDNO = '" + impPaidNo + "'\n"
                + "AND IMSPDSTS = 'P'\n"
                + ")TMP\n"
                + "LEFT JOIN IMSVEN ON IMSVEN.IMSVDCODE = TMP.IMSPDVCODE "
                + "LEFT JOIN IMSPHEAD ON IMSPHEAD.IMSPHPDNO = TMP.IMSPDPDNO ";
        ResultSet result = null;
        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            result = ps.executeQuery();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public ResultSet reportIMS300JV(String impPaidNo) {

        String sql = "SELECT TMP.IMSPDPDNO, TMP.IMSPDVCODE, IMSVDNAME\n"
                + ",(SELECT TOP 1 IMSPDCUR FROM IMSPDETAIL WHERE IMSPDPDNO = TMP.IMSPDPDNO \n"
                + "AND IMSPDVCODE = TMP.IMSPDVCODE) AS CUR\n"
                + ",FORMAT((SELECT SUM(IMSPDFINVAMT) FROM IMSPDETAIL WHERE IMSPDPDNO = TMP.IMSPDPDNO \n"
                + "AND IMSPDVCODE = TMP.IMSPDVCODE), '#,#0.00') AS FAMT\n"
                + ",FORMAT((SELECT TOP 1 IMSPDEXRATE FROM IMSPDETAIL WHERE IMSPDPDNO = TMP.IMSPDPDNO \n"
                + "AND IMSPDVCODE = TMP.IMSPDVCODE), '#,#0.000000') AS RATE\n"
                + ",(SELECT SUM(IMSPDPDAMT) FROM IMSPDETAIL WHERE IMSPDPDNO = TMP.IMSPDPDNO \n"
                + "AND IMSPDVCODE = TMP.IMSPDVCODE) AS TAMT\n"
                + ",(SELECT TOP 1 IMSPDINVNO FROM IMSPDETAIL WHERE IMSPDPDNO = TMP.IMSPDPDNO \n"
                + "AND IMSPDVCODE = TMP.IMSPDVCODE) AS INV_NO\n"
                + ",(SELECT TOP 1 IMSPDPROD FROM IMSPDETAIL WHERE IMSPDPDNO = TMP.IMSPDPDNO \n"
                + "AND IMSPDVCODE = TMP.IMSPDVCODE) AS PROD\n"
                + ",[dbo].[NUM2THAI](\n"
                + "(SELECT SUM(IMSPDPDAMT) FROM IMSPDETAIL WHERE IMSPDPDNO = TMP.IMSPDPDNO \n"
                + "AND IMSPDVCODE = TMP.IMSPDVCODE)\n"
                + ") AS TOTAL_THAI\n"
                + ",IMSPDFEE "
                + ",FORMAT(IMSPDFEE,'#,#0.00') AS IMSPDFEE2 "
                + ",format([IMSPHPDDT],'dd-MM-yyyy') as [IMSPHPDDT] "
                + "FROM(\n"
                + "SELECT DISTINCT IMSPDPDNO, IMSPDVCODE, IMSPDFEE\n"
                + "FROM IMSPDETAIL\n"
                + "WHERE IMSPDPDNO = '" + impPaidNo + "'\n"
                + "AND IMSPDSTS = 'P'\n"
                + ")TMP\n"
                + "LEFT JOIN IMSVEN ON IMSVEN.IMSVDCODE = TMP.IMSPDVCODE "
                + "LEFT JOIN IMSPHEAD ON IMSPHEAD.IMSPHPDNO = TMP.IMSPDPDNO ";
        ResultSet result = null;
        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            result = ps.executeQuery();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public ResultSet reportIMS300SP(String impPaidNo) {

        String sql = "SELECT IMSPDPDNO, (PAID_BY +' : '+ IMSIMNAME) AS PAID_BY\n"
                + ",(IMSPHBCODE+' : '+(SELECT TOP 1 IMSBADESC FROM IMSBMAS WHERE IMSBACODE = IMSPHBCODE)) AS BANK_H\n"
                + ",(SELECT TOP 1 IMCBACNO FROM IMSICB WHERE IMCBCODE = PAID_BY AND IMCBBCODE = IMSPHBCODE) AS BANK_HACNO\n"
                + ",FORMAT(IMSPHESTAMT, '#,#0.00') AS IMSPHESTAMT ,FORMAT(IMSPHRMAMT, '#,#0.00') AS IMSPHRMAMT\n"
                + ",(IMSPDVCODE+' : '+IMSVDNAME) AS IMSPDVCODE\n"
                + ",(IMSVDBCODE+' : '+(SELECT TOP 1 IMSBADESC FROM IMSBMAS WHERE IMSBACODE = IMSVDBCODE)) AS BANK_D\n"
                + ",IMSVDSWIFT, IMSVDBACNO, FORMAT(IMSPDEXRATE, '#,#0.0000') AS IMSPDEXRATE, FORMAT(IMSPDPDDT, 'dd-MM-yy') AS IMSPDPDDT\n"
                + ",IMSPDINVNO, IMSPDFPDAMT, IMSPDCUR, IMSPDPDAMT, 'THB' AS THCUR\n"
                + ",IMSPDFEE\n"
                + ",FORMAT(IMSPDFEE,'#,#0.00') AS IMSPDFEE2\n"
                + ",(\n"
                + "SELECT SUM(IMSPDFEE) \n"
                + "FROM(\n"
                + "SELECT DISTINCT IMSPDVCODE, IMSPDFEE\n"
                + "FROM IMSPDETAIL\n"
                + "WHERE IMSPDPDNO = TMP.IMSPDPDNO\n"
                + ")TT\n"
                + ") AS SUM_FEE\n"
                + "FROM(\n"
                + "SELECT IMSPDPDNO\n"
                + ",(SELECT TOP 1 IMSPHIMC FROM IMSPHEAD WHERE IMSPHPDNO = IMSPDPDNO) AS PAID_BY\n"
                + ",IMSPDVCODE, IMSPDEXRATE, IMSPDPDDT, IMSPDINVNO, IMSPDFPDAMT, IMSPDCUR, IMSPDPDAMT, IMSPDFEE\n"
                + "FROM IMSPDETAIL\n"
                + "WHERE IMSPDPDNO = '" + impPaidNo + "'\n"
                + "AND IMSPDSTS = 'P'\n"
                + ")TMP\n"
                + "LEFT JOIN IMSIMC ON IMSIMC.IMSIMCODE = TMP.PAID_BY\n"
                + "LEFT JOIN IMSPHEAD ON IMSPHEAD.IMSPHPDNO = TMP.IMSPDPDNO\n"
                + "LEFT JOIN IMSVEN ON IMSVEN.IMSVDCODE = TMP.IMSPDVCODE\n"
                + "ORDER BY IMSPDVCODE";
        ResultSet result = null;
        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            result = ps.executeQuery();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public int findNextLine(String code) {

        int max = 1;

        String sql = "SELECT MAX([IMSPDLINE]) + 1 AS MAX\n"
                + "FROM [IMSPDETAIL] "
                + "WHERE [IMSPDPDNO] = '" + code + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                String maxSt = result.getString("MAX");
                if (maxSt != null) {
                    max = Integer.parseInt(result.getString("MAX"));
                }

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return max;

    }

    public boolean add(String detail) {

        boolean result = false;

        String sql = "INSERT INTO IMSPDETAIL "
                + "([IMSPDCOM]\n"
                + "      ,[IMSPDIMNO]\n"
                + "      ,[IMSPDPDNO]\n"
                + "      ,[IMSPDLINE]\n"
                + "      ,[IMSPDINVNO]\n"
                + "      ,[IMSPDVCODE]\n"
                + "      ,[IMSPDCUR]\n"
                + "      ,[IMSPDPROD]\n"
                + "      ,[IMSPDINVAMT]\n"
                + "      ,[IMSPDFINVAMT]\n"
                + "      ,[IMSPDPDAMT]\n"
                + "      ,[IMSPDFPDAMT]\n"
                + "      ,[IMSPDEXRATE]\n"
                + "      ,[IMSPDBCODE]\n"
                + "      ,[IMSPDPDDT]\n"
                + "      ,[IMSPDSTS]\n"
                + "      ,[IMSPDEDT]\n"
                + "      ,[IMSPDCDT]\n"
                + "      ,[IMSPDUSER],IMSPDFEE) "
                + " VALUES(" + detail + ")";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public List<IMSPDETAIL> findForAdd(String code, String vcode) {

        List<IMSPDETAIL> UAList = new ArrayList<IMSPDETAIL>();

        String VCODE = "";
        if (!vcode.equals("")) {
            VCODE = "  AND IMSPDVCODE = '" + vcode + "' ";
        }

        String sql = "SELECT [IMSPDCOM]\n"
                + "      ,[IMSPDIMNO]\n"
                + "      ,[IMSPDPDNO]\n"
                + "      ,[IMSPDLINE]\n"
                + "      ,[IMSPDINVNO]\n"
                + "      ,[IMSPDVCODE]\n"
                + "      ,[IMSPDCUR]\n"
                + "      ,[IMSPDPROD]\n"
                + "      ,[IMSPDINVAMT]\n"
                + "      ,[IMSPDFINVAMT]\n"
                + "      ,[IMSPDPDAMT]\n"
                + "      ,[IMSPDFPDAMT]\n"
                + "      ,[IMSPDEXRATE]\n"
                + "      ,[IMSPDBCODE]\n"
                + "      ,[IMSPDPDDT]\n"
                + "      ,[IMSPDFEE]\n"
                + "      ,[IMSPDSTS]\n"
                + "      ,[IMSPDEDT]\n"
                + "      ,[IMSPDCDT]\n"
                + "      ,[IMSPDUSER]\n"
                + "  FROM [IMSPDETAIL]\n"
                + "  WHERE [IMSPDPDNO] = '" + code + "' "
                + VCODE
                + "  AND IMSPDSTS = 'S' "
                + "  ORDER BY IMSPDIMNO, [IMSPDLINE]";
//        System.out.println(sql);
//        System.out.println("-------------------------------------");

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSPDETAIL p = new IMSPDETAIL();

                p.setImportNo(result.getString("IMSPDIMNO"));
                p.setImp(result.getString("IMSPDPDNO"));
                p.setLine(result.getString("IMSPDLINE"));
                p.setInvoiceNo(result.getString("IMSPDINVNO"));
                p.setVendor(result.getString("IMSPDVCODE"));
                p.setCcy(result.getString("IMSPDCUR"));
                p.setProduct(result.getString("IMSPDPROD"));
                p.setInvAmount(result.getString("IMSPDINVAMT"));
                p.setForeignInvAmount(result.getString("IMSPDFINVAMT"));
                p.setBank(result.getString("IMSPDBCODE"));
                p.setPaidDate(result.getString("IMSPDPDDT"));
                p.setFee(result.getString("IMSPDFEE"));
                p.setSts(result.getString("IMSPDSTS"));
                p.setEdt(result.getString("IMSPDEDT"));
                p.setCdt(result.getString("IMSPDCDT"));
                p.setUser(result.getString("IMSPDUSER"));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<IMSPDETAIL> findByCodePaid(String code, String vcode) {

        List<IMSPDETAIL> UAList = new ArrayList<IMSPDETAIL>();

        String VCODE = "";
        if (!vcode.equals("")) {
            VCODE = "  AND IMSPDVCODE = '" + vcode + "' ";
        }

        String sql = "SELECT [IMSPDIMNO]\n"
                + "      ,[IMSPDPDNO]\n"
                + "      ,[IMSPDLINE]\n"
                + "      ,[IMSPDINVNO]\n"
                + "      ,[IMSPDVCODE]\n"
                + "      ,[IMSPDCUR]\n"
                + "      ,[IMSPDPROD]\n"
                + "      ,[IMSPDINVAMT]\n"
                + "      ,[IMSPDFINVAMT]\n"
                + "      ,[IMSPDPDAMT]\n"
                + "      ,[IMSPDFPDAMT]\n"
                + "      ,[IMSPDBCODE]\n"
                + "      ,[IMSPDPDDT]\n"
                + "      ,[IMSPDSTS]\n"
                + "      ,(SELECT SUM(IMSPDPDAMT) FROM IMSPDETAIL WHERE IMSPDPDNO = '" + code + "') AS SUM_INV_AMT\n"
                + "	 ,(SELECT SUM(IMSPDFPDAMT) FROM IMSPDETAIL WHERE IMSPDPDNO = '" + code + "') AS SUM_FOR_INV_AMT\n"
                + "  FROM [IMSPDETAIL]\n"
                + "  WHERE [IMSPDPDNO] = '" + code + "' "
                + VCODE
                + "  AND IMSPDSTS = 'S' "
                + "  ORDER BY IMSPDIMNO, [IMSPDLINE]";
//        System.out.println(sql);
//        System.out.println("-------------------------------------");

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String impNo = "";

            while (result.next()) {

                IMSPDETAIL p = new IMSPDETAIL();

                if (impNo.equals("")) {
                    p.setImportNo("<b>" + result.getString("IMSPDIMNO") + "</b>");
                    impNo = result.getString("IMSPDIMNO");
                } else {
                    if (!impNo.equals(result.getString("IMSPDIMNO"))) {
                        p.setImportNo("<b>" + result.getString("IMSPDIMNO") + "</b>");
                        impNo = result.getString("IMSPDIMNO");
                    } else {
                        p.setImportNo("<b style=\"opacity: 0;\">" + result.getString("IMSPDIMNO") + "</b>");
                        impNo = result.getString("IMSPDIMNO");
                    }
                }

                p.setBank(result.getString("IMSPDBCODE"));
                p.setVendor(result.getString("IMSPDVCODE"));

                if (result.getString("IMSPDPDDT") != null) {
                    p.setPaidDate(result.getString("IMSPDPDDT").split(" ")[0]);
                }

                p.setImp(result.getString("IMSPDIMNO"));
                p.setLine(result.getString("IMSPDLINE"));

                p.setInvoiceNo(result.getString("IMSPDINVNO"));
                p.setCcy(result.getString("IMSPDCUR"));
                p.setProduct(result.getString("IMSPDPROD"));

                if (result.getString("IMSPDSTS").equals("S")) {
                    p.setSts(result.getString("IMSPDSTS") + " : Selected");
                } else if (result.getString("IMSPDSTS").equals("D")) {
                    p.setSts(result.getString("IMSPDSTS") + " : Deleted");
                } else if (result.getString("IMSPDSTS").equals("P")) {
                    p.setSts(result.getString("IMSPDSTS") + " : Paid");
                } else {
                    p.setSts(result.getString("IMSPDSTS"));
                }

                p.setInvAmount(formatDou.format(Double.parseDouble(result.getString("IMSPDINVAMT"))));
                p.setForeignInvAmount(formatDou.format(Double.parseDouble(result.getString("IMSPDFINVAMT"))));

                if (result.getString("IMSPDPDAMT") != null) {
                    p.setPaidInvAmount(formatDou.format(Double.parseDouble(result.getString("IMSPDPDAMT"))));
                }
                if (result.getString("IMSPDFPDAMT") != null) {
                    p.setPaidForeignInvAmount(formatDou.format(Double.parseDouble(result.getString("IMSPDFPDAMT"))));
                }

                if (result.getString("SUM_INV_AMT") != null) {
                    p.setSumPaidInvAmount(formatDou.format(Double.parseDouble(result.getString("SUM_INV_AMT"))));
                }
                if (result.getString("SUM_FOR_INV_AMT") != null) {
                    p.setSumPaidForeignInvAmount(formatDou.format(Double.parseDouble(result.getString("SUM_FOR_INV_AMT"))));
                }
                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<IMSPDETAIL> findByCode(String code) {

        List<IMSPDETAIL> UAList = new ArrayList<IMSPDETAIL>();

        String sql = "SELECT [IMSPDIMNO]\n"
                + "      ,[IMSPDPDNO]\n"
                + "      ,[IMSPDLINE]\n"
                + "      ,[IMSPDINVNO]\n"
                + "      ,[IMSPDVCODE]\n"
                + "      ,[IMSPDCUR]\n"
                + "      ,[IMSPDPROD]\n"
                + "      ,[IMSPDINVAMT]\n"
                + "      ,[IMSPDFINVAMT]\n"
                + "      ,[IMSPDPDAMT]\n"
                + "      ,[IMSPDFPDAMT]\n"
                + "      ,[IMSPDBCODE]\n"
                + "      ,[IMSPDPDDT]\n"
                + "      ,[IMSPDSTS]\n"
                + "      ,(SELECT SUM(IMSPDPDAMT) FROM IMSPDETAIL WHERE IMSPDPDNO = '" + code + "') AS SUM_INV_AMT\n"
                + "	 ,(SELECT SUM(IMSPDFPDAMT) FROM IMSPDETAIL WHERE IMSPDPDNO = '" + code + "') AS SUM_FOR_INV_AMT\n"
                + "  FROM [IMSPDETAIL]\n"
                + "  WHERE [IMSPDPDNO] = '" + code + "' "
                + "  ORDER BY IMSPDIMNO, [IMSPDLINE]";
//        System.out.println(sql);
//        System.out.println("-------------------------------------");

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String impNo = "";

            while (result.next()) {

                IMSPDETAIL p = new IMSPDETAIL();

                if (impNo.equals("")) {
                    p.setImportNo("<b>" + result.getString("IMSPDIMNO") + "</b>");
                    impNo = result.getString("IMSPDIMNO");
                } else {
                    if (!impNo.equals(result.getString("IMSPDIMNO"))) {
                        p.setImportNo("<b>" + result.getString("IMSPDIMNO") + "</b>");
                        impNo = result.getString("IMSPDIMNO");
                    } else {
                        p.setImportNo("<b style=\"opacity: 0;\">" + result.getString("IMSPDIMNO") + "</b>");
                        impNo = result.getString("IMSPDIMNO");
                    }
                }

                p.setBank(result.getString("IMSPDBCODE"));
                p.setVendor(result.getString("IMSPDVCODE"));

                if (result.getString("IMSPDPDDT") != null) {
                    p.setPaidDate(result.getString("IMSPDPDDT").split(" ")[0]);
                }

                p.setImp(result.getString("IMSPDIMNO"));

                p.setInvoiceNo(result.getString("IMSPDINVNO"));
                p.setCcy(result.getString("IMSPDCUR"));
                p.setProduct(result.getString("IMSPDPROD"));

                if (result.getString("IMSPDSTS").equals("S")) {
                    p.setSts(result.getString("IMSPDSTS") + " : Selected");
                } else if (result.getString("IMSPDSTS").equals("D")) {
                    p.setSts(result.getString("IMSPDSTS") + " : Deleted");
                } else if (result.getString("IMSPDSTS").equals("P")) {
                    p.setSts(result.getString("IMSPDSTS") + " : Paid");
                } else {
                    p.setSts(result.getString("IMSPDSTS"));
                }

                p.setInvAmount(formatDou.format(Double.parseDouble(result.getString("IMSPDINVAMT"))));
                p.setForeignInvAmount(formatDou.format(Double.parseDouble(result.getString("IMSPDFINVAMT"))));

                if (result.getString("IMSPDPDAMT") != null) {
                    p.setPaidInvAmount(formatDou.format(Double.parseDouble(result.getString("IMSPDPDAMT"))));
                }
                if (result.getString("IMSPDFPDAMT") != null) {
                    p.setPaidForeignInvAmount(formatDou.format(Double.parseDouble(result.getString("IMSPDFPDAMT"))));
                }

                if (result.getString("SUM_INV_AMT") != null) {
                    p.setSumPaidInvAmount(formatDou.format(Double.parseDouble(result.getString("SUM_INV_AMT"))));
                }
                if (result.getString("SUM_FOR_INV_AMT") != null) {
                    p.setSumPaidForeignInvAmount(formatDou.format(Double.parseDouble(result.getString("SUM_FOR_INV_AMT"))));
                }
                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public List<IMSPDETAIL> findByCode2(String code, String vcode) {

        List<IMSPDETAIL> UAList = new ArrayList<IMSPDETAIL>();

        String VCODE = "";
        if (!vcode.equals("")) {
            VCODE = "  AND IMSPDVCODE = '" + vcode + "' ";
        }

        String sql = "SELECT [IMSPDIMNO]\n"
                + "      ,[IMSPDPDNO]\n"
                + "      ,[IMSPDLINE]\n"
                + "      ,[IMSPDINVNO]\n"
                + "      ,[IMSPDVCODE]\n"
                + "      ,[IMSPDCUR]\n"
                + "      ,[IMSPDPROD]\n"
                + "      ,[IMSPDINVAMT]\n"
                + "      ,[IMSPDFINVAMT]\n"
                + "      ,[IMSPDPDAMT]\n"
                + "      ,[IMSPDFPDAMT]\n"
                + "      ,[IMSPDBCODE]\n"
                + "      ,[IMSPDPDDT]\n"
                + "      ,[IMSPDSTS]\n"
                + "      ,(SELECT SUM(IMSPDPDAMT) FROM IMSPDETAIL WHERE IMSPDPDNO = '" + code + "') AS SUM_INV_AMT\n"
                + "	 ,(SELECT SUM(IMSPDFPDAMT) FROM IMSPDETAIL WHERE IMSPDPDNO = '" + code + "') AS SUM_FOR_INV_AMT\n"
                + "  FROM [IMSPDETAIL]\n"
                + "  WHERE [IMSPDPDNO] = '" + code + "' "
                + VCODE
                + "  ORDER BY IMSPDIMNO, [IMSPDLINE]";
//        System.out.println(sql);
//        System.out.println("-------------------------------------");

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String impNo = "";

            while (result.next()) {

                IMSPDETAIL p = new IMSPDETAIL();

                if (impNo.equals("")) {
                    p.setImportNo("<b>" + result.getString("IMSPDIMNO") + "</b>");
                    impNo = result.getString("IMSPDIMNO");
                } else {
                    if (!impNo.equals(result.getString("IMSPDIMNO"))) {
                        p.setImportNo("<b>" + result.getString("IMSPDIMNO") + "</b>");
                        impNo = result.getString("IMSPDIMNO");
                    } else {
                        p.setImportNo("<b style=\"opacity: 0;\">" + result.getString("IMSPDIMNO") + "</b>");
                        impNo = result.getString("IMSPDIMNO");
                    }
                }

                p.setLine(result.getString("IMSPDIMNO") + "-" + result.getString("IMSPDPDNO") + "-" + result.getString("IMSPDLINE"));

                p.setBank(result.getString("IMSPDBCODE"));
                p.setVendor(result.getString("IMSPDVCODE"));

                if (result.getString("IMSPDPDDT") != null) {
                    p.setPaidDate(result.getString("IMSPDPDDT").split(" ")[0]);
                }

                p.setImp(result.getString("IMSPDIMNO"));

                p.setInvoiceNo(result.getString("IMSPDINVNO"));
                p.setCcy(result.getString("IMSPDCUR"));
                p.setProduct(result.getString("IMSPDPROD"));

                if (result.getString("IMSPDSTS").equals("S")) {
                    p.setSts(result.getString("IMSPDSTS") + " : Selected");
                } else if (result.getString("IMSPDSTS").equals("D")) {
                    p.setSts(result.getString("IMSPDSTS") + " : Deleted");
                } else if (result.getString("IMSPDSTS").equals("P")) {
                    p.setSts(result.getString("IMSPDSTS") + " : Paid");
                } else {
                    p.setSts(result.getString("IMSPDSTS"));
                }

                p.setInvAmount(formatDou.format(Double.parseDouble(result.getString("IMSPDINVAMT"))));
                p.setForeignInvAmount(formatDou.format(Double.parseDouble(result.getString("IMSPDFINVAMT"))));

                if (result.getString("IMSPDPDAMT") != null) {
                    p.setPaidInvAmount(formatDou.format(Double.parseDouble(result.getString("IMSPDPDAMT"))));
                }
                if (result.getString("IMSPDFPDAMT") != null) {
                    p.setPaidForeignInvAmount(formatDou.format(Double.parseDouble(result.getString("IMSPDFPDAMT"))));
                }

                if (result.getString("SUM_INV_AMT") != null) {
                    p.setSumPaidInvAmount(formatDou.format(Double.parseDouble(result.getString("SUM_INV_AMT"))));
                }
                if (result.getString("SUM_FOR_INV_AMT") != null) {
                    p.setSumPaidForeignInvAmount(formatDou.format(Double.parseDouble(result.getString("SUM_FOR_INV_AMT"))));
                }
                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public boolean delete(String importPaidNo, String ven) {

        boolean result = false;

        String sql = "DELETE FROM IMSPDETAIL WHERE IMSPDPDNO = '" + importPaidNo + "' AND IMSPDVCODE = '" + ven + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean delete2(String importPaidNo) {

        boolean result = false;

        String sql = "delete [RMShipment].[dbo].[IMSPDETAIL]\n"
                + "  where [IMSPDPDNO]='" + importPaidNo + "' ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

}
