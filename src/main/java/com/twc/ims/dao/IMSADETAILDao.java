/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.dao;

import com.twc.ims.database.database;
import com.twc.ims.entity.IMSAHEAD;
import com.twc.ims.entity.IMSAPDETAIL;
import com.twc.ims.entity.IMSSDETAIL;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSADETAILDao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public String findIMC(String code) {

        String max = "";

        String sql = "SELECT TOP 1 [IMSADAVNO]\n"
                + "      ,[IMSADPIMNO]\n"
                + "	  ,[IMSHIMC]\n"
                + "  FROM [RMShipment].[dbo].[IMSADETAIL]\n"
                + "  LEFT JOIN [IMSSHEAD] ON [IMSSHEAD].[IMSHPIMNO] = [IMSADPIMNO]\n"
                + "  WHERE [IMSADAVNO] = '" + code + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                String maxSt = result.getString("IMSHIMC");
                if (maxSt != null) {
                    max = result.getString("IMSHIMC");
                }

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return max;

    }

    public List<IMSAPDETAIL> findByCQNO(String checkNo, String line) {

        List<IMSAPDETAIL> UAList = new ArrayList<IMSAPDETAIL>();

        String sql = "SELECT IMSAHCQNO, IMSADAVNO, IMSADLINE, IMSADPIMNO, IMSADINVNO\n"
                + "FROM IMSADETAIL\n"
                + "LEFT JOIN IMSAHEAD ON IMSAHEAD.IMSAHAVNO = IMSADETAIL.IMSADAVNO\n"
                + "WHERE IMSAHCQNO='" + checkNo + "' AND IMSADLINE = '" + line + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSAPDETAIL p = new IMSAPDETAIL();

                p.setCheckNo(result.getString("IMSAHCQNO"));
                p.setNo(result.getString("IMSADAVNO"));
                p.setPimno(result.getString("IMSADPIMNO"));
                p.setInvNoList(result.getString("IMSADINVNO"));

                UAList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }
        return UAList;

    }

    public List<IMSAPDETAIL> findAll(int line, String checkNo, String shipcom, String[] id, String[] invList) {

        List<IMSAPDETAIL> UAList = new ArrayList<IMSAPDETAIL>();

        DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

        String IDD = "";
        String idt = "";
        String INVD = "";
        String invt = "";

        if (id != null) {
            for (int i = 0; i < id.length; i++) {
                if (i == 0) {
                    idt += "'" + id[i] + "'";
                } else {
                    idt += ",'" + id[i] + "'";
                }
            }

            IDD = "and CQLINE not in (" + idt + ")\n";
        }

        if (invList != null) {
            for (int i = 0; i < invList.length; i++) {
                if (i == 0) {
                    invt += "'" + invList[i] + "'";
                } else {
                    invt += ",'" + invList[i] + "'";
                }
            }

            INVD = "and IMSADINVNO not in (" + invt + ")\n";
        }

        String sql = "select *\n"
                + "from(\n"
                + "SELECT IMSAHCQNO+'+'+cast(IMSADLINE as nvarchar) AS CQLINE, IMSADAVNO, IMSADPIMNO, (select [IMSIMCODE]+' : '+[IMSIMNAME] from [IMSIMC] where [IMSIMCODE] = [IMSHIMC]) as[IMSHIMC], IMSAHSHCD, IMSADLINE, IMSAHCQNO, IMSAHBCODE, IMSAHCQDT, IMSAHINVAMT,\n"
                + "       IMSADADVAMT, IMSHDBNO, IMSHDBDATE, IMSADINVNO, ISNULL(IMSADINVAMT,0) AS IMSADINVAMT, ISNULL(IMSCLTAX3,0) AS IMSCLTAX3, ISNULL(IMSCLTAX1,0) AS IMSCLTAX1,\n"
                + "	   (ISNULL(IMSADINVAMT,0) - ISNULL(IMSCLTAX3,0) - ISNULL(IMSCLTAX1,0)) AS AMOUNT, IMSHAPNO\n"
                + "FROM(\n"
                + "SELECT IMSADAVNO, IMSADPIMNO, IMSADLINE,\n"
                + "       (SELECT TOP 1 IMSAHSHCD FROM IMSAHEAD WHERE IMSAHAVNO = IMSADAVNO) AS IMSAHSHCD,\n"
                + "	   (SELECT TOP 1 IMSAHCQNO FROM IMSAHEAD WHERE IMSAHAVNO = IMSADAVNO) AS IMSAHCQNO,\n"
                + "	   (SELECT TOP 1 IMSAHBCODE FROM IMSAHEAD WHERE IMSAHAVNO = IMSADAVNO) AS IMSAHBCODE,\n"
                + "	   (SELECT TOP 1 IMSAHCQDT FROM IMSAHEAD WHERE IMSAHAVNO = IMSADAVNO) AS IMSAHCQDT,\n"
                + "	   (SELECT TOP 1 IMSAHINVAMT FROM IMSAHEAD WHERE IMSAHAVNO = IMSADAVNO) AS IMSAHINVAMT,\n"
                + "	   IMSADADVAMT,\n"
                + "	   IMSADINVNO, \n"
                + "	   (SELECT SUM(IMSCABLAMT) FROM IMSCAC WHERE IMSCAPIMNO = IMSADPIMNO) AS IMSADINVAMT\n"
                + "FROM IMSADETAIL\n"
                + ")TMP\n"
                + "LEFT JOIN IMSSHEAD ON IMSSHEAD.IMSHPIMNO = TMP.IMSADPIMNO\n"
                + "LEFT JOIN IMSCLR ON IMSCLR.IMSCLPIMNO = TMP.IMSADPIMNO\n"
                + "WHERE IMSAHCQNO IN (" + checkNo + ")\n"
                + "AND IMSAHSHCD = '" + shipcom + "'\n"
                + "AND (IMSHAPNO IS NULL OR IMSHAPNO = '')\n"
                + ")TTT\n"
                + "left join [IMSSHEAD] on [IMSSHEAD].[IMSHPIMNO] = TTT.IMSADPIMNO\n"
                + "where 1 = 1 \n"
                + IDD
                + INVD
                + "ORDER BY IMSAHCQNO, IMSADLINE ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            String current_checkNo = "";
            double sumByCheckNo = 0;
            IMSAPDETAIL current_p = null;

            while (result.next()) {

                IMSAPDETAIL p = new IMSAPDETAIL();

                p.setPimno(result.getString("IMSADPIMNO"));
                p.setImcD(result.getString("IMSHIMC"));
                p.setNo(result.getString("IMSADLINE"));

                double tot = Double.parseDouble(result.getString("IMSAHINVAMT"));
                String tqt = formatDou.format(tot);

                String checkbox = "";

                if (!current_checkNo.equals(result.getString("IMSAHCQNO"))) {

                    if (current_p != null) {
                        String rem = current_p.getBalance();
                        current_p.setBalance(rem);
                        UAList.add(current_p);
                    }

                    p.setCheckNo(result.getString("IMSAHCQNO"));
                    p.setBank(result.getString("IMSAHBCODE"));

                    if (result.getString("IMSAHCQDT") != null) {
                        p.setCheckDate(result.getString("IMSAHCQDT").split(" ")[0]);
                    }

                    p.setAdvance(tqt);

                    p.setCheckNoD(result.getString("IMSAHCQNO"));
                    p.setBankD(result.getString("IMSAHBCODE"));

                    if (result.getString("IMSAHCQDT") != null) {
                        p.setCheckDateD(result.getString("IMSAHCQDT").split(" ")[0]);
                    }

                    p.setAdvanceD(tqt);

                    current_checkNo = result.getString("IMSAHCQNO");

                    sumByCheckNo = 0;
                } else {
                    p.setCheckNoD(result.getString("IMSAHCQNO"));
                    p.setBankD(result.getString("IMSAHBCODE"));

                    if (result.getString("IMSAHCQDT") != null) {
                        p.setCheckDateD(result.getString("IMSAHCQDT").split(" ")[0]);
                    }

                    p.setAdvanceD(tqt);

                    UAList.add(current_p);
                }

                checkbox = "<input style=\"width: 20px; height: 20px;\" type=\"checkbox\" name=\"selectCk\" value=\"" + result.getString("IMSAHCQNO") + "+" + result.getString("IMSADLINE") + "\">";
                p.setCheckBox(checkbox);

                double tot1 = Double.parseDouble(result.getString("IMSADADVAMT"));
                String tqt1 = formatDou.format(tot1);
                p.setEstAdv(tqt1);

                p.setDnNo(result.getString("IMSHDBNO"));

                if (result.getString("IMSHDBDATE") != null) {
                    p.setDnDate(result.getString("IMSHDBDATE").split(" ")[0]);
                }

                p.setInvNoList(result.getString("IMSADINVNO"));

                double tot2 = Double.parseDouble(result.getString("IMSADINVAMT"));
                String tqt2 = formatDou.format(tot2);
                p.setBillAmt(tqt2);

                double tot3 = Double.parseDouble(result.getString("IMSCLTAX3"));
                String tqt3 = formatDou.format(tot3);
                p.setTax3(tqt3);

                double tot4 = Double.parseDouble(result.getString("IMSCLTAX1"));
                String tqt4 = formatDou.format(tot4);
                p.setTax1(tqt4);

                double tot5 = Double.parseDouble(result.getString("AMOUNT"));
                String tqt5 = formatDou.format(tot5);
                p.setAmount(tqt5);

                sumByCheckNo += tot5;
                String tqt6 = formatDou.format(sumByCheckNo);
                p.setTotAdv(tqt6);

                String tqt7 = formatDou.format(tot - sumByCheckNo);
                p.setBalance(tqt7);

                current_p = p;
                line++;

            }

            if (current_p != null) {
                String rem = current_p.getBalance();
                current_p.setBalance(rem);
                UAList.add(current_p);
            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public ResultSet reportIMS110(String advImpNo) {

        String sql = "SELECT IMSADAVNO AS ADVNO,\n"
                + "	   IMSAHSHCD AS SHIPCOM,\n"
                + "	   IMSSCNAME AS SHIPCOM_NAME,\n"
                + "	   IMSAHCQDT AS CHECKDATE,\n"
                + "	   IMSADLINE AS LINE,\n"
                + "	   IMSADPIMNO AS PIMNO,\n"
                + "	   IMSHIMC, IMSIMNAME,\n"
                + "	   IMSADINVNO AS ALL_INV,\n"
                + "	   IMSADINVAMT AS SUM_INV_AMT,\n"
                + "	   IMSADDUTY AS IMP_DUTY,\n"
                + "	   IMSADADVAMT AS ADV_IMP,\n"
                + "	   IMSAHINVAMT AS TOTAL_AMOUNT,\n"
                + "	   [dbo].[NUM2THAI](IMSAHINVAMT) AS THAI_BAHT\n"
                + "FROM IMSADETAIL DE\n"
                + "LEFT JOIN IMSAHEAD HD ON DE.IMSADAVNO = HD.IMSAHAVNO\n"
                + "LEFT JOIN IMSSHC SP ON HD.IMSAHSHCD = SP.IMSSCCODE\n"
                + "LEFT JOIN IMSSHEAD ON IMSSHEAD.IMSHPIMNO = DE.IMSADPIMNO\n"
                + "LEFT JOIN IMSIMC ON IMSIMC.IMSIMCODE = IMSSHEAD.IMSHIMC\n"
                + "WHERE IMSADAVNO = '" + advImpNo + "'";

        ResultSet result = null;
        try {

            PreparedStatement ps = connect.prepareStatement(sql);
            result = ps.executeQuery();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public List<IMSSDETAIL> findByCode(String code) {

        List<IMSSDETAIL> UAList = new ArrayList<IMSSDETAIL>();

        String sql = "SELECT [IMSADAVNO]\n"
                + "      ,[IMSADLINE]\n"
                + "      ,[IMSADPIMNO]\n"
                + "	  ,(select [IMSIMCODE]+' : '+[IMSIMNAME] from [IMSIMC] where [IMSIMCODE] = [IMSHIMC]) as[IMSHIMC]\n"
                + "      ,[IMSADINVNO]\n"
                + "      ,[IMSADTRDT]\n"
                + "      ,[IMSADINVAMT]\n"
                + "      ,[IMSADDUTY]\n"
                + "      ,[IMSADADVAMT]\n"
                + "      ,[IMSADTAXAMT]\n"
                + "      ,[IMSADTAXBASE]\n"
                + "      ,[IMSADTAXVAT]\n"
                + "      ,[IMSADTAXTOT]\n"
                + "FROM [IMSADETAIL] \n"
                + "left join [IMSSHEAD] on [IMSSHEAD].[IMSHPIMNO] = [IMSADETAIL].[IMSADPIMNO]\n"
                + "WHERE [IMSADAVNO] = '" + code + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSSDETAIL p = new IMSSDETAIL();

                p.setAdvImpNo(result.getString("IMSADAVNO"));
                p.setNo(result.getString("IMSADLINE"));
                p.setpImpNo(result.getString("IMSADPIMNO"));
                p.setImcD(result.getString("IMSHIMC"));
                p.setInvNoList(result.getString("IMSADINVNO"));

                if (result.getString("IMSADTRDT") != null) {
                    p.setTransDate(result.getString("IMSADTRDT").split(" ")[0]);
                }

                double ia = Double.parseDouble(result.getString("IMSADINVAMT"));
                String iat = formatDou.format(ia);

                p.setInvAmt(iat);
                p.setTaxDutyP(result.getString("IMSADDUTY"));

                p.setAdvImpAmt(formatDou.format(Double.parseDouble(result.getString("IMSADADVAMT"))));
                p.setTaxDuty(formatDou.format(Double.parseDouble(result.getString("IMSADTAXAMT"))));
                p.setTaxBase(formatDou.format(Double.parseDouble(result.getString("IMSADTAXBASE"))));
                p.setVat7(formatDou.format(Double.parseDouble(result.getString("IMSADTAXVAT"))));
                p.setTotAmt(formatDou.format(Double.parseDouble(result.getString("IMSADTAXTOT"))));

                UAList.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return UAList;

    }

    public int findNextLine(String code) {

        int max = 1;

        String sql = "SELECT MAX([IMSADLINE]) + 1 AS MAX\n"
                + "FROM [IMSADETAIL] "
                + "WHERE [IMSADAVNO] = '" + code + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                String maxSt = result.getString("MAX");
                if (maxSt != null) {
                    max = Integer.parseInt(result.getString("MAX"));
                }

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return max;

    }

    public String check(String code, String line) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "Select * FROM [RMShipment].[dbo].[IMSADETAIL] where [IMSADAVNO] = '" + code + "' "
                    + "AND [IMSADLINE] = '" + line + "' ";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(String advImpNo, String no, String pImpNo, String invNoList,
            String transDate, String invAmtData, String taxDutyP, String advImpAmt, String taxDuty,
            String taxBase, String vat7, String totAmt, String userid) {

        boolean result = false;

        String sql = "INSERT INTO IMSADETAIL"
                + " ([IMSADCOM]\n"
                + "      ,[IMSADAVNO]\n"
                + "      ,[IMSADLINE]\n"
                + "      ,[IMSADPIMNO]\n"
                + "      ,[IMSADINVNO]\n"
                + "      ,[IMSADTRDT]\n"
                + "      ,[IMSADINVAMT]\n"
                + "      ,[IMSADDUTY]\n"
                + "      ,[IMSADADVAMT]\n"
                + "      ,[IMSADTAXAMT]\n"
                + "      ,[IMSADTAXBASE]\n"
                + "      ,[IMSADTAXVAT]\n"
                + "      ,[IMSADTAXTOT]\n"
                + "      ,[IMSADEDT]\n"
                + "      ,[IMSADCDT]\n"
                + "      ,[IMSADUSER])"
                + " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, ?)";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, "TWC");

            ps.setString(2, advImpNo);

            ps.setString(3, no);

            ps.setString(4, pImpNo);

            ps.setString(5, invNoList);

            ps.setString(6, transDate);

            ps.setString(7, invAmtData);

            ps.setString(8, taxDutyP);

            ps.setString(9, advImpAmt);

            ps.setString(10, taxDuty);

            ps.setString(11, taxBase);

            ps.setString(12, vat7);

            ps.setString(13, totAmt);

            ps.setString(14, userid);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String advImpNo, String no, String pImpNo, String invNoList,
            String transDate, String invAmtData, String taxDutyP, String advImpAmt, String taxDuty,
            String taxBase, String vat7, String totAmt, String userid) {

        boolean result = false;

        String sql = "UPDATE IMSADETAIL"
                + " SET [IMSADPIMNO] = ?\n"
                + "      ,[IMSADINVNO] = ?\n"
                + "      ,[IMSADTRDT] = ?\n"
                + "      ,[IMSADINVAMT] = ?\n"
                + "      ,[IMSADDUTY] = ?\n"
                + "      ,[IMSADADVAMT] = ?\n"
                + "      ,[IMSADTAXAMT] = ?\n"
                + "      ,[IMSADTAXBASE] = ?\n"
                + "      ,[IMSADTAXVAT] = ?\n"
                + "      ,[IMSADTAXTOT] = ?\n"
                + "      ,[IMSADEDT] = CURRENT_TIMESTAMP\n"
                + "      ,[IMSADUSER] = ?"
                + " WHERE [IMSADAVNO] = ? AND [IMSADLINE] = ? ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(12, advImpNo);

            ps.setString(13, no);

            ps.setString(1, pImpNo);

            ps.setString(2, invNoList);

            ps.setString(3, transDate);

            ps.setString(4, invAmtData);

            ps.setString(5, taxDutyP);

            ps.setString(6, advImpAmt);

            ps.setString(7, taxDuty);

            ps.setString(8, taxBase);

            ps.setString(9, vat7);

            ps.setString(10, totAmt);

            ps.setString(11, userid);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public String findInvNoList(String code, String line) {

        String max = "";

        String sql = "SELECT IMSADINVNO FROM IMSADETAIL WHERE IMSADAVNO = '" + code + "' AND IMSADLINE = '" + line + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                max = result.getString("IMSADINVNO");

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return max;

    }

    public String findPimNo(String code, String line) {

        String max = "";

        String sql = "SELECT IMSADPIMNO FROM IMSADETAIL WHERE IMSADAVNO = '" + code + "' AND IMSADLINE = '" + line + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                max = result.getString("IMSADPIMNO");

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return max;

    }

    public boolean delete(String code, String line) {

        boolean result = false;

        String sql = "DELETE FROM IMSADETAIL WHERE IMSADAVNO = '" + code + "' AND IMSADLINE = '" + line + "' ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }
        return result;

    }

}
