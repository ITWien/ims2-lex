/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.dao;

import com.twc.ims.database.database;
import com.twc.ims.entity.IMSSHEAD;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSFORMDao extends database {

    public String getThaiBaht(String number) {

        String tb = "";

        String sql = "select [dbo].[NUM2THAI]('" + number + "') as THAIBAHT";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                tb = result.getString("THAIBAHT");

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return tb;

    }

    public IMSSHEAD getImpDet(String impNo) {

        IMSSHEAD hd = new IMSSHEAD();

        String sql = "SELECT [IMSHPIMNO]\n"
                + "      ,[IMSHIMNO]\n"
                + "      ,[IMSHIMC]\n"
                + "      ,[IMSHPRV]\n"
                + "  FROM [IMSSHEAD]\n"
                + "  where (cast([IMSHPIMNO] as nvarchar)='" + impNo + "' or [IMSHIMNO]='" + impNo + "')";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                hd.setPimNo(result.getString("IMSHPIMNO"));
                hd.setImpNo(result.getString("IMSHIMNO"));
                hd.setImc(result.getString("IMSHIMC"));
                hd.setPrv(result.getString("IMSHPRV"));

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return hd;

    }

    public List<IMSSHEAD> getImpList(String impNo) {

        List<IMSSHEAD> hdList = new ArrayList<IMSSHEAD>();

        String sql = "SELECT top 30 [IMSHPIMNO]\n"
                + "      ,[IMSHIMNO]\n"
                + "  FROM [IMSSHEAD]\n"
                + "  where (cast([IMSHPIMNO] as nvarchar) like '%" + impNo + "%' or [IMSHIMNO] like '%" + impNo + "%')\n"
                + "  order by [IMSHPIMNO]";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSSHEAD hd = new IMSSHEAD();
                hd.setPimNo(result.getString("IMSHPIMNO"));
                hd.setImpNo(result.getString("IMSHIMNO"));
                hdList.add(hd);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return hdList;

    }
}
