/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.ims.dao;

import com.twc.ims.database.database;
import com.twc.ims.entity.IMSAPHEAD;
import com.twc.ims.entity.IMSRP700;
import com.twc.ims.entity.IMSSHC;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class IMSRP700Dao extends database {

    DecimalFormat formatDou = new DecimalFormat("#,###,###,##0.00");

    public String findTotImpAmt(String ym, String imc) {

        String TotImpAmt = "";

        String sql = "SELECT ISNULL(SUM([IMSRPBAHT]),0) as SUMTIAB\n"
                + "  FROM [RMShipment].[dbo].[IMSRP700]\n"
                + "  WHERE [IMSRPPERIOD] = '" + ym + "' AND [IMSRPIMC] = '" + imc + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                TotImpAmt = result.getString("SUMTIAB");

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return TotImpAmt;

    }

    public List<IMSRP700> findByCode(String ym, String imc) {

        List<IMSRP700> ul = new ArrayList<IMSRP700>();

        String sql = "SELECT [IMSRPPERIOD]\n"
                + "      ,[IMSRPCCY]\n"
                + "      ,[IMSRPMACHINE]\n"
                + "      ,[IMSRPOTHER]\n"
                + "      ,[IMSRPTOTAL]\n"
                + "      ,[IMSRPRATE]\n"
                + "      ,[IMSRPBAHT]\n"
                + "      ,[IMSRPBALCCY]\n"
                + "      ,[IMSRPTOTCCY]\n"
                + "      ,[IMSRPALLCCY]\n"
                + "      ,[IMSRPSUM]\n"
                + "      ,[IMSRPHSB]\n"
                + "      ,[IMSRPSCB]\n"
                + "      ,[IMSRPBBL]\n"
                + "      ,[IMSRPMIZ]\n"
                + "      ,[IMSRPKRT]\n"
                + "      ,[IMSRPTOTPAY]\n"
                + "      ,[IMSRPNOPAY]\n"
                + "  FROM [RMShipment].[dbo].[IMSRP700]\n"
                + "  WHERE [IMSRPPERIOD] = '" + ym + "' AND [IMSRPIMC] = '" + imc + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP700 p = new IMSRP700();

                p.setPeriod(result.getString("IMSRPPERIOD"));
                p.setCcy(result.getString("IMSRPCCY"));
                p.setMacImpAmt(result.getString("IMSRPMACHINE"));
                p.setMatImpAmt(result.getString("IMSRPOTHER"));
                p.setTotalImpAmt(result.getString("IMSRPTOTAL"));
                p.setRate(result.getString("IMSRPRATE"));
                p.setTotalImpAmtB(result.getString("IMSRPBAHT"));
                p.setBalAmtPrevMonth(result.getString("IMSRPBALCCY"));
                p.setAccInvAmt(result.getString("IMSRPTOTCCY"));
                p.setAllBankBalPayment(result.getString("IMSRPALLCCY"));
                p.setSumitomo(result.getString("IMSRPSUM"));
                p.setHsb(result.getString("IMSRPHSB"));
                p.setScb(result.getString("IMSRPSCB"));
                p.setBbl(result.getString("IMSRPBBL"));
                p.setMizuho(result.getString("IMSRPMIZ"));
                p.setK_tokyo(result.getString("IMSRPKRT"));
                p.setTotal(result.getString("IMSRPTOTPAY"));
                p.setNoPaymentAmt(result.getString("IMSRPNOPAY"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP700> findByYear(String year, String imc) {

        List<IMSRP700> ul = new ArrayList<IMSRP700>();

        String sql = "SELECT [IMSRPPERIOD]\n"
                + "      ,[IMSRPCCY]\n"
                + "      ,FORMAT([IMSRPMACHINE],'#,#0.00') AS [IMSRPMACHINE]\n"
                + "      ,FORMAT([IMSRPOTHER],'#,#0.00') AS [IMSRPOTHER]\n"
                + "      ,FORMAT([IMSRPTOTAL],'#,#0.00') AS [IMSRPTOTAL]\n"
                + "      ,FORMAT([IMSRPRATE],'#,#0.000000') AS [IMSRPRATE]\n"
                + "      ,FORMAT([IMSRPBAHT],'#,#0.00') AS [IMSRPBAHT]\n"
                + "      ,FORMAT([IMSRPBALCCY],'#,#0.00') AS [IMSRPBALCCY]\n"
                + "      ,FORMAT([IMSRPTOTCCY],'#,#0.00') AS [IMSRPTOTCCY]\n"
                + "      ,FORMAT([IMSRPALLCCY],'#,#0.00') AS [IMSRPALLCCY]\n"
                + "      ,FORMAT([IMSRPSUM],'#,#0.00') AS [IMSRPSUM]\n"
                + "      ,FORMAT([IMSRPHSB],'#,#0.00') AS [IMSRPHSB]\n"
                + "      ,FORMAT([IMSRPSCB],'#,#0.00') AS [IMSRPSCB]\n"
                + "      ,FORMAT([IMSRPBBL],'#,#0.00') AS [IMSRPBBL]\n"
                + "      ,FORMAT([IMSRPMIZ],'#,#0.00') AS [IMSRPMIZ]\n"
                + "      ,FORMAT([IMSRPKRT],'#,#0.00') AS [IMSRPKRT]\n"
                + "      ,FORMAT([IMSRPTOTPAY],'#,#0.00') AS [IMSRPTOTPAY]\n"
                + "      ,FORMAT([IMSRPNOPAY],'#,#0.00') AS [IMSRPNOPAY]\n"
                + "	 ,FORMAT(ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE IMSRPPERIOD = MN.IMSRPPERIOD),0),'#,#0.00')  AS SUMTIAB\n"
                + "  FROM [RMShipment].[dbo].[IMSRP700] MN\n"
                + "  WHERE LEFT([IMSRPPERIOD],4) = '" + year + "'\n"
                + "  AND IMSRPIMC = '" + imc + "'\n"
                + "  ORDER BY IMSRPPERIOD ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP700 p = new IMSRP700();

                p.setPeriod(result.getString("IMSRPPERIOD"));
                p.setCcy(result.getString("IMSRPCCY"));
                p.setMacImpAmt(result.getString("IMSRPMACHINE"));
                p.setMatImpAmt(result.getString("IMSRPOTHER"));
                p.setTotalImpAmt(result.getString("IMSRPTOTAL"));
                p.setRate(result.getString("IMSRPRATE"));
                p.setTotalImpAmtB(result.getString("IMSRPBAHT"));
                p.setBalAmtPrevMonth(result.getString("IMSRPBALCCY"));
                p.setAccInvAmt(result.getString("IMSRPTOTCCY"));
                p.setAllBankBalPayment(result.getString("IMSRPALLCCY"));
                p.setSumitomo(result.getString("IMSRPSUM"));
                p.setHsb(result.getString("IMSRPHSB"));
                p.setScb(result.getString("IMSRPSCB"));
                p.setBbl(result.getString("IMSRPBBL"));
                p.setMizuho(result.getString("IMSRPMIZ"));
                p.setK_tokyo(result.getString("IMSRPKRT"));
                p.setTotal(result.getString("IMSRPTOTPAY"));
                p.setNoPaymentAmt(result.getString("IMSRPNOPAY"));

                p.setSumTiab(result.getString("SUMTIAB"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public List<IMSRP700> findByYearTotal(String year, String imc) {

        List<IMSRP700> ul = new ArrayList<IMSRP700>();

        String sql = "DECLARE @YEAR NVARCHAR(6) = '" + year + "'\n"
                + "DECLARE @IMC NVARCHAR(3) = '" + imc + "'\n"
                + "\n"
                + "SELECT 'GTOTAL' AS IMSRPPERIOD, CCY AS IMSRPCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPMACHINE) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPIMC = @IMC AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPMACHINE\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPOTHER) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPIMC = @IMC AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPOTHER\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPTOTAL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPIMC = @IMC AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPTOTAL\n"
                + ",FORMAT(ISNULL((CASE WHEN (ISNULL((SELECT SUM(IMSRPTOTAL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPIMC = @IMC AND IMSRPCCY = CCY),0))=0 THEN 0 \n"
                + "ELSE (ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPIMC = @IMC AND IMSRPCCY = CCY),0)\n"
                + "/ISNULL((SELECT SUM(IMSRPTOTAL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPIMC = @IMC AND IMSRPCCY = CCY),0))\n"
                + "END),0),'#,#0.000000') AS IMSRPRATE\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPIMC = @IMC AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPBAHT\n"
                + ",FORMAT(ISNULL((SELECT TOP 1 IMSRPNOPAY FROM IMSRP700 \n"
                + "WHERE IMSRPPERIOD = (SELECT TOP 1 IMSRPPERIOD FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPIMC = @IMC AND IMSRPCCY = CCY ORDER BY IMSRPPERIOD DESC) \n"
                + "AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPBALCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPTOTCCY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPIMC = @IMC AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPTOTCCY\n"
                + ",FORMAT(ISNULL((SELECT TOP 1 IMSRPNOPAY FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPIMC = @IMC AND IMSRPCCY = CCY ORDER BY IMSRPPERIOD DESC),0),'#,#0.00') AS IMSRPALLCCY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPSUM) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPIMC = @IMC AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPSUM\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPHSB) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPIMC = @IMC AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPHSB\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPSCB) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPIMC = @IMC AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPSCB\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBBL) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPIMC = @IMC AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPBBL\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPMIZ) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPIMC = @IMC AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPMIZ\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPKRT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPIMC = @IMC AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPKRT\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPTOTPAY) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPIMC = @IMC AND IMSRPCCY = CCY),0),'#,#0.00') AS IMSRPTOTPAY\n"
                + ",FORMAT(ISNULL((SELECT TOP 1 IMSRPNOPAY FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPIMC = @IMC AND IMSRPCCY = CCY ORDER BY IMSRPPERIOD DESC),0),'#,#0.00') AS IMSRPNOPAY\n"
                + ",FORMAT(ISNULL((SELECT SUM(IMSRPBAHT) FROM IMSRP700 WHERE LEFT(IMSRPPERIOD,4) = @YEAR AND IMSRPIMC = @IMC),0),'#,#0.00') AS SUMTIAB\n"
                + "FROM(\n"
                + "SELECT IMSCUCODE AS CCY\n"
                + "FROM IMSCUR\n"
                + "WHERE IMSCUCODE != 'THB'\n"
                + ")CUR";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                IMSRP700 p = new IMSRP700();

                p.setPeriod(result.getString("IMSRPPERIOD"));
                p.setCcy(result.getString("IMSRPCCY"));
                p.setMacImpAmt(result.getString("IMSRPMACHINE"));
                p.setMatImpAmt(result.getString("IMSRPOTHER"));
                p.setTotalImpAmt(result.getString("IMSRPTOTAL"));
                p.setRate(result.getString("IMSRPRATE"));
                p.setTotalImpAmtB(result.getString("IMSRPBAHT"));
                p.setBalAmtPrevMonth(result.getString("IMSRPBALCCY"));
                p.setAccInvAmt(result.getString("IMSRPTOTCCY"));
                p.setAllBankBalPayment(result.getString("IMSRPALLCCY"));
                p.setSumitomo(result.getString("IMSRPSUM"));
                p.setHsb(result.getString("IMSRPHSB"));
                p.setScb(result.getString("IMSRPSCB"));
                p.setBbl(result.getString("IMSRPBBL"));
                p.setMizuho(result.getString("IMSRPMIZ"));
                p.setK_tokyo(result.getString("IMSRPKRT"));
                p.setTotal(result.getString("IMSRPTOTPAY"));
                p.setNoPaymentAmt(result.getString("IMSRPNOPAY"));

                p.setSumTiab(result.getString("SUMTIAB"));

                ul.add(p);

            }
            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return ul;

    }

    public boolean add(String ym, String pym, String userid, String imc) {

        boolean result = false;

        String sql = "DECLARE @YM NVARCHAR(6) = '" + ym + "'\n"
                + "DECLARE @PYM NVARCHAR(6) = '" + pym + "'\n"
                + "DECLARE @IMC NVARCHAR(3) = '" + imc + "'\n"
                + "DECLARE @USER NVARCHAR(10) = '" + userid + "'\n"
                + "\n"
                + "INSERT INTO IMSRP700 ([IMSRPCOM]\n"
                + "      ,[IMSRPPERIOD]\n"
                + "      ,[IMSRPCCY]\n"
                + "      ,[IMSRPMACHINE]\n"
                + "      ,[IMSRPOTHER]\n"
                + "      ,[IMSRPTOTAL]\n"
                + "      ,[IMSRPRATE]\n"
                + "      ,[IMSRPBAHT]\n"
                + "      ,[IMSRPBALCCY]\n"
                + "      ,[IMSRPTOTCCY]\n"
                + "      ,[IMSRPALLCCY]\n"
                + "      ,[IMSRPSUM]\n"
                + "      ,[IMSRPHSB]\n"
                + "      ,[IMSRPSCB]\n"
                + "      ,[IMSRPBBL]\n"
                + "      ,[IMSRPMIZ]\n"
                + "      ,[IMSRPKRT]\n"
                + "      ,[IMSRPTOTPAY]\n"
                + "      ,[IMSRPNOPAY]\n"
                + "      ,[IMSRPEDT]\n"
                + "      ,[IMSRPCDT]\n"
                + "      ,[IMSRPUSER]\n"
                + "      ,[IMSRPIMC])\n"
                + "SELECT 'TWC' AS COM, @YM AS PERIOD, CCY, MAC_IMP_AMT, MAT_IMP_AMT, (MAC_IMP_AMT + MAT_IMP_AMT) AS TOT_IMP_AMT\n"
                + ",NULL AS RATE, NULL AS TOT_IMP_AMT_B\n"
                + ",ISNULL((SELECT TOP 1 IMSRPNOPAY FROM IMSRP700 WHERE IMSRPPERIOD = @PYM AND IMSRPCCY LIKE '%'+CCY+'%'),0) AS BAL_AMT_PREV_MONTH\n"
                + ",NULL AS ACC_INV_AMT\n"
                + ",ISNULL((SELECT TOP 1 IMSRPNOPAY FROM IMSRP700 WHERE IMSRPPERIOD = @PYM AND IMSRPCCY LIKE '%'+CCY+'%'),0) AS ALL_BANK_BAL_PAYMENT\n"
                + ",ISNULL((\n"
                + "SELECT SUM(IMSPDFINVAMT)\n"
                + "FROM IMSPDETAIL\n"
                + "WHERE (SELECT TOP 1 IMSPHBCODE FROM IMSPHEAD WHERE IMSPHPDNO = IMSPDPDNO) = 'SUMITOMO' \n"
                + "--AND (SELECT TOP 1 FORMAT(IMSHTRDT,'yyyyMM') FROM IMSSHEAD WHERE IMSHPIMNO = IMSPDIMNO) = @YM\n"
                + "AND (SELECT TOP 1 FORMAT(IMSPHPDDT,'yyyyMM') FROM IMSPHEAD WHERE IMSPHPDNO = IMSPDPDNO) = @YM\n"
                + "AND (SELECT TOP 1 IMSHIMC FROM IMSSHEAD WHERE IMSHPIMNO = IMSPDIMNO) = @IMC\n"
                + "AND IMSPDCUR = CCY\n"
                + "),0) AS SUMITOMO\n"
                + ",ISNULL((\n"
                + "SELECT SUM(IMSPDFINVAMT)\n"
                + "FROM IMSPDETAIL\n"
                + "WHERE (SELECT TOP 1 IMSPHBCODE FROM IMSPHEAD WHERE IMSPHPDNO = IMSPDPDNO) = 'HSB' \n"
                + "--AND (SELECT TOP 1 FORMAT(IMSHTRDT,'yyyyMM') FROM IMSSHEAD WHERE IMSHPIMNO = IMSPDIMNO) = @YM\n"
                + "AND (SELECT TOP 1 FORMAT(IMSPHPDDT,'yyyyMM') FROM IMSPHEAD WHERE IMSPHPDNO = IMSPDPDNO) = @YM\n"
                + "AND (SELECT TOP 1 IMSHIMC FROM IMSSHEAD WHERE IMSHPIMNO = IMSPDIMNO) = @IMC\n"
                + "AND IMSPDCUR = CCY\n"
                + "),0) AS HSB\n"
                + ",ISNULL((\n"
                + "SELECT SUM(IMSPDFINVAMT)\n"
                + "FROM IMSPDETAIL\n"
                + "WHERE (SELECT TOP 1 IMSPHBCODE FROM IMSPHEAD WHERE IMSPHPDNO = IMSPDPDNO) = 'SCB' \n"
                + "--AND (SELECT TOP 1 FORMAT(IMSHTRDT,'yyyyMM') FROM IMSSHEAD WHERE IMSHPIMNO = IMSPDIMNO) = @YM\n"
                + "AND (SELECT TOP 1 FORMAT(IMSPHPDDT,'yyyyMM') FROM IMSPHEAD WHERE IMSPHPDNO = IMSPDPDNO) = @YM\n"
                + "AND (SELECT TOP 1 IMSHIMC FROM IMSSHEAD WHERE IMSHPIMNO = IMSPDIMNO) = @IMC\n"
                + "AND IMSPDCUR = CCY\n"
                + "),0) AS SCB\n"
                + ",ISNULL((\n"
                + "SELECT SUM(IMSPDFINVAMT)\n"
                + "FROM IMSPDETAIL\n"
                + "WHERE (SELECT TOP 1 IMSPHBCODE FROM IMSPHEAD WHERE IMSPHPDNO = IMSPDPDNO) = 'BBL' \n"
                + "--AND (SELECT TOP 1 FORMAT(IMSHTRDT,'yyyyMM') FROM IMSSHEAD WHERE IMSHPIMNO = IMSPDIMNO) = @YM\n"
                + "AND (SELECT TOP 1 FORMAT(IMSPHPDDT,'yyyyMM') FROM IMSPHEAD WHERE IMSPHPDNO = IMSPDPDNO) = @YM\n"
                + "AND (SELECT TOP 1 IMSHIMC FROM IMSSHEAD WHERE IMSHPIMNO = IMSPDIMNO) = @IMC\n"
                + "AND IMSPDCUR = CCY\n"
                + "),0) AS BBL\n"
                + ",ISNULL((\n"
                + "SELECT SUM(IMSPDFINVAMT)\n"
                + "FROM IMSPDETAIL\n"
                + "WHERE ((SELECT TOP 1 IMSPHBCODE FROM IMSPHEAD WHERE IMSPHPDNO = IMSPDPDNO) = 'MIZUHO-P' OR (SELECT TOP 1 IMSPHBCODE FROM IMSPHEAD WHERE IMSPHPDNO = IMSPDPDNO) = 'MIZUHO-R') \n"
                + "--AND (SELECT TOP 1 FORMAT(IMSHTRDT,'yyyyMM') FROM IMSSHEAD WHERE IMSHPIMNO = IMSPDIMNO) = @YM\n"
                + "AND (SELECT TOP 1 FORMAT(IMSPHPDDT,'yyyyMM') FROM IMSPHEAD WHERE IMSPHPDNO = IMSPDPDNO) = @YM\n"
                + "AND (SELECT TOP 1 IMSHIMC FROM IMSSHEAD WHERE IMSHPIMNO = IMSPDIMNO) = @IMC\n"
                + "AND IMSPDCUR = CCY\n"
                + "),0) AS MIZUHO\n"
                + ",ISNULL((\n"
                + "SELECT SUM(IMSPDFINVAMT)\n"
                + "FROM IMSPDETAIL\n"
                + "WHERE (SELECT TOP 1 IMSPHBCODE FROM IMSPHEAD WHERE IMSPHPDNO = IMSPDPDNO) = 'KRUNGSRI-TOKYO' \n"
                + "--AND (SELECT TOP 1 FORMAT(IMSHTRDT,'yyyyMM') FROM IMSSHEAD WHERE IMSHPIMNO = IMSPDIMNO) = @YM\n"
                + "AND (SELECT TOP 1 FORMAT(IMSPHPDDT,'yyyyMM') FROM IMSPHEAD WHERE IMSPHPDNO = IMSPDPDNO) = @YM\n"
                + "AND (SELECT TOP 1 IMSHIMC FROM IMSSHEAD WHERE IMSHPIMNO = IMSPDIMNO) = @IMC\n"
                + "AND IMSPDCUR = CCY\n"
                + "),0) AS KRUNGSRI_TOKYO\n"
                + ",ISNULL((\n"
                + "SELECT SUM(IMSPDFINVAMT)\n"
                + "FROM IMSPDETAIL\n"
                + "WHERE (SELECT TOP 1 IMSPHBCODE FROM IMSPHEAD WHERE IMSPHPDNO = IMSPDPDNO) IN ('SUMITOMO','HSB','SCB','BBL','MIZUHO-P','MIZUHO-R','KRUNGSRI-TOKYO') \n"
                + "--AND (SELECT TOP 1 FORMAT(IMSHTRDT,'yyyyMM') FROM IMSSHEAD WHERE IMSHPIMNO = IMSPDIMNO) = @YM\n"
                + "AND (SELECT TOP 1 FORMAT(IMSPHPDDT,'yyyyMM') FROM IMSPHEAD WHERE IMSPHPDNO = IMSPDPDNO) = @YM\n"
                + "AND (SELECT TOP 1 IMSHIMC FROM IMSSHEAD WHERE IMSHPIMNO = IMSPDIMNO) = @IMC\n"
                + "AND IMSPDCUR = CCY\n"
                + "),0) AS TOTAL\n"
                + ",NULL AS NO_PAYMENT_AMT, CURRENT_TIMESTAMP AS EDT, CURRENT_TIMESTAMP AS CDT, @USER AS USERID, @IMC\n"
                + "FROM(\n"
                + "SELECT IMSCUCODE AS CCY\n"
                + ",ISNULL((\n"
                + "SELECT SUM(IMSSDFINVAMT)\n"
                + "FROM IMSSDETAIL\n"
                + "WHERE IMSSDCUR = IMSCUCODE\n"
                + "AND (SELECT TOP 1 IMSHPRV FROM IMSSHEAD WHERE IMSHPIMNO = IMSSDPIMNO AND FORMAT(IMSHTRDT,'yyyyMM') = @YM) LIKE 'M%'\n"
                + "AND (SELECT TOP 1 IMSHIMC FROM IMSSHEAD WHERE IMSHPIMNO = IMSSDPIMNO AND FORMAT(IMSHTRDT,'yyyyMM') = @YM) = @IMC\n"
                + "),0) AS MAC_IMP_AMT\n"
                + ",ISNULL((\n"
                + "SELECT SUM(IMSSDFINVAMT)\n"
                + "FROM IMSSDETAIL\n"
                + "WHERE IMSSDCUR = IMSCUCODE\n"
                + "AND (SELECT TOP 1 IMSHPRV FROM IMSSHEAD WHERE IMSHPIMNO = IMSSDPIMNO AND FORMAT(IMSHTRDT,'yyyyMM') = @YM) NOT LIKE 'M%'\n"
                + "AND (SELECT TOP 1 IMSHIMC FROM IMSSHEAD WHERE IMSHPIMNO = IMSSDPIMNO AND FORMAT(IMSHTRDT,'yyyyMM') = @YM) = @IMC\n"
                + "),0) AS MAT_IMP_AMT\n"
                + "FROM(\n"
                + "SELECT IMSCUCODE\n"
                + "FROM IMSCUR\n"
                + "WHERE IMSCUCODE != 'THB'\n"
                + ")CUR\n"
                + ")DET";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public boolean edit(String ym, String ccy, String rate, String totalImpAmtB, String accInvAmt, String noPaymentAmt, String imc) {

        boolean result = false;

        String sql = "UPDATE IMSRP700 "
                + "SET IMSRPRATE = " + rate + ""
                + ", IMSRPBAHT = " + totalImpAmtB + ""
                + ", IMSRPTOTCCY = " + accInvAmt + ""
                + ", IMSRPNOPAY = " + noPaymentAmt + ""
                + ", IMSRPEDT = CURRENT_TIMESTAMP"
                + " WHERE IMSRPPERIOD = '" + ym + "' AND IMSRPCCY = '" + ccy + "' AND IMSRPIMC = '" + imc + "'";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public void delete(String code, String imc) {

        String sql = "DELETE FROM IMSRP700 WHERE IMSRPPERIOD = '" + code + "' AND IMSRPIMC = '" + imc + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
